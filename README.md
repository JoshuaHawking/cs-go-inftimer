# InfTimer - A fork of Influx Timer by Mehis

InfTimer is a fork of [Influx Timer by Mehis](https://github.com/TotallyMehis/Influx-Timer). InfTimer is a bunnyhop / surf / general timer for Counter Strike:Global Offensive and runs on Sourcemod. The plugin is designed with stored times, a ranking system, ability to do TAS and custom styles such as parkour. The plugin was originally designed for [House of Climb](houseofclimb.com), however I chose to release it as a valuable resource to the community as well as the stagnation of the timer means I have no reason to not share it.

While most things are avaliable in this repo, [Rocket Jump](https://www.youtube.com/watch?v=ReSs14apPSM) and Parkour will not be included by the request of the House of Climb owners, in which the code was originally written by. I will respect their choice and remove Rocket Jump / Parkour source code, but there will be references to these styles within the repo.

# Setup
If you'd like to try the timer out, you'll need to compile it yourself. You may be faced with SQL errors, compiling errors and the gods attacking you. All the requirements such as includes should be included with the source. You'll also need:
  - MySQL Database (InfTimer does not support SQLite)
  - A CS:GO server with the latest SM/MM
  - For ranking, the ability to setup a crontab/automated SQL script
  - A little bit of luck.

Influx Timer and therefore InfTimer were designed in small modules, allowing us to easily remove and add features as and when we pleased. While this was the intention, some parts are designed with other sections required and will cause errors when they are not met. I recommend keeping `influx_records` as this file is important to ranking, the HUD and probably a few more things.

After compiling, you should be able to drop all the .smx's into the plugins folder, and drop the configuration folders in the same structure as found in the repo. 

### Ranking
Ranking requires a automated SQL script to update the databases on a regular timescale. This was originally designed to reduce database load, but should have been done at map change. A crontab needs to be setup to run the  `scripts/UpdatePointLeaderboard.sh` every hour or so, in which the player ranks will update after that.

### Levels
Inside `addons/sourcemod/configs/hocranks.cfg`, the ranks file can be found. It's in the usual KeyValues format, with the highest point ranks at the top. Adding new ranks is as easy as just placing into the file:
```
	"1" // Same as rank number
	{ 
    "RankNumber"            "1" // Number of rank (lowest rank is 1, highest rank is 15 etc.)
    "Name"                  "New_Player" // Name of the rank
		"Tag"			              "NEW" // Tag of the rank in chat
		"Colour"			          "{WHITE}" // Colour of the tag
		"Points"				        "100" // Points required for the rank
	}
```

Ensure that the ranks are kept in order, with the largest point values at the top and lowest at the bottom of the file.

# Extras in the repo
Also included in this repo are custom versions of [Movement Unlocker](https://forums.alliedmods.net/showthread.php?t=255298), [Rock the Vote / MapChooser Extended](https://forums.alliedmods.net/showthread.php?t=156974?t=156974) and [Chat Processor](https://github.com/SkyGuardian/Chat-Processor). Movement Unlocker is required for the legit hard styles and modify values such as stamina etc. Chat Processor is required for the rank tags, and also requires custom configurations. These may be prone to change in the future and may break with CS:GO updates fairly easily.

The included RTV / MapChooser have changes to add the HoC tag to them and possibly some fixes some bugs that I found on the way.

# Why not use Influx Timer?
At the time, Influx Timer was still a fairly new timer and lacked a lot of features from our old server's timer. I took the job of developing the new timer. I liked Influx Timer's approach to the timer, using lots of seperate files allowing easy customisation of the timer. I wish I kept this practice much more strongly as it would most likely help the compatibility of this timer in the future.

I hope people can benefit from seeing the timer's source and how I changed it to fit our needs. Sourcepawn is a strange language to other ones I have worked on, and you can definitely see the difference in the initial commits compared to the later ones where I really began to pick the language up.

# What would I do differently?
Firstly, use git much more effectively. While I did try to commit as often as I could, I could have definitely used it when implementing new features using branches. It would definitely help with keeping the code much more organised. Also, when commiting, keeping clean and easy to understand commit messages. No one likes reading `bug fix` in their commit message. And also, not using profanties and such in code. Unfortunately, I've tried removing them all and ended up removing a lot of the date information for the commits. I apologise to anyone reading for anything offensive included in the commits, and will change this for the future.

Secondly, I'd better design my code around the modular design of Influx Timer. Ensuring that files check their dependencies better and don't error when they aren't there would be essential and was ultimately down to my poor knowledge of the language and how dependencies, natives etc. worked. Keeping the modular design would make InfTimer much easier to change in the future and also use. 

Thirdly, I'd design more around using ConVars / server variables rather than magic numbers in code. I found myself needed to CTRL-F a lot during this project, and using the implementation of ConVars in CS:GO would have greatly benefitted my code allowing easily manipulation of settings, rather than having to recompile to change a number. This was again down to lazy design, as I never intended to release the source code or for it to be used on any server other than House of Climb.

# Final Thanks
  - Thanks to Mehis for the help he gave me way back in the beginning on how his timer worked and helping me with some strange bugs along the way.
  - Thanks to Zipcore for helping me with the ranking/record system and understanding strafes, velocities and vectors a lot more. 
  - Thanks to Sikari for giving Sourcemod support and ideas when needed.
  - Thanks to all the House of Climb players who helped me test the timer, enjoy the timer and made it what it is
  - Thanks to the House of Climb owners, who allowed me to use their resources as I pleased, such as test servers and databases whenever I needed for whatever project I worked on.

Apologies for the git mess :-)
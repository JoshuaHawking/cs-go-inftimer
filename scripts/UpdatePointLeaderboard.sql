SET @rank = 0;
USE eubhop;
UPDATE inf_ranks SET `points` = (SELECT SUM(points) FROM inf_times WHERE inf_ranks.uid = inf_times.uid);
UPDATE inf_ranks SET `pointrank` = @rank := (@rank + 1) ORDER BY points DESC;
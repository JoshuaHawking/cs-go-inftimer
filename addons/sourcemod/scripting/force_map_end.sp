#include <sourcemod>
#include <cstrike>

bool g_bAllowRoundEnd = false;

public Plugin myinfo = 
{
	name = "Force Map End",
	author = "Zipcore, 1NutWunDeR, Fusion",
	description = "",
	version = "1.0",
	url = ""
}

public OnPluginStart()
{
	CreateTimer(1.0, CheckRemainingTime, INVALID_HANDLE, TIMER_REPEAT);
}

public Action CheckRemainingTime(Handle timer)
{
	Handle hTmp;	
	hTmp = FindConVar("mp_timelimit");
	int iTimeLimit = GetConVarInt(hTmp);			
	if (hTmp != INVALID_HANDLE)
		CloseHandle(hTmp);	
	if (iTimeLimit > 0)
	{
		int timeleft;
		GetMapTimeLeft(timeleft);
		
		switch(timeleft)
		{
			case 1800: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B30 minutes");
			case 1200: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B20 minutes");
			case 600: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B10 minutes");
			case 300: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B5 minutes");
			case 120: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B2 minutes");
			case 60: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B60 seconds");
			case 30: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B30 seconds");
			case 15: PrintToChatAll(" \x08[\x06HoC\x08]\x08 Time remaining: \x0B15 seconds");
			case -1: PrintToChatAll(" \x08[\x06HoC\x08]\x08 \x0B3..");
			case -2: PrintToChatAll(" \x08[\x06HoC\x08]\x08 \x0B2..");
			case -3: PrintToChatAll(" \x08[\x06HoC\x08]\x08 \x0B1..");
		}
		
		if(timeleft < -3 && !g_bAllowRoundEnd)
		{
			g_bAllowRoundEnd = true;
			CS_TerminateRound(0.5, CSRoundEnd_TerroristWin, true);
		}
	}
}

public Action CS_OnTerminateRound(float &delay, CSRoundEndReason &reason)
{
	g_bAllowRoundEnd = false;
	return Plugin_Continue;
}
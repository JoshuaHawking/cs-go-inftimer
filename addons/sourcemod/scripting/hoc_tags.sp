#include <sourcemod>
#include <cstrike>

#include <msharedutil/misc>
#include <chat-processor>

#undef REQUIRE_PLUGIN

#define TAGLENGTH 			24
#define COLOURLENGTH 		4

#define MAX_BLOCKEDWORDS 	512
#define BLOCKEDLENGTH		32

#define TRANSLATION_FILE 	"tags.phrases"
#define MYSQL_CONFIG_NAME   "tags"
#define MYSQL_TABLE_NAME 	"tags"

#define BLOCKED_FILE		"tags_blockedwords"
#define CHAT_PREFIX			" \x08[\x06HoC\x08]\x08 "

#define MAX_STEAMAUTH_LENGTH	21

// Delete the menu handle if ending, return if cancelling.
#define MENU_HANDLE(%0,%1)       if ( %1 == MenuAction_End ) { delete %0; return 0; } else if ( %1 == MenuAction_Cancel ) { return 0; }
#define MENU_HANDLE_BACK(%0,%1,%2)       if ( %1 == MenuAction_End ) { delete %0; return 0; } else if ( %1 == MenuAction_Cancel && %2 != MenuCancel_ExitBack ) { return 0; }

char g_szClientTag[MAXPLAYERS + 1][TAGLENGTH];
int g_iClientTagColour[MAXPLAYERS + 1];
int g_iClientChatColour[MAXPLAYERS + 1];
int g_iClientNameColour[MAXPLAYERS + 1];

char g_szClientExternalTag[MAXPLAYERS + 1][TAGLENGTH];

char g_szBlockedWords[MAX_BLOCKEDWORDS][BLOCKEDLENGTH];
int g_iBlockedWords;

Handle g_hDB;

char szColours[][][] = 
{
	{ "White", "\x01" }, 
	{ "Team Colours", "\x03" }, 
	{ "Red", "\x02" }, 
	{ "Light Red", "\x07" }, 
	{ "Lighter Red", "\x0F" }, 
	{ "Green", "\x04" }, 
	{ "Light Green", "\x05" }, 
	{ "Lime Green", "\x06" }, 
	{ "Blue", "\x0C" }, 
	{ "Sky Blue", "\x0B" }, 
	{ "Light Yellow", "\x09" }, 
	{ "Gold", "\x10" }, 
	{ "Pink", "\x0E" }, 
	{ "Grey", "\x08" }
}

float g_flLastSetTagTime[MAXPLAYERS + 1];
float g_flLastTagsTime[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	author = "Fusion", 
	url = "www.houseofclimb.com", 
	name = "Standalone Tags", 
	description = "!tags", 
	version = "1.0.0"
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	LoadTranslations(TRANSLATION_FILE);
	
	// COMMANDS
	RegAdminCmd("sm_tags", Cmd_Tags, ADMFLAG_RESERVATION);
	RegAdminCmd("sm_settag", Cmd_SetTag, ADMFLAG_RESERVATION);
	
	return APLRes_Success;
}

public void OnAllPluginsLoaded()
{
	char szError[128];
	
	// Connect to database
	if (SQL_CheckConfig(MYSQL_CONFIG_NAME))
	{
		g_hDB = SQL_Connect(MYSQL_CONFIG_NAME, true, szError, sizeof(szError));
	}
	
	if (g_hDB == null)
	{
		SetFailState("Couldn't retrieve database handle!");
	}
	
	// Create table
	SQL_TQuery(g_hDB, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...MYSQL_TABLE_NAME..." (auth VARCHAR(21) PRIMARY KEY, tag VARCHAR(24), tagcolour INTEGER DEFAULT 0, namecolour INTEGER DEFAULT 0, chatcolour INTEGER DEFAULT 0)", _, DBPrio_High);
	
	if (!LoadBlockedWords())
	{
		SetFailState("Failed to load blocked words!");
	}
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data)
{
}

public void Influx_OnClientIdRetrieved(int client)
{
	g_szClientTag[client] = "";
	g_szClientExternalTag[client] = "";
	g_iClientTagColour[client] = 0;
	g_iClientNameColour[client] = 0;
	g_iClientChatColour[client] = 0;
	g_flLastSetTagTime[client] = 0.0;
	g_flLastTagsTime[client] = 0.0;
	
	CreateTimer(5.0, LoadTagInfo, client, _);
}

public Action LoadTagInfo(Handle timer, int client)
{
	DB_GetClientTagInfo(client);
}

bool LoadBlockedWords()
{
	
	char szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "configs");
	
	if (!DirExists(szPath))return false;
	
	Format(szPath, sizeof(szPath), "%s/%s.cfg", szPath, BLOCKED_FILE);
	
	Handle hFile = OpenFile(szPath, "rt");
	
	if (hFile == INVALID_HANDLE) {
		return false;
	}
	
	char szLine[BLOCKEDLENGTH];
	
	int iLine = 0;
	while (!IsEndOfFile(hFile) && ReadFileLine(hFile, szLine, sizeof(szLine)))
	{
		if (iLine >= MAX_BLOCKEDWORDS)break;
		
		int iLineLength;
		
		iLineLength = strlen(szLine);
		
		// Remove line ending
		if (szLine[iLineLength - 1] == '\n') {
			szLine[--iLineLength] = '\0';
		}
		
		if (szLine[0] == '\0')continue;
		
		strcopy(g_szBlockedWords[iLine], BLOCKEDLENGTH, szLine);
		
		iLine++;
	}
	
	g_iBlockedWords = iLine;
	return true;
}
/*
	Commands
*/
public Action Cmd_Tags(int client, int args)
{
	if (HandleCmdSpam(client, 5.0, g_flLastTagsTime[client], true))
	{
		return Plugin_Handled;
	}
	
	Menu_Tags(client);
	
	return Plugin_Handled;
}

public Action Cmd_SetTag(int client, int args)
{
	if (HandleCmdSpam(client, 2.5, g_flLastSetTagTime[client], true))
	{
		return Plugin_Handled;
	}
	
	// Check valid client.
	if (!client)
	{
		return Plugin_Handled;
	}
	
	// Build tag
	char arg[128];
	char szTag[TAGLENGTH];
	
	for (int i = 1; i <= args; i++)
	{
		GetCmdArg(i, arg, sizeof(arg));
		Format(szTag, sizeof(szTag), "%s %s", szTag, arg);
	}
	
	TrimString(szTag);
	
	// Check for blocked words unless they are admin
	if (!CheckCommandAccess(client, "sm_admin", ADMFLAG_GENERIC))
	{
		for (int i = 0; i < g_iBlockedWords; i++)
		{
			if (StrContains(szTag, g_szBlockedWords[i], false) != -1)
			{
				PrintToChat(client, "%s%t", CHAT_PREFIX, "TAG_BLOCKED");
				return Plugin_Handled;
			}
		}
	}
	
	DB_SetClientTag(client, szTag);
	
	return Plugin_Handled;
}

/*
	Menus
*/

bool Menu_Tags(int client)
{
	char temp[128];
	
	// Build menu
	Menu menu = new Menu(Hndlr_MainMenu);
	FormatEx(temp, sizeof(temp), "Tags Menu\n \nUse !settag to set your tag:\n!settag VIP\n(%i Character Max)\n ", TAGLENGTH);
	menu.SetTitle(temp);
	menu.AddItem("cleartag", "Clear Tag");
	menu.AddItem("tagcolour", "Tag Colour");
	menu.AddItem("namecolour", "Name Colour");
	menu.AddItem("chatcolour", "Chat Colour");
	
	if (CheckCommandAccess(client, "sm_admin", ADMFLAG_GENERIC))
	{
		menu.AddItem("playersetup", "Check Player Setup");
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_MainMenu(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	if (!IsClientInGame(client))return 0;
	
	char szInfo[32];
	GetMenuItem(menu, index, szInfo, sizeof(szInfo));
	
	// Set the users tag to nothing
	if (StrEqual(szInfo, "cleartag"))
	{
		DB_SetClientTag(client, "");
		Menu_Tags(client);
		return 0;
	}
	else if (StrEqual(szInfo, "tagcolour"))
	{
		Menu_SelectColour(client, "tags")
		return 0;
	}
	else if (StrEqual(szInfo, "namecolour"))
	{
		Menu_SelectColour(client, "name")
		return 0;
	}
	else if (StrEqual(szInfo, "chatcolour"))
	{
		Menu_SelectColour(client, "chat")
		return 0;
	}
	else if (StrEqual(szInfo, "playersetup"))
	{
		Menu_SelectPlayerSetup(client);
		return 0;
	}
	
	return 0;
}

bool Menu_SelectColour(int client, char property[16])
{
	// Build menu
	Menu menu = new Menu(Hndlr_SelectColour);
	menu.SetTitle("Select a colour");
	
	char szInfo[32];
	for (int i = 0; i < sizeof(szColours); i++)
	{
		FormatEx(szInfo, sizeof(szInfo), "colours_%s_%i", property, i);
		menu.AddItem(szInfo, szColours[i][0]);
	}
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_SelectColour(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to the main menu.
		Menu_Tags(client);
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[3][8];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int colour = StringToInt(buffer[2]);
	
	if (StrEqual(buffer[1], "tags"))
	{
		DB_SetClientTagColour(client, colour);
	}
	else if (StrEqual(buffer[1], "name"))
	{
		DB_SetClientNameColour(client, colour);
	}
	else if (StrEqual(buffer[1], "chat"))
	{
		DB_SetClientChatColour(client, colour);
	}
	
	Menu_Tags(client);
	
	return 0;
}

bool Menu_SelectPlayerSetup(int client)
{
	Menu menu = new Menu(Hndlr_SelectPlayerSetup);
	menu.SetTitle("Select a player to view their setup");
	
	char szInfo[32];
	char szPlayer[MAXLENGTH_NAME];
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if (!IsClientInGame(i))continue;
		
		if (IsFakeClient(i))continue;
		
		FormatEx(szInfo, sizeof(szInfo), "player_%i", i);
		
		GetClientName(i, szPlayer, sizeof(szPlayer));
		menu.AddItem(szInfo, szPlayer);
	}
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_SelectPlayerSetup(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to the main menu.
		Menu_Tags(client);
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[2][8];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int viewedClient = StringToInt(buffer[1])
	
	if (!IsClientInGame(viewedClient))return 0;
	
	if (IsFakeClient(viewedClient))return 0;
	
	Menu_PlayerSetup(client, viewedClient);
	
	return 0;
}

bool Menu_PlayerSetup(int client, int viewedClient)
{
	Menu menu = new Menu(Hndlr_PlayerSetup);
	char szDisplay[256];
	char szTemp[128];
	
	GetClientAuthId(viewedClient, AuthId_Steam3, szTemp, sizeof(szTemp), true);
	
	Format(szDisplay, sizeof(szDisplay), "%N\n \nSteam ID: %s\nTag: %s\nTag Colour: %s\nChat Colour: %s\nName Colour: %s\n ", 
		viewedClient, 
		szTemp, 
		(g_szClientTag[viewedClient][0] == '\0' ? "None" : g_szClientTag[viewedClient]), 
		szColours[g_iClientTagColour[viewedClient]][0], 
		szColours[g_iClientChatColour[viewedClient]][0], 
		szColours[g_iClientNameColour[viewedClient]][0]
		);
	
	menu.SetTitle(szDisplay);
	
	char szInfo[32];
	
	FormatEx(szInfo, sizeof(szInfo), "clear_%i", viewedClient);
	
	menu.AddItem(szInfo, "Remove Setup");
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_PlayerSetup(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to the main menu.
		Menu_SelectPlayerSetup(client);
		return 0;
	}
	
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[2][8];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int viewedClient = StringToInt(buffer[1])
	
	if (!IsClientInGame(viewedClient))return 0;
	
	if (IsFakeClient(viewedClient))return 0;
	
	DB_SetClientTag(viewedClient, "");
	DB_SetClientTagColour(viewedClient, 0);
	DB_SetClientNameColour(viewedClient, 0);
	DB_SetClientChatColour(viewedClient, 0);
	
	Menu_SelectPlayerSetup(client);
	
	return 0;
}

/*
	Database Stuff
*/
stock bool DB_GetEscaped(char[] out, int len, const char[] def = "")
{
	if (!SQL_EscapeString(g_hDB, out, out, len))
	{
		strcopy(out, len, def);
		
		return false;
	}
	
	return true;
}

DB_GetClientTagInfo(int client)
{
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return false;
	}
	
	char query[512];
	FormatEx(query, sizeof(query), "SELECT tag, tagcolour, namecolour, chatcolour FROM "...MYSQL_TABLE_NAME..." WHERE auth = '%s'", szAuth);
	
	SQL_TQuery(g_hDB, Thrd_GetClientTagInfo, query, client, DBPrio_Low);
	return true;
}

public void Thrd_GetClientTagInfo(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		LogError("Failed database query: get tag info for client");
		return;
	}
	
	while (SQL_FetchRow(res))
	{
		SQL_FetchString(res, 0, g_szClientTag[client], sizeof(g_szClientTag[]));
		g_iClientTagColour[client] = SQL_FetchInt(res, 1);
		g_iClientChatColour[client] = SQL_FetchInt(res, 2);
		g_iClientNameColour[client] = SQL_FetchInt(res, 3);
	}
	
}

bool DB_SetClientTag(int client, char szTag[TAGLENGTH])
{
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return false;
	}
	
	RemoveChars(szTag, "`'\"");
	DB_GetEscaped(szTag, (sizeof(szTag) * 2) + 1)
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...MYSQL_TABLE_NAME..." (auth, tag) VALUES('%s', '%s') ON DUPLICATE KEY UPDATE auth='%s', tag='%s';", szAuth, szTag, szAuth, szTag);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteString(szTag);
	
	SQL_TQuery(g_hDB, Thrd_SetClientTag, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientTag(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		LogError("Failed database query: update tag for client");
		return;
	}
	
	char szTag[TAGLENGTH];
	int client;
	
	data.Reset();
	client = data.ReadCell();
	data.ReadString(szTag, sizeof(szTag));
	
	g_szClientTag[client] = szTag;
	if (StrEqual(szTag, ""))
	{
		PrintToChat(client, "%s%t", CHAT_PREFIX, "SETTAG_BLANK_SUCCESS", szTag);
	}
	else
	{
		PrintToChat(client, "%s%t", CHAT_PREFIX, "SETTAG_SUCCESS", szTag, szColours[g_iClientTagColour[client]][1]);
	}
}

// Colour is int (index of szColours)
bool DB_SetClientTagColour(int client, int colour)
{
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return false;
	}
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...MYSQL_TABLE_NAME..." (auth, tagcolour) VALUES('%s', %i) ON DUPLICATE KEY UPDATE auth='%s', tagcolour=%i;", szAuth, colour, szAuth, colour);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteCell(colour);
	
	SQL_TQuery(g_hDB, Thrd_SetClientTagColour, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientTagColour(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		LogError("Failed database query: update tag colour for client");
		return;
	}
	
	int client;
	int colour;
	
	data.Reset();
	client = data.ReadCell();
	colour = data.ReadCell();
	
	g_iClientTagColour[client] = colour;
	PrintToChat(client, "%s%t", CHAT_PREFIX, "SETCOLOUR_SUCCESS", "tag", szColours[colour][1], szColours[colour][0]);
}

// Colour is int (index of szColours)
bool DB_SetClientNameColour(int client, int colour)
{
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return false;
	}
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...MYSQL_TABLE_NAME..." (auth, namecolour) VALUES('%s', %i) ON DUPLICATE KEY UPDATE auth='%s', namecolour=%i;", szAuth, colour, szAuth, colour);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteCell(colour);
	
	SQL_TQuery(g_hDB, Thrd_SetClientNameColour, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientNameColour(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		LogError("Failed database query: update name colour for client");
		return;
	}
	
	int client;
	int colour;
	
	data.Reset();
	client = data.ReadCell();
	colour = data.ReadCell();
	
	g_iClientNameColour[client] = colour;
	PrintToChat(client, "%s%t", CHAT_PREFIX, "SETCOLOUR_SUCCESS", "name", szColours[colour][1], szColours[colour][0]);
}

// Colour is int (index of szColours)
bool DB_SetClientChatColour(int client, int colour)
{
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return false;
	}
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...MYSQL_TABLE_NAME..." (auth, chatcolour) VALUES('%s', %i) ON DUPLICATE KEY UPDATE auth='%s', chatcolour=%i;", szAuth, colour, szAuth, colour);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteCell(colour);
	
	SQL_TQuery(g_hDB, Thrd_SetClientChatColour, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientChatColour(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		LogError("Failed database query: update chat colour for client");
		return;
	}
	
	int client;
	int colour;
	
	data.Reset();
	client = data.ReadCell();
	colour = data.ReadCell();
	
	g_iClientChatColour[client] = colour;
	PrintToChat(client, "%s%t", CHAT_PREFIX, "SETCOLOUR_SUCCESS", "chat", szColours[colour][1], szColours[colour][0]);
}

/*
	Add Tags
*/
public Action OnChatMessage(int & author, ArrayList recipients, eChatFlags & flag, char[] name, char[] message, bool & bProcessColors, bool & bRemoveColors)
{
	char extra[12];
	GetExtraTag(author, flag, extra, sizeof(extra));
	
	bProcessColors = false;
	
	Format(name, MAXLENGTH_NAME, " %s%s%s%s%s%s\x03%s%s\x01", 
		g_szClientExternalTag[author], 
		(g_szClientExternalTag[author][0] == '\0' ? "" : " "), 
		szColours[g_iClientTagColour[author]][1], 
		g_szClientTag[author], 
		(g_szClientTag[author][0] == '\0' ? "" : " "), 
		extra, 
		szColours[g_iClientNameColour[author]][1], 
		name);
		
	
	Format(message, MAXLENGTH_MESSAGE, "%s%s", 
		szColours[g_iClientChatColour[author]][1], 
		message
		);
	return Plugin_Changed;
}

GetExtraTag(int client, eChatFlags flag, char[] extra, int extralen)
{
	if (flag == ChatFlag_Team)
	{
		if (GetClientTeam(client) == CS_TEAM_CT)
		{
			strcopy(extra, extralen, "{TEAM}CT ");
		}
		else if (GetClientTeam(client) == CS_TEAM_T)
		{
			strcopy(extra, extralen, "{TEAM}T ");
		}
	}
	if (GetClientTeam(client) == CS_TEAM_SPECTATOR)
	{
		strcopy(extra, extralen, "{TEAM}SPEC ");
	}
}

HandleCmdSpam(int client, float delay = 1.0, float &lasttime, bool bPrint = false)
{
	float dif = (lasttime + delay) - GetEngineTime();
	
	if (dif > 0.0)
	{
		if (bPrint)
			PrintToChat(client, "%s%t", CHAT_PREFIX, "COMMANDCOOLDOWN", dif);
		
		return true;
	}
	
	lasttime = GetEngineTime();
	
	return false;
}

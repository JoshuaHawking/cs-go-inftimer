#include <sourcemod>
#include <cstrike>
#include <smlib>

#include <influx/core>
#include <influx/playerinfo>
#include <influx/records>
#include <influx/hud>

#include <msharedutil/arrayvec>
#include <msharedutil/ents>
#include <msharedutil/misc>

#undef REQUIRE_PLUGIN
#include <influx/recording>

Handle g_hSQL;

// Menus
enum PlayerInfoMenu
{
	UserId, 
	String:SteamId[MAX_STEAMAUTH_LENGTH], 
	String:Name[MAX_NAME_LENGTH], 
	Handle:MainMenu, 
	Handle:SubMenu, 
	Run, 
	Mode, 
	Style
}
Handle g_hPInfoMenus[INF_MAXPLAYERS][PlayerInfoMenu];
float g_flLastPlayerInfoTime[INF_MAXPLAYERS];

// MENU STORAGE
int g_nLastSelectedMode[INF_MAXPLAYERS];
int g_nLastSelectedStyle[INF_MAXPLAYERS];

#include "influx_playerinfo/db.sp"
#include "influx_playerinfo/db_cb.sp"
#include "influx_playerinfo/menus.sp"
#include "influx_playerinfo/menus_hndlrs.sp"

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Playerinfo", 
	description = "!playerinfo menu", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	// NATIVES
	RegConsoleCmd("sm_playerinfo", Cmd_PlayerInfo);
	RegConsoleCmd("sm_pinfo", Cmd_PlayerInfo);
	RegConsoleCmd("sm_profile", Cmd_PlayerInfo);
	RegConsoleCmd("sm_player", Cmd_PlayerInfo);
	
	// NATIVES
	CreateNative("Influx_DisplayPlayerInfo", Native_DisplayPlayerInfo);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	RegPluginLibrary(INFLUX_LIB_PLAYERINFO);
	LoadTranslations(INFLUX_PHRASES);
}

public void OnAllPluginsLoaded()
{
	g_hSQL = Influx_GetDB();
	if (g_hSQL == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
}

// Add new line to menu
stock AddMenuLine(char[] sz, int len, char[] newline, any...)
{
	// Format the new line
	char formatted[256];
	VFormat(formatted, sizeof(formatted), newline, 4);
	
	// Format the new string
	Format(sz, len, "%s\n%s", sz, formatted);
}
// Add spacer
stock AddMenuSpacer(char[] sz, int len)
{
	Format(sz, len, "%s\n \n", sz);
}

public Native_DisplayPlayerInfo(Handle hPlugin, int nParams)
{
	int client, userid, mode, style;
	client = GetNativeCell(1);
	userid = GetNativeCell(2);
	
	g_hPInfoMenus[client][UserId] = userid;
	
	if (!client || !userid)return;
	
	mode = GetNativeCell(3);
	style = GetNativeCell(4);
	
	// No mode specified, bring them to mode select menu
	if(mode == MODE_INVALID)
	{
		Show_PInfoModeMenu(client, userid);
		return;
	}
	g_hPInfoMenus[client][Mode] = mode;
	
	// No style specified, bring them to style select menu
	if(style == STYLE_INVALID)
	{
		Show_PInfoStyleMenu(client, userid, mode);
		return;
	}
	g_hPInfoMenus[client][Style] = style;
	
	Menu_PlayerInfo(client);
}
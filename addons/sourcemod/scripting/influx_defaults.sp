#include <sourcemod>
#include <clientprefs>

#include <influx/core>

#undef REQUIRE_PLUGIN

Handle g_hDefaultMode;
Handle g_hDefaultStyle;

float g_flLastDefaultModeTime[INF_MAXPLAYERS];
float g_flLastDefaultStyleTime[INF_MAXPLAYERS];

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Defaults", 
	description = "Allows players to select default modes etc.", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	RegConsoleCmd("sm_defaultmode", Cmd_DefaultMode);
	RegConsoleCmd("sm_defaultstyle", Cmd_DefaultStyle);
	
	// COOKIES
	g_hDefaultMode = RegClientCookie("influx_defaultmode", "Default mode for Influx", CookieAccess_Private);
	g_hDefaultStyle = RegClientCookie("influx_defaultstyle", "Default style for Influx", CookieAccess_Private);
}

public void Influx_OnClientIdRetrieved(int client, int uid)
{
	if (IsFakeClient(client))return;
	
	if (!AreClientCookiesCached(client))return;
	
	// Get default mode
	char szMode[32];
	GetClientCookie(client, g_hDefaultMode, szMode, sizeof(szMode));
	int iMode;
	
	if (szMode[0] == '\0')
	{
		Format(szMode, sizeof(szMode), "%i", MODE_AUTO);
		SetClientCookie(client, g_hDefaultMode, szMode);
		iMode = MODE_AUTO;
	}
	else
	{
		iMode = StringToInt(szMode);
	}
	
	// Still on default mode
	if(Influx_GetClientMode(client) == MODE_AUTO && iMode != MODE_AUTO)
	{
		Influx_SetClientMode(client, iMode);
	}
	
	// Get default style
	char szStyle[32];
	GetClientCookie(client, g_hDefaultStyle, szStyle, sizeof(szStyle));
	int iStyle;
	
	if (szStyle[0] == '\0')
	{
		Format(szStyle, sizeof(szStyle), "%i", STYLE_NORMAL);
		SetClientCookie(client, g_hDefaultStyle, szStyle);
		iStyle = STYLE_NORMAL;
	}
	else
	{
		iStyle = StringToInt(szStyle);
	}
	
	
	// Still on default style
	if(Influx_GetClientStyle(client) == STYLE_NORMAL && iStyle != STYLE_NORMAL)
	{
		Influx_SetClientStyle(client, iStyle);
	}
}

public OnClientConnected(int client)
{
	g_flLastDefaultModeTime[client] = 0.0;
}

public Action Cmd_DefaultMode(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (Inf_HandleCmdSpam(client, 5.0, g_flLastDefaultModeTime[client], true))
	{
		return Plugin_Handled;
	}
	
	// Show menu
	Menu menu = new Menu(Hndlr_DefaultMode)
	menu.SetTitle("Set your default mode");
	
	char szMode[MAX_MODE_NAME];
	char szItem[4];
	
	int iDefaultMode = GetClientDefaultMode(client);
	
	for (int mode = 0; mode < MAX_MODES; mode++)
	{
		Influx_GetModeName(mode, szMode, sizeof(szMode));
		if (!StrEqual(szMode, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i", mode);
			
			menu.AddItem(szItem, szMode, (iDefaultMode == mode ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT));
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

// Set the user's default mode
public int Hndlr_DefaultMode(Menu menu, MenuAction action, int client, int index)
{
	// Close menu if they have selected to exit.
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int iMode = StringToInt(szInfo);
	
	SetClientCookie(client, g_hDefaultMode, szInfo);
	
	char szMode[MAX_MODE_NAME];
	Influx_GetModeName(iMode, szMode, sizeof(szMode));
	
	Influx_PrintToChat(_, client, "%t", "SETDEFAULTMODE", szMode);
	
	return 0;
} 

int GetClientDefaultMode(int client)
{
	if (IsFakeClient(client))return MODE_INVALID;
	
	if (!AreClientCookiesCached(client))return MODE_INVALID;
	
	// Get default mode
	char szMode[32];
	GetClientCookie(client, g_hDefaultMode, szMode, sizeof(szMode));

	if (szMode[0] == '\0')
	{
		return MODE_AUTO;
	}
	else
	{
		return StringToInt(szMode);
	}
}




public Action Cmd_DefaultStyle(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (Inf_HandleCmdSpam(client, 5.0, g_flLastDefaultStyleTime[client], true))
	{
		return Plugin_Handled;
	}
	
	// Show menu
	Menu menu = new Menu(Hndlr_DefaultStyle)
	menu.SetTitle("Set your default style");
	
	char szStyle[MAX_STYLE_NAME];
	char szItem[4];
	
	int iDefaultStyle = GetClientDefaultStyle(client);
	
	for (int style = 0; style < MAX_STYLES; style++)
	{
		Influx_GetStyleName(style, szStyle, sizeof(szStyle));
		if (!StrEqual(szStyle, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i", style);
			
			menu.AddItem(szItem, szStyle, (iDefaultStyle == style ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT));
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

// Set the user's default mode
public int Hndlr_DefaultStyle(Menu menu, MenuAction action, int client, int index)
{
	// Close menu if they have selected to exit.
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int iStyle = StringToInt(szInfo);
	
	SetClientCookie(client, g_hDefaultStyle, szInfo);
	
	char szStyle[MAX_MODE_NAME];
	Influx_GetStyleName(iStyle, szStyle, sizeof(szStyle));
	
	Influx_PrintToChat(_, client, "%t", "SETDEFAULTSTYLE", szStyle);
	
	return 0;
} 

int GetClientDefaultStyle(int client)
{
	if (IsFakeClient(client))return STYLE_INVALID;
	
	if (!AreClientCookiesCached(client))return STYLE_INVALID;
	
	// Get default mode
	char szStyle[32];
	GetClientCookie(client, g_hDefaultStyle, szStyle, sizeof(szStyle));

	if (szStyle[0] == '\0')
	{
		return STYLE_NORMAL;
	}
	else
	{
		return StringToInt(szStyle);
	}
}
#include <sourcemod>
#include <cstrike>
#include <smlib>

#include <influx/core>
#include <influx/jumps>
#include <influx/playerranks>

#include <msharedutil/misc>
#include <chat-processor>

//#define DEBUG_LEVELS

// Information on ranks and current level
int g_iPlayerRanks[INF_MAXPLAYERS];
int g_iPlayerLevel[INF_MAXPLAYERS];
int g_iPlayerRegistered;

// Storing player jumps (both this map & cached)
int g_iPlayerJumps[INF_MAXPLAYERS];
int g_iCachedPlayerJumps[INF_MAXPLAYERS];

enum Level {
	LevelNumber, 
	String:ChatTag[32], 
	String:ScoreboardTag[32], 
	String:Colour[32], 
	Jumps
}

ArrayList g_hLevels;
Handle g_hLevelTemplate[Level];

// CONVARS
ConVar g_ConVar_RankType;

#include "influx_playerranks/db.sp"
#include "influx_playerranks/db_cb.sp"

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Player Ranks", 
	description = "Teehee ranks :^)", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	LoadTranslations(INFLUX_PHRASES);
	
	// CONVARS
	// 0 = jumps applied on run finish
	// 1 = all jumps count
	g_ConVar_RankType = CreateConVar("influx_ranktype", "1", "How ranks are counted", FCVAR_NOTIFY, true, 0.0, true, 1.0);
	
	// NATIVES
	CreateNative("Influx_GetClientPlayerRank", Native_GetClientPlayerRank);
	
	// EVENTS
	HookEvent("player_jump", E_PlayerJump);
	
	// COMMANDS 
	RegConsoleCmd("sm_playerrank", Cmd_PlayerRank);
	RegConsoleCmd("sm_prank", Cmd_PlayerRank);
	
	return APLRes_Success;
}

public void OnAllPluginsLoaded()
{
	Handle db = Influx_GetDB();
	
	if (db == null)
	{
		SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	}
	
	SQL_TQuery(db, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...INF_TABLE_RANKS..." (steamid VARCHAR(34) PRIMARY KEY, jumps INTEGER DEFAULT 0, prestige INTEGER DEFAULT 0, lastconnectedpoints INTEGER DEFAULT 0)", _, DBPrio_High);
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data) {  }

public OnPluginStart()
{
	// Hook spawn
	HookEvent("player_spawn", E_PlayerSpawn);
}

public void OnMapStart()
{
	DB_UpdateRegisteredClients();
	
	// Load levels into cache.
	if (!LoadLevels())
	{
		SetFailState(INF_CON_PRE..."Couldn't load levels!");
	}
}

public void OnClientPutInServer(int client)
{
	DB_UpdatePlayerRank(client);
	
	if (IsFakeClient(client))return;
	
	// Update ranks & print join message.
	CreateTimer(1.5, Timer_PrintJoinMessage, client);
	CreateTimer(3.0, Timer_UpdateLevel, client);
}

public Action Timer_PrintJoinMessage(Handle timer, any client) {
	if (!IsClientInGame(client))return;
	char szClient[MAX_NAME_LENGTH];
	GetClientName(client, szClient, sizeof(szClient));
	Influx_RemoveChatColors(szClient, sizeof(szClient));
	
	Influx_PrintToChatAll(_, client, "%T", "PLAYERCONNECTED", LANG_SERVER, szClient, g_iPlayerRanks[client], g_iPlayerRegistered);
}

/*
	Adding Jumps
*/

// Add jumps to database
public void OnClientDisconnect(int client)
{
	// Check whether we count every single jump.
	if (!g_ConVar_RankType.BoolValue)return;
	
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	char szSteamID[MAX_STEAMAUTH_LENGTH];
	if (!Inf_GetClientSteam(client, szSteamID, sizeof(szSteamID)))
	{
		return;
	}
	
	char szQuery[192];
	FormatEx(szQuery, sizeof(szQuery), "UPDATE "...INF_TABLE_RANKS..." SET jumps = jumps + %i WHERE steamid = '%s'", 
		g_iPlayerJumps[client], 
		szSteamID
		);
	
	SQL_TQuery(db, Thrd_Update, szQuery, GetClientUserId(client), DBPrio_High);
}

// Counting every jump
public void E_PlayerJump(Event event, const char[] szEvent, bool bImUselessWhyDoIExist)
{
	// Check whether we count every single jump.
	if (!g_ConVar_RankType.BoolValue)return;
	
	int client;
	if (!(client = GetClientOfUserId(GetEventInt(event, "userid"))))return;
	
	if (!IsPlayerAlive(client))return;
	
	// Only when running.
	if (Influx_GetClientState(client) != STATE_RUNNING)return;
	
	g_iPlayerJumps[client]++;
}

// Counting on run finish
public void Influx_OnTimerFinishPost(int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags)
{
	// Check whether jumps are applied only on a run finish.
	if (g_ConVar_RankType.BoolValue)return;
	
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	char szSteamID[INF_MAXSTEAMID];
	
	if (!Inf_GetClientSteam(client, szSteamID, sizeof(szSteamID)))
	{
		return;
	}
	
	g_iPlayerJumps[client] = Influx_GetClientJumpCount(client);
}

/*
	Levels
*/

// Get level when the client joins.
public Action Timer_UpdateLevel(Handle timer, any client) {
	if (!IsClientInGame(client))return;
	int nPlayerJumps = g_iCachedPlayerJumps[client];
	for (int level = 0; level < GetArraySize(g_hLevels); level++)
	{
		Handle hLevel[Level];
		GetArrayArray(g_hLevels, level, hLevel[LevelNumber]);
		
		#if defined DEBUG_LEVELS
		LogAction(-1, -1, "Jumps needed for level %i: %i", level, hLevel[Jumps]);
		#endif
		
		if (hLevel[Jumps] <= nPlayerJumps) {  // More jumps
			#if defined DEBUG_LEVELS
			LogAction(-1, -1, "Level for client selected: %i", level);
			#endif
			g_iPlayerLevel[client] = hLevel[LevelNumber];
			CS_SetClientClanTag(client, hLevel[ScoreboardTag]);
			break;
		}
	}
	
	// If client doesn't have a level, clear their tag.
	if (g_iPlayerLevel[client] < 1) {
		CS_SetClientClanTag(client, "");
	}
}

/*
	Clantags
*/

// Check if client changes clantag
public void OnClientSettingsChanged(int client)
{
	if (!client || !IsClientInGame(client))return;
	
	if (g_iPlayerLevel[client] > 0) {
		Handle hLevel[Level];
		GetArrayArray(g_hLevels, g_hLevels.Length - g_iPlayerLevel[client], hLevel[LevelNumber]);
		CS_SetClientClanTag(client, hLevel[ScoreboardTag]);
	} else {
		CS_SetClientClanTag(client, "");
	}
}

// Check when the client respawns whether they have changed their tag.
public void E_PlayerSpawn(Event event, const char[] szEvent, bool bImUselessWhyDoIExist)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	if (!client)return;
	
	if (g_iPlayerLevel[client] > 0) {
		Handle hLevel[Level];
		GetArrayArray(g_hLevels, g_hLevels.Length - g_iPlayerLevel[client], hLevel[LevelNumber]);
		CS_SetClientClanTag(client, hLevel[ScoreboardTag]);
	} else {
		CS_SetClientClanTag(client, "");
	}
}


/*
	Chat Messages & Commands
*/
public Action OnChatMessage(int & author, ArrayList recipients, eChatFlags & flag, char[] name, char[] message, bool & bProcessColors, bool & bRemoveColors)
{
	// Add tag to chat message
	if (g_iPlayerLevel[author] > 0) {
		Handle hLevel[Level];
		GetArrayArray(g_hLevels, g_hLevels.Length - g_iPlayerLevel[author], hLevel[LevelNumber]);
		Format(name, MAXLENGTH_NAME, " {CHATCLR}[%s%s{CHATCLR}] {DEFAULT}%s", hLevel[Colour], hLevel[ChatTag], name);
		Influx_FormatChatColors(name, MAXLENGTH_NAME);
		return Plugin_Changed;
	}
	return Plugin_Continue;
}

public Action Cmd_PlayerRank(int client, int args)
{
	if (!IsClientInGame(client))return Plugin_Handled;
	
	Influx_PrintToChat(_, client, "%T", "PLAYERRANK", client, g_iPlayerRanks[client], g_iPlayerRegistered);
	
	return Plugin_Handled;
}

// Other
bool LoadLevels()
{
	
	char szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "configs");
	
	if (!DirExistsEx(szPath))return false;
	
	Format(szPath, sizeof(szPath), "%s/%s", szPath, INF_LEVELS_FILE);
	
	KeyValues kv = new KeyValues("Levels");
	kv.ImportFromFile(szPath);
	
	if (!kv.GotoFirstSubKey())
	{
		return false;
	}
	
	g_hLevels = CreateArray(sizeof(g_hLevelTemplate));
	
	do
	{
		Handle hLevelTemplate[Level];
		
		hLevelTemplate[LevelNumber] = kv.GetNum("LevelNumber", 0);
		kv.GetString("ChatTag", hLevelTemplate[ChatTag], sizeof(hLevelTemplate[ChatTag]));
		kv.GetString("ScoreboardTag", hLevelTemplate[ScoreboardTag], sizeof(hLevelTemplate[ScoreboardTag]));
		kv.GetString("Colour", hLevelTemplate[Colour], sizeof(hLevelTemplate[Colour]));
		hLevelTemplate[Jumps] = kv.GetNum("Jumps", 0);
		
		PushArrayArray(g_hLevels, hLevelTemplate[LevelNumber]);
	} while (kv.GotoNextKey());
	
	delete kv;
	return true;
}


public int Native_GetClientPlayerRank(Handle hPlugin, int nParams)
{
	if (g_iPlayerRegistered < g_iPlayerRanks[GetNativeCell(1)]) {
		g_iPlayerRegistered++;
	}
	
	return g_iPlayerRanks[GetNativeCell(1)];
	
}

public int Native_GetRegisteredPlayerCount(Handle hPlugin, int nParams)
{
	return g_iPlayerRegistered;
} 
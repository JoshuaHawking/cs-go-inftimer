#include <sourcemod>

#include <influx/core>
#include <influx/zones>
#include <influx/zones_stage>
#include <influx/zones_checkpoint>

#undef REQUIRE_PLUGIN
ConVar g_ConVar_NumDecimals;

bool g_bLib_Zones_CP;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Stage (Time Comparison)", 
	description = "", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	g_ConVar_NumDecimals = CreateConVar("influx_stagechat_numdecimals", "2", "Number of decimals to use when printing to chat.", FCVAR_NOTIFY, true, 0.0, true, 3.0);
}

public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	// LIBRARIES
	g_bLib_Zones_CP = LibraryExists(INFLUX_LIB_ZONES_CP);
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_ZONES_CP))g_bLib_Zones_CP = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_ZONES_CP))g_bLib_Zones_CP = false;
}

public void Influx_OnClientCPSavePost(int client, int cpnum)
{
	float flCPTime = INVALID_RUN_TIME;
	
	if (Influx_GetClientCurrentBestTime(client) == INVALID_RUN_TIME)
	{
		Influx_PrintToChat(_, client, "%t", "STAGENOTIME", 
			cpnum + 1
			);
		return;
	}
	
	if (g_bLib_Zones_CP && (GetEngineTime() - Influx_GetClientLastCPTouch(client)) < 2.0)
	{
		flCPTime = Influx_GetClientLastCPBestTime(client);
		
		char szFormSec[10];
		Inf_DecimalFormat(g_ConVar_NumDecimals.IntValue, szFormSec, sizeof(szFormSec));
		
		char szSymbol[1];
		char szTimeFormatted[10];
		float flTimeDifference;
		
		float time = Influx_GetClientLastCPTime(client);
		bool bIsFaster = ((flCPTime >= time) ? true : false);
		// No World Record
		if (time >= flCPTime) {
			szSymbol = "+";
			flTimeDifference = time - flCPTime;
		}
		else // New World Record
		{
			szSymbol = "-";
			flTimeDifference = flCPTime - time;
		}
		
		// Format time
		Inf_FormatSeconds(flTimeDifference, szTimeFormatted, sizeof(szTimeFormatted), szFormSec);
		
		Influx_PrintToChat(_, client, "%t", "STAGETIME", 
			cpnum + 1, 
			(bIsFaster) ? "LIMEGREEN" : "LIGHTRED", 
			szSymbol, 
			szTimeFormatted
			);
	}
} 
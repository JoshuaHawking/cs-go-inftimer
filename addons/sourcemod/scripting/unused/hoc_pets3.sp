#include <sdktools>
#include <sdkhooks>

#include <pets>

#include <msharedutil/misc>

#define SOUND_SPAWN "ambient/machines/catapult_throw.wav"

#define STATE_IDLE		1
#define STATE_WALKING	2
#define STATE_JUMPING	3

#define HEIGHT_GROUND	1
#define HEIGHT_HOVER	2
#define HEIGHT_FLY		3

#define PETCOLOR_NORMAL	0
#define PETCOLOR_RED	1
#define PETCOLOR_GREEN	2
#define PETCOLOR_BLUE	3
#define PETCOLOR_PINK	4
#define PETCOLOR_ORANGE	5
#define PETCOLOR_CYAN	6
#define PETCOLOR_LIME	7
#define PETCOLOR_BLACK	8
#define PETCOLOR_TEAM	9
#define PETCOLOR_LTEAM	10
#define PETCOLOR_DTEAM	11
#define PETCOLOR_MAGIC	12

#define PET_HUGCRAB		1

enum petContent
{
	String:iName[64], 
	String:iDesc[64], 
	String:iModel[128], 
	String:iSoundGeneric[128], 
	String:iSoundAmount[128], 
	String:iSoundGeneric_2[128], 
	String:iSoundGeneric_3[128], 
	String:iSoundGeneric_4[128], 
	String:iSoundGeneric_5[128], 
	String:iSoundGeneric_6[128], 
	String:iSoundGeneric_7[128], 
	String:iSoundGeneric_8[128], 
	String:iSoundGeneric_9[128], 
	String:iSoundGeneric_10[128], 
	
	String:iSkinName_1[64], 
	String:iSkinName_2[64], 
	String:iSkinName_3[64], 
	String:iSkinName_4[64], 
	String:iSkinName_5[64], 
	String:iSkinName_6[64], 
	String:iSkinName_7[64], 
	String:iSkinName_8[64], 
	String:iSkinName_9[64], 
	String:iSkinName_10[64], 
	
	String:iSoundJump[128], 
	iPitch, 
	String:iAnimIdle[64], 
	String:iAnimWalk[64], 
	String:iAnimJump[64], 
	iHeight, 
	iHeight_Custom, 
	SkinID, 
	SkinsAmount, 
	ADMFLAG, 
	CanBeColored, 
	Hidden, 
	Float:ModelScale
};

stock petInfo[256][petContent]

int g_iPet[MAXPLAYERS + 1];
int g_iPetType[MAXPLAYERS + 1];
char g_szPetName[MAXPLAYERS + 1][32];
int g_iPetColor[MAXPLAYERS + 1];
int g_iPetHappiness[MAXPLAYERS + 1];
int g_iPetParticle[MAXPLAYERS + 1];
int g_iPetState[MAXPLAYERS + 1];
int g_iPetSkin[MAXPLAYERS + 1];
bool g_bPetChangeCooldown[MAXPLAYERS + 1];
bool g_bDBEntryExist[MAXPLAYERS + 1];

bool g_bHidePets[MAXPLAYERS + 1];

int LastIndex = 0;
bool MenuNotUsable = false;
Handle soundTimer[MAXPLAYERS + 1] = INVALID_HANDLE;

ConVar g_ConVar_MoodEnabled;
ConVar g_ConVar_ChangeCooldown;
ConVar g_ConVar_ReloadOnMapChange;

Handle g_hDB;

public Plugin myinfo = 
{
	name = "SourcePets", 
	author = "Oshizu & noodleboy347 - Improved by Fusion", 
	description = "Get off me you", 
	version = "1.18", 
}

// Extra definitions
#define CHAT_PREFIX				" \x08[\x0BPets\x08] "

#define MYSQL_CONFIG_NAME		"pets"
#define MYSQL_TABLE_NAME		"pets"

#define MAX_STEAMAUTH_LENGTH	21

public TF2_OnWaitingForPlayersStart()
{
	MenuNotUsable = true;
}

public TF2_OnWaitingForPlayersEnd()
{
	MenuNotUsable = false;
}

stock CreateLoopingParticle(ent, String:particleType[])
{
	new particle = CreateEntityByName("info_particle_system");
	if (IsValidEdict(particle))
	{
		char tName[32];
		GetEntPropString(ent, Prop_Data, "m_iName", tName, sizeof(tName));
		DispatchKeyValue(particle, "targetname", "tf2particle");
		DispatchKeyValue(particle, "parentname", tName);
		DispatchKeyValue(particle, "effect_name", particleType);
		DispatchSpawn(particle);
		ActivateEntity(particle);
		
		AcceptEntityInput(particle, "start");
	}
	return particle;
}

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	RegPluginLibrary(LIB_PETS);
	
	return APLRes_Success;
}

public OnPluginStart()
{
	
	RegAdminCmd("sm_pet", Cmd_PetsMenu, ADMFLAG_RESERVATION)
	RegAdminCmd("sm_pets", Cmd_PetsMenu, ADMFLAG_RESERVATION)
	RegAdminCmd("sm_petname", Cmd_PetName, ADMFLAG_RESERVATION)
	RegAdminCmd("sm_petstatus", Cmd_PetStatus, ADMFLAG_RESERVATION)
	RegAdminCmd("sm_reloadpets", Cmd_ReloadPets, ADMFLAG_ROOT)
	
	
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("player_death", Event_Death);
	
	CreateTimer(GetRandomFloat(28.0, 45.0), RandomizeMood, _, TIMER_REPEAT)
	
	CreateConVar("pets_version", "1.18", "- Do Not Touch!", FCVAR_NOTIFY | FCVAR_REPLICATED)
	
	// ConVars
	g_ConVar_ChangeCooldown = CreateConVar("pets_change_cooldown", "4", "- Cooldown between changing your pet. Anything below 1 will result in no cooldown", _, true, 0.0);
	g_ConVar_MoodEnabled = CreateConVar("pets_mood_enabled", "0", "- Should pets have various moods by default?", _, true, 0.0, true, 1.0);
	g_ConVar_ReloadOnMapChange = CreateConVar("pets_reload_mapchange", "0", "- Should plugin attempt to reload config on map change?", _, true, 0.0, true, 1.0);
	
	// Natives
	CreateNative("Pets_GetClientHidePets", Native_GetClientHidePets);
	CreateNative("Pets_SetClientHidePets", Native_SetClientHidePets);
	
	LoadConfig()
	
	/*
		Load Database
	*/
	DB_LoadDatabase();
}

public Action Cmd_ReloadPets(client, args)
{
	LoadConfig()
	ReplyToCommand(client, "Pets config has been successfully reloaded.")
	return Plugin_Handled;
}

stock LoadConfig()
{
	Handle kv = CreateKeyValues("Pets");
	
	char location[96]
	char text[96]
	char loc[4]
	BuildPath(Path_SM, location, sizeof(location), "configs/pets.cfg");
	for (new i = 1; i <= 256; i++)
	{
		FileToKeyValues(kv, location);
		Format(text, sizeof(text), "%i", i)
		if (!KvJumpToKey(kv, text))
		{
			if (i == 1 || i == 0)
			{
				SetFailState("[Pets] Unable to read sourcemod/configs/pets.cfg file. Have you setup everything correctly?")
			}
			LastIndex = i - 1
			break;
		}
		KvGetString(kv, "name", petInfo[i][iName], 64);
		KvGetString(kv, "desc", petInfo[i][iDesc], 64);
		KvGetString(kv, "model", petInfo[i][iModel], 128);
		
		decl String:flags[32]
		KvGetString(kv, "adminflag", flags, 32, "0");
		
		if (StrEqual(flags, "0"))
		{
			petInfo[i][ADMFLAG] = 0;
		}
		else if (StrEqual(flags, "a"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_RESERVATION;
		}
		else if (StrEqual(flags, "b"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_GENERIC;
		}
		else if (StrEqual(flags, "c"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_KICK;
		}
		else if (StrEqual(flags, "d"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_BAN;
		}
		else if (StrEqual(flags, "e"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_UNBAN;
		}
		else if (StrEqual(flags, "f"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_SLAY;
		}
		else if (StrEqual(flags, "g"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CHANGEMAP;
		}
		else if (StrEqual(flags, "h"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CONVARS;
		}
		else if (StrEqual(flags, "i"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CONFIG;
		}
		else if (StrEqual(flags, "j"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CHAT;
		}
		else if (StrEqual(flags, "k"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_VOTE;
		}
		else if (StrEqual(flags, "l"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_PASSWORD;
		}
		else if (StrEqual(flags, "m"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_RCON;
		}
		else if (StrEqual(flags, "n"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CHEATS;
		}
		else if (StrEqual(flags, "o"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CUSTOM1;
		}
		else if (StrEqual(flags, "p"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CUSTOM2;
		}
		else if (StrEqual(flags, "q"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CUSTOM3;
		}
		else if (StrEqual(flags, "r"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CUSTOM4;
		}
		else if (StrEqual(flags, "s"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CUSTOM5;
		}
		else if (StrEqual(flags, "t"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_CUSTOM6;
		}
		else if (StrEqual(flags, "z"))
		{
			petInfo[i][ADMFLAG] = ADMFLAG_ROOT;
		}
		
		petInfo[i][CanBeColored] = KvGetNum(kv, "can_be_colored", 1)
		
		petInfo[i][SkinsAmount] = KvGetNum(kv, "skins")
		petInfo[i][iSoundAmount] = KvGetNum(kv, "sound_idle_amount")
		petInfo[i][Hidden] = KvGetNum(kv, "hidden")
		if (petInfo[i][iSoundAmount] > 0)
		{
			KvGetString(kv, "sound_idle", petInfo[i][iSoundGeneric], 128);
			if (petInfo[i][iSoundAmount] > 1)
			{
				for (new e = 2; e <= petInfo[i][iSoundAmount]; e++)
				{
					Format(text, sizeof(text), "sound_idle_%i", e)
					switch (e)
					{
						case 2:KvGetString(kv, text, petInfo[i][iSoundGeneric_2], 128, "Mia");
						case 3:KvGetString(kv, text, petInfo[i][iSoundGeneric_3], 128, "Mia");
						case 4:KvGetString(kv, text, petInfo[i][iSoundGeneric_4], 128, "Mia");
						case 5:KvGetString(kv, text, petInfo[i][iSoundGeneric_5], 128, "Mia");
						case 6:KvGetString(kv, text, petInfo[i][iSoundGeneric_6], 128, "Mia");
						case 7:KvGetString(kv, text, petInfo[i][iSoundGeneric_7], 128, "Mia");
						case 8:KvGetString(kv, text, petInfo[i][iSoundGeneric_8], 128, "Mia");
						case 9:KvGetString(kv, text, petInfo[i][iSoundGeneric_9], 128, "Mia");
						case 10:KvGetString(kv, text, petInfo[i][iSoundGeneric_10], 128, "Mia");
					}
					if (StrEqual(text, "mia"))
					{
						break;
					}
				}
			}
			if (petInfo[i][SkinsAmount] > 1)
			{
				KvGetString(kv, "skin_1_name", petInfo[i][iSkinName_1], 64)
				for (new e = 2; e <= petInfo[i][SkinsAmount]; e++)
				{
					Format(text, sizeof(text), "skin_%i_name", e)
					switch (e)
					{
						case 2:KvGetString(kv, text, petInfo[i][iSkinName_2], 64);
						case 3:KvGetString(kv, text, petInfo[i][iSkinName_3], 64);
						case 4:KvGetString(kv, text, petInfo[i][iSkinName_4], 64);
						case 5:KvGetString(kv, text, petInfo[i][iSkinName_5], 64);
						case 6:KvGetString(kv, text, petInfo[i][iSkinName_6], 64);
						case 7:KvGetString(kv, text, petInfo[i][iSkinName_7], 64);
						case 8:KvGetString(kv, text, petInfo[i][iSkinName_8], 64);
						case 9:KvGetString(kv, text, petInfo[i][iSkinName_9], 64);
						case 10:KvGetString(kv, text, petInfo[i][iSkinName_10], 64);
					}
				}
			}
		}
		
		KvGetString(kv, "sound_jumping", petInfo[i][iSoundJump], 128);
		petInfo[i][iPitch] = KvGetNum(kv, "pitch")
		KvGetString(kv, "anim_idle", petInfo[i][iAnimIdle], 64);
		KvGetString(kv, "anim_walk", petInfo[i][iAnimWalk], 64);
		KvGetString(kv, "anim_jump", petInfo[i][iAnimJump], 64);
		petInfo[i][iHeight] = KvGetNum(kv, "height_type")
		petInfo[i][iHeight_Custom] = KvGetNum(kv, "height_custom")
		petInfo[i][SkinID] = KvGetNum(kv, "skin")
		petInfo[i][ModelScale] = KvGetFloat(kv, "modelscale")
	}
	CloseHandle(kv);
}

public Action Cmd_PetName(int client, int args)
{
	// Check valid client.
	if (!client)
	{
		return Plugin_Handled;
	}
	
	// Build tag
	char arg[128];
	char szName[32];
	
	for (int i = 1; i <= args; i++)
	{
		GetCmdArg(i, arg, sizeof(arg));
		Format(szName, sizeof(szName), "%s %s", szName, arg);
	}
	
	TrimString(szName);
	
	if (strlen(szName) < 2)
	{
		PrintToChat(client, "%sYou can't use that name!", CHAT_PREFIX);
	}
	else
	{
		Format(g_szPetName[client], sizeof(g_szPetName[]), "%s", szName)
		PrintToChat(client, "%sSet your pet's name to \x06%s", CHAT_PREFIX, szName);
	}
	
	return Plugin_Handled;
}

public Action RandomizeMood(Handle timer)
{
	if (g_ConVar_MoodEnabled.BoolValue)
	{
		for (new i = 1; i <= MaxClients; i++)
		{
			if (IsClientInGame(i) && g_iPetType[i] > 0)
			{
				g_iPetHappiness[i] = RoundFloat(GetRandomFloat(1.00, 27.00))
			}
		}
	}
}

PetSkin(client)
{
	Handle menu = CreateMenu(PetsSkin_Handler, MenuAction_Select | MenuAction_End);
	SetMenuExitBackButton(menu, true)
	
	SetMenuTitle(menu, "Pets - Choose Pet's Skin:");
	
	char Text[64]
	char value[4]
	switch (g_iPetSkin[client])
	{
		case 0:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_1])
		case 1:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_2])
		case 2:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_3])
		case 3:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_4])
		case 4:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_5])
		case 5:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_6])
		case 6:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_7])
		case 7:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_8])
		case 8:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_9])
		case 9:Format(Text, sizeof(Text), "Current Skin: %s", petInfo[g_iPetType[client]][iSkinName_10])
	}
	AddMenuItem(menu, "X", Text, ITEMDRAW_DISABLED)
	
	for (new i = 0; i < LastIndex - 1; i++)
	{
		switch (i)
		{
			case 0:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_1])
			case 1:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_2])
			case 2:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_3])
			case 3:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_4])
			case 4:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_5])
			case 5:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_6])
			case 6:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_7])
			case 7:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_8])
			case 8:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_9])
			case 9:Format(Text, sizeof(Text), "%s", petInfo[g_iPetType[client]][iSkinName_10])
		}
		IntToString(i, value, sizeof(value))
		AddMenuItem(menu, value, Text)
	}
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public PetsSkin_Handler(Handle menu, MenuAction:action, client, param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			decl String:item[64];
			GetMenuItem(menu, param2, item, sizeof(item));
			g_iPetSkin[client] = StringToInt(item);
			
			if (IsValidPet(g_iPet[client]))
			{
				if (g_iPetSkin[client] > 0)
				{
					SetEntProp(g_iPet[client], Prop_Send, "m_nSkin", g_iPetSkin[client])
				}
				else
				{
					SetEntProp(g_iPet[client], Prop_Send, "m_nSkin", petInfo[g_iPetType[client]][SkinID]);
				}
			}
			
			PetSkin(client)
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch (param2)
			{
				case MenuCancel_ExitBack:
				{
					Cmd_PetsMenu(client, 0);
					return;
				}
			}
		}
	}
}

public Action Cmd_PetsMenu(client, args)
{
	if (MenuNotUsable)
	{
		return Plugin_Handled;
	}
	Handle menu = CreateMenu(PetsMenu_Handler, MenuAction_Select | MenuAction_End);
	
	char szMessage[128];
	szMessage = "\n "
	if (StrEqual(g_szPetName[client], "Unnamed Fella"))
	{
		szMessage = "\n \nHint: Your pet is currently unnnamed. \nUse !petname command in chat to give your pet a name!\n "
	}
	
	SetMenuTitle(menu, "Pets - Main Menu%s", szMessage);
	
	AddMenuItem(menu, "1", "Choose your pet")
	if (petInfo[g_iPetType[client]][CanBeColored] == 1)
	{
		AddMenuItem(menu, "2", "Pet's Color")
	}
	else
	{
		AddMenuItem(menu, "2", "Pet's Color", ITEMDRAW_DISABLED)
	}
	if (petInfo[g_iPetType[client]][SkinsAmount] > 1)
	{
		AddMenuItem(menu, "3", "Pet's Skin")
	}
	else
	{
		AddMenuItem(menu, "X", "Pet's Skin", ITEMDRAW_DISABLED)
	}
	if (g_ConVar_MoodEnabled.BoolValue)
	{
		AddMenuItem(menu, "0", "Pet's Status")
	}
	/*	if(g_iPetType[client] > 0)
	{
		Format(pet_types, sizeof(pet_types), "Pet Type: %s", petInfo[g_iPetType[client]][iName])
		AddMenuItem(menu, "2", pet_types)
	}
	else
	{
		AddMenuItem(menu, "2", "Pet Type: No Pet Choosen")
	}
	
	if(petInfo[g_iPetType[client]][SkinsAmount] > 1)
	{
		switch(g_iPetSkin[client]) 
		{
			case 0: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_1])
			case 1: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_2])
			case 2: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_3])
			case 3: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_4])
			case 4: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_5])
			case 5: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_6])
			case 6: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_7])
			case 7: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_8])
			case 8: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_9])
			case 9: Format(pet_types, sizeof(pet_types), "Skin: %s", petInfo[g_iPetType[client]][iSkinName_10])
		}
		AddMenuItem(menu, "3", pet_types)
	}
	
	switch(g_iPetColor[client])
	{
		case 0:	AddMenuItem(menu, "1", "Pet Color: No Color");
		case 1:	AddMenuItem(menu, "1", "Pet Color: Red");
		case 2:	AddMenuItem(menu, "1", "Pet Color: Green");
		case 3:	AddMenuItem(menu, "1", "Pet Color: Blue");
		case 4:	AddMenuItem(menu, "1", "Pet Color: Pink");
		case 5:	AddMenuItem(menu, "1", "Pet Color: Orange");
		case 6:	AddMenuItem(menu, "1", "Pet Color: Cyan");
		case 7:	AddMenuItem(menu, "1", "Pet Color: Lime");
		case 8:	AddMenuItem(menu, "1", "Pet Color: Black");
		case 9:	AddMenuItem(menu, "1", "Pet Color: Team Based");
		case 10: AddMenuItem(menu, "1", "Pet Color: L-Team");
		case 11: AddMenuItem(menu, "1", "Pet Color: D-Team");
		case 12: AddMenuItem(menu, "1", "Pet Color: Magic");
	}*/
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public PetsMenu_Handler(Handle menu, MenuAction:action, client, param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			decl String:item[64];
			GetMenuItem(menu, param2, item, sizeof(item));
			new menuv = StringToInt(item)
			if (menuv == 0)
			{
				Cmd_PetStatus(client, 0)
			}
			else if (menuv == 1)
			{
				PetType(client)
			}
			else if (menuv == 2)
			{
				PetColor(client)
			}
			else if (menuv == 3)
			{
				PetSkin(client)
			}
			else
			{
				Cmd_PetsMenu(client, 0)
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
	}
}

PetType(client)
{
	Handle menu = CreateMenu(PetsType_Handler, MenuAction_Select | MenuAction_End);
	SetMenuExitBackButton(menu, true)
	
	char Text[64]
	char value[4]
	if (g_iPetType[client] == 0)
	{
		Format(Text, sizeof(Text), "Current Pet: No Pet")
	}
	else
	{
		Format(Text, sizeof(Text), "Current Pet: %s", petInfo[g_iPetType[client]][iName])
	}
	SetMenuTitle(menu, "Pets - Choose Your Pet:\n%s", Text);
	
	AddMenuItem(menu, "150", "No Pet")
	
	new bits;
	for (new i = 1; i <= 256; i++)
	{
		if (strlen(petInfo[i][iName]) > 1)
		{
			IntToString(i, value, sizeof(value))
			bits = GetUserFlagBits(client)
			if (petInfo[i][ADMFLAG] == 0 || bits & petInfo[i][ADMFLAG] || bits & ADMFLAG_ROOT)
			{
				Format(Text, sizeof(Text), "%s", petInfo[i][iName])
				AddMenuItem(menu, value, Text)
			}
			else
			{
				if (petInfo[i][Hidden] != 1)
				{
					Format(Text, sizeof(Text), "%s (No Access)", petInfo[i][iName])
					AddMenuItem(menu, value, Text, ITEMDRAW_DISABLED)
				}
			}
		}
		else
		{
			break;
		}
	}
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public PetsType_Handler(Handle menu, MenuAction:action, client, param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			decl String:item[64];
			GetMenuItem(menu, param2, item, sizeof(item));
			new value = StringToInt(item);
			if (!g_bPetChangeCooldown[client])
			{
				if (g_iPetType[client] != value)
				{
					if (g_ConVar_ChangeCooldown.IntValue >= 1)
					{
						g_bPetChangeCooldown[client] = true;
						CreateTimer(float(g_ConVar_ChangeCooldown.IntValue), ResetCooldown, EntIndexToEntRef(client))
					}
					
					if (value == 150)
					{
						if (g_iPetType[client] != 0)
						{
							g_iPetType[client] = 0;
							g_iPetSkin[client] = 0;
							KillPet(client);
							g_bPetChangeCooldown[client] = false;
						}
					}
					else
					{
						if (IsPlayerAlive(client))
						{
							g_iPetType[client] = value;
							g_iPetSkin[client] = 0;
							KillPet(client);
							SpawnPet(client);
						}
					}
				}
				PetType(client)
			}
			else
			{
				PrintToChat(client, "%sYou can't change your pet right now. Please try again later!", CHAT_PREFIX)
				PetType(client)
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch (param2)
			{
				case MenuCancel_ExitBack:
				{
					Cmd_PetsMenu(client, 0);
					return;
				}
			}
		}
	}
}

public Action ResetCooldown(Handle timer, any:ref)
{
	new client = EntRefToEntIndex(ref)
	if (client != INVALID_ENT_REFERENCE)
	{
		g_bPetChangeCooldown[client] = false;
	}
}

PetColor(client)
{
	Handle menu = CreateMenu(PetsColor_Handler, MenuAction_Select | MenuAction_End);
	SetMenuExitBackButton(menu, true)
	
	char Text[64]
	switch (g_iPetColor[client])
	{
		case 0:Format(Text, sizeof(Text), "Current Color: No Color")
		case 1:Format(Text, sizeof(Text), "Current Color: Red")
		case 2:Format(Text, sizeof(Text), "Current Color: Green")
		case 3:Format(Text, sizeof(Text), "Current Color: Blue")
		case 4:Format(Text, sizeof(Text), "Current Color: Pink")
		case 5:Format(Text, sizeof(Text), "Current Color: Orange")
		case 6:Format(Text, sizeof(Text), "Current Color: Cyan")
		case 7:Format(Text, sizeof(Text), "Current Color: Lime")
		case 8:Format(Text, sizeof(Text), "Current Color: Black")
		case 9:Format(Text, sizeof(Text), "Current Color: Team Based")
		case 10:Format(Text, sizeof(Text), "Current Color: L-Team")
		case 11:Format(Text, sizeof(Text), "Current Color: D-Team")
		case 12:Format(Text, sizeof(Text), "Current Color: Magic")
	}
	
	SetMenuTitle(menu, "Pets - Choose Your Pet's Color:\n%s", Text);
	
	AddMenuItem(menu, "0", "No Color");
	AddMenuItem(menu, "1", "Red");
	AddMenuItem(menu, "2", "Green");
	AddMenuItem(menu, "3", "Blue");
	AddMenuItem(menu, "4", "Pink");
	AddMenuItem(menu, "5", "Orange");
	AddMenuItem(menu, "6", "Cyan");
	AddMenuItem(menu, "7", "Lime");
	AddMenuItem(menu, "8", "Black");
	AddMenuItem(menu, "9", "Team Based");
	AddMenuItem(menu, "10", "L-Team");
	AddMenuItem(menu, "11", "D-Team");
	AddMenuItem(menu, "12", "Magic");
	
	DisplayMenu(menu, client, MENU_TIME_FOREVER);
}

public PetsColor_Handler(Handle menu, MenuAction:action, client, param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			decl String:item[64];
			GetMenuItem(menu, param2, item, sizeof(item));
			g_iPetColor[client] = StringToInt(item);
			
			if (petInfo[g_iPetType[client]][CanBeColored] == 1)
			{
				if (IsValidPet(g_iPet[client]))
				{
					SetEntityRenderColor(g_iPet[client], 255, 255, 255)
					switch (g_iPetColor[client])
					{
						case PETCOLOR_RED:SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
						case PETCOLOR_GREEN:SetEntityRenderColor(g_iPet[client], 0, 255, 0, 255);
						case PETCOLOR_BLUE:SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
						case PETCOLOR_PINK:SetEntityRenderColor(g_iPet[client], 255, 0, 255, 255);
						case PETCOLOR_ORANGE:SetEntityRenderColor(g_iPet[client], 255, 128, 0, 255);
						case PETCOLOR_CYAN:SetEntityRenderColor(g_iPet[client], 128, 255, 255, 255);
						case PETCOLOR_LIME:SetEntityRenderColor(g_iPet[client], 128, 255, 0, 255);
						case PETCOLOR_BLACK:SetEntityRenderColor(g_iPet[client], 0, 0, 0, 255);
						case PETCOLOR_TEAM:
						{
							if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
							else SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
						}
						case PETCOLOR_LTEAM:
						{
							if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 255, 128, 128, 255);
							else SetEntityRenderColor(g_iPet[client], 128, 128, 255, 255);
						}
						case PETCOLOR_DTEAM:
						{
							if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 128, 0, 0, 255);
							else SetEntityRenderColor(g_iPet[client], 0, 0, 128, 255);
						}
						case PETCOLOR_MAGIC:SetEntityRenderColor(g_iPet[client], GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, 255);
					}
				}
			}
			else
			{
				PrintToChat(client, "%sSorry but this pet is not colorable!", CHAT_PREFIX)
				Cmd_PetsMenu(client, 0)
			}
			PetColor(client)
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
		case MenuAction_Cancel:
		{
			switch (param2)
			{
				case MenuCancel_ExitBack:
				{
					Cmd_PetsMenu(client, 0);
					return;
				}
			}
		}
	}
}

/*public PetsMenu_Handler(Handle menu, MenuAction:action, client, param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			decl String:item[64];
			GetMenuItem(menu, param2, item, sizeof(item));
			new menuv = StringToInt(item)
			if(menuv == 0)
			{
				Cmd_PetStatus(client, 0)
			}
			else if(menuv == 1)
			{
				if(g_iPetColor[client] > -1 && g_iPetColor[client] < 12)
				{
					g_iPetColor[client]++
				}
				else
				{
					g_iPetColor[client] = 0;
				}
				
				if(g_iPet[client] > 0 && IsValidEntity(g_iPet[client]))
				{
					SetEntityRenderColor(g_iPet[client], 255, 255, 255)
					switch(g_iPetColor[client])
					{
						case PETCOLOR_RED: SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
						case PETCOLOR_GREEN: SetEntityRenderColor(g_iPet[client], 0, 255, 0, 255);
						case PETCOLOR_BLUE: SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
						case PETCOLOR_PINK: SetEntityRenderColor(g_iPet[client], 255, 0, 255, 255);
						case PETCOLOR_ORANGE: SetEntityRenderColor(g_iPet[client], 255, 128, 0, 255);
						case PETCOLOR_CYAN: SetEntityRenderColor(g_iPet[client], 128, 255, 255, 255);
						case PETCOLOR_LIME: SetEntityRenderColor(g_iPet[client], 128, 255, 0, 255);
						case PETCOLOR_BLACK: SetEntityRenderColor(g_iPet[client], 0, 0, 0, 255);
						case PETCOLOR_TEAM:
						{
							if(GetClientTeam(client) == 2) SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
							else	SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
						}
						case PETCOLOR_LTEAM:
						{
							if(GetClientTeam(client) == 2) SetEntityRenderColor(g_iPet[client], 255, 128, 128, 255);
							else	SetEntityRenderColor(g_iPet[client], 128, 128, 255, 255);
						}
						case PETCOLOR_DTEAM:
						{
							if(GetClientTeam(client) == 2) SetEntityRenderColor(g_iPet[client], 128, 0, 0, 255);
							else	SetEntityRenderColor(g_iPet[client], 0, 0, 128, 255);
						}
						case PETCOLOR_MAGIC: SetEntityRenderColor(g_iPet[client], GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, 255);
					}
				}
				Cmd_PetsMenu(client, 0)
			}
			else if(menuv == 2)
			{
				if(g_iPetType[client] < LastIndex)
				{
					g_iPetType[client]++
				}
				else
				{
					g_iPetType[client] = 1;
				}
				Cmd_PetsMenu(client, 0)
			}
			else if(menuv == 3)
			{
				if(g_iPetSkin[client] < petInfo[g_iPetType[client]][SkinsAmount]-1)
				{
					g_iPetSkin[client]++
				} 
				else
				{
					g_iPetSkin[client] = 0;
				}
				
				if(g_iPet[client] > 0 && IsValidEntity(g_iPet[client]))
				{
					if(g_iPetSkin[client] > 0)
					{
						SetEntProp(g_iPet[client], Prop_Send, "m_nSkin", g_iPetSkin[client])
					}
					else
					{
						SetEntProp(g_iPet[client], Prop_Send, "m_nSkin", petInfo[g_iPetType[client]][SkinID]);
					}
				}
				Cmd_PetsMenu(client, 0)
			}
		}
		case MenuAction_End:
		{
			CloseHandle(menu);
		}
	}
}*/

public OnPluginEnd()
{
	for (new i = 1; i <= GetMaxClients(); i++)
	{
		if (!IsValidEntity(i))continue;
		if (!IsValidEntity(g_iPet[i]))continue;
		KillPet(i);
	}
}

public OnMapStart()
{
	for (new i = 1; i <= GetMaxClients(); i++)
	{
		if (!IsValidEntity(i))continue;
		if (!IsValidEntity(g_iPet[i]))continue;
		KillPet(i);
	}
	PrecacheSound(SOUND_SPAWN);
	for (new i = 1; i < sizeof(petInfo); i++)
	{
		if (strlen(petInfo[i][iModel]) < 4)
		{
			break;
		}
		PrecacheModel(petInfo[i][iModel]);
		
		if (strlen(petInfo[i][iSoundGeneric]) > 3)
		{
			PrecacheSound(petInfo[i][iSoundGeneric]);
			for (new e = 2; e <= petInfo[i][iSoundAmount]; e++)
			{
				switch (e)
				{
					case 2:PrecacheSound(petInfo[i][iSoundGeneric_2]);
					case 3:PrecacheSound(petInfo[i][iSoundGeneric_3]);
					case 4:PrecacheSound(petInfo[i][iSoundGeneric_4]);
					case 5:PrecacheSound(petInfo[i][iSoundGeneric_5]);
					case 6:PrecacheSound(petInfo[i][iSoundGeneric_6]);
					case 7:PrecacheSound(petInfo[i][iSoundGeneric_7]);
					case 8:PrecacheSound(petInfo[i][iSoundGeneric_8]);
					case 9:PrecacheSound(petInfo[i][iSoundGeneric_9]);
					case 10:PrecacheSound(petInfo[i][iSoundGeneric_10]);
				}
			}
		}
		if (strlen(petInfo[i][iSoundJump]) > 3)
		{
			PrecacheSound(petInfo[i][iSoundJump]);
		}
	}
	
	Handle kv = CreateKeyValues("Pets");
	
	char location[96]
	char text[96]
	char loc[4]
	BuildPath(Path_SM, location, sizeof(location), "configs/pets.cfg");
	FileToKeyValues(kv, location);
	KvRewind(kv);
	
	KvJumpToKey(kv, "Downloads");

	for (new y = 1; y <= 8152; y++)
	{
		IntToString(y, loc, sizeof(loc))
		KvGetString(kv, loc, text, sizeof(text), "mia")
		AddFileToDownloadsTable(text)
		
		LogAction(-1, -1, "%i Added to downloads table: %s", y, text);
		if (StrEqual(text, "mia"))
		{
			break;
		}
	}
	
	if (g_ConVar_ReloadOnMapChange.BoolValue)
	{
		LoadConfig()
	}
}

public OnClientPutInServer(client)
{
	g_iPet[client] = 0;
	g_iPetColor[client] = 0;
	g_iPetHappiness[client] = 0;
	g_iPetType[client] = 0;
	g_szPetName[client] = "Unnamed Fella";
	g_iPetSkin[client] = 0;
	g_bPetChangeCooldown[client] = false;
	
	g_bDBEntryExist[client] = false;
	
	DB_LoadClient(client);
}

public OnClientDisconnect(client)
{
	KillPet(client);
	DB_SaveClient(client);
	
}

public Action E_SetTransmit_Pet(int ent, int client)
{
	// Check whether the entity is a pet
	if (!IsValidPet(ent))
		return Plugin_Continue;
	
	// Check whether it's the client's own pet
	if (ent == g_iPet[client])
		return Plugin_Continue;
	
	if (g_bHidePets[client])
		return Plugin_Handled;
	
	return Plugin_Continue;
}

public Action Cmd_PetStatus(client, args)
{
	if (MenuNotUsable)
	{
		return Plugin_Handled;
	}
	if (g_iPetType[client] == 0 && g_iPet[client] < 1)
	{
		PrintToChat(client, "%sYou don't have a pet! Specify what kind of pet you want before reviewing him!", CHAT_PREFIX);
		Cmd_PetsMenu(client, 0)
		return Plugin_Handled;
	}
	
	Handle panel = CreatePanel();
	
	if (strlen(g_szPetName[client]) < 2)
	{
		Format(g_szPetName[client], sizeof(g_szPetName[]), "Unnamed Fella");
	}
	
	decl String:line[64];
	Format(line, sizeof(line), "%s the %s", g_szPetName[client], petInfo[g_iPetType[client]][iName]);
	
	SetPanelTitle(panel, line);
	
	if (g_ConVar_MoodEnabled.BoolValue)
	{
		if (g_iPetHappiness[client] == 0)
		{
			Format(line, sizeof(line), "Mood: Unknown");
		}
		else
		{
			if (g_iPetHappiness[client] >= 16)
				Format(line, sizeof(line), "Mood: Supreme");
			else if (g_iPetHappiness[client] >= 8)
				Format(line, sizeof(line), "Mood: Content");
			else if (g_iPetHappiness[client] >= 4)
				Format(line, sizeof(line), "Mood: Poor");
			else
				Format(line, sizeof(line), "Mood: Terrible");
		}
		DrawPanelText(panel, line);
	}
	
	DrawPanelText(panel, " ");
	
	//	if(petEnabled[client])
	//		DrawPanelItem(panel, "Put Away");
	//	else
	//		DrawPanelItem(panel, "Send Out");
	
	DrawPanelItem(panel, "Back");
	
	SendPanelToClient(panel, client, Menu_Pet, 60);
	CloseHandle(panel);
	return Plugin_Handled;
}

public Menu_Pet(Handle menu, MenuAction:action, client, option)
{
	if (action == MenuAction_Select)
	{
		Cmd_PetsMenu(client, 0)
	}
}

public Action Event_Spawn(Handle event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (g_iPetType[client] > 0)
	{
		if (IsValidPet(g_iPet[client]))
			KillPet(client);
		SpawnPet(client);
	}
}

public Action Event_Death(Handle event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (IsValidPet(g_iPet[client]))
		KillPet(client);
}

SpawnPet(client)
{
	if (g_iPetType[client] == 0)
		return;
	
	if (g_iPetHappiness[client] == 0)
	{
		g_iPetHappiness[client] = RoundFloat(GetRandomFloat(5.00, 20.00))
	}
	decl Float:pos[3];
	GetClientAbsOrigin(client, pos);
	OffsetLocation(pos);
	
	if (!(g_iPet[client] = CreateEntityByName("prop_dynamic_override")))
		return;
	PrecacheModel(petInfo[g_iPetType[client]][iModel]);
	SetEntityModel(g_iPet[client], petInfo[g_iPetType[client]][iModel]);
	DispatchKeyValue(g_iPet[client], "targetname", "tf2_pet")
	DispatchSpawn(g_iPet[client]);
	TeleportEntity(g_iPet[client], pos, NULL_VECTOR, NULL_VECTOR);
	
	if (g_iPetSkin[client] > 0)
	{
		SetEntProp(g_iPet[client], Prop_Send, "m_nSkin", g_iPetSkin[client])
	}
	else
	{
		SetEntProp(g_iPet[client], Prop_Send, "m_nSkin", petInfo[g_iPetType[client]][SkinID]);
	}
	
	if (petInfo[g_iPetType[client]][ModelScale] > 0.00)
	{
		SetEntPropFloat(g_iPet[client], Prop_Send, "m_flModelScale", petInfo[g_iPetType[client]][ModelScale])
	}
	
	new value = RoundFloat(GetRandomFloat(1.00, float(petInfo[g_iPetType[client]][iSoundAmount])))
	switch (value)
	{
		case 1:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 2:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_2], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 3:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_3], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 4:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_4], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 5:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_5], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 6:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_6], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 7:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_7], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 8:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_8], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 9:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_9], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 10:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_10], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
	}
	EmitAmbientSound(SOUND_SPAWN, pos);
	
	SetEntityRenderMode(g_iPet[client], RENDER_TRANSADD);
	if (petInfo[g_iPetType[client]][CanBeColored] == 1)
	{
		switch (g_iPetColor[client])
		{
			case PETCOLOR_RED:SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
			case PETCOLOR_GREEN:SetEntityRenderColor(g_iPet[client], 0, 255, 0, 255);
			case PETCOLOR_BLUE:SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
			case PETCOLOR_PINK:SetEntityRenderColor(g_iPet[client], 255, 0, 255, 255);
			case PETCOLOR_ORANGE:SetEntityRenderColor(g_iPet[client], 255, 128, 0, 255);
			case PETCOLOR_CYAN:SetEntityRenderColor(g_iPet[client], 128, 255, 255, 255);
			case PETCOLOR_LIME:SetEntityRenderColor(g_iPet[client], 128, 255, 0, 255);
			case PETCOLOR_BLACK:SetEntityRenderColor(g_iPet[client], 0, 0, 0, 255);
			case PETCOLOR_TEAM:
			{
				if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
				else SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
			}
			case PETCOLOR_LTEAM:
			{
				if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 255, 128, 128, 255);
				else SetEntityRenderColor(g_iPet[client], 128, 128, 255, 255);
			}
			case PETCOLOR_DTEAM:
			{
				if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 128, 0, 0, 255);
				else SetEntityRenderColor(g_iPet[client], 0, 0, 128, 255);
			}
			case PETCOLOR_MAGIC:SetEntityRenderColor(g_iPet[client], GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, 255);
		}
	}
	if (strlen(g_szPetName[client]) < 2)
	{
		Format(g_szPetName[client], sizeof(g_szPetName[]), "Unnamed Fella");
	}
	
	if (g_ConVar_MoodEnabled.BoolValue)
	{
		if (g_iPetHappiness[client] >= 17)
		{
			PrintToChat(client, " \x06%s\x01: Hi, %N!", g_szPetName[client], client);
		}
		else if (g_iPetHappiness[client] >= 9)
		{
			PrintToChat(client, " \x06%s\x01: Hi, %N.", g_szPetName[client], client);
		}
		else if (g_iPetHappiness[client] >= 5)
		{
			PrintToChat(client, " \x06%s\x01: Hey %N, I'm kind of angry...", g_szPetName[client], client);
		}
		else if (g_iPetHappiness[client] >= 1)
		{
			PrintToChat(client, " \x06%s\x01: Arghhh...", g_szPetName[client]);
		}
	}
	else
	{
		PrintToChat(client, " \x06%s\x01: Hi, %N!", g_szPetName[client], client);
	}
	
	SDKHook(client, SDKHook_PreThink, PetThink);
	SDKHook(g_iPet[client], SDKHook_SetTransmit, E_SetTransmit_Pet);
	
	if (soundTimer[client] != INVALID_HANDLE)
	{
		KillTimer(soundTimer[client]);
		soundTimer[client] = INVALID_HANDLE;
	}
	soundTimer[client] = CreateTimer(10.0, Timer_GenericSound, client);
}

public PetThink(client)
{
	if (!IsValidPet(g_iPet[client]))
	{
		SDKUnhook(client, SDKHook_PreThink, PetThink);
		return;
	}
	
	// Get locations, angles, distances
	decl Float:pos[3], Float:ang[3], Float:clientPos[3];
	GetEntPropVector(g_iPet[client], Prop_Data, "m_vecOrigin", pos);
	GetEntPropVector(g_iPet[client], Prop_Data, "m_angRotation", ang);
	GetClientAbsOrigin(client, clientPos);
	
	new Float:dist = GetVectorDistance(clientPos, pos);
	new Float:distX = clientPos[0] - pos[0];
	new Float:distY = clientPos[1] - pos[1];
	new Float:speed = (dist - 64.0) / 54;
	Math_Clamp(speed, -4.0, 4.0);
	if (FloatAbs(speed) < 0.3)
		speed *= 0.1;
	
	// Teleport to owner if too far
	if (dist > 1024.0)
	{
		decl Float:posTmp[3];
		GetClientAbsOrigin(client, posTmp);
		OffsetLocation(posTmp);
		TeleportEntity(g_iPet[client], posTmp, NULL_VECTOR, NULL_VECTOR);
		GetEntPropVector(g_iPet[client], Prop_Data, "m_vecOrigin", pos);
	}
	
	// Set new location data	
	if (pos[0] < clientPos[0])pos[0] += speed;
	if (pos[0] > clientPos[0])pos[0] -= speed;
	if (pos[1] < clientPos[1])pos[1] += speed;
	if (pos[1] > clientPos[1])pos[1] -= speed;
	
	// Height
	switch (petInfo[g_iPetType[client]][iHeight])
	{
		case 1:pos[2] = clientPos[2]
		case 2:pos[2] = clientPos[2] + petInfo[g_iPetType[client]][iHeight_Custom];
	}
	
	// Pet states
	if (!(GetEntityFlags(client) & FL_ONGROUND))
		SetPetState(client, STATE_JUMPING);
	else if (FloatAbs(speed) > 0.2)
		SetPetState(client, STATE_WALKING);
	else
		SetPetState(client, STATE_IDLE);
	
	// Look at owner
	ang[1] = (ArcTangent2(distY, distX) * 180) / 3.14;
	
	// Finalize new location
	/*if(TR_GetPointContents(pos) == 1)
		return;*/
	
	TeleportEntity(g_iPet[client], pos, ang, NULL_VECTOR);
	
	if (g_iPetParticle[client] != 0)
	{
		pos[2] += 4.0;
		TeleportEntity(g_iPetParticle[client], pos, ang, NULL_VECTOR);
	}
}

stock Entity_GetAbsOrigin(entity, Float:vec[3]) // Thanks to SMLIB for this stock!
{
	GetEntPropVector(entity, Prop_Send, "m_vecOrigin", vec);
}

stock any:Math_Clamp(any:value, any:min, any:max) // Thanks to SMLIB for this stock!
{
	value = Math_Min(value, min);
	value = Math_Max(value, max);
	
	return value;
}

/*
Float:GetClientPos(client)
{
	decl Float:XYZ[3]
	GetEntPropVector(g_iPet[client], Prop_Send, "m_vecOrigin", XYZ);
	
	new Float:Area[3]
	Area[0] = XYZ[0]
	Area[1] = XYZ[1]
	Area[2] = -900000000000000.0
	
	Handle trace = TR_TraceRayFilterEx(XYZ, Area, MASK_NPCSOLID, RayType_EndPoint, FilterSCP, client);
	if (TR_DidHit(trace))
	{
		decl Float:Stuff[3];
		TR_GetEndPosition(Stuff, trace);
		return Stuff[2];
	}
	CloseHandle(trace);
	return XYZ[2];
}*/

public bool:FilterSCP(entity, contentsMask, any:client)
{
	if (entity == client || entity == g_iPet[client])
	{
		return false;
	}
	
	return true;
}

public Action Timer_GenericSound(Handle timer, any:client)
{
	if (!IsValidPet(g_iPet[client]))
	{
		KillTimer(timer);
		soundTimer[client] = INVALID_HANDLE;
		return Plugin_Handled;
	}
	
	decl Float:pos[3];
	GetEntPropVector(g_iPet[client], Prop_Data, "m_vecOrigin", pos);
	
	new value = RoundFloat(GetRandomFloat(1.00, float(petInfo[g_iPetType[client]][iSoundAmount])))
	switch (value)
	{
		case 1:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 2:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_2], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 3:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_3], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 4:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_4], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 5:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_5], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 6:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_6], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 7:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_7], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 8:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_8], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 9:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_9], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
		case 10:EmitAmbientSound(petInfo[g_iPetType[client]][iSoundGeneric_10], pos, g_iPet[client], _, _, 0.5, petInfo[g_iPetType[client]][iPitch]);
	}
	
	
	soundTimer[client] = CreateTimer(GetRandomFloat(20.0, 60.0), Timer_GenericSound, client);
	return Plugin_Continue;
}

SetPetState(client, status)
{
	decl Float:pos[3];
	GetEntPropVector(g_iPet[client], Prop_Data, "m_vecOrigin", pos);
	if (g_iPetState[client] == status)return;
	switch (status)
	{
		case STATE_IDLE:SetPetAnim(client, petInfo[g_iPetType[client]][iAnimIdle]);
		case STATE_WALKING:SetPetAnim(client, petInfo[g_iPetType[client]][iAnimWalk]);
		case STATE_JUMPING:
		{
			SetPetAnim(client, petInfo[g_iPetType[client]][iAnimJump]);
			EmitAmbientSound(petInfo[g_iPetType[client]][iSoundJump], pos, g_iPet[client], _, _, 0.13, petInfo[g_iPetType[client]][iPitch]);
		}
	}
	g_iPetState[client] = status;
}

SetPetAnim(client, const String:anim[])
{
	SetVariantString(anim);
	AcceptEntityInput(g_iPet[client], "SetAnimation");
}

OffsetLocation(Float:pos[3])
{
	pos[0] += GetRandomFloat(-128.0, 128.0);
	pos[1] += GetRandomFloat(-128.0, 128.0);
}

KillPet(client)
{
	if (g_iPet[client] == 0)
		return;
	
	decl Float:pos[3];
	Entity_GetAbsOrigin(g_iPet[client], pos);
	
	SDKUnhook(client, SDKHook_PreThink, PetThink);
	SDKUnhook(g_iPet[client], SDKHook_SetTransmit, E_SetTransmit_Pet);
	
	AcceptEntityInput(g_iPet[client], "Kill");
	g_iPet[client] = 0;
	
	if (g_iPetParticle[client] != 0)
	{
		AcceptEntityInput(g_iPetParticle[client], "Kill");
		g_iPetParticle[client] = 0;
	}
}

stock bool:IsValidPet(entity)
{
	if (entity > 0 && IsValidEntity(entity))
	{
		decl String:strName[16];
		GetEntPropString(entity, Prop_Data, "m_iName", strName, sizeof(strName));
		if (StrEqual(strName, "tf2_pet"))
		{
			return true;
		}
	}
	return false;
}

stock any:Math_Min(any:value, any:min) // Thanks to SMLIB for this stock!
{
	if (value < min) {
		value = min;
	}
	
	return value;
}

stock any:Math_Max(any:value, any:max) // Thanks to SMLIB for this stock!
{
	if (value > max) {
		value = max;
	}
	
	return value;
}

/*
	Database Stuff
*/
DB_LoadDatabase()
{
	char szError[128];
	
	// Connect to database
	if (SQL_CheckConfig(MYSQL_CONFIG_NAME))
	{
		g_hDB = SQL_Connect(MYSQL_CONFIG_NAME, true, szError, sizeof(szError));
	}
	
	if (g_hDB == null)
	{
		SetFailState("Couldn't retrieve database handle!");
	}
	
	// Create table
	SQL_TQuery(g_hDB, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...MYSQL_TABLE_NAME..." (`auth` VARCHAR(21) NOT NULL, `pettype` INT NULL, `petname` VARCHAR(32) NULL, `petskin` INT NULL, `petcolor` INT NULL, PRIMARY KEY (`auth`));", _, DBPrio_High);
	
}

stock bool DB_GetEscaped(char[] out, int len, const char[] def = "")
{
	if (!SQL_EscapeString(g_hDB, out, out, len))
	{
		strcopy(out, len, def);
		
		return false;
	}
	
	return true;
}

DB_LoadClient(int client)
{
	if (g_hDB == null)
	{
		return;
	}
	
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return;
	}
	
	char query[512];
	FormatEx(query, sizeof(query), "SELECT `pettype`, `petname`, `petskin`, `petcolor` FROM "...MYSQL_TABLE_NAME..." WHERE auth = '%s'", szAuth);
	
	SQL_TQuery(g_hDB, Thrd_GetClientSettings, query, GetClientUserId(client), DBPrio_Low);
	return;
}

public void Thrd_GetClientSettings(Handle db, Handle res, const char[] szError, int userId)
{
	if (res == null)
	{
		LogError("Failed database query: getting pet info for client");
		return;
	}
	
	int client;
	
	// User has disconnected
	if (!(client = GetClientOfUserId(userId)))
	{
		return;
	}
	
	while (SQL_FetchRow(res))
	{
		g_iPetType[client] = SQL_FetchInt(res, 0);
		SQL_FetchString(res, 1, g_szPetName[client], sizeof(g_szPetName[]));
		g_iPetSkin[client] = SQL_FetchInt(res, 2);
		g_iPetColor[client] = SQL_FetchInt(res, 3);
		
		// Spawn pet
		if (IsValidPet(g_iPet[client]))
			KillPet(client);
		SpawnPet(client);
		
		g_bDBEntryExist[client] = true;
	}
	
}

DB_SaveClient(int client)
{
	if (g_hDB == null)
	{
		return;
	}
	
	// Check whether they have a pet set or have cleared their pet if they previously had a entry in the database
	if (g_iPetType[client] == 0 && !g_bDBEntryExist[client])
	{
		return;
	}
	
	// Get auth
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return;
	}
	
	// Escape pet name
	
	char szPetName[64];
	szPetName = g_szPetName[client];
	
	RemoveChars(szPetName, "`'\"");
	DB_GetEscaped(szPetName, (sizeof(szPetName) * 2) + 1)
	
	// Build query
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...MYSQL_TABLE_NAME..." (auth, pettype, petname, petskin, petcolor) VALUES('%s', %i, '%s', %i, %i) ON DUPLICATE KEY UPDATE auth='%s', pettype=%i, petname='%s',petskin=%i,petcolor=%i;", szAuth, g_iPetType[client], szPetName, g_iPetSkin[client], g_iPetColor[client], szAuth, g_iPetType[client], szPetName, g_iPetSkin[client], g_iPetColor[client]);
	
	SQL_TQuery(g_hDB, Thrd_Empty, query, _, DBPrio_Normal);
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data)
{
}

/*
	Natives
*/
public Native_SetClientHidePets(Handle hPlugin, int nParams)
{
	g_bHidePets[GetNativeCell(1)] = GetNativeCell(2);
}
public Native_GetClientHidePets(Handle hPlugin, int nParams)
{
	return g_bHidePets[GetNativeCell(1)];
} 
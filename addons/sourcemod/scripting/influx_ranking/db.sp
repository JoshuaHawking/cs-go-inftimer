DB_UpdateRanks()
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	int run, mode, style;
	
	for (run = 1; run <= MAX_RUNS; run++) {
		for (mode = 0; mode < MAX_MODES; mode++) {
			for (style = 0; style < MAX_STYLES; style++) {
				if (g_bUpdateNeeded[run][mode][style])
				{
					char query[4096];
					/*
						@rank = The rank of the record
						@recordCount = the amount of recods for that map, runid, mode and style.
						@top10Reduced = The amount of points that need to be given 
					*/
					SQL_TQuery(db, Thrd_UpdateRanks, "SET @rank=0;", _, DBPrio_High);
					
					FormatEx(query, sizeof(query), "SET @recordCount = (SELECT COUNT(recordid) FROM "...INF_TABLE_TIMES..." WHERE mapid=%i AND runid=%i AND mode=%i AND style=%i);", 
						Influx_GetCurrentMapId(), 
						run, 
						mode, 
						style);
					
					SQL_TQuery(db, Thrd_UpdateRanks, query, _, DBPrio_High);
					
					
					FormatEx(query, sizeof(query), "SET @top10Reduced = (%i * ((@recordCount / %i) * %f));", 
						TOP10_POINTS, 
						TOP10_LIMIT, 
						TOP10_REDUCTION);
					
					SQL_TQuery(db, Thrd_UpdateRanks, query, _, DBPrio_High);
					
					
					FormatEx(query, sizeof(query), "SET @top10Points = IF(@top10Reduced > %i, %i, @top10Reduced);", 
						TOP10_POINTS, 
						TOP10_POINTS
						);
					
					SQL_TQuery(db, Thrd_UpdateRanks, query, _, DBPrio_High);
					
					FormatEx(query, sizeof(query), "UPDATE "...INF_TABLE_TIMES..." SET "...
						"`rank` = @rank:= (@rank+1),"...
						"`percentage` = @percentage := ((@rank / @recordCount) * 100),"...
						// Base Points
						"`points` = (%i "...
						// Top 10 Points (calculated above)
						"+ IF(`rank` <= 10, @top10Points, 0) "...
						// Percentage Points
						"+ (%i * (1 - (@rank / @recordCount))) "...
						// Tiers
						"* IF(@percentage <= %i, %f, IF(@percentage <= %i, %f, IF(@percentage <= %i, %f, IF(@percentage <= %i, %f, IF(@percentage <= %i, %f, 1)))))"...
						")"...
						"WHERE mapid = %i AND runid = %i AND mode = %i AND style = %i ORDER BY `rectime` ASC; ", 
						BASE_POINTS, 
						PERCENTAGE_POINTS, 
						TIER1_PERCENTAGE, 
						TIER1_MULTIPLIER, 
						TIER2_PERCENTAGE, 
						TIER2_MULTIPLIER, 
						TIER3_PERCENTAGE, 
						TIER3_MULTIPLIER, 
						TIER4_PERCENTAGE, 
						TIER4_MULTIPLIER, 
						TIER5_PERCENTAGE, 
						TIER5_MULTIPLIER, 
						Influx_GetCurrentMapId(), 
						run, 
						mode, 
						style);
					
					SQL_TQuery(db, Thrd_UpdateRanks, query, _, DBPrio_High);
				}
			}
		}
	}
}

public void Thrd_UpdateRanks(Handle db, Handle res, const char[] szError, any data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "updating point rankings");
		return;
	}
}

DB_GetRankedPlayers()
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	// Get count of ranked players
	char query[1024];
	query = "SELECT COUNT(*) FROM "...INF_TABLE_RANKS;
	
	SQL_TQuery(db, Thrd_UpdateRankedPlayers, query, _, DBPrio_High);
}

public void Thrd_UpdateRankedPlayers(Handle db, Handle res, const char[] szError, any data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting ranked players");
		return;
	}
	
	// Get SQL result
	while (SQL_FetchRow(res))
	{
		g_iLeaderboardPointRanked = SQL_FetchInt(res, 0);
	}
	
	return;
}

DB_GetPoints(int client)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	// Get points from times table, as well as their last connected points.
	char query[1024];
	FormatEx(query, sizeof(query), "SELECT SUM(_t.points), lastconnectedpoints, pointrank FROM "...INF_TABLE_TIMES..." AS _t INNER JOIN "...INF_TABLE_RANKS..." AS _r ON _r.uid=_t.uid WHERE _t.uid=%i", 
		Influx_GetClientId(client)
		);
	
	SQL_TQuery(db, Thrd_UpdatePoints, query, GetClientUserId(client), DBPrio_High);
}

public void Thrd_UpdatePoints(Handle db, Handle res, const char[] szError, int userid)
{
	int client = GetClientOfUserId(userid);
	
	if (!client || !IsClientInGame(client))return;
	
	if (res == null)
	{
		// No result found > insert a new entry into the ranks table.
		char query[1024];
		FormatEx(query, sizeof(query), "INSERT INTO "...INF_TABLE_RANKS..." (uid) VALUES (%i)", 
			Influx_GetClientId(client)
			);
		
		SQL_TQuery(db, Thrd_Empty, query, _, DBPrio_High);
		
		// Set their points and last seen points to zero.
		g_iRankPoints[client] = 0;
		g_iRankLCPoints[client] = 0;
		g_iLeaderboardPointRank[client] = -1;
		
		// Print message.
		Influx_PrintToChat(_, client, "%t", "POINTAMOUNT", g_iRankPoints[client], "{CHATCLR} - {LIMEGREEN}Unranked");
		
		return;
	}
	
	// Get SQL result
	while (SQL_FetchRow(res))
	{
		g_iRankPoints[client] = SQL_FetchInt(res, 0);
		g_iRankLCPoints[client] = SQL_FetchInt(res, 1);
		g_iLeaderboardPointRank[client] = SQL_FetchInt(res, 2);
	}
	
	int iPoints = g_iRankPoints[client];
	int iLastConnected = g_iRankLCPoints[client];
	
	int iPointRank = g_iLeaderboardPointRank[client];
	
	char szRanked[64];
	if (iPointRank != -1)
	{
		FormatEx(szRanked, sizeof(szRanked), "{CHATCLR} - Ranked {LIMEGREEN}%i/%i{CHATCLR}", iPointRank, g_iLeaderboardPointRanked);
	}
	else
	{
		szRanked = "{CHATCLR} - {LIMEGREEN}Unranked";
	}
	// Gained points
	if (iPoints > iLastConnected)
	{
		Influx_PrintToChat(_, client, "%t", "POINTAMOUNTGAINED", iPoints, "LIMEGREEN", "+", iPoints - iLastConnected, szRanked);
	}
	// Lost points
	else if (iPoints < iLastConnected)
	{
		Influx_PrintToChat(_, client, "%t", "POINTAMOUNTGAINED", iPoints, "LIGHTRED", "-", iLastConnected - iPoints, szRanked);
	}
	// No change
	else
	{
		Influx_PrintToChat(_, client, "%t", "POINTAMOUNT", iPoints, szRanked);
	}
	
	char szSteamID[INF_MAXSTEAMID];
	
	if (!Inf_GetClientSteam(client, szSteamID, sizeof(szSteamID)))
	{
		return;
	}
	
	// Update last connected points
	char query[1024];
	FormatEx(query, sizeof(query), "INSERT INTO "...INF_TABLE_RANKS..." (uid, lastconnectedpoints) VALUES (%i, %i) ON DUPLICATE KEY UPDATE lastconnectedpoints=%i", 
		Influx_GetClientId(client), 
		iPoints, 
		iPoints
		);
	
	SQL_TQuery(db, Thrd_Empty, query, _, DBPrio_High);
	
	return;
}

DB_GetTop100(int client)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	// Get points from times table, as well as their last connected points.
	char query[1024];
	FormatEx(query, sizeof(query), "SELECT _r.uid, pointrank, points, name FROM "...INF_TABLE_RANKS..." AS _r INNER JOIN "...INF_TABLE_USERS..." AS _u ON _r.uid=_u.uid WHERE pointrank != -1 ORDER BY pointrank LIMIT 100;");
	
	SQL_TQuery(db, Thrd_GetTop100, query, GetClientUserId(client), DBPrio_High);
}

public void Thrd_GetTop100(Handle db, Handle res, const char[] szError, int userid)
{
	int client = GetClientOfUserId(userid);
	
	if (!client || !IsClientInGame(client))return;
	
	Menu menu = new Menu(Hndlr_Top100);
	menu.SetTitle("Top 100 Players");
	
	char szName[MAX_NAME_LENGTH];
	
	char szInfo[16];
	char szTitle[64];
	
	// Get SQL result
	while (SQL_FetchRow(res))
	{
		// Build info
		FormatEx(szInfo, sizeof(szInfo), "%i", SQL_FetchInt(res, 0));
		
		SQL_FetchString(res, 3, szName, sizeof(szName));
		
		// Build title
		FormatEx(szTitle, sizeof(szTitle), "#%i - %s (%i points)", SQL_FetchInt(res, 1), szName, SQL_FetchInt(res, 2));
		menu.AddItem(szInfo, szTitle, (g_bLib_PlayerInfo ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED));
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
}
public int Hndlr_Top100(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int uid = StringToInt(szInfo);
	
	if (g_bLib_PlayerInfo)
	{
		Influx_DisplayPlayerInfo(client, uid, MODE_INVALID, STYLE_INVALID);
	}
	
	return 0;
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data)
{
} 
public Action Cmd_RankInfo(int client, int args)
{
	// Check valid client.
	if (client)
	{
		char szClientRank[128];
		char szNextRank[128];
		
		// Check if client is ranked.
		if (g_iPlayerRank[client] > 0)
		{
			// Get current rank
			Handle hRank[Rank];
			GetArrayArray(g_hRanks, g_hRanks.Length - g_iPlayerRank[client], hRank[RankNumber]);
			FormatEx(szClientRank, sizeof(szClientRank), "%s%s {CHATCLR}(%i points)", hRank[Colour], hRank[Tag], g_iRankPoints[client]);
			
			// Get next rank (if they have one)
			if (g_iPlayerRank[client] != g_hRanks.Length)
			{
				GetArrayArray(g_hRanks, g_hRanks.Length - (g_iPlayerRank[client] + 1), hRank[RankNumber]);
				FormatEx(szNextRank, sizeof(szNextRank), "%s%s {CHATCLR}(%i points)", hRank[Colour], hRank[Tag], hRank[Points]);
			}
		}
		else
		{
			// They are currently unranked
			szClientRank = "{LIMEGREEN}Unranked{CHATCLR}";
			Handle hRank[Rank];
			
			// Get next rank (if they have one)
			if (g_iPlayerRank[client] != g_hRanks.Length)
			{
				GetArrayArray(g_hRanks, g_hRanks.Length - 1, hRank[RankNumber]);
				FormatEx(szNextRank, sizeof(szNextRank), "%s%s {CHATCLR}(%i points)", hRank[Colour], hRank[Tag], hRank[Points]);
			}
		}
		
		// Print to message.
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {CHATCLR}------------- {LIMEGREEN}Rank Info {CHATCLR}-------------");
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {CHATCLR}Current Rank: %s", szClientRank);
		// Check next rank
		if (!StrEqual(szNextRank, ""))
		{
			Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {CHATCLR}Next Rank: %s", szNextRank);
		}
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, "  ");
		
	}
	
	return Plugin_Handled;
} 

public Action Cmd_PlayerRank(int client, int args)
{
	if (!IsClientInGame(client))return Plugin_Handled;
	
	if (g_iLeaderboardPointRank[client] <= 0)
	{
		Influx_PrintToChat(_, client, "%T", "PLAYERRANK_UNRANKED", LANG_SERVER);
	}
	else
	{
		Influx_PrintToChat(_, client, "%T", "PLAYERRANK", LANG_SERVER, g_iLeaderboardPointRank[client], g_iLeaderboardPointRanked);
	}
	
	return Plugin_Handled;
}

public Action Cmd_Top100(int client, int args)
{
	if (!client)return Plugin_Handled;

	if (Inf_HandleCmdSpam(client, 5.0, g_flLastTop100Time[client], true))
	{
		return Plugin_Handled;
	}
	
	DB_GetTop100(client);
	
	return Plugin_Handled;
}
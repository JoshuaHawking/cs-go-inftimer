#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <smlib>

#include <influx/core>
#include <influx/recording>
#include <influx/sync>
#include <influx/hud>

#include <msharedutil/arrayvec>
#include <msharedutil/ents>
#include <msharedutil/misc>


#undef REQUIRE_PLUGIN
#include <influx/help>
#include <influx/pause>
#include <influx/practice>
#include <influx/tas>


#include <influx/profiler>
#define DEBUG_PROFILING

//#define DEBUG_LOADRECORDINGS
//#define DEBUG_INSERTFRAME
#define DEBUG
//#define DEBUG_REPLAY


#define DEF_REPLAYBOTNAME       "!replay to view SR replays! %i"


enum
{
	SLOT_PRIMARY = 0, 
	SLOT_SECONDARY, 
	SLOT_MELEE, 
	
	SLOTS_SAVED
};


#define PLAYBACK_START      -1
#define PLAYBACK_END        -2

#define REPLAYBOT_COUNT      2
#define REPLAYBOT_QUEUE      8

#if defined DEBUG_PROFILING
	int iProfiling;
	int iProfiling2;
#endif

// ANTI-SPAM
float g_flLastReplayMenu[INF_MAXPLAYERS];


// FINISH STUFF
float g_flFinishedTime[INF_MAXPLAYERS];
int g_iFinishedRunId[INF_MAXPLAYERS];
int g_iFinishedMode[INF_MAXPLAYERS];
int g_iFinishedStyle[INF_MAXPLAYERS];

// REPLAY

int g_iReplayBots[REPLAYBOT_COUNT];
ArrayList g_hReplay[REPLAYBOT_COUNT];

int g_iReplayRunId[REPLAYBOT_COUNT];
int g_iReplayMode[REPLAYBOT_COUNT];
int g_iReplayStyle[REPLAYBOT_COUNT];

char g_szReplayName[REPLAYBOT_COUNT][MAX_BEST_NAME];
float g_flReplayTime[REPLAYBOT_COUNT];

int g_iReplayRequester[REPLAYBOT_COUNT];
bool g_bReplayedOnce[REPLAYBOT_COUNT];
bool g_bForcedReplay[REPLAYBOT_COUNT];

// NEW REPLAY
enum RecordCache
{
	ArrayList:Recording, 
	UserId, 
	Float:Time, 
	RunId, 
	Mode, 
	Style, 
	String:PlayerName[MAX_BEST_NAME]
};
Handle g_hReplayCache[MAX_RUNS + 1][MAX_MODES][MAX_STYLES];
Handle nCacheTemplate[RecordCache];

// QUEUES
enum
{
	QUEUE_REQUESTER = 0,
	
	// Record information
	QUEUE_USERID,
	QUEUE_RUNID,
	QUEUE_MODE,
	QUEUE_STYLE,
	
	QUEUE_SIZE
}
ArrayList g_hReplayQueue;

// TIMER DISPLAY
int g_iReplayRunStartTick[REPLAYBOT_COUNT];
float g_flReplayRunSync[REPLAYBOT_COUNT];

// RECORDING
ArrayList g_hRunRec;

ArrayList g_hRec[INF_MAXPLAYERS];
bool g_bIsRec[INF_MAXPLAYERS];
int g_nCurRec[INF_MAXPLAYERS];

int g_fCurButtons[INF_MAXPLAYERS];
int g_iCurWep[INF_MAXPLAYERS];
char g_szWep_Prim[INF_MAXPLAYERS][32];
char g_szWep_Sec[INF_MAXPLAYERS][32];


float g_flTeleportDistSq;

float g_flTickrate;


// CONVARS
ConVar g_ConVar_WeaponSwitch;
ConVar g_ConVar_WeaponAttack;
ConVar g_ConVar_WeaponAttack2;
ConVar g_ConVar_MaxLength;
ConVar g_ConVar_StartTime;
ConVar g_ConVar_BotName;

ConVar g_ConVar_RecordAllowanceEnabled;
ConVar g_ConVar_RecordAllowance;

bool g_bRecordAllowanceEnabled;

ConVar g_ConVar_Admin_ChangeReplayFlags;


int g_nMaxRecLength;

// FORWARDS
Handle g_hForward_OnRequestNewRecording;

// LIBRARIES
bool g_bLib_Pause;
bool g_bLib_Practice;

// TAS SPECIFIC THINGS
/*
	In order for slow motion recording to work, frames must be skipped in order for the slow motion to 
	appear real time. For example, 0.25x slowdown must skip 3 frames for every 1 frame.
*/	
int g_iSlowMotionFrames[INF_MAXPLAYERS];

#include "influx_recording/cmds.sp"
#include "influx_recording/file.sp"
#include "influx_recording/menus.sp"
#include "influx_recording/menus_hndlrs.sp"
#include "influx_recording/runcmd.sp"
#include "influx_recording/recording.sp"


public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Recording", 
	description = "Records run and then plays them for your viewing pleasure!", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	RegPluginLibrary(INFLUX_LIB_RECORDING);
	
	// NATIVES
	CreateNative("Influx_GetBotIndex", Native_GetBotIndex);
	CreateNative("Influx_GetReplayBot", Native_GetReplayBot);
	CreateNative("Influx_GetReplayRunId", Native_GetReplayRunId);
	CreateNative("Influx_GetReplayMode", Native_GetReplayMode);
	CreateNative("Influx_GetReplayStyle", Native_GetReplayStyle);
	CreateNative("Influx_GetReplayTime", Native_GetReplayTime);
	CreateNative("Influx_GetReplayCurrentSync", Native_GetReplayCurrentSync);
	CreateNative("Influx_GetReplayCurrentTime", Native_GetReplayCurrentTime);
	CreateNative("Influx_GetReplayName", Native_GetReplayName);
	
	CreateNative("Influx_HasValidRecording", Native_HasValidRecording);
	CreateNative("Influx_RequestRecording", Native_RequestRecording);
}

public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	g_hRunRec = new ArrayList(RUNREC_SIZE);
	
	RegConsoleCmd("sm_replay", Cmd_Replay);
	RegConsoleCmd("sm_playwr", Cmd_Replay);
	//RegConsoleCmd( "sm_myreplay", Cmd_MyReplay );
	//RegConsoleCmd( "sm_test_replay", Cmd_Test_Replay );
	
	
	// CONVARS
	g_ConVar_WeaponSwitch = CreateConVar("influx_recording_wepswitching", "1", "Do weapon switching on replay.", FCVAR_NOTIFY);
	g_ConVar_WeaponAttack = CreateConVar("influx_recording_attack", "1", "Do weapon shooting on replay.", FCVAR_NOTIFY);
	g_ConVar_WeaponAttack2 = CreateConVar("influx_recording_attack2", "1", "Do right click on replay.", FCVAR_NOTIFY);
	
	g_ConVar_MaxLength = CreateConVar("influx_recording_maxlength", "90", "Max recording length in minutes.", FCVAR_NOTIFY);
	g_ConVar_MaxLength.AddChangeHook(E_CvarChange_MaxLength);
	
	g_ConVar_BotName = CreateConVar("influx_recording_bostname", DEF_REPLAYBOTNAME, "Replay bot's name.", FCVAR_NOTIFY);
	g_ConVar_BotName.AddChangeHook(E_CvarChange_BotName);
	
	g_ConVar_StartTime = CreateConVar("influx_recording_startwait", "1.5", "How long we wait at the start before starting playback.", FCVAR_NOTIFY);
	
	g_ConVar_RecordAllowanceEnabled = CreateConVar("influx_recording_allowance_enabled", "0", "Whether replays that are not the server record can be recorded (within a time allowance)", FCVAR_NOTIFY, true, 0.0, true, 1.0);
	g_ConVar_RecordAllowanceEnabled.AddChangeHook(E_CvarChange_RecordAllowanceEnabled);
	
	g_ConVar_RecordAllowance = CreateConVar("influx_recording_allowance", "1.5", "How close must replays be to the SR to be saved? (50% longer default)", FCVAR_NOTIFY, true, 1.0, true, 5.0);
	
	g_ConVar_Admin_ChangeReplayFlags = CreateConVar("influx_recording_changereplayflags", "z", "Required flags to change replay without limits.");
	
	AutoExecConfig(true, "recording", "influx");
	
	g_hForward_OnRequestNewRecording = CreateGlobalForward("Influx_OnRequestNewRecording", ET_Hook, Param_Cell, Param_CellByRef);
	
	// LIBRARIES
	g_bLib_Pause = LibraryExists(INFLUX_LIB_PAUSE);
	g_bLib_Practice = LibraryExists(INFLUX_LIB_PRACTICE);
	
	
	// EVENTS
	HookEvent("player_spawn", E_PlayerSpawn);
	// Remove replay change name
	HookUserMessage(GetUserMessageId("SayText2"), SayText2, true);
	
	
	// Round it just in case.
	g_flTickrate = float(RoundFloat(1.0 / GetTickInterval()));
	
	SetTeleDistance(3500.0, g_flTickrate);
	SetMaxRecordingLength(g_flTickrate);
	
	#if defined DEBUG_PROFILING
		iProfiling = Influx_RegisterProfiler("influx_recording SaveRecording");
		iProfiling2 = Influx_RegisterProfiler("influx_recording AddToDynamic");
	#endif

	g_hReplayQueue = new ArrayList(QUEUE_SIZE);
	// Reset cache and queue
	ResetCache();
}

ResetCache() {
	// Init cache
	int style, run, mode;
	
	// Create all arrays
	for (run = 1; run <= MAX_RUNS; run++)
	{
		for (mode = 0; mode < MAX_MODES; mode++)
		{
			for (style = 0; style < MAX_STYLES; style++)
			{
				if (g_hReplayCache[run][mode][style] != INVALID_HANDLE)
				{
					ClearArray(g_hReplayCache[run][mode][style]);
				}
				else
				{
					// Set new array as no handle currently exists.
					g_hReplayCache[run][mode][style] = CreateArray(sizeof(nCacheTemplate));
				}
			}
		}
	}
	
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = true;
	if (StrEqual(lib, INFLUX_LIB_PRACTICE))g_bLib_Practice = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = false;
	if (StrEqual(lib, INFLUX_LIB_PRACTICE))g_bLib_Practice = false;
}

public void Influx_RequestHelpCmds()
{
	Influx_AddHelpCommand("replay", "Open replay menu.");
}

public void Influx_OnRequestResultFlags()
{
	Influx_AddResultFlag("Don't save recording file", RES_RECORDING_DONTSAVE);
}

public void Influx_OnPostRunLoad()
{
	// Update our run array.
	g_hRunRec.Clear();
	
	ArrayList runs = Influx_GetRunsArray();
	
	int data[RUNREC_SIZE];
	int len = GetArrayLength_Safe(runs);
	for (int i = 0; i < len; i++)
	{
		data[RUNREC_RUN_ID] = runs.Get(i, RUN_ID);
		g_hRunRec.PushArray(data);
	}
	
	LoadAllRecordings();
}

public void Influx_OnRunCreated(int runid)
{
	int data[RUNREC_SIZE];
	data[RUNREC_RUN_ID] = runid;
	
	g_hRunRec.PushArray(data);
}

public void OnMapEnd()
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		g_iReplayBots[i] = -1;
		g_hReplay[i] = null;
	}
	
	// Null replay
	for (int i = 0; i < INF_MAXPLAYERS; i++)
	{
		delete g_hRec[i];
	}
	
	int style, run, mode;
	// Clear all arrays
	for (run = 1; run <= MAX_RUNS; run++)
	{
		for (mode = 0; mode < MAX_MODES; mode++)
		{
			for (style = 0; style < MAX_STYLES; style++)
			{
				if (g_hReplayCache[run][mode][style] != INVALID_HANDLE)
				{
					ClearArray(g_hReplayCache[run][mode][style]);
				}
			}
		}
	}
}

/*
	Configs & CVars
*/
public void OnConfigsExecuted()
{
	ConVar cvar;
	
	
	// Bot cvars
	cvar = FindConVar("bot_stop");
	if (cvar != null)
	{
		cvar.SetBool(true);
		delete cvar;
	}
	
	cvar = FindConVar("bot_quota_mode");
	if (cvar != null)
	{
		cvar.SetString("normal");
		delete cvar;
	}
	
	cvar = FindConVar("bot_join_after_player");
	if (cvar != null)
	{
		cvar.SetBool(false);
		delete cvar;
	}
	
	cvar = FindConVar("bot_chatter");
	if (cvar != null)
	{
		cvar.SetString("off");
		delete cvar;
	}
	
	cvar = FindConVar("bot_join_team");
	if (cvar != null)
	{
		cvar.SetString("any");
		delete cvar;
	}
	
	cvar = FindConVar("bot_quota");
	if (cvar != null)
	{
		cvar.SetInt(REPLAYBOT_COUNT);
		delete cvar;
	}
	
	
	// Playback stuff
	float maxvel = 3500.0;
	
	cvar = FindConVar("sv_maxvelocity");
	if (cvar != null)
	{
		maxvel = cvar.FloatValue;
		delete cvar;
	}
	
	SetTeleDistance(maxvel, g_flTickrate);
	
	SetMaxRecordingLength(g_flTickrate);
}

// Determine our maximum position difference.
// If our playback's previous position and current position distance is larger than this (squared) we teleport the bot.
// And no, it is not the sv_maxvelocity value. That is only for per axis.
// Only map that you can even come close to this in is bhop_forest_trials where you can use the infinity room.
stock void SetTeleDistance(float maxvel, float tickrate)
{
	float maxtickspd = SquareRoot(maxvel * maxvel + maxvel * maxvel) * (1.0 / tickrate);
	
	g_flTeleportDistSq = maxtickspd * maxtickspd;
	
	#if defined DEBUG
	PrintToServer(INF_DEBUG_PRE..."Max dist per tick: %.1f (sv_maxvelocity: %.0f | tickrate: %.0f)", maxtickspd, maxvel, tickrate);
	#endif
}

public void E_CvarChange_MaxLength(ConVar convar, const char[] oldval, const char[] newval)
{
	SetMaxRecordingLength(g_flTickrate);
}

public void E_CvarChange_BotName(ConVar convar, const char[] oldval, const char[] newval)
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		SetBotName(i);
	}
}

public void E_CvarChange_RecordAllowanceEnabled(ConVar convar, const char[] oldval, const char[] newval)
{
	g_bRecordAllowanceEnabled = GetConVarBool(convar);
}

stock void SetMaxRecordingLength(float tickrate)
{
	g_nMaxRecLength = g_ConVar_MaxLength.IntValue * 60 * RoundFloat(tickrate);
}

/*
	Events
*/
public void E_PlayerSpawn(Event event, const char[] szEvent, bool bImUselessWhyDoIExist)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	if (!client)return;
	
	if (GetClientTeam(client) <= CS_TEAM_SPECTATOR || !IsPlayerAlive(client))return;
	
	
	if (IsFakeClient(client))
	{
		RequestFrame(E_PlayerSpawn_Delay, GetClientUserId(client));
	}
}

public void E_PlayerSpawn_Delay(int client)
{
	if (!(client = GetClientOfUserId(client)))return;
	
	if (!IsPlayerAlive(client))return;
	
	
	SetEntityCollisionGroup(client, 1); // Disable collisions with players and triggers.
	
	//SetEntityGravity( client, 0.0 );
	
	SetEntityMoveType(client, MOVETYPE_NOCLIP);
	
	SetEntProp(client, Prop_Data, "m_takedamage", 0);
}

// Check for change name
public Action SayText2(UserMsg:msg_id, Handle bf, players[], playersNum, bool reliable, bool init)
{
	if (!reliable)
	{
		return Plugin_Continue;
	}
	
	char buffer[25];
	
	if (GetUserMessageType() == UM_Protobuf) // CSGO
	{
		PbReadString(bf, "msg_name", buffer, sizeof(buffer));
		
		if (StrEqual(buffer, "#Cstrike_Name_Change"))
		{
			int client = PbReadInt(bf, "ent_idx");
			// If fake client, block change.
			if (IsFakeClient(client))
			{
				return Plugin_Handled;
			}
			return Plugin_Continue;
		}
	}
	return Plugin_Continue;
}

/*
	Replay Bot
*/
// Set bot name
stock void SetBotName(int botIndex)
{
	if (!IsValidReplayBot(botIndex))return;
	
	char szName[MAX_NAME_LENGTH];
	g_ConVar_BotName.GetString(szName, sizeof(szName));
	
	if (szName[0] == '\0')
	{
		strcopy(szName, sizeof(szName), DEF_REPLAYBOTNAME);
	}
		
	FormatEx(szName, sizeof(szName), szName, botIndex + 1);
	SetClientInfo(g_iReplayBots[botIndex], "name", szName);
}
// Get a bot index that is not currently playing a replay.
stock int GetInactiveBotIndex()
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		if (g_hReplay[i] == null)
		{
			return i;
		}
	}
	return -1;
}
// Check whether any of the bots are currently playing a replay.
stock bool CheckBotsRecording(ArrayList rec)
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		if (g_hReplay[i] == rec)
		{
			return true;
		}
	}
	return false;
}
// Check whether a certain bot index is valid (alive and everything like that)
// If no bot index is passed: Check whether all replay bots are valid.
stock bool IsValidReplayBot(int botIndex = -1)
{
	// No bot index specified to check
	if (botIndex == -1)
	{
		for (int i = 0; i < REPLAYBOT_COUNT; i++)
		{
			if (!IS_ENT_PLAYER(g_iReplayBots[i]) || !IsClientInGame(g_iReplayBots[i]) || !IsFakeClient(g_iReplayBots[i]))
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		if (!IS_ENT_PLAYER(g_iReplayBots[botIndex]) || !IsClientInGame(g_iReplayBots[botIndex]) || !IsFakeClient(g_iReplayBots[botIndex]))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}
// If the passed client is a bot, the index of the bot will be returned.
stock int GetBotIndex(int client)
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		if (g_iReplayBots[i] == client)
		{
			return i;
		}
	}
	return -1;
}
// Get a index for a new bot. 
stock int GetNewBotIndex()
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		if (!IS_ENT_PLAYER(g_iReplayBots[i]) || !IsClientInGame(g_iReplayBots[i]) || !IsFakeClient(g_iReplayBots[i]))
		{
			return i;
		}
	}
	return -1;
}
// Set the passed client to become a bot.
stock void SetReplayBot(int bot)
{
	int botIndex = GetNewBotIndex();
	
	g_iReplayBots[botIndex] = bot;
	
	g_iReplayRunId[botIndex] = -1;
	g_iReplayMode[botIndex] = MODE_INVALID;
	g_iReplayStyle[botIndex] = STYLE_INVALID;
	g_flReplayTime[botIndex] = INVALID_RUN_TIME;
	g_szReplayName[botIndex][0] = '\0';
	
	g_bForcedReplay[botIndex] = false;
	g_iReplayRequester[botIndex] = -1;
	
	
	SetBotName(botIndex);
	
	CS_SetClientClanTag(bot, "NONE");
	
	CreateTimer(5.0, Timer_SetColour, bot, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_SetColour(Handle timer, int bot)
{
	SetEntityRenderColor(bot, 255, 0, 0, 255);
}

/*
	Observing 
*/
stock bool IsObservingTarget(int client, int target)
{
	return (!IsPlayerAlive(client) && GetClientObserverTarget(client) == target && GetClientObserverMode(client) != OBS_MODE_ROAMING);
}

stock bool ObserveTarget(int client, int target)
{
	// Can't spectate a dead player!
	if (!IsPlayerAlive(target))return false;
	
	
	if (IsPlayerAlive(client))
	{
		ChangeClientTeam(client, CS_TEAM_SPECTATOR);
	}
	else if (GetClientObserverTarget(client) == target)
	{
		return true; // We're already spectating the target!
	}
	
	
	SetClientObserverMode(client, OBS_MODE_IN_EYE);
	SetClientObserverTarget(client, target);
	
	return true;
}

/*
	Client & Timer Functions
*/
public void OnClientPutInServer(int client)
{
	g_bIsRec[client] = false;
	
	
	ArrayList rec = g_hRec[client];
	if (rec != null)DeleteRecording(rec);
	g_hRec[client] = null;
	
	if (!IsFakeClient(client))
	{
		Inf_SDKHook(client, SDKHook_PostThinkPost, E_PostThinkPost_Client);
	}
	else
	{
		int boxIndex = GetBotIndex(client);
		if (boxIndex == -1)
		{
			SetReplayBot(client);
		}
	}
	
	g_flLastReplayMenu[client] = 0.0;
	g_iSlowMotionFrames[client] = 0;
}

stock void OnClientDisconnect(int client)
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		if (client == g_iReplayRequester[i])
		{
			g_iReplayRequester[i] = -1;
		}
		if (client == g_iReplayBots[i])
		{
			g_iReplayBots[i] = -1;
		}
	}
}

public void Influx_OnTimerResetPost(int client)
{
	g_bIsRec[client] = false;
}

public void Influx_OnTimerStartPost(int client, int runid)
{
	if (g_bLib_Practice && Influx_IsClientPractising(client))return;
	
	
	StartRecording(client, true);
	
	// Cache "finish" stuff here if we don't get to the end.
	g_flFinishedTime[client] = INVALID_RUN_TIME;
	
	g_iFinishedRunId[client] = runid;
	g_iFinishedMode[client] = Influx_GetClientMode(client);
	g_iFinishedStyle[client] = Influx_GetClientStyle(client);
}

public Action Influx_OnTimerFinish(int client)
{
	/*
		Get an overriden version if there is one avaliable.
	*/
	ArrayList recording;
	recording = new ArrayList(REC_SIZE);
	
	Call_StartForward(g_hForward_OnRequestNewRecording);
	Call_PushCell(client);
	Call_PushCellRef(recording);
	
	Action iResults;
	int error = Call_Finish(iResults);

	if (error != SP_ERROR_NONE)
	{
		ThrowNativeError(error, "Forward has failed to fire.");
		CloseHandle(recording);
		return Plugin_Continue;
	}
	
	if(iResults == Plugin_Changed)
	{
		g_hRec[client] = recording;
	}
	
	return Plugin_Continue;
}

public void Influx_OnTimerFinishPost(int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags)
{
	if (!g_bIsRec[client])return;
	
	g_flFinishedTime[client] = time;
	
	g_iFinishedRunId[client] = runid;
	g_iFinishedMode[client] = mode;
	g_iFinishedStyle[client] = style;
	
	FinishRecording(client, true);
	
	// Check if valid time
	float flTimeAllowance = (g_ConVar_RecordAllowance.FloatValue * prev_best);
	bool bTimeValid = (flTimeAllowance >= time) ? true : false;
	bool bIsPB = (flags & (RES_TIME_PB)) ? true : false;
	bool bIsFirstOwnRecord = (flags & RES_TIME_FIRSTOWNREC) ? true : false;
	
	bool bIsFirstRecord = (flags & (RES_TIME_FIRSTREC)) ? true : false;
	bool bIsBest = (flags & RES_TIME_ISBEST) ? true : false;
	
	bool bReplayCreated = false;
	
	// Check for convar and whether the replay is valid.
	if ((g_bRecordAllowanceEnabled && bTimeValid && (bIsPB || bIsFirstOwnRecord)) || bIsFirstRecord || bIsBest)
	{
		/*
			Remove placeholder frames for TAS
		
		if(style == STYLE_TAS || true)
		{
			ArrayList recCopy;
			decl data[REC_SIZE];
			int offset = 0;
			recCopy = g_hRec[client].Clone();
			PrintToChatAll("Length: %i", recCopy.Length);
			for (int i = 0; i < recCopy.Length; i++)
			{
				recCopy.GetArray(i, data);
				
				PrintToChatAll("Found plcaceholder frames? Offset: %i", offset);
				if(data[REC_POS] == 0.0 && data[REC_POS + 1] == 0.0 && data[REC_POS + 2] == 0.0)
				{
					PrintToChatAll("Found placeholder, removing.");
					RemoveFromArray(g_hRec[client], i - offset);
					offset++;
				}
			}
		}
		*/
		// Make sure this run allows us to save the recording.
		if (!(flags & RES_RECORDING_DONTSAVE))
		{
			#if defined DEBUG_PROFILING
				Influx_StartProfiling(iProfiling);
			#endif
			
			if (SaveRecording(client, g_hRec[client], runid, mode, style, time))
			{
				Influx_PrintToChat(_, client, "%t", "REPLAYSAVED");
			}
			else
			{
				Influx_PrintToChat(_, client, "%t", "REPLAYSAVEFAILED");
			}
			#if defined DEBUG_PROFILING
				Influx_EndProfiling(iProfiling);
			#endif
		}
		
		#if defined DEBUG_PROFILING
			Influx_StartProfiling(iProfiling2);
		#endif
			
		int userid = Influx_GetClientId(client);
		ArrayList rec;
		GetRunRec(userid, runid, mode, style, rec);
		
		
		if (rec != null)
		{
			DeleteFromCache(userid, runid, mode, style);
		}
		
		decl String:szName[32];
		GetClientName(client, szName, sizeof(szName));
		
		rec = g_hRec[client].Clone();
		
		Handle nNewCache[RecordCache];
		
		nNewCache[Recording] = rec;
		nNewCache[UserId] = userid;
		nNewCache[Time] = time;
		nNewCache[RunId] = runid;
		nNewCache[Mode] = mode;
		nNewCache[Style] = style;
		GetClientName(client, nNewCache[PlayerName], sizeof(nNewCache[PlayerName]));
		
		// Push cache array
		PushArrayArray(g_hReplayCache[runid][mode][style], nNewCache[0]);
		
		bReplayCreated = true;
		#if defined DEBUG_PROFILING
			Influx_EndProfiling(iProfiling2);
		#endif
	}
	// just the time wasn't valid
	else if (g_bRecordAllowanceEnabled && !bTimeValid && (bIsPB || bIsFirstOwnRecord))
	{
		Influx_PrintToChat(_, client, "%t", "REPLAYSAVETIME", time - flTimeAllowance);
	}
	
	// If g_bRecordAllowanceEnabled is false, and someone gets a new time, we need to delete their old replay as it won't reflect their new time.
	if(!g_bRecordAllowanceEnabled && (bIsPB || bIsFirstOwnRecord) && !bReplayCreated)
	{
		int userid = Influx_GetClientId(client);
		ArrayList rec;
		GetRunRec(userid, runid, mode, style, rec);
		
		
		if (rec != null)
		{
			DeleteFromCache(userid, runid, mode, style);
		}
	}
	
}

/*
	Other functions
*/

// Malicious ucmd angles will crash the server.
stock void FixAngles(float &pitch, float &yaw)
{
	if (pitch > 90.0)
	{
		pitch = 90.0;
	}
	else if (pitch < -90.0)
	{
		pitch = -90.0;
	}
	
	if (yaw > 180.0)
	{
		yaw = 180.0;
	}
	else if (yaw < -180.0)
	{
		yaw = -180.0;
	}
}

stock bool CanUserChangeReplay(int client)
{
	if (client == 0)return true;
	
	
	char szFlags[32];
	g_ConVar_Admin_ChangeReplayFlags.GetString(szFlags, sizeof(szFlags));
	
	int wantedflags = ReadFlagString(szFlags);
	
	return ((GetUserFlagBits(client) & wantedflags) == wantedflags);
}

// Replay cache functions
stock Handle GetReplayCache(int userid, int runid, int mode, int style)
{
	for (int i = 0; i < GetArraySize(g_hReplayCache[runid][mode][style]); i++)
	{
		Handle nCache[RecordCache];
		GetArrayArray(g_hReplayCache[runid][mode][style], i, nCache[0]);
		
		if (nCache[UserId] == userid)
			return nCache;
	}
	
	Handle empty[RecordCache];
	empty[Recording] = null; // Just to make sure;
	return empty;
}

stock GetRunRec(int userid, int runid, int mode, int style, ArrayList &rec)
{
	for (int i = 0; i < GetArraySize(g_hReplayCache[runid][mode][style]); i++)
	{
		Handle nCache[RecordCache];
		GetArrayArray(g_hReplayCache[runid][mode][style], i, nCache[0]);
		
		if (nCache[UserId] == userid)
		{
			rec = nCache[Recording];
			return;
		}
	}
	
	return;
}

stock DeleteFromCache(int userid, int runid, int mode, int style)
{
	for (int i = 0; i < GetArraySize(g_hReplayCache[runid][mode][style]); i++)
	{
		Handle nCache[RecordCache];
		GetArrayArray(g_hReplayCache[runid][mode][style], i, nCache[0]);
		
		if (nCache[UserId] == userid)
		{
			DeleteRecording(nCache[Recording]);
			RemoveFromArray(g_hReplayCache[runid][mode][style], i);
			return;
		}
	}
	return;
}
// NATIVES
public int Native_GetBotIndex(Handle hPlugin, int nParms) {
	return GetBotIndex(GetNativeCell(1));
}
public int Native_GetReplayBot(Handle hPlugin, int nParms) {
	return g_iReplayBots[GetNativeCell(1)];
}
public int Native_GetReplayRunId(Handle hPlugin, int nParms) { return g_iReplayRunId[GetNativeCell(1)]; }
public int Native_GetReplayMode(Handle hPlugin, int nParms) { return g_iReplayMode[GetNativeCell(1)]; }
public int Native_GetReplayStyle(Handle hPlugin, int nParms) { return g_iReplayStyle[GetNativeCell(1)]; }
public int Native_GetReplayTime(Handle hPlugin, int nParms) { return view_as<int>(g_flReplayTime[GetNativeCell(1)]); }
public int Native_GetReplayCurrentSync(Handle hPlugin, int nParms) { return view_as<int>(g_flReplayRunSync[GetNativeCell(1)]); }
public int Native_GetReplayCurrentTime(Handle hPlugin, int nParms) {
	if (g_iReplayRunStartTick[GetNativeCell(1)] == -1) { return view_as<int>(g_flReplayTime[GetNativeCell(1)]); }
	return view_as<int>(TickCountToTime(GetGameTickCount() - g_iReplayRunStartTick[GetNativeCell(1)]));
}
public int Native_GetReplayName(Handle hPlugin, int nParms)
{
	SetNativeString(2, g_szReplayName[GetNativeCell(1)], GetNativeCell(3), true);
	
	return 1;
}
public Native_HasValidRecording(Handle hPlugin, int nParms)
{
	// UserID
	// RunID
	// Mode
	// Style
	ArrayList rec;
	GetRunRec(GetNativeCell(1), GetNativeCell(2), GetNativeCell(3), GetNativeCell(4), rec);
	return rec != null;
}
public Native_RequestRecording(Handle hPlugin, int nParms)
{
	// Requester
	// UserID
	// RunID
	// Mode
	// Style
	return RequestRecording(GetNativeCell(1), GetNativeCell(2), GetNativeCell(3), GetNativeCell(4), GetNativeCell(5));
} 
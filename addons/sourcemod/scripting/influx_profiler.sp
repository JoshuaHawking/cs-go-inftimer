#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <profiler>
#include <influx/profiler>

public Plugin myinfo = 
{
	name = "CS:GO plugin profiler", 
	author = "Eddy", 
	description = "profiler plugin", 
	version = "1", 
	url = "http://a.b.c"
}

#define MAX_PROFILERS 100				// Maximum number of profiling log points
#define MAX_CONCURENT_PROFILERS 10		// Maximum number of concurent profilers (nested)
#define MAX_PROFILER_NAME 256			// Maximum length of a profiler's name	

Handle g_Prof[MAX_CONCURENT_PROFILERS];
char g_ProfName[MAX_PROFILERS][MAX_PROFILER_NAME];
float g_ProfTotal[MAX_PROFILERS];
float g_ProfMax[MAX_PROFILERS];
float g_ProfMin[MAX_PROFILERS];
int g_ProfCalls[MAX_PROFILERS];
int g_ProfConIndex[MAX_PROFILERS];
int g_ProfIndex = 0;
int g_ProfNr = 0;

int profiler1ID;
bool startedCounting;

public ProfilerRegister(char name[MAX_PROFILER_NAME])
{
	
	if (g_ProfIndex >= MAX_PROFILERS)
	{
		LogMessage("Maximum number of profilers exceeded please increase MAX_PROFILERS");
	}
	
	g_ProfName[g_ProfIndex] = name;
	g_ProfTotal[g_ProfIndex] = 0.0;
	g_ProfMax[g_ProfIndex] = 0.0;
	g_ProfMin[g_ProfIndex] = 10000000.0;
	g_ProfCalls[g_ProfIndex] = 0;
	g_ProfIndex++;
	return g_ProfIndex - 1;
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, err_max)
{
	RegPluginLibrary(INFLUX_LIB_PROFILER);
	
	CreateNative("Influx_RegisterProfiler", Native_RegisterProfiler);
	CreateNative("Influx_StartProfiling", Native_StartProfiling);
	CreateNative("Influx_EndProfiling", Native_EndProfiling);
	
	return APLRes_Success;
}

public Native_RegisterProfiler(Handle plugin, numParams)
{
	char map[MAX_PROFILER_NAME];
	GetNativeString(1, map, sizeof(map));
	return ProfilerRegister(map);
}

public Native_StartProfiling(Handle plugin, numParams)
{
	ProfilerStart(GetNativeCell(1));
}

public Native_EndProfiling(Handle plugin, numParams)
{
	ProfilerEnd(GetNativeCell(1));
}

public Action CMDInfo(client, args)
{
	PrintToConsole(client, "commands:");
	PrintToConsole(client, "sm_profiler : show profiler log");
	PrintToConsole(client, "sm_profiler_clear : set all counters to zero");
	PrintToConsole(client, "log: total nr of profilers %i concurrent %i", g_ProfIndex, g_ProfNr);
	ProfilerLogData(client);
}

public Action CMDClear(client, args)
{
	ProfilerClearData();
}

public ProfilerStart(profileId)
{
	if (g_ProfNr >= MAX_CONCURENT_PROFILERS) {
		LogMessage("Profiling exceeded max number of nested profilers");
		return;
	}
	StartProfiling(g_Prof[g_ProfNr]);
	g_ProfConIndex[profileId] = g_ProfNr;
	g_ProfNr++;
}

public ProfilerEnd(profileId)
{
	int currProfIndex = g_ProfConIndex[profileId];
	StopProfiling(g_Prof[currProfIndex]);
	float timeSpend = GetProfilerTime(g_Prof[currProfIndex]);
	
	//LogMessage("End profiling %i time spend %f", profileId, timeSpend);
	
	g_ProfTotal[profileId] = g_ProfTotal[profileId] + timeSpend;
	g_ProfCalls[profileId] = g_ProfCalls[profileId] + 1;
	
	if (timeSpend > g_ProfMax[profileId])
		g_ProfMax[profileId] = timeSpend;
	if (timeSpend < g_ProfMin[profileId])
		g_ProfMin[profileId] = timeSpend;
	
	g_ProfNr--;
}

public ProfilerClearData()
{
	for (int i = 0; i < g_ProfIndex; i++)
	{
		g_ProfTotal[i] = 0.0;
		g_ProfMax[i] = 0.0;
		g_ProfMin[i] = 10000000.0;
		g_ProfCalls[i] = 0;
	}
}

public ProfilerLogData(client)
{
	PrintToConsole(client, "name                           calls    total        avg         min        max   variance ");
	
	float avg = 0.0;
	float varianceLow = 0.0;
	float varianceHigh = 0.0;
	float variance = 0.0;
	float total;
	int calls;
	for (int i = 0; i < g_ProfIndex; i++)
	{
		total = g_ProfTotal[i];
		calls = g_ProfCalls[i];
		if (calls > 0) {
			avg = (total / calls);
			varianceLow = (g_ProfMax[i] - avg) / avg * 100.0;
			varianceHigh = (g_ProfMax[i] - avg) / avg * 100.0;
			if (varianceHigh > varianceLow)
				variance = varianceHigh;
			else
				variance = varianceLow;
			PrintToConsole(client, "%25s %10i %8.4f %4.8f %4.8f %4.8f %8.2f", g_ProfName[i], calls, total, avg, g_ProfMin[i], g_ProfMax[i], variance);
		} else {
			PrintToConsole(client, "%25s not used", g_ProfName[i]);
		}
	}
}

public OnGameFrame()
{
	if (!startedCounting)
		startedCounting = true;
	else
		ProfilerEnd(profiler1ID);
	
	ProfilerStart(profiler1ID);
}

public OnPluginStart()
{
	RegAdminCmd("sm_profiler", CMDInfo, ADMFLAG_GENERIC, "");
	RegAdminCmd("sm_profiler_clear", CMDClear, ADMFLAG_GENERIC, "");
	
	for (int i = 0; i < MAX_CONCURENT_PROFILERS; i++)
	{
		g_Prof[i] = CreateProfiler();
	}
	
	startedCounting = false;
	profiler1ID = ProfilerRegister("Total time");
}

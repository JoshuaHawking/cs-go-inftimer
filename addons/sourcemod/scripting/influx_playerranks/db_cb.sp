public void Thrd_LoadPlayerRank(Handle db, Handle res, const char[] szError, int uid)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting player rank", GetClientOfUserId(uid), "Couldn't retrieve your player rank!");
	}
	
	int client;
	if ((client = GetClientOfUserId(uid)) == 0)return;
	
	char szSteamID[INF_MAXSTEAMID];
	
	if (!Inf_GetClientSteam(client, szSteamID, sizeof(szSteamID)))
	{
		return;
	}
	
	if (SQL_GetRowCount(res) == 0)
	{
		decl String:szQuery[128];
		FormatEx(szQuery, sizeof(szQuery), "INSERT INTO "...INF_TABLE_RANKS..." (steamid, jumps, prestige) VALUES ('%s', 0, 0)", szSteamID);
		
		SQL_LockDatabase(db);
		
		if (!SQL_FastQuery(db, szQuery))
		{
			SQL_UnlockDatabase(db);
			
			Inf_DB_LogError(db, "entering new entry into player ranks", client, "Couldn't create a ranks entry!");
			return;
		}
		
		SQL_UnlockDatabase(db);
		
		// Reget player rank.
		DB_UpdatePlayerRank(client);
		return;
	}
	
	while (SQL_FetchRow(res))
	{
		g_iPlayerRanks[client] = SQL_FetchInt(res, 0);
		g_iCachedPlayerJumps[client] = SQL_FetchInt(res, 1);
	}
	return;
}
public void Thrd_UpdateRegisteredClients(Handle db, Handle res, const char[] szError, any data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting registered clients");
		return;
	}
	
	int clients;
	while (SQL_FetchRow(res))
	{
		SQL_FieldNameToNum(res, "registeredClients", clients);
		g_iPlayerRegistered = SQL_FetchInt(res, clients);
	}
}
public void Thrd_Update(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "updating player's record with rank points");
	}
}

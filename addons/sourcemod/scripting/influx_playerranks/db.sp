public void DB_UpdatePlayerRank( int client )
{
    Handle db = Influx_GetDB();
    if ( db == null ) SetFailState( INF_CON_PRE..."Couldn't retrieve database handle!" );
    
    char szSteamID[INF_MAXSTEAMID];

    if ( !Inf_GetClientSteam(client, szSteamID, sizeof(szSteamID) ) )
    {
        return;
    }
    
    decl String:szQuery[300];
    FormatEx( szQuery, sizeof( szQuery ), "SELECT x.position, x.jumps FROM (SELECT t.steamid, t.jumps, @rownum := @rownum + 1 AS position FROM "...INF_TABLE_RANKS..." t JOIN (SELECT @rownum := 0) r ORDER BY `prestige` DESC, `jumps` DESC) x WHERE x.steamid = '%s'", szSteamID);
    
    SQL_TQuery( db, Thrd_LoadPlayerRank, szQuery, GetClientUserId(client), DBPrio_High );    
}

public void DB_UpdateRegisteredClients() {
    Handle db = Influx_GetDB();
    if ( db == null ) SetFailState( INF_CON_PRE..."Couldn't retrieve database handle!" );
    
    // Count amount of players in rank table
    char szQuery[49];
    szQuery = "SELECT COUNT(*) as registeredClients FROM "...INF_TABLE_RANKS;
    
    SQL_TQuery( db, Thrd_UpdateRegisteredClients, szQuery, _, DBPrio_High );
}


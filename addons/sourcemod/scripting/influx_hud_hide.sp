
#include <influx/core>
#include <influx/hud>

#undef REQUIRE_PLUGIN
#include <influx/help>
#include <pets>

bool g_bLib_Pets;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - HUD | Hide Command", 
	description = "Displays the hide menu.", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_hide", Cmd_Hide, "Display hide settings.");
	RegConsoleCmd("sm_hideplayers", Cmd_Hide);
	RegConsoleCmd("sm_hidebots", Cmd_Hide);
	
	g_bLib_Pets = LibraryExists(LIB_PETS);
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, LIB_PETS))g_bLib_Pets = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, LIB_PETS))g_bLib_Pets = false;
}

public void Influx_OnRequestHUDMenuCmds()
{
	Influx_AddHUDMenuCmd("sm_hide", "Hide Players/Bots");
}

public void Influx_RequestHelpCmds()
{
	Influx_AddHelpCommand("hide", "Hide players, bots or viewmodel.");
}


public Action Cmd_Hide(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	int hideflags = Influx_GetClientHideFlags(client);
	
	
	Menu menu = new Menu(Hndlr_Settings);
	menu.SetTitle("Hide Settings:\n ");
	
	menu.AddItem("players", (hideflags & HIDEFLAG_HIDE_PLAYERS) ? "Players: OFF" : "Players: ON");
	menu.AddItem("bots", (hideflags & HIDEFLAG_HIDE_BOTS) ? "Bots: OFF" : "Bots: ON");
	menu.AddItem("viewmodel", (hideflags & HIDEFLAG_VIEWMODEL) ? "Viewmodel: OFF" : "Viewmodel: ON");
	if (g_bLib_Pets)
	{
		menu.AddItem("pets", "Pets");
	}
	if (FindPluginByFile("influx_hud_recchat.smx"))
	{
		menu.AddItem("recchat", "Record Printing");
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public int Hndlr_Settings(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	int hideflags = Influx_GetClientHideFlags(client);
	
	char szInfo[32];
	GetMenuItem(menu, index, szInfo, sizeof(szInfo));
	
	if (StrEqual(szInfo, "players"))
	{
		if (hideflags & HIDEFLAG_HIDE_PLAYERS)
		{
			hideflags &= ~HIDEFLAG_HIDE_PLAYERS;
		}
		else
		{
			hideflags |= HIDEFLAG_HIDE_PLAYERS;
		}
	}
	else if (StrEqual(szInfo, "bots"))
	{
		if (hideflags & HIDEFLAG_HIDE_BOTS)
		{
			hideflags &= ~HIDEFLAG_HIDE_BOTS;
		}
		else
		{
			hideflags |= HIDEFLAG_HIDE_BOTS;
		}
	}
	else if (StrEqual(szInfo, "viewmodel"))
	{
		// Pass in argument to disable printtochat
		FakeClientCommand(client, "sm_viewmodel 1");
		
		FakeClientCommand(client, "sm_hide");
		
		return 0;
	}
	else if (StrEqual(szInfo, "pets"))
	{
		if(g_bLib_Pets)
		{
			FakeClientCommand(client, "sm_pets");
			
			return 0;
		}
	}
	else if (StrEqual(szInfo, "recchat"))
	{
		if (FindPluginByFile("influx_hud_recchat.smx"))
		{
			FakeClientCommand(client, "sm_recchat");
			
			return 0;
		}
	}
	
	Influx_SetClientHideFlags(client, hideflags);
	
	FakeClientCommand(client, "sm_hide");
	
	return 0;
} 
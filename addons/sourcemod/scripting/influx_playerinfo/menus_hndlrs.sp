/*
	!playerinfo Menus
*/

// Get the user id that the search provided and show the mode selection menu.
public int Hndlr_PlayerInfoSearch(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int uid = StringToInt(szInfo);
	
	g_hPInfoMenus[client][UserId] = uid;
	
	Show_PInfoModeMenu(client, uid);
	
	return 0;
}

// Set the user's mode selection and then present them with the option of a style.
public int Hndlr_PInfoModeSelect(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	char buffer[2][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int uid = StringToInt(buffer[0]);
	int mode = StringToInt(buffer[1]);
	
	g_nLastSelectedMode[client] = mode;
	
	g_hPInfoMenus[client][UserId] = uid;
	g_hPInfoMenus[client][Mode] = mode;
	
	Show_PInfoStyleMenu(client, uid, mode);
	return 0;
}

// Set the user's style selection and then present them with the playerinfo menu.
public int Hndlr_PInfoStyleSelect(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	char buffer[3][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int uid = StringToInt(buffer[0]);
	int mode = StringToInt(buffer[1]);
	int style = StringToInt(buffer[2]);
	
	g_nLastSelectedMode[client] = mode;
	g_nLastSelectedStyle[client] = style;
	
	g_hPInfoMenus[client][UserId] = uid;
	g_hPInfoMenus[client][Mode] = mode;
	g_hPInfoMenus[client][Style] = style;
	
	Menu_PlayerInfo(client);
	return 0;
}

// Check what option they have selected.
public int Hndlr_PlayerInfo(Menu menu, MenuAction action, int client, int index)
{
	// Digusting mess due to how we handle the menus.
	// TODO: Clean up this section of the code. It works for the time being.
	if (menu == INVALID_HANDLE)return 0;
	if (client <= 0 || !IsClientConnected(client) || !IsClientInGame(client))return 0;
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	// Check whether they want to view the PB for this map.
	if (StrEqual("viewpb", szInfo)) {
		Menu_ViewPBRunSelect(client);
		return 1;
	}
	// Check whether they want to view all the user's records
	else if (StrEqual("viewrecords", szInfo))
	{
		Menu subMenu = new Menu(Hndlr_PInfoReturn);
		subMenu.SetTitle("Loading...");
		subMenu.ExitBackButton = true;
		
		g_hPInfoMenus[client][SubMenu] = subMenu;
		
		DB_GetClientAllTimes(client);
		
		subMenu.Display(client, MENU_TIME_FOREVER);
		
		return 1;
	}
	// Check whether they want to view all the user's server records
	else if (StrEqual("viewsr", szInfo))
	{
		Menu subMenu = new Menu(Hndlr_PInfoReturn);
		subMenu.SetTitle("Loading...");
		subMenu.ExitBackButton = true;
		
		g_hPInfoMenus[client][SubMenu] = subMenu;
		
		DB_GetClientBestTimes(client);
		
		subMenu.Display(client, MENU_TIME_FOREVER);
		
		return 1;
	}
	// Check whether they want to view all the user's maps they haven't completed.
	else if (StrEqual("viewincomplete", szInfo))
	{
		Menu subMenu = new Menu(Hndlr_PInfoReturn);
		subMenu.SetTitle("Loading...");
		subMenu.ExitBackButton = true;
		
		g_hPInfoMenus[client][SubMenu] = subMenu;
		
		DB_GetIncompleteMaps(client);
		
		subMenu.Display(client, MENU_TIME_FOREVER);
		
		return 1;
	}
	// Check whether they want to change the style/mode.
	else if (StrEqual("changeinfo", szInfo))
	{
		Show_PInfoModeMenu(client, g_hPInfoMenus[client][UserId]);
		
		return 1;
	}
	return 1;
}

// Set their run to the one they have selected and show the user's PB detail
public int Hndlr_ViewPBRun(Menu menu, MenuAction action, int client, int index)
{
	if (action == MenuAction_End || action == MenuAction_Cancel) {
		g_hPInfoMenus[client][MainMenu] = INVALID_HANDLE;
		return 0;
	}
	
	char szInfo[4];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int run = StringToInt(szInfo);
	
	g_hPInfoMenus[client][Run] = run;
	Menu_ViewPB(client);
	return 1;
}

// Used for any generic return, where back should return to main menu.
public int Hndlr_PInfoReturn(Menu menu, MenuAction action, int client, int index)
{
	// Digusting mess due to how we handle the menus.
	// TODO: Clean up this section of the code. It works for the time being.
	if (menu == INVALID_HANDLE)return 0;
	if (client <= 0 || !IsClientConnected(client) || !IsClientInGame(client))return 0;
	MENU_HANDLE_BACK(menu, action, index)
	// Go back to menu
	Menu_PlayerInfo(client);
	
	return 1;
}

// Just used as a placeholder for where no handle is needed.
public int Hndlr_Null(Menu menu, MenuAction action, int client, int index) {  } 
stock bool DB_GetEscaped(char[] out, int len, const char[] def = "")
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	if (!SQL_EscapeString(db, out, out, len))
	{
		strcopy(out, len, def);
		
		return false;
	}
	
	return true;
}
// Playerinfo Menu
DB_PlayerInfoSearch(int client, char player[MAX_NAME_LENGTH])
{
	// Escape
	RemoveChars(player, "`'\"");
	
	if (!DB_GetEscaped(player, sizeof(player), "N/A"))
	{
		// Abort if the escape failed
		Influx_PrintToChat(_, client, "%t", "PARSEPLAYERFAILED");
		return;
	}
	
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	decl String:query[512];
	FormatEx(query, sizeof(query), "SELECT uid, name FROM "...INF_TABLE_USERS..." WHERE name LIKE \"%%%s%%\" ORDER BY name LIMIT 50;", player);
	
	SQL_TQuery(db, Thrd_PlayerInfoSearch, query, client, DBPrio_Low);
}
DB_GetClientMenuInfo(int client)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	decl String:query[512];
	FormatEx(query, sizeof(query), "SELECT steamid, name FROM "...INF_TABLE_USERS..." WHERE uid=%i;", g_hPInfoMenus[client][UserId]);
	
	SQL_TQuery(db, Thrd_GetClientInfo, query, client, DBPrio_Low);
}
DB_GetClientAllTimes(int client)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	decl String:query[512];
	FormatEx(query, sizeof(query), "SELECT rectime, _m.mapname FROM "...INF_TABLE_TIMES..." AS _t INNER JOIN "...INF_TABLE_MAPS..." AS _m ON _t.mapid=_m.mapid WHERE uid=%i AND mode=%i AND style=%i AND runid=1 ORDER BY _m.mapname;", g_hPInfoMenus[client][UserId], g_hPInfoMenus[client][Mode], g_hPInfoMenus[client][Style]);
	
	SQL_TQuery(db, Thrd_GetClientAllTimes, query, client, DBPrio_Low);
}
DB_GetClientBestTimes(int client)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	decl String:query[512];
	FormatEx(query, sizeof(query), "SELECT rectime, _m.mapname FROM "...INF_TABLE_TIMES..." AS _t INNER JOIN "...INF_TABLE_MAPS..." AS _m ON _t.mapid=_m.mapid WHERE uid=%i AND mode=%i AND style=%i AND runid=1 AND rank=1 ORDER BY _m.mapname", g_hPInfoMenus[client][UserId], g_hPInfoMenus[client][Mode], g_hPInfoMenus[client][Style]);
	
	SQL_TQuery(db, Thrd_GetClientBestTimes, query, client, DBPrio_Low);
}
/*
    TODO: Test optimisation for this

    SELECT mapname FROM fusion_testbhhop.inf_maps
    LEFT OUTER JOIN inf_times ON (
    	inf_times.mapid=inf_maps.mapid 
    	AND inf_times.uid=1 
    	AND inf_times.runid=1 
    	AND inf_times.mode=0 
    	AND inf_times.style=1
    	)
    WHERE inf_times.mapid IS NULL
*/
DB_GetIncompleteMaps(int client)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	decl String:query[1024];
	FormatEx(query, sizeof(query), "SELECT mapname FROM "...INF_TABLE_MAPS..." "...
		"LEFT OUTER JOIN "...INF_TABLE_TIMES..." ON ("...
		INF_TABLE_TIMES...".mapid="...INF_TABLE_MAPS...".mapid "...
		"AND "...INF_TABLE_TIMES...".uid=%i "...
		"AND "...INF_TABLE_TIMES...".runid=1 "...
		"AND "...INF_TABLE_TIMES...".mode=%i "...
		"AND "...INF_TABLE_TIMES...".style=%i "...
		")"...
		"WHERE "...INF_TABLE_TIMES...".mapid IS NULL GROUP BY mapname;", 
		g_hPInfoMenus[client][UserId], 
		g_hPInfoMenus[client][Mode], 
		g_hPInfoMenus[client][Style]
		);
	
	SQL_TQuery(db, Thrd_GetIncompleteMaps, query, client, DBPrio_Low);
}
/*
	Commands
*/

public Action Cmd_PlayerInfo(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (Inf_HandleCmdSpam(client, 5.0, g_flLastPlayerInfoTime[client], true))
	{
		return Plugin_Handled;
	}
	
	// No arguments provided
	if (1 > args)
	{
		Show_PInfoModeMenu(client, Influx_GetClientId(client));
	}
	else
	{
		// Firstly string together all the arguments
		char szName[MAX_NAME_LENGTH];
		char arg[128];
		for (int i = 1; i <= args; i++)
		{
			GetCmdArg(i, arg, sizeof(arg));
			FormatEx(szName, sizeof(szName), "%s %s", szName, arg);
		}
		
		// Remove whitespace and search for name
		TrimString(szName);
		
		DB_PlayerInfoSearch(client, szName);
	}
	return Plugin_Handled;
}

/*
	!playerinfo Menu
*/

// Setup the menu to allow user to select the mode they want to view the playerinfo for.
bool Show_PInfoModeMenu(int client, int uid)
{
	decl String:szMode[MAX_MODE_NAME];
	decl String:szItem[64];
	int mode;
	
	int len = Influx_GetModesArray().Length;
	if (len < 2) {
		// No mode to currently select, just load into the style selection; assuming auto run.
		Show_PInfoStyleMenu(client, uid, Influx_GetModesArray().Get(0, MODE_ID), true);
		return true;
	}
	
	Menu menu = new Menu(Hndlr_PInfoModeSelect);
	menu.SetTitle("Select a mode");
	
	for (mode = 0; mode < MAX_MODES; mode++)
	{
		Influx_GetModeName(mode, szMode, sizeof(szMode));
		if (!StrEqual(szMode, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i_%i", uid, mode);
			
			menu.AddItem(szItem, szMode);
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}

// Setup the menu to allow user to select the style they want to view the playerinfo for.
bool Show_PInfoStyleMenu(int client, int uid, int mode, bool forced = false)
{
	decl String:szStyle[MAX_STYLE_NAME];
	decl String:szItem[64];
	int style;
	
	int len = Influx_GetStylesArray().Length;
	if (len < 2) {
		// No style to currently select, just load into the playerinfo module
		g_nLastSelectedMode[client] = mode;
		g_nLastSelectedStyle[client] = Influx_GetStylesArray().Get(0, STYLE_ID);
		
		g_hPInfoMenus[client][UserId] = uid;
		g_hPInfoMenus[client][Mode] = mode;
		g_hPInfoMenus[client][Style] = Influx_GetStylesArray().Get(0, STYLE_ID);
		
		Menu_PlayerInfo(client);
		return true;
	}
	
	Menu menu = new Menu(Hndlr_PInfoStyleSelect);
	menu.SetTitle("Select a style");
	menu.ExitBackButton = !(forced);
	
	for (style = 0; style < MAX_STYLES; style++)
	{
		Influx_GetStyleName(style, szStyle, sizeof(szStyle));
		if (!StrEqual(szStyle, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i_%i_%i", uid, mode, style);
			
			menu.AddItem(szItem, szStyle);
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}

// Setup the playerinfo menu.
bool Menu_PlayerInfo(int client)
{
	Menu menu = new Menu(Hndlr_PlayerInfo);
	menu.SetTitle("Loading...");
	
	g_hPInfoMenus[client][MainMenu] = menu;
	
	// Allow the DB functions to update the title.
	DB_GetClientMenuInfo(client);
	
	menu.AddItem("viewpb", "View Personal Best (Current Map)");
	menu.AddItem("viewrecords", "View All Records");
	menu.AddItem("viewsr", "View All Server Records ");
	menu.AddItem("viewincomplete", "View Incomplete Maps");
	menu.AddItem("changeinfo", "Change Mode/Style");
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}

// Setup the menu to allow the user to choose which run to display the PB for (only avaliable on current map option)
bool Menu_ViewPBRunSelect(int client)
{
	decl String:szRun[MAX_RUN_NAME];
	decl String:szItem[64];
	int run;
	
	int len = Influx_GetRunsArray().Length;
	if (len < 2) {
		// No mode to currently select, just load into the style selection; assuming auto run.
		g_hPInfoMenus[client][Run] = Influx_GetRunsArray().Get(0, RUN_ID);
		Menu_ViewPB(client);
		return true;
	}
	
	Menu menu = new Menu(Hndlr_ViewPBRun);
	menu.SetTitle("Select a run");
	
	for (run = 1; run <= MAX_RUNS; run++)
	{
		Influx_GetRunName(run, szRun, sizeof(szRun));
		if (!StrEqual(szRun, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i", run);
			
			menu.AddItem(szItem, szRun);
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}

// Setup the menu to show a PB for the current map.
bool Menu_ViewPB(int client)
{
	int run = g_hPInfoMenus[client][Run];
	int mode = g_hPInfoMenus[client][Mode];
	int style = g_hPInfoMenus[client][Style];
	
	// Show PB of the player from the !playerinfo command.
	int record = Influx_GetRecordPosition(
		g_hPInfoMenus[client][UserId], 
		run, 
		mode, 
		style
	);
	
	// We currently have no pb, so display a lack of PB and option to return to the main menu.
	if (record == -1)
	{
		Menu menu = new Menu(Hndlr_PInfoReturn);
		char szTitle[128];
		FormatEx(szTitle, sizeof(szTitle), "No PB found for %s", g_hPInfoMenus[client][Name]);
		
		menu.SetTitle(szTitle);
		menu.AddItem("", "showmenu", ITEMDRAW_NOTEXT);
		menu.ExitBackButton = true;
		menu.Display(client, MENU_TIME_FOREVER);
		g_hPInfoMenus[client][SubMenu] = menu;
		
		return true;
	}
	
	// Display record data.
	Menu menu = new Menu(Hndlr_PInfoReturn);
	Influx_DisplayRecord(run, mode, style, menu, record, client, true);

	g_hPInfoMenus[client][SubMenu] = menu;
	
	return true;
} 
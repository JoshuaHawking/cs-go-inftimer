// Player info
public void Thrd_PlayerInfoSearch(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting suggested player names");
		return;
	}
	
	Menu menu = new Menu(Hndlr_PlayerInfoSearch);
	
	int players;
	while (SQL_FetchRow(res))
	{
		char uid[8];
		IntToString(SQL_FetchInt(res, 0), uid, sizeof(uid));
		
		// Add item to menu
		char szPlayer[MAX_NAME_LENGTH];
		SQL_FetchString(res, 1, szPlayer, sizeof(szPlayer));
		
		menu.AddItem(uid, szPlayer);
		players++;
	}
	
	if (players == 0)
	{
		Influx_PrintToChat(_, client, "%t", "PLAYERSEARCHFAILED");
		return;
	}
	
	if (players == 50)
	{
		Influx_PrintToChat(_, client, "%t", "PLAYERSEARCHMAX", 50);
	}
	
	char szTitle[64];
	FormatEx(szTitle, sizeof(szTitle), "Select a player [%i total]", players);
	AddMenuSpacer(szTitle, sizeof(szTitle));
	menu.SetTitle(szTitle);
	menu.Display(client, MENU_TIME_FOREVER);
}

public void Thrd_GetClientInfo(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting client info");
		return;
	}
	
	while (SQL_FetchRow(res))
	{
		SQL_FetchString(res, 0, g_hPInfoMenus[client][SteamId], MAX_STEAMAUTH_LENGTH);
		SQL_FetchString(res, 1, g_hPInfoMenus[client][Name], MAX_NAME_LENGTH);
	}
	
	// Update menu title
	char szTitle[256];
	FormatEx(szTitle, sizeof(szTitle), "Viewing %s's Profile", g_hPInfoMenus[client][Name]);
	
	// Get mode & style name
	char szMode[MAX_MODE_NAME];
	char szStyle[MAX_STYLE_NAME];
	Influx_GetModeName(g_hPInfoMenus[client][Mode], szMode, sizeof(szMode));
	Influx_GetStyleName(g_hPInfoMenus[client][Style], szStyle, sizeof(szStyle));
	AddMenuLine(szTitle, sizeof(szTitle), "%s - %s", szMode, szStyle);
	
	AddMenuSpacer(szTitle, sizeof(szTitle));
	
	AddMenuLine(szTitle, sizeof(szTitle), "Steam ID: %s", g_hPInfoMenus[client][SteamId]);
	AddMenuSpacer(szTitle, sizeof(szTitle));
	SetMenuTitle(g_hPInfoMenus[client][MainMenu], szTitle);
	
	DisplayMenu(g_hPInfoMenus[client][MainMenu], client, MENU_TIME_FOREVER);
	
	return;
}
public void Thrd_GetClientAllTimes(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting all client times");
		return;
	}
	
	char szSecFormat[16];
	Influx_GetSecondsFormat_PersonalBest(szSecFormat, sizeof(szSecFormat));
	
	int records = 0;
	while (SQL_FetchRow(res))
	{
		// Format time
		char szTime[16];
		float time = SQL_FetchFloat(res, 0);
		Inf_FormatSeconds(time, szTime, sizeof(szTime), szSecFormat);
		
		// Format map
		char szMap[32];
		SQL_FetchString(res, 1, szMap, sizeof(szMap));
		char szDisplay[48];
		
		// Add to menu
		FormatEx(szDisplay, sizeof(szDisplay), "%s - %s", szMap, szTime);
		AddMenuItem(g_hPInfoMenus[client][SubMenu], "", szDisplay, ITEMDRAW_DISABLED);
		records++;
	}
	
	// Update menu title
	char szTitle[256];
	if (records == 0)
	{
		FormatEx(szTitle, sizeof(szTitle), "No records found for %s", g_hPInfoMenus[client][Name]);
		AddMenuItem(g_hPInfoMenus[client][SubMenu], "", "showmenu", ITEMDRAW_NOTEXT);
	} else {
		FormatEx(szTitle, sizeof(szTitle), "Viewing %s's Records [%i total]", g_hPInfoMenus[client][Name], records);
	}
	
	SetMenuTitle(g_hPInfoMenus[client][SubMenu], szTitle);
	
	DisplayMenu(g_hPInfoMenus[client][SubMenu], client, MENU_TIME_FOREVER);
	return;
}
public void Thrd_GetClientBestTimes(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting best client times");
		return;
	}
	
	char szSecFormat[16];
	Influx_GetSecondsFormat_PersonalBest(szSecFormat, sizeof(szSecFormat));
	
	int records = 0;
	while (SQL_FetchRow(res))
	{
		// Format time
		char szTime[16];
		float time = SQL_FetchFloat(res, 0);
		Inf_FormatSeconds(time, szTime, sizeof(szTime), szSecFormat);
		
		// Format map
		char szMap[32];
		SQL_FetchString(res, 1, szMap, sizeof(szMap));
		char szDisplay[48];
		
		// Add to menu
		FormatEx(szDisplay, sizeof(szDisplay), "%s - %s", szMap, szTime);
		AddMenuItem(g_hPInfoMenus[client][SubMenu], "", szDisplay, ITEMDRAW_DISABLED);
		records++;
	}
	
	// Update menu title
	char szTitle[256];
	if (records == 0)
	{
		FormatEx(szTitle, sizeof(szTitle), "No server records found for %s", g_hPInfoMenus[client][Name]);
		AddMenuItem(g_hPInfoMenus[client][SubMenu], "", "showmenu", ITEMDRAW_NOTEXT);
	} else {
		FormatEx(szTitle, sizeof(szTitle), "Viewing %s's Server Records [%i total]", g_hPInfoMenus[client][Name], records);
	}
	
	SetMenuTitle(g_hPInfoMenus[client][SubMenu], szTitle);
	
	DisplayMenu(g_hPInfoMenus[client][SubMenu], client, MENU_TIME_FOREVER);
	return;
}

public void Thrd_GetIncompleteMaps(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting best client times");
		return;
	}
	int maps = 0;
	while (SQL_FetchRow(res))
	{
		// Format map
		char szMap[32];
		SQL_FetchString(res, 0, szMap, sizeof(szMap));
		
		// Add to menu
		AddMenuItem(g_hPInfoMenus[client][SubMenu], "", szMap, ITEMDRAW_DISABLED);
		maps++;
	}
	
	// Update menu title
	char szTitle[256];
	if (maps == 0)
	{
		FormatEx(szTitle, sizeof(szTitle), "%s has completed every map!", g_hPInfoMenus[client][Name]);
		AddMenuItem(g_hPInfoMenus[client][SubMenu], "", "showmenu", ITEMDRAW_NOTEXT);
	} else {
		FormatEx(szTitle, sizeof(szTitle), "Viewing %s's Incomplete Maps [%i total]", g_hPInfoMenus[client][Name], maps);
	}
	
	SetMenuTitle(g_hPInfoMenus[client][SubMenu], szTitle);
	
	DisplayMenu(g_hPInfoMenus[client][SubMenu], client, MENU_TIME_FOREVER);
	return;
} 
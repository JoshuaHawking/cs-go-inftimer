public void Thrd_RefreshCache(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "getting cache for records");
		return;
	}
	CollectCache(res, data);
}
CollectCache(Handle db, DataPack data)
{
	int run;
	
	data.Reset();
	run = data.ReadCell();
	
	if (g_bCacheLoaded[run])return;
	
	g_bCacheLoaded[run] = true;
	
	while (SQL_FetchRow(db))
	{
		Handle nNewCache[RecordCache];
		
		// Record Information
		nNewCache[RecordId] = SQL_FetchInt(db, 0);
		nNewCache[MapId] = SQL_FetchInt(db, 1);
		nNewCache[RunId] = SQL_FetchInt(db, 2);
		nNewCache[Mode] = SQL_FetchInt(db, 3);
		nNewCache[Style] = SQL_FetchInt(db, 4);
		nNewCache[FinishCount] = SQL_FetchInt(db, 5);
		nNewCache[Time] = SQL_FetchFloat(db, 6);
		SQL_FetchString(db, 7, nNewCache[DateString], 32);
		
		// User details
		nNewCache[UserId] = SQL_FetchInt(db, 8);
		SQL_FetchString(db, 9, nNewCache[UserName], 32);
		
		// Strafes / Jumps / Sync 
		nNewCache[StrafeCount] = SQL_FetchInt(db, 10);
		nNewCache[JumpCount] = SQL_FetchInt(db, 11);
		nNewCache[StrafeSync] = SQL_FetchFloat(db, 12);
		nNewCache[JumpSync] = SQL_FetchFloat(db, 13);
		
		// Speed / Distance Tracking
		nNewCache[AvgSpeed] = SQL_FetchFloat(db, 14);
		nNewCache[FinalSpeed] = SQL_FetchFloat(db, 15);
		nNewCache[MaxSpeed] = SQL_FetchFloat(db, 16);
		
		nNewCache[MouseMovement] = SQL_FetchFloat(db, 17);
		nNewCache[DistanceTravelled] = SQL_FetchFloat(db, 18);
		
		// Custom stats
		nNewCache[Custom1] = SQL_FetchInt(db, 19);
		nNewCache[Custom2] = SQL_FetchInt(db, 20);
		nNewCache[Custom3] = SQL_FetchInt(db, 21);
		
		PushArrayArray(g_hRecordCache[run][nNewCache[Mode]][nNewCache[Style]], nNewCache[0]);
	}
}
public void Thrd_UpdateRanks(Handle db, Handle res, const char[] szError, any data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "updating record ranks");
		return;
	}
}
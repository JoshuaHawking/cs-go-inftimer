stock bool DB_GetEscaped(char[] out, int len, const char[] def = "")
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	if (!SQL_EscapeString(db, out, out, len))
	{
		strcopy(out, len, def);
		
		return false;
	}
	
	return true;
}
// WR Menu
DB_RefreshCache() {
	
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	int run;
	for (run = 1; run <= MAX_RUNS; run++)
	{
		//g_bCacheLoaded[mode][style][run] = false;
		decl String:query[512];
		FormatEx(query, sizeof(query), "SELECT recordid, mapid, runid, mode, style, finishcount, rectime, recdate, uid, name, strf_num, jump_num, strf_sync, jump_sync, avgspeed, finalspeed, maxspeed, mousemovement, distancetravelled, custom1, custom2, custom3 FROM "...INF_TABLE_TIMES..." NATURAL JOIN "...INF_TABLE_USERS..." WHERE mapid = %i AND runid = %i ORDER BY rectime ASC;", Influx_GetCurrentMapId(), run);
		DataPack data = new DataPack();
		data.WriteCell(run);
		
		SQL_TQuery(db, Thrd_RefreshCache, query, data, DBPrio_Low);
	}
}
DB_RefreshRunCache(int run) {
	
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	//g_bCacheLoaded[mode][style][run] = false;
	decl String:query[1024];
	FormatEx(query, sizeof(query), "SELECT recordid, mapid, runid, mode, style, finishcount, rectime, recdate, uid, name, strf_num, jump_num, strf_sync, jump_sync, avgspeed, finalspeed, maxspeed, mousemovement, distancetravelled, custom1, custom2, custom3 FROM "...INF_TABLE_TIMES..." NATURAL JOIN "...INF_TABLE_USERS..." WHERE mapid = %i AND runid = %i ORDER BY rectime ASC;", Influx_GetCurrentMapId(), run);
	DataPack data = new DataPack();
	data.WriteCell(run);
	
	SQL_TQuery(db, Thrd_RefreshCache, query, data, DBPrio_Low);
}

DB_UpdateRanks()
{
	// Firstly we check whether influx_ranking is enabled. If it is, we let that module deal with updating the ranks.
	if (g_bLib_Ranking)return;
	
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	int mode, style, run;
	
	for (run = 1; run <= MAX_RUNS; run++) {
		for (mode = 0; mode < MAX_MODES; mode++) {
			for (style = 0; style < MAX_STYLES; style++) {
				if (g_bUpdateNeeded[run][mode][style])
				{
					char query[2048];
					FormatEx(query, sizeof(query), "SET @r=0;");
					SQL_TQuery(db, Thrd_UpdateRanks, query, _, DBPrio_High);
					FormatEx(query, sizeof(query), "UPDATE "...INF_TABLE_TIMES..." SET `rank` = @r:= (@r+1) WHERE mapid=%i AND runid=%i AND mode=%i AND style=%i ORDER BY `rectime` ASC;", Influx_GetCurrentMapId(), run, mode, style);
					SQL_TQuery(db, Thrd_UpdateRanks, query, _, DBPrio_High);
				}
			}
		}
	}
}
/*
	Commands
*/

public Action Cmd_MapRecords(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (Inf_HandleCmdSpam(client, 1.0, g_flLastRecPrintTime[client], true))
	{
		return Plugin_Handled;
	}
	
	Show_RecordMenu(client);
	
	return Plugin_Handled;
}

/*
	!wr Menu
*/

// Begin showing the record menu - assume their run then load them into the mode selection.
bool Show_RecordMenu(int client)
{
	// If no runs currently exist, say there are no records.
	if (Influx_GetRunsArray().Length <= 0) {
		Influx_PrintToChat(_, client, "%t", "NOSERVERRECORDS");
		return false;
	}
	
	int run;
	
	// If player isn't alive, or currently not set to a certain run, assume the first run (main).
	if (!IsPlayerAlive(client) || Influx_GetClientRunId(client) == -1) {
		run = Influx_GetRunsArray().Get(0, RUN_ID);
	} else {
		run = Influx_GetClientRunId(client);
	}
	
	Show_RecordModeMenu(client, run);
	
	return true;
}

// Allow the user to select their mode for viewing SRs.
bool Show_RecordModeMenu(int client, int runid)
{
	decl String:szRun[MAX_RUN_NAME];
	decl String:szMode[MAX_MODE_NAME];
	decl String:szItem[64];
	int mode;
	
	Influx_GetRunName(runid, szRun, sizeof(szRun));
	
	int len = Influx_GetModesArray().Length;
	if (len < 2) {
		// No mode to currently select, just load into the style selection; assuming auto run.
		Show_RecordStyleMenu(client, runid, Influx_GetModesArray().Get(0, MODE_ID), true);
		return true;
	}
	
	Menu menu = new Menu(Hndlr_RecordModeSelect);
	menu.SetTitle("WR Menu\nRun: %s\n ", szRun);
	
	for (mode = 0; mode < MAX_MODES; mode++)
	{
		Influx_GetModeName(mode, szMode, sizeof(szMode));
		if (!StrEqual(szMode, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i_%i", runid, mode);
			
			menu.AddItem(szItem, szMode);
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}

// Allow the user to select their style for viewing SRs.
bool Show_RecordStyleMenu(int client, int runid, int mode, bool forced = false)
{
	decl String:szRun[MAX_RUN_NAME];
	decl String:szStyle[MAX_STYLE_NAME];
	decl String:szItem[64];
	int style;
	
	Influx_GetRunName(runid, szRun, sizeof(szRun));
	
	int len = Influx_GetStylesArray().Length;
	if (len < 2) {
		// No style to currently select, just load into the wr menu
		Menu_PrintRecords(client, runid, mode, Influx_GetStylesArray().Get(0, STYLE_ID));
		return true;
	}
	
	Menu menu = new Menu(Hndlr_RecordStyleSelect);
	menu.SetTitle("WR Menu\nRun: %s\n ", szRun);
	menu.ExitBackButton = !(forced);
	
	for (style = 0; style < MAX_STYLES; style++)
	{
		Influx_GetStyleName(style, szStyle, sizeof(szStyle));
		if (!StrEqual(szStyle, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i_%i_%i", runid, mode, style);
			
			menu.AddItem(szItem, szStyle);
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}

// Setup the menu that prints all records for a certain run,mode,style.
bool Menu_PrintRecords(int client, int runid, int mode, int style)
{
	char szMap[128];
	GetCurrentMapSafe(szMap, sizeof(szMap));
	
	Menu menu = new Menu(Hndlr_PrintRecords);
	int nItems = 0;
	char szSecFormat[16];
	
	Influx_GetSecondsFormat_PersonalBest(szSecFormat, sizeof(szSecFormat));
	for (new i = 0; i < GetArraySize(g_hRecordCache[runid][mode][style]); i++)
	{
		Handle nNewCache[RecordCache];
		GetArrayArray(g_hRecordCache[runid][mode][style], i, nNewCache[0]);
		
		char szMenuText[92];
		char szTime[16];
		Inf_FormatSeconds(nNewCache[Time], szTime, sizeof(szTime), szSecFormat);
		FormatEx(szMenuText, sizeof(szMenuText), "#%i | %s - %s", i + 1, nNewCache[UserName], szTime);
		
		char szItem[32];
		FormatEx(szItem, sizeof(szItem), "%i_%i_%i_%i", runid, mode, style, i);
		menu.AddItem(szItem, szMenuText);
		nItems++;
	}
	
	if (nItems == 0)
	{
		menu.SetTitle("No records found for %s!", szMap, nItems);
		menu.AddItem("", "", ITEMDRAW_SPACER);
		menu.AddItem("", "", ITEMDRAW_SPACER);
	}
	else
	{
		menu.SetTitle("Top times on %s [%i total]", szMap, nItems);
	}
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}
// Setup the menu to show a single record and it's stats.
bool Menu_PrintSingleRecord(int client, int runid, int mode, int style, int record)
{
	// Get record array.
	Handle nCache[RecordCache];
	GetArrayArray(g_hRecordCache[runid][mode][style], record, nCache[0]);
	
	Menu menu = new Menu(Hndlr_PrintSingleRecord);
	DisplayRecordMenu(nCache, menu, record, client);
	
	return true;
}

/*
	Admin Options
*/

// Setup menu to show the admin information for a record
// This is main just the runid, mode, style, recordnum and an option to the delete the record.
bool Menu_AdminRecordInfo(int client, int runid, int mode, int style, int record)
{
	
	char szMap[128];
	GetCurrentMapSafe(szMap, sizeof(szMap));
	
	Menu menu = new Menu(Hndlr_AdminInfo);
	
	// Get record array.
	Handle nNewCache[RecordCache];
	GetArrayArray(g_hRecordCache[runid][mode][style], record, nNewCache[0]);
	
	char szMenuText[512];
	char szMenuInfo[128];
	
	// Format view profile option
	// We use this option as our foundation for the text below. 
	FormatEx(szMenuText, sizeof(szMenuText), "View user profile (%s)", nNewCache[UserName]);
	
	// Display record info
	AddMenuSpacer(szMenuText, sizeof(szMenuText));
	AddMenuLine(szMenuText, sizeof(szMenuText), "User ID: %i", nNewCache[UserId]);
	AddMenuLine(szMenuText, sizeof(szMenuText), "Map ID: %i", nNewCache[MapId]);
	AddMenuLine(szMenuText, sizeof(szMenuText), "Run ID: %i", runid);
	AddMenuLine(szMenuText, sizeof(szMenuText), "Mode ID: %i", mode);
	AddMenuLine(szMenuText, sizeof(szMenuText), "Style ID: %i", style);
	
	AddMenuSpacer(szMenuText, sizeof(szMenuText));
	
	// Any other stats
	AddMenuLine(szMenuText, sizeof(szMenuText), "Position: %i", record + 1);
	
	AddMenuSpacer(szMenuText, sizeof(szMenuText));
	
	char szUser[16];
	FormatEx(szUser, sizeof(szUser), "user_%i_%i_%i", nNewCache[UserId], mode, style);
	menu.AddItem(szUser, szMenuText, (!g_bLib_PlayerInfo ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT));
	
	// Just check whether they can remove records.
	if (CanUserRemoveRecords(client)) {
		FormatEx(szMenuInfo, sizeof(szMenuInfo), "dele_%i_%i_%i_%i_%i", nNewCache[UserId], nNewCache[MapId], runid, mode, style);
		menu.AddItem(szMenuInfo, "Delete This Record");
	}
	
	menu.SetTitle("Displaying #%i for %s by %s\n ", record + 1, szMap, nNewCache[UserName]);
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

// Confirm the deletion of the record.
bool Menu_ConfirmDeletion(int client, int uid, int mapid, int runid, int mode, int style)
{
	Menu menu = new Menu(Hndlr_ConfirmDeletion);
	
	char szInfo[32];
	FormatEx(szInfo, sizeof(szInfo), "%i_%i_%i_%i_%i", uid, mapid, runid, mode, style);
	menu.SetTitle("Delete this record?");
	menu.AddItem("no", "Cancel");
	menu.AddItem("no", "Cancel");
	menu.AddItem("no", "Cancel");
	menu.AddItem(szInfo, "Confirm");
	menu.AddItem("no", "Cancel");
	menu.AddItem("no", "Cancel");
	
	menu.Display(client, MENU_TIME_FOREVER);
}



void DisplayRecordMenu(Handle recordCache[RecordCache], Menu &menu, int recordIndex, int client = 0, bool disableOptions = false)
{
	char szMap[128];
	GetCurrentMapSafe(szMap, sizeof(szMap));
	
	int runid = recordCache[RunId];
	int mode = recordCache[Mode];
	int style = recordCache[Style];
	
	char szMenuText[512];
	menu.SetTitle("Displaying #%i for %s by %s\n ", recordIndex + 1, szMap, recordCache[UserName]);
	
	// Format view profile option
	// We use this option as our foundation for the text below. 
	FormatEx(szMenuText, sizeof(szMenuText), "View user profile (%s)", recordCache[UserName]);
	
	// Format time:
	char szTime[16];
	char szSecFormat[16];
	Influx_GetSecondsFormat_PersonalBest(szSecFormat, sizeof(szSecFormat));
	Inf_FormatSeconds(recordCache[Time], szTime, sizeof(szTime), szSecFormat);
	
	// Add time and date.
	AddMenuSpacer(szMenuText, sizeof(szMenuText));
	
	char szMode[MAX_MODE_NAME];
	char szStyle[MAX_STYLE_NAME];
	Influx_GetModeName(mode, szMode, sizeof(szMode));
	Influx_GetStyleName(mode, szStyle, sizeof(szStyle));
	AddMenuLine(szMenuText, sizeof(szMenuText), "%s - %s", szMode, szStyle);
	
	AddMenuSpacer(szMenuText, sizeof(szMenuText));
	AddMenuLine(szMenuText, sizeof(szMenuText), "Time: %s (Finished %i time%s)", szTime, recordCache[FinishCount], (recordCache[FinishCount] == 1 ? "" : "s"));
	AddMenuLine(szMenuText, sizeof(szMenuText), "Date: %s", recordCache[DateString]);
	
	// Add jumps / strafes / sync
	AddMenuSpacer(szMenuText, sizeof(szMenuText));
	
	if (g_bLib_Jumps)
	{
		AddMenuLine(szMenuText, sizeof(szMenuText), "Jumps: %i", recordCache[JumpCount]);
	}
	
	if (g_bLib_Strafes)
	{
		AddMenuLine(szMenuText, sizeof(szMenuText), "Strafes: %i", recordCache[StrafeCount]);
	}
	
	if (g_bLib_Sync)
	{
		AddMenuLine(szMenuText, sizeof(szMenuText), "Sync: %.2f%%", recordCache[StrafeSync]);
	}
	
	// Only display spacer if we have printed something from above.
	if (g_bLib_Jumps || g_bLib_Strafes || g_bLib_Sync) {
		AddMenuSpacer(szMenuText, sizeof(szMenuText));
	}
	
	if (g_bLib_Speed)
	{
		// Add avg, max and final speed
		AddMenuLine(szMenuText, sizeof(szMenuText), "Speed: Avg: %.0f | Final: %.0f | Max: %.0f", recordCache[AvgSpeed], recordCache[FinalSpeed], recordCache[MaxSpeed]);
		
		// Add distance
		AddMenuLine(szMenuText, sizeof(szMenuText), "Distance Travelled: %.1f units", recordCache[DistanceTravelled]);
		AddMenuLine(szMenuText, sizeof(szMenuText), "Mouse Movement: %.2f%%", recordCache[MouseMovement]);
		
		AddMenuSpacer(szMenuText, sizeof(szMenuText));
	}
		
	if (g_bLib_Rocketjump)
	{
		AddMenuLine(szMenuText, sizeof(szMenuText), "Rockets Shot: %i", recordCache[Custom3]);
		AddMenuSpacer(szMenuText, sizeof(szMenuText));
	}
	
	// Display other stats
	if (g_bLib_Style_Parkour && style == STYLE_PARKOUR)
	{
		// Add boost count
		AddMenuLine(szMenuText, sizeof(szMenuText), "Wall Boosts: %i", recordCache[Custom1]);
		AddMenuSpacer(szMenuText, sizeof(szMenuText));
	}
	
	// Add view user profile, just disable if the disable options is on.
	char szUser[16];
	FormatEx(szUser, sizeof(szUser), "user_%i_%i_%i", recordCache[UserId], mode, style);
	// Check whether playerinfo module is enabled.
	menu.AddItem(szUser, szMenuText, ((!g_bLib_PlayerInfo || disableOptions) ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT));
	
	if (!disableOptions)
	{
		// Display watch replay if WR
		if (g_bLib_Recording) {
			if (Influx_HasValidRecording(recordCache[UserId], runid, mode, style))
			{
				char szMenuInfo[16];
				FormatEx(szMenuInfo, sizeof(szMenuInfo), "demo_%i_%i_%i_%i", recordCache[UserId], runid, mode, style);
				menu.AddItem(szMenuInfo, "Watch Replay");
			}
			else
			{
				menu.AddItem("", "No Replay", ITEMDRAW_DISABLED);
			}
		}
		
		if (CheckCommandAccess(client, "sm_admin", ADMFLAG_GENERIC)) {
			char szMenuInfo[16];
			FormatEx(szMenuInfo, sizeof(szMenuInfo), "sudo_%i_%i_%i_%i", runid, mode, style, recordIndex);
			menu.AddItem(szMenuInfo, "Admin Info");
		}
	}
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
} 
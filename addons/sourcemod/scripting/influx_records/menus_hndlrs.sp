/*
	!wr Menu
*/

// Set the user's mode selection and then present them with the option of a style.
public int Hndlr_RecordModeSelect(Menu menu, MenuAction action, int client, int index)
{
	// Close menu if they have selected to exit.
	MENU_HANDLE_BACK(menu, action, index)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[2][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	
	int runid = StringToInt(buffer[0]);
	int mode = StringToInt(buffer[1]);
	
	g_nLastSelectedRun[client] = runid;
	g_nLastSelectedMode[client] = mode;
	
	Show_RecordStyleMenu(client, runid, mode);
	return 0;
}

// Set the user's style selection then present them with the WR menu.
public int Hndlr_RecordStyleSelect(Menu menu, MenuAction action, int client, int index)
{
	// Close menu if they have selected to exit.
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They selected to go back, so go back to the first menu.
		Show_RecordMenu(client)
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[3][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	
	int runid = StringToInt(buffer[0]);
	int mode = StringToInt(buffer[1]);
	int style = StringToInt(buffer[2]);
	
	// Check for valid styles
	if (!VALID_MODE(mode))return 0;
	
	if (!VALID_STYLE(style))return 0;
	
	g_nLastSelectedRun[client] = runid;
	g_nLastSelectedMode[client] = mode;
	g_nLastSelectedStyle[client] = style;
	
	Menu_PrintRecords(client, runid, mode, style);
	
	return 0;
}

// They have selected a record to view.
public int Hndlr_PrintRecords(Menu menu, MenuAction action, int client, int index)
{
	// Close menu if they have selected to exit.
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to the style selection menu.
		Show_RecordStyleMenu(client, g_nLastSelectedRun[client], g_nLastSelectedMode[client]);
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[4][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int runid = StringToInt(buffer[0]);
	int mode = StringToInt(buffer[1]);
	int style = StringToInt(buffer[2]);
	int record = StringToInt(buffer[3]);
	
	// Print a single record and it's details.
	Menu_PrintSingleRecord(client, runid, mode, style, record);
	
	return 1;
}

// They have selected to go back, view the user's profile or watch the replay.
public int Hndlr_PrintSingleRecord(Menu menu, MenuAction action, int client, int index)
{
	// Close menu if they have selected to exit.
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to all the menus.
		Menu_PrintRecords(client, g_nLastSelectedRun[client], g_nLastSelectedMode[client], g_nLastSelectedStyle[client]);
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	// Get first 4 characters (either back or user or demo)
	char szCheck[5];
	strcopy(szCheck, sizeof(szCheck), szInfo);
	
	// Check for whether they want to view player's profile
	if (StrEqual(szCheck, "user"))
	{
		// Ensure player info module is loaded.
		if (!g_bLib_PlayerInfo)return 0;
		
		char buffer[4][8];
		if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
		{
			return 0;
		}
		
		int userid = StringToInt(buffer[1]);
		int mode = StringToInt(buffer[2]);
		int style = StringToInt(buffer[3]);
		
		// Load straight into the menu
		Influx_DisplayPlayerInfo(client, userid, mode, style);

		return 1;
	}
	// Check whether they want to view the replay
	else if (StrEqual(szCheck, "demo"))
	{
		if (!g_bLib_Recording)
		{
			// We shouldn't really ever get here
			return 0;
		}
		char buffer[5][6];
		if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
		{
			return 0;
		}
		
		int userid = StringToInt(buffer[1]);
		int runid = StringToInt(buffer[2]);
		int mode = StringToInt(buffer[3]);
		int style = StringToInt(buffer[4]);
		
		int queue;
		if ((queue = Influx_RequestRecording(client, userid, runid, mode, style)) == -1) // Not valid
		{
			Influx_PrintToChat(_, client, "%t", "ALREADYQUEUED");
		}
		else
		{
			if (queue == 0)return 1;
			Influx_PrintToChat(_, client, "%t", "REPLAYQUEUED", queue);
		}
		return 1;
	}
	// Check whether they want to load the admin menu
	else if (StrEqual(szCheck, "sudo")) 
	{
		if (!CheckCommandAccess(client, "sm_admin", ADMFLAG_GENERIC))return 0;
		
		char buffer[5][6];
		if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
		{
			return 0;
		}
		
		int runid = StringToInt(buffer[1]);
		int mode = StringToInt(buffer[2]);
		int style = StringToInt(buffer[3]);
		int record = StringToInt(buffer[4]);
		
		Menu_AdminRecordInfo(client, runid, mode, style, record);
		
		return 1;
	}
	
	return 0;
}

public int Hndlr_AdminInfo(Menu menu, MenuAction action, int client, int index)
{
	// Close menu if they have selected to exit.
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so return to the list of records.
		Menu_PrintRecords(client, g_nLastSelectedRun[client], g_nLastSelectedMode[client], g_nLastSelectedStyle[client]);
		return 0;
	}
	
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	// Get first 4 characters (either back or user or demo)
	char szCheck[5];
	strcopy(szCheck, sizeof(szCheck), szInfo);
	
	// Check whether they want to view the user profile
	if (StrEqual(szCheck, "user"))
	{
		// Ensure player info module is loaded.
		if (!g_bLib_PlayerInfo)return 0;
		
		char buffer[4][8];
		if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
		{
			return 0;
		}
		
		int userid = StringToInt(buffer[1]);
		int mode = StringToInt(buffer[2]);
		int style = StringToInt(buffer[3]);
		
		// Load straight into the menu
		Influx_DisplayPlayerInfo(client, userid, mode, style);

		return 1;
	}
	// Check whether they want to remove the record
	else if (StrEqual(szCheck, "dele"))
	{
		char buffer[6][6];
		if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
		{
			return 0;
		}
		
		if (!CanUserRemoveRecords(client))return 0;
		
		int uid = StringToInt(buffer[1]);
		int mapid = StringToInt(buffer[2]);
		
		int runid = StringToInt(buffer[3]);
		int mode = StringToInt(buffer[4]);
		int style = StringToInt(buffer[5]);
		
		// Confirm that they want to delete the record
		// int issuer, int uid, int mapid, int runid, int mode, int style
		Menu_ConfirmDeletion(client, uid, mapid, runid, mode, style);
		
		return 1;
	}
	
	return 1;
}

// Confirm whether they want to delete the record or not.
public int Hndlr_ConfirmDeletion(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	// They don't want to delete the record
	if (StrEqual(szInfo, "no")) {
		Menu_PrintRecords(client, g_nLastSelectedRun[client], g_nLastSelectedMode[client], g_nLastSelectedStyle[client]);
		return 1;
	}
	
	char buffer[5][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int uid = StringToInt(buffer[0]);
	int mapid = StringToInt(buffer[1]);
	
	int runid = StringToInt(buffer[2]);
	int mode = StringToInt(buffer[3]);
	int style = StringToInt(buffer[4]);
	
	// Delete the record
	Influx_DeleteRecord(client, uid, mapid, runid, mode, style);
	
	return 1;
}

// Just used as a placeholder for where no handle is needed.
public int Hndlr_Null(Menu menu, MenuAction action, int client, int index) {} 
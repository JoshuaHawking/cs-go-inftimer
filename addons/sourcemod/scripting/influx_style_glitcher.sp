#include <sourcemod>
#include <smlib>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

#define GLITCHER_GRAVITY 200.0

enum VelocityOverride {
	
	VelocityOvr_None = 0, 
	VelocityOvr_Velocity, 
	VelocityOvr_OnlyWhenNegative, 
	VelocityOvr_InvertReuseVelocity
}

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Style - Glitcher", 
	description = "", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	// CMDS
	RegConsoleCmd("sm_glitcher", Cmd_Style_Glitcher, INF_NAME..." - Change your style to glitcher.");
	RegConsoleCmd("sm_glitch", Cmd_Style_Glitcher, "");
	RegConsoleCmd("sm_highjump", Cmd_Style_Glitcher, "");	
	
	// EVENTS
	HookEvent("player_jump", E_PlayerJump);
}

public void OnAllPluginsLoaded()
{
	if (!Influx_AddStyle(STYLE_GLITCHER, "Glitcher", "GLITCH", false))
	{
		SetFailState(INF_CON_PRE..."Couldn't add style!");
	}
}

public void OnPluginEnd()
{
	Influx_RemoveStyle(STYLE_GLITCHER);
}

public void Influx_OnRequestStyles()
{
	OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
    Influx_AddStyleInfo(STYLE_GLITCHER, "Glitcher gives you a higher jump than you usually have.");
}

public Action Influx_OnSearchType(const char[] szArg, Search_t &type, int &value)
{
	if (StrEqual(szArg, "glitcher", false))
	{
		value = STYLE_GLITCHER;
		type = SEARCH_STYLE;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

public Action Cmd_Style_Glitcher(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	Influx_SetClientStyle(client, STYLE_GLITCHER);
	
	return Plugin_Handled;
}

public Action Influx_OnCheckClientStyle(int client, int style, float vel[3])
{
	if (style != STYLE_GLITCHER)return Plugin_Continue;
	
	return Plugin_Stop;
}

// Counting every jump
public void E_PlayerJump(Event event, const char[] szEvent, bool bImUselessWhyDoIExist)
{
	int client;
	if (!(client = GetClientOfUserId(GetEventInt(event, "userid"))))return;
	
	if (!IsPlayerAlive(client))return;
	
	if (Influx_GetClientStyle(client) != STYLE_GLITCHER)
		return;
		
	CreateTimer(0.0, Timer_Push, client, TIMER_FLAG_NO_MAPCHANGE);
	return;
}

public Action Timer_Push(Handle timer, int client)
{
	Push_Client(client);
	return Plugin_Stop;
}

stock Push_Client(client)
{
	if (GLITCHER_GRAVITY > 0.0)
	{
		Client_Push(client, Float: { -90.0, 0.0, 0.0 }, GLITCHER_GRAVITY, VelocityOverride: { VelocityOvr_None, VelocityOvr_None, VelocityOvr_None } );
	}
}

Client_Push(client, float clientEyeAngle[3], float power, VelocityOverride:override[3] = VelocityOvr_None)
{
	float forwardVector[3];
	float newVel[3];
	
	GetAngleVectors(clientEyeAngle, forwardVector, NULL_VECTOR, NULL_VECTOR);
	NormalizeVector(forwardVector, forwardVector);
	ScaleVector(forwardVector, power);
	
	Entity_GetAbsVelocity(client, newVel);
	
	for (new i = 0; i < 3; i++) {
		switch (override[i]) {
			case VelocityOvr_Velocity: {
				newVel[i] = 0.0;
			}
			case VelocityOvr_OnlyWhenNegative: {
				if (newVel[i] < 0.0) {
					newVel[i] = 0.0;
				}
			}
			case VelocityOvr_InvertReuseVelocity: {
				if (newVel[i] < 0.0) {
					newVel[i] *= -1.0;
				}
			}
		}
		
		newVel[i] += forwardVector[i];
	}
	
	Entity_SetAbsVelocity(client, newVel);
} 
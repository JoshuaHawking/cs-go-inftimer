#include <sourcemod>

#include <influx/core>
#include <influx/hud>

#include <msharedutil/misc>


#undef REQUIRE_PLUGIN
#include <influx/help>
#include <influx/recording>
#include <influx/strafes>
#include <influx/sync>
#include <influx/playerranks>
#include <influx/jumps>
#include <influx/pause>
#include <influx/practice>
#include <influx/records>
//#include <influx/strfsync>
#include <influx/truevel>
#include <influx/zones_stage>
#include <influx/zones_checkpoint>
#include <influx/ranking>
#include <rocketjump>

// Colours
#define CLR_TIME_SR				"#00F900"
#define CLR_TIME_PB				"#5CABEB"
#define CLR_TIME_DEFAULT		"#E00000"
#define CLR_TIME_STOPPED		"#E00000"

#define CLR_PRACTICE			"#E0B300"
#define CLR_PAUSED				"#E0B300"

#define CLR_RUN_MAIN			"#00F900"
#define CLR_RUN_BONUS			"#7953B2"

#define CLR_HOC					"#3B9F3B"
#define CLR_PB					"#007BE0"
#define CLR_SR					"#E0B300"

// LIBRARIES
//bool g_bLib_Strafes;
//bool g_bLib_Jumps;
bool g_bLib_Pause;
bool g_bLib_Practice;
bool g_bLib_Recording;
//bool g_bLib_StrfSync;
bool g_bLib_Truevel;
bool g_bLib_Stage;
bool g_bLib_Records;
bool g_bLib_Rocketjump;

char g_szMap[48];

// Showkeys stuff
bool g_bShowKeys[INF_MAXPLAYERS];
float g_flLastShowKeysTime[INF_MAXPLAYERS];

// CONVARS
ConVar g_ConVar_ShowStyleOnly;
bool g_bShowStyleOnly;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - HUD | Draw CS:GO", 
	description = "Displays info on player's screen w/ Showkeys", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	if (GetEngineVersion() != Engine_CSGO)
	{
		FormatEx(szError, error_len, "Bad engine version!");
		
		return APLRes_Failure;
	}
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegConsoleCmd("sm_showkeys", Cmd_ShowKeys);
	
	// LIBRARIES
	//g_bLib_Strafes = LibraryExists( INFLUX_LIB_STRAFES );
	//g_bLib_Jumps = LibraryExists( INFLUX_LIB_JUMPS );
	g_bLib_Pause = LibraryExists(INFLUX_LIB_PAUSE);
	g_bLib_Practice = LibraryExists(INFLUX_LIB_PRACTICE);
	g_bLib_Recording = LibraryExists(INFLUX_LIB_RECORDING);
	//g_bLib_StrfSync = LibraryExists( INFLUX_LIB_STRFSYNC );
	g_bLib_Truevel = LibraryExists(INFLUX_LIB_TRUEVEL);
	g_bLib_Stage = LibraryExists(INFLUX_LIB_ZONES_STAGE);
	g_bLib_Records = LibraryExists(INFLUX_LIB_RECORDS);
	g_bLib_Rocketjump = LibraryExists(LIB_ROCKETJUMP);
	
	
	// CONVARS
	g_ConVar_ShowStyleOnly = CreateConVar("influx_hud_showstyleonly", "0", "1 = Show style only on HUD   |   0 = Show both mode and style on HUD", _, true, 0.0, true, 1.0);
	g_ConVar_ShowStyleOnly.AddChangeHook(E_ConVarChanged_ShowStyleOnly);
	g_bShowStyleOnly = g_ConVar_ShowStyleOnly.BoolValue;
}

public void OnMapStart()
{
	GetCurrentMapSafe(g_szMap, sizeof(g_szMap));
}

public void OnLibraryAdded(const char[] lib)
{
	//if ( StrEqual( lib, INFLUX_LIB_STRAFES ) ) g_bLib_Strafes = true;
	//if ( StrEqual( lib, INFLUX_LIB_JUMPS ) ) g_bLib_Jumps = true;
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = true;
	if (StrEqual(lib, INFLUX_LIB_PRACTICE))g_bLib_Practice = true;
	if (StrEqual(lib, INFLUX_LIB_RECORDING))g_bLib_Recording = true;
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = true;
	if (StrEqual(lib, INFLUX_LIB_ZONES_STAGE))g_bLib_Stage = true;
	if (StrEqual(lib, INFLUX_LIB_RECORDS))g_bLib_Records = true;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_Rocketjump = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	//if ( StrEqual( lib, INFLUX_LIB_STRAFES ) ) g_bLib_Strafes = false;
	//if ( StrEqual( lib, INFLUX_LIB_JUMPS ) ) g_bLib_Jumps = false;
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = false;
	if (StrEqual(lib, INFLUX_LIB_PRACTICE))g_bLib_Practice = false;
	if (StrEqual(lib, INFLUX_LIB_RECORDING))g_bLib_Recording = false;
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = false;
	if (StrEqual(lib, INFLUX_LIB_ZONES_STAGE))g_bLib_Stage = false;
	if (StrEqual(lib, INFLUX_LIB_RECORDS))g_bLib_Records = false;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_Rocketjump = false;
}

public Action Cmd_ShowKeys(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (Inf_HandleCmdSpam(client, 5.0, g_flLastShowKeysTime[client], true))
	{
		return Plugin_Handled;
	}
	
	// Toggle
	g_bShowKeys[client] = !g_bShowKeys[client];
	Influx_PrintToChat(_, client, "%t", "SHOWKEYSENABLED", (g_bShowKeys[client] ? "ON" : "OFF"));
	
	return Plugin_Handled;
}

public OnClientConnected(int client)
{
	g_bShowKeys[client] = false;
	g_flLastShowKeysTime[client] = 0.0;
}

public Action Influx_OnDrawHUD(int client, int target, HudType_t hudtype)
{
	decl String:szMsg[512];
	
	szMsg = "<font size='16'>";
	
	char szTemp[64];
	char szTemp2[64];
	char szSecFormat[16];
	
	int hideflags = Influx_GetClientHideFlags(client);
	
	
	if (hudtype == HUDTYPE_HINT)
	{
		if (!IsFakeClient(target)) {
			// DISPLAY FOR HUMANS
			
			if (hideflags & HIDEFLAG_TIME) {
				return Plugin_Stop;
			}
			
			bool bShowKeys = g_bShowKeys[client];
			int iButtons = GetClientButtons(target);
			
			/*
				Formatting: 
				
				Each parameter added requires the string to be padded with spaces to 28:
				%28s
				
				Then this requires a tab afterwards to keep the formatting the same.
				\t
				
				This then results in the parameter being added with the string formatter of:
				%28s\t
				
				When added to szMsg, the whole line will look like this:
				FormatEx(szMsg, sizeof(szMsg), "%s%28s\t", szMsg, szTemp);
			*/
			
			/*
				First Line
				Time - Speed
			*/
			
			RunState_t state = Influx_GetClientState(target);
			
			// Get PB for timer display.
			float flClientBest = Influx_GetClientCurrentPB(target);
			float flMapBest = Influx_GetClientCurrentBestTime(target);
			char szClientBest[14];
			char szStyle[MAX_STYLE_NAME];
			char szMode[MAX_MODE_NAME];
			
			if (flClientBest == INVALID_RUN_TIME) {
				flClientBest = 0.0;
			}
			
			Influx_GetSecondsFormat_PersonalBest(szSecFormat, sizeof(szSecFormat));
			Inf_FormatSeconds(flClientBest, szClientBest, sizeof(szClientBest), szSecFormat);
			
			// Check whether they are in practice.
			if (g_bLib_Practice && Influx_IsClientPractising(target))
			{
				FormatEx(szTemp, sizeof(szTemp), "Time: <font color='%s'>Practice</font>", CLR_PRACTICE);
				FormatEx(szMsg, sizeof(szMsg), "%s%24s\t\t", szMsg, szTemp);
			}
			// Check whether they are paused
			else if (g_bLib_Pause && Influx_IsClientPaused(target)) // Paused
			{
				FormatEx(szTemp, sizeof(szTemp), "Time: <font color='%s'>Paused</font>", CLR_PAUSED);
				FormatEx(szMsg, sizeof(szMsg), "%s%24s\t\t", szMsg, szTemp);
			}
			// In start / end zone
			else if (state == STATE_START || state == STATE_NONE || state == STATE_FINISHED)
			{
				int id;
				// Get best time
				float flBestTime = Influx_GetRunBestTime(Influx_GetClientRunId(target), Influx_GetClientMode(target), Influx_GetClientStyle(target), id);
				
				// Format 
				Inf_FormatSeconds(flBestTime, szTemp2, sizeof(szTemp2), szSecFormat);
				
				FormatEx(szTemp, sizeof(szTemp), "SR: <font color='%s'>%s</font>", CLR_SR, szTemp2);
				FormatEx(szMsg, sizeof(szMsg), "%s%24s\t\t", szMsg, szTemp);
			}
			// Running
			else if (state >= STATE_RUNNING)
			{
				
				// Get current time
				float flCurrentTime = Influx_GetClientTime(target);
				Influx_GetSecondsFormat_Timer(szSecFormat, sizeof(szSecFormat));
				
				// Format 
				Inf_FormatSeconds(flCurrentTime, szTemp2, sizeof(szTemp2), szSecFormat);
				
				FormatEx(szTemp, sizeof(szTemp), "Time: <font color='%s'>%s  #%i</font>", 
					(flMapBest > flCurrentTime || flMapBest <= INVALID_RUN_TIME ? CLR_TIME_SR : (flClientBest > flCurrentTime ? CLR_TIME_PB : CLR_TIME_DEFAULT)), 
					szTemp2, 
					Influx_GetNewPossibleRank(Influx_GetClientRunId(target), Influx_GetClientMode(target), Influx_GetClientStyle(target), flCurrentTime));
				FormatEx(szMsg, sizeof(szMsg), "%s%24s\t\t", szMsg, szTemp); // Double \t because <font> throws off the formatting
			}
			
			szTemp = "";
			
			FormatEx(szTemp, sizeof(szTemp), "Speed: %.0f", GetSpeed(target));
			FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
			
			szTemp = "";
			
			/*
				Second Line
				PB - Rank
			*/
			
			FormatEx(szTemp, sizeof(szTemp), "PB: <font color='%s'>%s</font>", CLR_PB, szClientBest);
			FormatEx(szMsg, sizeof(szMsg), "%s%24s\t\t", szMsg, szTemp); // Double \t because <font> throws off the formatting
			
			szTemp = "";
			
			int iRank;
			if (g_bLib_Records) {
				iRank = Influx_GetRecordRank(target, Influx_GetClientRunId(target), Influx_GetClientMode(target), Influx_GetClientStyle(target));
				if (iRank <= 0) {
					szTemp = "Rank: N/A"
				}
				else
				{
					FormatEx(szTemp, sizeof(szTemp), "Rank: #%i", iRank);
				}
			}
			
			FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
			
			szTemp = "";
			
			/*
				Third line
				
				In start zone:
				Player Rank - Map
				
				Anything else:
				Sync - Stage
				
				Show Keys:
				Key Display
				
			*/
			
			if (!bShowKeys)
			{
				// In start zone
				if (state == STATE_START || state == STATE_NONE || state == STATE_FINISHED)
				{
					// Get player rank
					int iPlayerRank = Influx_GetClientSkillRank(target);
					if (iPlayerRank < 0)
					{
						FormatEx(szTemp, sizeof(szTemp), "Prank: N/A");
					}
					else
					{
						FormatEx(szTemp, sizeof(szTemp), "Prank: #%i", iPlayerRank);
					}
					
					FormatEx(szMsg, sizeof(szMsg), "%s%28s\t", szMsg, szTemp);
					
					szTemp = "";
					
					// Get map
					FormatEx(szTemp, sizeof(szTemp), "Map: %s", g_szMap);
					FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
					
					szTemp = "";
				}
				else
				{
					// Check whether we are supporting rocketjump.
					if (g_bLib_Rocketjump)
					{
						FormatEx(szTemp, sizeof(szTemp), "Rockets: %i", RJ_GetRocketShotCount(target));
						FormatEx(szMsg, sizeof(szMsg), "%s%28s\t", szMsg, szTemp);
					}
					else
					{
						FormatEx(szTemp, sizeof(szTemp), "Sync: %.1f", Influx_GetClientStrafeSync(target));
						FormatEx(szMsg, sizeof(szMsg), "%s%28s\t", szMsg, szTemp);
					}
					
					szTemp = "";
					
					// Get stage
					if (g_bLib_Stage) {
						if (Influx_GetClientStageCount(target) < 2) {
							szTemp = "Stage: Linear"
						} else {
							Format(szTemp, sizeof(szTemp), "Stage: %i/%i", Influx_GetClientStage(target), Influx_GetClientStageCount(target));
						}
					} else {
						szTemp = "Stage: Linear"
					}
					
					FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
					
					szTemp = "";
					
				}
			}
			else
			{
				FormatEx(szMsg, sizeof(szMsg), "%s\t    %s\t        %s          \t%s\n", szMsg, (iButtons & IN_ATTACK ? "A1" : "__"), (iButtons & IN_FORWARD ? "W" : "_"), (iButtons & IN_ATTACK2 ? "A2" : "__"));
			}
			
			/*
				Fourth line
				
				In start zone:
				Welcome to House of Climb!
				
				Anything else:
				Mode, Style - Run
				
				Show Keys:
				Key Display
				
			*/
			
			if (!bShowKeys)
			{
				// In start zone
				if (state == STATE_START || state == STATE_NONE || state == STATE_FINISHED)
				{
					FormatEx(szTemp, sizeof(szTemp), "Welcome to the <font color='%s'>House of Climb</font>!", CLR_HOC);
					FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
				}
				else
				{
					Influx_GetStyleName(Influx_GetClientStyle(target), szStyle, sizeof(szStyle));
					
					// Stops long names from disrupting everything
					if (g_bShowStyleOnly)
					{
						FormatEx(szTemp, sizeof(szTemp), "%s", szStyle);
						FormatEx(szTemp, sizeof(szTemp), "%24s\t\t", szTemp);
					}
					else
					{
						Influx_GetModeName(Influx_GetClientMode(target), szMode, sizeof(szMode));
						FormatEx(szTemp, sizeof(szTemp), "%s - %s", szMode, szStyle);
						FormatEx(szTemp, sizeof(szTemp), "%24s\t", szTemp);
					}
					FormatEx(szMsg, sizeof(szMsg), "%s%.28s", szMsg, szTemp);
					
					// Get run
					int run = Influx_GetClientRunId(target);
					if (run == 1)
					{
						Influx_GetRunName(run, szTemp2, sizeof(szTemp2));
						FormatEx(szTemp, sizeof(szTemp), "<font color='%s'>%s</font>", CLR_RUN_MAIN, szTemp2);
					}
					else
					{
						Influx_GetRunName(run, szTemp2, sizeof(szTemp2));
						FormatEx(szTemp, sizeof(szTemp), "<font color='%s'>%s</font>", CLR_RUN_BONUS, szTemp2);
					}
					
					FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
				}
				
				szTemp = "";
			}
			else
			{
				FormatEx(szMsg, sizeof(szMsg), "%s\t%s\t     %s %s %s \t%s\n", szMsg, (iButtons & IN_DUCK ? "CTRL" : "____"), (iButtons & IN_MOVELEFT ? "A" : "_"), (iButtons & IN_BACK ? "S" : "_"), (iButtons & IN_MOVERIGHT ? "D" : "_"), (iButtons & IN_JUMP ? "JUMP" : "____"));
			}
			
			FormatEx(szMsg, sizeof(szMsg), "%s</font>", szMsg);
		}
		else
		{
			// DISPLAY FOR BOTS	
			
			// Draw recording bot info.
			if (g_bLib_Recording)
			{
				int botIndex = Influx_GetBotIndex(target);
				
				// Check whether a replay is currently playing.
				
				if (Influx_GetReplayTime(botIndex) == INVALID_RUN_TIME)
				{
					FormatEx(szMsg, sizeof(szMsg), "%sYou can request a replay by using <font color='%s'>!replay</font> or selecting <font color='%s'>'Watch Replay'</font> in the !wr menu", szMsg, CLR_HOC, CLR_HOC);
				}
				else
				{
					char szStyle[MAX_STYLE_NAME];
					char szMode[MAX_MODE_NAME];
					
					// Not a bot.
					if (botIndex == -1)return Plugin_Stop;
					
					/*
					First Line
					Time - Speed
				*/
					
					float flTime = Influx_GetReplayCurrentTime(botIndex);
					
					if (flTime < 0.0)
					{
						flTime = 0.0;
					}
					
					Influx_GetSecondsFormat_Timer(szSecFormat, sizeof(szSecFormat));
					Inf_FormatSeconds(flTime, szTemp2, sizeof(szTemp2), szSecFormat);
					
					FormatEx(szTemp, sizeof(szTemp), "Time: <font color='%s'>%s</font>", CLR_TIME_SR, szTemp2);
					FormatEx(szMsg, sizeof(szMsg), "%s%28s\t\t", szMsg, szTemp); // Double \t because <font> throws off the formatting
					
					
					FormatEx(szTemp, sizeof(szTemp), "Speed: %.0f", GetSpeed(target));
					FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
					
					/*
					Second Line
					Sync - Player
				*/
					
					FormatEx(szTemp, sizeof(szTemp), "Sync: %.1f", Influx_GetReplayCurrentSync(botIndex));
					FormatEx(szMsg, sizeof(szMsg), "%s%28s\t", szMsg, szTemp);
					
					char szName[32];
					Influx_GetReplayName(botIndex, szName, sizeof(szName));
					
					FormatEx(szTemp, sizeof(szTemp), "Player: <font color='%s'>%s</font>", CLR_HOC, szName);
					FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
					
					/*
					Third Line
					Mode, Style - Run
				*/
					
					Influx_GetStyleName(Influx_GetReplayStyle(botIndex), szStyle, sizeof(szStyle));
					
					// Stops long names from disrupting everything
					if (g_bShowStyleOnly)
					{
						FormatEx(szTemp, sizeof(szTemp), "%s", szStyle);
						FormatEx(szTemp, sizeof(szTemp), "%24s\t\t", szTemp);
					}
					else
					{
						Influx_GetModeName(Influx_GetReplayMode(botIndex), szMode, sizeof(szMode));
						FormatEx(szTemp, sizeof(szTemp), "%s - %s", szMode, szStyle);
						FormatEx(szTemp, sizeof(szTemp), "%24s\t", szTemp);
					}
					FormatEx(szMsg, sizeof(szMsg), "%s%.28s", szMsg, szTemp);
					
					// Get run
					int run = Influx_GetReplayRunId(botIndex);
					if (run == 1)
					{
						Influx_GetRunName(run, szTemp2, sizeof(szTemp2));
						FormatEx(szTemp, sizeof(szTemp), "<font color='%s'>%s</font>", CLR_RUN_MAIN, szTemp2);
					}
					else
					{
						Influx_GetRunName(run, szTemp2, sizeof(szTemp2));
						FormatEx(szTemp, sizeof(szTemp), "<font color='%s'>%s</font>", CLR_RUN_BONUS, szTemp2);
					}
					
					
					FormatEx(szMsg, sizeof(szMsg), "%s%s\n", szMsg, szTemp);
				}
			}
			
			FormatEx(szMsg, sizeof(szMsg), "%s</font>", szMsg);
		}
		
		if (szMsg[0] != '\0')
		{
			PrintHintText(client, szMsg);
		}
	}
	return Plugin_Stop;
}

public void E_ConVarChanged_ShowStyleOnly(ConVar convar, const char[] oldValue, const char[] newValue)
{
	g_bShowStyleOnly = g_ConVar_ShowStyleOnly.BoolValue;
}

// Check if they want truevel.
stock float GetSpeed(int client)
{
	return (g_bLib_Truevel && Influx_IsClientUsingTruevel(client)) ? GetEntityTrueSpeed(client) : GetEntitySpeed(client);
}

stock void ShowPanel(int client, const char[] msg)
{
	Panel panel = new Panel();
	panel.SetTitle(msg);
	panel.Send(client, Hndlr_Panel_Empty, 3);
	
	delete panel;
}

public int Hndlr_Panel_Empty(Menu menu, MenuAction action, int client, int param2) {  }

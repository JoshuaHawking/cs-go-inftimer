#include "influx_core/menus_hndlrs.sp"


public Action Cmd_Credits(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	if (g_bLib_Hud)
	{
		Influx_SetNextMenuTime(client, GetEngineTime() + 5.0);
	}
	
	Panel panel = new Panel();
	
	panel.SetTitle(INF_NAME..." - House Of Climb\nCredits:");
	panel.DrawItem("", ITEMDRAW_SPACER);
	panel.DrawText("Mehis - Author");
	panel.DrawItem("", ITEMDRAW_SPACER);
	panel.DrawText("Fusion - House of Climb Developer");
	panel.DrawItem("", ITEMDRAW_SPACER);
	panel.DrawText("Yeckoh - Game server, testing, moral support");
	panel.DrawItem("", ITEMDRAW_SPACER);
	panel.DrawText("Kyle - Webhost, moral support");
	panel.DrawItem("", ITEMDRAW_SPACER);
	
	panel.DrawItem("Exit", ITEMDRAW_CONTROL);
	
	panel.DrawItem("", ITEMDRAW_SPACER);
	panel.Send(client, Hndlr_Panel_Empty, MENU_TIME_FOREVER);
	
	
	delete panel;
	
	return Plugin_Handled;
}

public Action Cmd_Change_Run(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	Menu menu = new Menu(Hndlr_Change_Run);
	
	char szInfo[8];
	char szRun[MAX_RUN_NAME];
	
	GetRunName(g_iRunId[client], szRun, sizeof(szRun));
	menu.SetTitle("Change Run\nCurrent: %s\n ", szRun);
	
	int len = GetArrayLength_Safe(g_hRuns);
	int id;
	for (int i = 0; i < len; i++)
	{
		GetRunNameByIndex(i, szRun, sizeof(szRun));
		
		id = g_hRuns.Get(i, RUN_ID);
		FormatEx(szInfo, sizeof(szInfo), "%i", id);
		
		Format(szRun, sizeof(szRun), "%s (ID: %i)", szRun, id);
		
		menu.AddItem(szInfo, szRun, (g_iRunId[client] == id) ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public Action Cmd_Change_Mode(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	int len = g_hModes.Length;
	if (len < 2)return Plugin_Handled;
	
	
	Menu menu = new Menu(Hndlr_Change_Mode);
	
	char szInfo[8];
	char szMode[MAX_MODE_NAME];
	
	GetModeName(g_iModeId[client], szMode, sizeof(szMode));
	menu.SetTitle("Change Mode\nCurrent: %s\n ", szMode);
	
	
	int id;
	for (int i = 0; i < len; i++)
	{
		GetModeNameByIndex(i, szMode, sizeof(szMode));
		
		id = g_hModes.Get(i, MODE_ID);
		FormatEx(szInfo, sizeof(szInfo), "%i", id);
		
		menu.AddItem(szInfo, szMode, (g_iModeId[client] == id) ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public Action Cmd_Change_Style(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	// Check whether we have less than two styles and modes.
	int modelen = g_hModes.Length;
	int stylelen = g_hStyles.Length;
	if (modelen < 2 && stylelen < 2)return Plugin_Handled;
	
	Menu menu = new Menu(Hndlr_Change_Style);
	
	// Format Title
	char szTitle[64];
	char szMode[MAX_MODE_NAME];
	char szStyle[MAX_STYLE_NAME];
	
	GetModeName(g_iModeId[client], szMode, sizeof(szMode));
	GetStyleName(g_iStyleId[client], szStyle, sizeof(szStyle));
	
	szTitle = "Change Mode/Style";
	AddMenuLine(szTitle, sizeof(szTitle), "Current: %s - %s", szMode, szStyle);
	AddMenuSpacer(szTitle, sizeof(szTitle));
	
	menu.SetTitle(szTitle);
	
	int mode, style;
	char szItem[32];
	
	// Load modes
	for (mode = 0; mode < MAX_MODES; mode++)
	{
		GetModeName(mode, szMode, sizeof(szMode));
		if (!StrEqual(szMode, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "mode_%i", mode);
			
			menu.AddItem(szItem, szMode, mode == g_iModeId[client] ? ITEMDRAW_DISABLED : ITEMDRAW_CONTROL );
		}
	}
	
	menu.AddItem("", "", ITEMDRAW_SPACER);
	
	// Load styles
	for (style = 0; style < MAX_STYLES; style++)
	{
		GetStyleName(style, szStyle, sizeof(szStyle));
		if (!StrEqual(szStyle, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "style_%i", style);
			
			menu.AddItem(szItem, szStyle, style == g_iStyleId[client] ? ITEMDRAW_DISABLED : ITEMDRAW_CONTROL );
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

// Add new line to menu
stock AddMenuLine(char[] sz, int len, char[] newline, any...)
{
	// Format the new line
	char formatted[256];
	VFormat(formatted, sizeof(formatted), newline, 4);
	
	// Format the new string
	Format(sz, len, "%s\n%s", sz, formatted);
}
// Add spacer
stock AddMenuSpacer(char[] sz, int len)
{
	Format(sz, len, "%s\n \n", sz);
} 
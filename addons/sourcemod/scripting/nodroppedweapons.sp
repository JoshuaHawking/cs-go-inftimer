#include <sdktools>

public OnPluginStart()
{
    HookEventEx("entity_visible", entity_visible)
}

public entity_visible(Handle event, char[] name, bool dontBroadcast)
{
    //    "userid"        "short"        // The player who sees the entity
    //    "subject"        "short"        // Entindex of the entity they see
    //    "classname"        "string"    // Classname of the entity they see
    //    "entityname"    "string"    // name of the entity they see

    char buffer[30];
    GetEventString(event, "classname", buffer, sizeof(buffer));

    if(StrContains(buffer, "weapon_", false) == 0)
    {
        int ref = EntIndexToEntRef(GetEventInt(event, "subject"));

        if(ref != 0 && GetEntProp(ref, Prop_Send, "m_iState") == 0 )
        {
            AcceptEntityInput(ref, "Kill");
        }
    }
}  
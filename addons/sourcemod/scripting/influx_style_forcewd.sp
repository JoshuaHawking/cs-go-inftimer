#include <sourcemod>
#include <sdktools>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Style - Force WD",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    // CMDS
    RegConsoleCmd( "sm_wdonly", Cmd_Style_ForceWD, "Change your style to Force WD." );
    RegConsoleCmd( "sm_forcewd", Cmd_Style_ForceWD, "" );
    RegConsoleCmd( "sm_wd", Cmd_Style_ForceWD, "" );
}

public void OnAllPluginsLoaded()
{
    if ( !Influx_AddStyle( STYLE_FORCEWD, "Force WD", "WD" ) )
    {
        SetFailState( INF_CON_PRE..."Couldn't add style!" );
    }
}

public void OnPluginEnd()
{
    Influx_RemoveStyle( STYLE_FORCEWD );
}

public void Influx_OnRequestStyles()
{
    OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
    Influx_AddStyleInfo(STYLE_FORCEWD, "Force WD means you can only use your W and D keys.");
}

public Action Influx_OnSearchType( const char[] szArg, Search_t &type, int &value )
{
    if (StrEqual( szArg, "wdonly", false )
    ||  StrEqual( szArg, "forcewd", false )
    ||  StrEqual( szArg, "wd", false ) )
    {
        value = STYLE_FORCEWD;
        type = SEARCH_STYLE;
        
        return Plugin_Stop;
    }
    
    return Plugin_Continue;
}

public Action Cmd_Style_ForceWD( int client, int args )
{
    if ( !client ) return Plugin_Handled;
    
    
    Influx_SetClientStyle( client, STYLE_FORCEWD );
    
    return Plugin_Handled;
}

public Action Influx_OnCheckClientStyle( int client, int style, float vel[3] )
{
    if ( style != STYLE_FORCEWD ) return Plugin_Continue;
    
#define FWD     0
#define SIDE    1
    
    // If they are not holding W and D
    if ( !(vel[FWD] > 0.0) || !(vel[SIDE] > 0.0) )
    {
        vel[SIDE] = 0.0;
        vel[FWD] = 0.0;
    }
    
    // Holding A
    if ( vel[SIDE] < 0.0 )
    {
        vel[SIDE] = 0.0;
    }
    
    // Holding S
    if ( vel[FWD] < 0.0 )
    {
        vel[FWD] = 0.0;
    }
    
    return Plugin_Stop;
}
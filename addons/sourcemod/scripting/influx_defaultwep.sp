#include <sourcemod>
#include <cstrike>
#include <smlib>

#include <influx/core>

// LIBARIES
bool g_bLib_RocketJump;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Default Weapon", 
	description = "Give default weapon.", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	HookEvent("player_spawn", E_PlayerSpawn);
}

/*
	After Spawning
*/

// Add weapons
public Action E_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	CreateTimer(0.1, GiveWeapons, event.GetInt("userid"), TIMER_FLAG_NO_MAPCHANGE);
}

public Action GiveWeapons(Handle timer, int uid)
{
	int client = GetClientOfUserId(uid);
	
	if (!client)return Plugin_Handled;
	if (!IsClientInGame(client) || !IsPlayerAlive(client) || IsFakeClient(client))return Plugin_Handled;
	
	int entity = GetPlayerWeaponSlot(client, 0);
	if (entity != -1)
		AcceptEntityInput(entity, "Kill");
	
	RequestFrame(RequestFrame_Callback, client);
	
	return Plugin_Handled;
}

public void RequestFrame_Callback(int client)
{
	GivePlayerItem(client, "weapon_usp_silencer");
} 
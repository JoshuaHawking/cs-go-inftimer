#include <sourcemod>

#include <influx/core>
#include <influx/truevel>

#include <msharedutil/arrayvec>
#include <msharedutil/ents>

#undef REQUIRE_PLUGIN
#include <influx/strafes>

// SSJ
bool g_bSSJEnabled[INF_MAXPLAYERS];
bool g_bPrespeedEnabled[INF_MAXPLAYERS];
int g_iSSJInterval[INF_MAXPLAYERS];
bool g_bStrafesEnabled[INF_MAXPLAYERS];
bool g_bPercentageEnabled[INF_MAXPLAYERS];

int g_iSSJJumps[INF_MAXPLAYERS];
float g_flSSJSpeed[INF_MAXPLAYERS];

bool g_bRepeatEnabled[INF_MAXPLAYERS];

// LIBARIES
bool g_bLib_Truevel;
bool g_bLib_Strafes;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - SSJ", 
	description = "Speed on your sixth jump.", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegConsoleCmd("sm_ssj", Cmd_SSJMenu);
	RegConsoleCmd("sm_sixjump", Cmd_SSJMenu);
	RegConsoleCmd("sm_sixthjump", Cmd_SSJMenu);
	RegConsoleCmd("sm_js", Cmd_SSJMenu);
	RegConsoleCmd("sm_jumpstats", Cmd_SSJMenu);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	g_bLib_Truevel = LibraryExists(INFLUX_LIB_TRUEVEL);
	g_bLib_Strafes = LibraryExists(INFLUX_LIB_STRAFES);
	
	// Hook jump
	HookEvent("player_jump", Event_PlayerJump)
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = true;
	if (StrEqual(lib, INFLUX_LIB_STRAFES))g_bLib_Strafes = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = false;
	if (StrEqual(lib, INFLUX_LIB_STRAFES))g_bLib_Strafes = true;
}

public void Influx_OnTimerStartPost(int client, int runid)
{
	g_iSSJJumps[client] = 0;
	g_flSSJSpeed[client] = 0.0;
	
	if (!(GetEntityFlags(client) & FL_ONGROUND))
	{
		g_iSSJJumps[client] = 1;
		if (g_iSSJInterval[client] == 1 || g_bPrespeedEnabled[client])
		{
			PrintJumpMessage(client, g_bPrespeedEnabled[client]);
		}
	}
	else if (g_bPrespeedEnabled[client])
	{
		PrintJumpMessage(client, true);
	}
}

public OnClientPutInServer(int client)
{
	g_bSSJEnabled[client] = false;
	g_iSSJJumps[client] = 0;
	g_flSSJSpeed[client] = 0.0;
	g_iSSJInterval[client] = 6;
	g_bPrespeedEnabled[client] = false;
	g_bPercentageEnabled[client] = true;
	g_bStrafesEnabled[client] = true;
	g_bRepeatEnabled[client] = true;
}

public Action Cmd_SSJMenu(int client, int args)
{
	// Load menu
	Menu menu = new Menu(Hndlr_SSJMenu);
	
	menu.SetTitle("SSJ Settings");
	
	char szItem[64];
	
	FormatEx(szItem, sizeof(szItem), "SSJ: %s", (g_bSSJEnabled[client] ? "ON" : "OFF"));
	menu.AddItem("enable", szItem);
	
	FormatEx(szItem, sizeof(szItem), "Prespeed: %s\n ", (g_bPrespeedEnabled[client] ? "ON" : "OFF"));
	menu.AddItem("prespeed", szItem);
	
	FormatEx(szItem, sizeof(szItem), "Jump Interval: %i jump%s", g_iSSJInterval[client], (g_iSSJInterval[client] == 1 ? "" : "s"));
	menu.AddItem("interval", szItem);
	
	if (g_bLib_Strafes)
	{
		FormatEx(szItem, sizeof(szItem), "Strafes: %s", (g_bStrafesEnabled[client] ? "ON" : "OFF"));
		menu.AddItem("strafes", szItem);
	}
	
	FormatEx(szItem, sizeof(szItem), "Percentage Gain: %s", (g_bPercentageEnabled[client] ? "ON" : "OFF"));
	menu.AddItem("percentage", szItem);
	
	FormatEx(szItem, sizeof(szItem), "Repeat Stats: %s", (g_bRepeatEnabled[client] ? "ON" : "OFF"));
	menu.AddItem("repeat", szItem);
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int Hndlr_SSJMenu(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	if (StrEqual(szInfo, "enable"))
	{
		g_bSSJEnabled[client] = !g_bSSJEnabled[client];
	}
	else if (StrEqual(szInfo, "prespeed"))
	{
		g_bPrespeedEnabled[client] = !g_bPrespeedEnabled[client];
	}
	else if (StrEqual(szInfo, "interval"))
	{
		g_iSSJInterval[client]++;
		if (g_iSSJInterval[client] == 13)
		{
			g_iSSJInterval[client] = 1;
		}
	}
	else if (StrEqual(szInfo, "strafes"))
	{
		g_bStrafesEnabled[client] = !g_bStrafesEnabled[client];
	}
	else if (StrEqual(szInfo, "percentage"))
	{
		g_bPercentageEnabled[client] = !g_bPercentageEnabled[client];
	}
	else if (StrEqual(szInfo, "repeat"))
	{
		g_bRepeatEnabled[client] = !g_bRepeatEnabled[client];
	}
	
	Cmd_SSJMenu(client, 0);
	
	return 0;
}

public Action Cmd_ToggleSSJ(int client, int args)
{
	if (client)
	{
		// Toggle
		g_bSSJEnabled[client] = !g_bSSJEnabled[client];
		Influx_PrintToChat(_, client, "%t", "SSJENABLED", (g_bSSJEnabled[client] ? "ON" : "OFF"));
		
		g_iSSJJumps[client] = 0;
		g_flSSJSpeed[client] = 0.0;
	}
	return Plugin_Handled;
}

public Event_PlayerJump(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (Influx_GetClientState(client) != STATE_RUNNING)return;
	
	if (g_bSSJEnabled[client])
	{
		g_iSSJJumps[client]++;
		
		if ((g_iSSJJumps[client] % g_iSSJInterval[client]) == 0) {
			if(!g_bRepeatEnabled[client] && g_iSSJInterval[client] != g_iSSJJumps[client])
			{
				return;
			}
			PrintJumpMessage(client);
			g_flSSJSpeed[client] = GetSpeed(client);
		}
	}
}

PrintJumpMessage(int client, bool bPrespeed = false)
{
	int[] clients = new int[MaxClients];
	int nClients = 0;
	
	bool allow;
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if (!IsClientInGame(i))continue;
		
		if (IsFakeClient(i))continue;
		
		
		if (i == client
			 || (!IsPlayerAlive(i) && GetClientObserverTarget(i) == client))
		{
			allow = true;
		}
		else
		{
			allow = false;
		}
		
		
		if (allow)
		{
			clients[nClients++] = i;
		}
	}
	
	if (!nClients)return;
	
	
	// Print Jump Info
	// Format info
	char szSpeed[64];
	FormatEx(szSpeed, sizeof(szSpeed), "{MAINCLR1}%.1f{CHATCLR}", GetSpeed(client));
	
	char szSpeedGain[128];
	char szSymbol[16];
	char szColour[16];
	
	float flSpeedDifference;
	
	if (GetSpeed(client) >= g_flSSJSpeed[client]) {
		szColour = "{LIMEGREEN}";
		szSymbol = "+";
		flSpeedDifference = GetSpeed(client) - g_flSSJSpeed[client];
	}
	else // New Personal Best Record
	{
		szColour = "{LIGHTRED}";
		szSymbol = "-";
		flSpeedDifference = g_flSSJSpeed[client] - GetSpeed(client);
	}
	
	char szPercentage[64];
	if (g_bPercentageEnabled[client] && !bPrespeed)
	{
		FormatEx(szPercentage, sizeof(szPercentage), "{CHATCLR}(%s%.1f%%{CHATCLR})", szColour, ((flSpeedDifference / g_flSSJSpeed[client]) * 100));
	}
	
	FormatEx(szSpeedGain, sizeof(szSpeedGain), "%s%s%.1f %s", szColour, szSymbol, flSpeedDifference, szPercentage);
	
	char szJumps[24];
	if (bPrespeed)
	{
		szJumps = "Prespeed";
	}
	else
	{
		FormatEx(szJumps, sizeof(szJumps), "%i", g_iSSJJumps[client]);
	}
	
	char szStrafes[96];
	if (g_bLib_Strafes && g_bStrafesEnabled[client])
	{
		FormatEx(szStrafes, sizeof(szStrafes), "{CHATCLR}| Strafes {MAINCLR1}%i ", Influx_GetClientStrafeCount(client));
	}
	
	Influx_PrintToChatEx(_, client, clients, nClients, "%t", "SSJINFO", szSpeed, szSpeedGain, szJumps, szStrafes);
}

// Check if they want truevel.
stock float GetSpeed(int client)
{
	return (g_bLib_Truevel && Influx_IsClientUsingTruevel(client)) ? GetEntityTrueSpeed(client) : GetEntitySpeed(client);
} 
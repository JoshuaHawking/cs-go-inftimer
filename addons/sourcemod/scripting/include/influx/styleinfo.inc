#if defined _influx_styleinfo_included
    #endinput
#endif
#define _influx_styleinfo_included


#define INFLUX_LIB_STYLEINFO             "influx_styleinfo"

forward void Influx_RequestModeInfo();
forward void Influx_RequestStyleInfo();


native bool Influx_AddModeInfo(int mode, const char[] modeInfo);
native bool Influx_RemoveModeInfo(int mode);

native bool Influx_AddStyleInfo(int style, const char[] styleInfo);
native bool Influx_RemoveStyleInfo(int style);


public SharedPlugin __pl_influx_styleinfo =
{
    name = INFLUX_LIB_STYLEINFO,
    file = INFLUX_LIB_STYLEINFO...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_styleinfo_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_AddModeInfo" );
    MarkNativeAsOptional( "Influx_RemoveModeInfo" );
    MarkNativeAsOptional( "Influx_AddStyleInfo" );
    MarkNativeAsOptional( "Influx_RemoveStyleInfo" );
}
#endif

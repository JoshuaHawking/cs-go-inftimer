#if defined _influx_ranking_included
    #endinput
#endif
#define _influx_ranking_included


#define INFLUX_LIB_RANKING          "influx_ranking"

#define INF_RANKS_FILE				"hocranks.cfg"

native int Influx_GetClientSkillRank(int client)

public SharedPlugin __pl_influx_ranking =
{
    name = INFLUX_LIB_RANKING,
    file = INFLUX_LIB_RANKING...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_ranking_SetNTVOptional()
{
}
#endif
#if defined _influx_speed_included
#endinput
#endif
#define _influx_speed_included


#define INFLUX_LIB_SPEED          "influx_speed"


native float Influx_GetClientFinalSpeed(int client);
native float Influx_GetClientAverageSpeed(int client);
native float Influx_GetClientMaxSpeed(int client);
native float Influx_GetClientDistanceTravelled(int client);
native float Influx_GetClientMouseMovement(int client);

public SharedPlugin __pl_influx_speed = 
{
	name = INFLUX_LIB_SPEED, 
	file = INFLUX_LIB_SPEED...".smx", 
	#if defined REQUIRE_PLUGIN
	required = 1
	#else
	required = 0
	#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_speed_SetNTVOptional()
{
	MarkNativeAsOptional("Influx_GetClientFinalSpeed");
	MarkNativeAsOptional("Influx_GetClientAverageSpeed");
	MarkNativeAsOptional("Influx_GetClientMaxSpeed");
	MarkNativeAsOptional("Influx_GetClientDistanceTravelled");
	MarkNativeAsOptional("Influx_GetClientMouseMovement");
}
#endif
#if defined _influx_tags_included
    #endinput
#endif
#define _influx_tags_included


#define INFLUX_LIB_TAGS          "influx_tags"

#define INF_BLOCKED_FILE		 "influx_tags_blocked"

native void Influx_SetExternalChatTag(int client, char[] tag);
native void Influx_GetExternalChatTag(int client, char[] buffer);

public SharedPlugin __pl_influx_tags =
{
    name = INFLUX_LIB_TAGS,
    file = INFLUX_LIB_TAGS...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_tags_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_SetExternalChatTag" );
    MarkNativeAsOptional( "Influx_GetExternalChatTag" );
}
#endif
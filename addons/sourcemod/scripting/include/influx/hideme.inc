#if defined _influx_hideme_included
    #endinput
#endif
#define _influx_hideme_included


#define INFLUX_LIB_HIDEME          "influx_hideme"


native bool Influx_IsClientHidden( int client );


public SharedPlugin __pl_influx_hideme =
{
    name = INFLUX_LIB_HIDEME,
    file = INFLUX_LIB_HIDEME...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_hideme_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_IsClientHidden" );
}
#endif
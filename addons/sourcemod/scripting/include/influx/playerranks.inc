#if defined _influx_playerranks_included
    #endinput
#endif
#define _influx_playerranks_included


#define INFLUX_LIB_PLAYERRANKS          "influx_playerranks"
#define INF_MAX_LEVELS                  50
#define INF_LEVELS_FILE                 "hoclevels.cfg"

native int Influx_GetClientPlayerRank( int client );
native int Influx_GetRegisteredPlayerCount();

public SharedPlugin __pl_influx_playerranks =
{
    name = INFLUX_LIB_PLAYERRANKS,
    file = INFLUX_LIB_PLAYERRANKS...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_playerranks_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_GetClientPlayerRank" );
    MarkNativeAsOptional( "Influx_GetRegisteredPlayerCount" );
}
#endif
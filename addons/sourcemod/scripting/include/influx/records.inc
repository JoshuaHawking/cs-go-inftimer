#if defined _influx_records_included
#endinput
#endif
#define _influx_records_included


#define INFLUX_LIB_RECORDS          "influx_records"

native int Influx_GetRecordRank(int client, int run, int mode, int style);
native int Influx_GetRecordTotalRank(int run, int mode, int style);
native int Influx_GetNewPossibleRank(int run, int mode, int style, float time);
native Influx_RefreshCache();
native Influx_RefreshRunCache(int run);

native int Influx_GetRecordPosition(int userid, int run, int mode, int style);

// Thanks zipcore :^)
enum RecordCache
{
	// Record Information
	RecordId, 
	MapId, 
	RunId, 
	Mode, 
	Style, 
	FinishCount, 
	Float:Time, 
	String:DateString[16], 
	
	// User details
	UserId, 
	String:UserName[MAX_NAME_LENGTH], 
	
	// Strafes / Jumps / Sync
	StrafeCount, 
	JumpCount, 
	Float:StrafeSync, 
	Float:JumpSync, 
	
	// Speed / Distance Tracking
	Float:AvgSpeed, 
	Float:FinalSpeed, 
	Float:MaxSpeed, 
	
	Float:MouseMovement, 
	Float:DistanceTravelled, 
	
	// Custom Stats
	Custom1, 
	Custom2, 
	Custom3
}

// Displays a menu that shows the stats of a record
//
// @param run        		Run ID
// @param mode          	Mode ID
// @param style				Style ID
// @param menu				The menu that will be used
// @param recordIndex		The record index of the record
// @param client			Client that the menu should be customised
// @param disableOptions	Disables the options such as view player profile etc.
// @return menu				Returns the menu that has been created
native void Influx_DisplayRecord(int run, int mode, int style, Menu &menu, int recordIndex, int client, bool disableOptions = false);

// Displays a menu that shows the stats of a record using a cached record handle
//
// @param recordCache		The handle that contains the record
// @param menuHandler		The menu handler that is used for the menu definition
// @param recordIndex		The record index of the record
// @param client			Client that the menu should be customised
// @param disableOptions	Disables the options such as view player profile etc.
// @return menu				Returns the menu that has been created
native void Influx_DisplayRecordFromCache(Handle recordCache[RecordCache], Menu &menu, int recordIndex, int client, bool disableOptions = false);

public SharedPlugin __pl_influx_records = 
{
	name = INFLUX_LIB_RECORDS, 
	file = INFLUX_LIB_RECORDS...".smx", 
	#if defined REQUIRE_PLUGIN
	required = 1
	#else
	required = 0
	#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_records_SetNTVOptional()
{
	MarkNativeAsOptional("Influx_GetRecordRank");
	MarkNativeAsOptional("Influx_GetRecordTotalRank");
	MarkNativeAsOptional("Influx_GetNewPossibleRank");
	MarkNativeAsOptional("Influx_RefreshCache");
	MarkNativeAsOptional("Influx_RefreshRunCache");
	
	MarkNativeAsOptional("Influx_GetRecordPosition");
	MarkNativeAsOptional("Influx_DisplayRecord");
	MarkNativeAsOptional("Influx_DisplayRecordFromCache");
}
#endif
#if defined _influx_tas_included
    #endinput
#endif
#define _influx_tas_included


#define INFLUX_LIB_STYLE_TAS          "influx_style_tas"

#define TAS_SLOWMOTION_SPEED		0.25
#define TAS_SLOWMOTION_FRAMES		4 		// Every interval between each frame being recorded. most likely (1 / TAS_SLOWMOTION_SPEED) - 1 
									

native bool Influx_IsClientTASPaused( int client );
native bool Influx_IsClientTASRewinding( int client );
native bool Influx_IsClientTASSlowMotion( int client );

native void Influx_SetClientTASPaused( int client , bool paused );
native void Influx_ResetClientForTAS( int client ); 

public SharedPlugin __pl_influx_tas =
{
    name = INFLUX_LIB_STYLE_TAS,
    file = INFLUX_LIB_STYLE_TAS...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_tas_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_IsClientTASPaused" );
    MarkNativeAsOptional( "Influx_IsClientTASRewinding" );
    MarkNativeAsOptional( "Influx_IsClientTASSlowMotion" );
    MarkNativeAsOptional( "Influx_SetClientTASPaused" );
    MarkNativeAsOptional( "Influx_ResetClientForTAS" );
}
#endif
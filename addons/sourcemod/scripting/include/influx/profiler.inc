#if defined _influx_profiler_included
    #endinput
#endif
#define _influx_profiler_included


#define INFLUX_LIB_PROFILER      "influx_profiler"

/**
 * Register a new profiler with a set name
 * @param num1    name of the profiled object
 * @return        index of the profiler for this object
 */
native Influx_RegisterProfiler(String:path[]);

/**
 * Start timing a section of code with a registered profiler
 * @param num1    id of the profiled object
 * @return        nothing
 */
native Influx_StartProfiling(index);

/**
 * End timing a section of code with a registered profiler, and add to the profiler log
 * @param num1    id of the profiled object
 * @return        nothing
 */
native Influx_EndProfiling(index);

public SharedPlugin __pl_influx_profiler =
{
	name = INFLUX_LIB_PROFILER,
	file = INFLUX_LIB_PROFILER...".smx",
#if defined REQUIRE_PLUGIN
	required = 1
#else
	required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_profiler_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_RegisterProfiler" );
    MarkNativeAsOptional( "Influx_StartProfiling" );
    MarkNativeAsOptional( "Influx_EndProfiling" );
}
#endif


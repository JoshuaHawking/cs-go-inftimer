#if defined _influx_parkour_included
    #endinput
#endif
#define _influx_parkour_included


#define INFLUX_LIB_STYLE_PARKOUR          "influx_style_parkour"


native int Influx_GetClientParkourBoosts( int client );

public SharedPlugin __pl_influx_parkour =
{
    name = INFLUX_LIB_STYLE_PARKOUR,
    file = INFLUX_LIB_STYLE_PARKOUR...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_parkour_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_GetClientParkourBoosts" );
}
#endif
#if defined _influx_speclist_included
    #endinput
#endif
#define _influx_speclist_included


#define INFLUX_LIB_SPECLIST      "influx_speclist"


native bool Influx_HasSpeclistEnabled( int client );
native void Influx_SetSpeclistEnabled( int client, bool enabled );


public SharedPlugin __pl_influx_speclist =
{
    name = INFLUX_LIB_SPECLIST,
    file = INFLUX_LIB_SPECLIST...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_speclist_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_HasSpeclistEnabled" );
    MarkNativeAsOptional( "Influx_SetSpeclistEnabled" );
}
#endif
#if defined _influx_playerinfo_included
#endinput
#endif
#define _influx_playerinfo_included


#define INFLUX_LIB_PLAYERINFO          "influx_playerinfo"

native Influx_DisplayPlayerInfo(int client, int userid, int mode = MODE_INVALID, int style = STYLE_INVALID);

public SharedPlugin __pl_influx_playerinfo = 
{
	name = INFLUX_LIB_PLAYERINFO, 
	file = INFLUX_LIB_PLAYERINFO...".smx", 
	#if defined REQUIRE_PLUGIN
	required = 1
	#else
	required = 0
	#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_playerinfo_SetNTVOptional()
{
	MarkNativeAsOptional("Influx_DisplayPlayerInfo");
}
#endif
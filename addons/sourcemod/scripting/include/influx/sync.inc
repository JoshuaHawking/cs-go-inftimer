#if defined _influx_sync_included
    #endinput
#endif
#define _influx_sync_included


#define INFLUX_LIB_SYNC          "influx_sync"


native float Influx_GetClientStrafeSync( int client );
native float Influx_GetClientJumpSync( int client );


public SharedPlugin __pl_influx_sync =
{
    name = INFLUX_LIB_SYNC,
    file = INFLUX_LIB_SYNC...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_sync_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_GetClientStrafeSync" );
    MarkNativeAsOptional( "Influx_GetClientJumpSync" );
}
#endif
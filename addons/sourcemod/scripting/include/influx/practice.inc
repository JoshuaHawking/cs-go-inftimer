#if defined _influx_practice_included
    #endinput
#endif
#define _influx_practice_included


#define INFLUX_LIB_PRACTICE      "influx_practice"


// Return != Plugin_Continue to stop practice mode.
forward Action Influx_OnClientPracticeStart( int client );

native bool Influx_IsClientPractising( int client );

native void Influx_StartPractising( int client );
native void Influx_EndPractising( int client );


#define IS_PRAC(%0,%1)      ( %0 && Influx_IsClientPractising(%1) )


public SharedPlugin __pl_influx_practice =
{
	name = INFLUX_LIB_PRACTICE,
	file = INFLUX_LIB_PRACTICE...".smx",
#if defined REQUIRE_PLUGIN
	required = 1
#else
	required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_practice_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_IsClientPractising" );
    
    MarkNativeAsOptional( "Influx_StartPractising" );
    MarkNativeAsOptional( "Influx_EndPractising" );
}
#endif
#if defined _pets_included
    #endinput
#endif
#define _pets_included


#define LIB_PETS         "hoc_pets"

native void Pets_SetPetsHidden( int client, bool hide );
native bool Pets_HasPetsHidden( int client );
native void Pets_SetPetsGreeting( int client, bool enable );
native bool Pets_HasPetsGreeting( int client );

public SharedPlugin __pl_pets =
{
    name = LIB_PETS,
    file = LIB_PETS...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_pets_SetNTVOptional()
{
    MarkNativeAsOptional( "Pets_SetPetsHidden" );
    MarkNativeAsOptional( "Pets_HasPetsHidden" );
    MarkNativeAsOptional( "Pets_SetPetsGreeting" );
    MarkNativeAsOptional( "Pets_HasPetsGreeting" );
}
#endif
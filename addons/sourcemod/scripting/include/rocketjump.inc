#if defined _rocketjump_included
    #endinput
#endif
#define _rocketjump_included


#define LIB_ROCKETJUMP          "rocketjump"

native int RJ_GetRocketShotCount( int client );
native void RJ_SetRocketShotCount( int client, int count );


public SharedPlugin __pl_rocketjump =
{
    name = LIB_ROCKETJUMP,
    file = LIB_ROCKETJUMP...".smx",
#if defined REQUIRE_PLUGIN
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_rocketjump_SetNTVOptional()
{
    MarkNativeAsOptional( "RJ_GetRocketShotCount" );
    MarkNativeAsOptional( "RJ_SetRocketShotCount" );
}
#endif
#pragma semicolon 1
#pragma tabsize 0
 
#include <cstrike>
#include <sourcemod>
#include <sdktools>

public Plugin myinfo =
{
    name = "RoundStart Fix",
    author = "HouseOfClimb.com",
    description = "",
    version = "1.0",
    url = "http://houseofclimb.com"
};

public Action CS_OnTerminateRound(float &delay, CSRoundEndReason &reason) {
    return Plugin_Handled;
}
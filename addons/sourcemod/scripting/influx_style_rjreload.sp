#include <sourcemod>
#include <sdktools>
#include <sdkhooks>

#include <smlib>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Style - RJ Reload", 
	description = "", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	// CMDS
	RegConsoleCmd("sm_rjreload", Cmd_Style_RJReload, "Change your style to RJ Reload.");
}

public void OnAllPluginsLoaded()
{
	if (!Influx_AddStyle(STYLE_RJRELOAD, "Reload", "RJR"))
	{
		SetFailState(INF_CON_PRE..."Couldn't add style!");
	}
}

public void OnPluginEnd()
{
	Influx_RemoveStyle(STYLE_RJRELOAD);
}

public void Influx_OnRequestStyles()
{
	OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
	Influx_AddStyleInfo(STYLE_RJRELOAD, "Reload requires you to reload your rocket launcher.");
}

public Action Influx_OnSearchType(const char[] szArg, Search_t &type, int &value)
{
	if (StrEqual(szArg, "rjreload", false)
		 || StrEqual(szArg, "reload", false)
		)
	{
		value = STYLE_RJRELOAD;
		type = SEARCH_STYLE;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

public Action Cmd_Style_RJReload(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	Influx_SetClientStyle(client, STYLE_RJRELOAD);
	
	return Plugin_Handled;
}

public void Influx_OnClientStyleChangePost(int client, int style)
{
	if (style != STYLE_RJRELOAD)
	{
		return;
	}
	
	Client_SetWeaponClipAmmo(client, "weapon_nova", 8);
	Client_SetWeaponPlayerAmmo(client, "weapon_nova", 32);
	
	return;
}



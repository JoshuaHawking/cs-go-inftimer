#include <sourcemod>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

#define EASY_GRAVITY 0.5
#define EASY_TIMESCALE 0.8

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Style - Easy", 
	description = "", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	// CMDS
	RegConsoleCmd("sm_easy", Cmd_Style_Easy, INF_NAME..." - Change your style to easy.");
	RegConsoleCmd("sm_lowgrav", Cmd_Style_Easy, "");
	RegConsoleCmd("sm_lowgravity", Cmd_Style_Easy, "");
}

public void OnAllPluginsLoaded()
{
	if (!Influx_AddStyle(STYLE_EASY, "Easy", "EASY", false))
	{
		SetFailState(INF_CON_PRE..."Couldn't add style!");
	}
}

public void OnPluginEnd()
{
	Influx_RemoveStyle(STYLE_EASY);
}

public void Influx_OnRequestStyles()
{
	OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
    Influx_AddStyleInfo(STYLE_EASY, "Easy consists of a low gravity modifier as well as a slowed down time scale, meaning you'll move slower and have high gravity.");
}

public void Influx_OnTimerStartPost(int client, int runid)
{
	if (Influx_GetClientStyle(client) == STYLE_EASY)
	{
		// Change gravity & timescale
		SetEntityGravity(client, EASY_GRAVITY);
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", EASY_TIMESCALE);
	}
}

public Action OnPlayerRunCmd(int client)
{
	if (Influx_GetClientStyle(client) != STYLE_EASY)return;
	
	if(GetEntityGravity(client) != EASY_GRAVITY)
	{
		//PrintToChatAll("CHANGED GRAVIT: %.1f", GetEntityGravity(client));
	}
	
	// Check gravity.
	if(GetEntityGravity(client) == 1.0 || GetEntityGravity(client) == 0.0)
	{
		// Reset gravity.		
		SetEntityGravity(client, EASY_GRAVITY);
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", EASY_TIMESCALE);
	}
}

public Action Influx_OnSearchType(const char[] szArg, Search_t &type, int &value)
{
	if (StrEqual(szArg, "easy", false))
	{
		value = STYLE_EASY;
		type = SEARCH_STYLE;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

public Action Cmd_Style_Easy(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	Influx_SetClientStyle(client, STYLE_EASY);
	
	return Plugin_Handled;
}

public Action Influx_OnCheckClientStyle(int client, int style, float vel[3])
{
	if (style != STYLE_EASY)return Plugin_Continue;
	
	return Plugin_Stop;
}

public void Influx_OnClientStyleChangePost(int client, int style)
{
	if (style != STYLE_EASY)
	{
		SetEntityGravity(client, 1.0);
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", 1.0);
		return;
	}
	
	// Change gravity & timescale
	SetEntityGravity(client, EASY_GRAVITY);
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", EASY_TIMESCALE);
	
	return;
} 
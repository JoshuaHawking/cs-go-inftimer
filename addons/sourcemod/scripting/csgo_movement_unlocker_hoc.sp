#include <sourcemod>
#include <sdktools>

// Add #define INFLUX_TIMER if influx is supported.
#define INFLUX_TIMER

#if defined INFLUX_TIMER
#include <influx/core>
#endif

Address g_iPatchAddress;
int g_iPatchRestore[100];
int g_iPatchRestoreBytes;

#define PLUGIN_VERSION "1.0"

public Plugin myinfo = 
{
	name = "CS:GO Movement Unlocker + conditional drag func", 
	author = "Peace-Maker / eddit and extended by eddy, Josh", 
	description = "Removes max speed limitation from players on the ground. Extended by eddy to add conditional drag. Edited by Josh to support Influx timer.", 
	version = "1", 
	url = "http://www.wcfan.de/ && https://houseofclimb.com"
}

int iBaseVelocityOffset;

#if defined INFLUX_TIMER
// Influx timer globals
// Player globals
bool g_bOnGround[MAXPLAYERS + 1];
int g_iOnGroundPlayerFrames[MAXPLAYERS + 1];
bool g_bOnGroundPlayerMaxSpeed[MAXPLAYERS + 1];
bool g_bOnGroundPlayerHasKeyPress[MAXPLAYERS + 1];
int g_iOnGroundPlayerButtons[MAXPLAYERS + 1] =  { 0, ... };

// Custom settings enabled for that mode
bool g_bCustomSettingsEnable[MAX_MODES + 1]

// Properties used by the artificial drag on land plugin
bool g_bOnGroundDragEnable[MAX_MODES + 1];
int g_iOnGroundMaxFramesBeforeDrag[MAX_MODES + 1];
int g_iOnGroundMaxFramesBeforeFrictionMax[MAX_MODES + 1];
float g_flOnGroundFriction[MAX_MODES + 1];

// Properties used to define a specific max speed on a not perfect jump
// this clamps the speed at a set speed whenever the jump is not perfect
bool g_bSpeedMaxEnable[MAX_MODES + 1];
float g_flSpeedMaxSpeed[MAX_MODES + 1];
float g_iBadFrameDragCoefficient[MAX_MODES + 1];
#endif

public OnPluginStart()
{
	// Load the gamedata file.
	Handle hGameConf = LoadGameConfigFile("csgo_movement_unlocker.games");
	
	if (hGameConf == INVALID_HANDLE)
		SetFailState("Can't find csgo_movement_unlocker.games.txt gamedata.");
	
	// Get the address near our patch area inside CGameMovement::WalkMove
	Address iAddr = GameConfGetAddress(hGameConf, "WalkMoveMaxSpeed");
	if (iAddr == Address_Null)
	{
		CloseHandle(hGameConf);
		SetFailState("Can't find WalkMoveMaxSpeed address.");
	}
	
	// Get the offset from the start of the signature to the start of our patch area.
	int iCapOffset = GameConfGetOffset(hGameConf, "CappingOffset");
	if (iCapOffset == -1)
	{
		CloseHandle(hGameConf);
		SetFailState("Can't find CappingOffset in gamedata.");
	}
	
	// Move right in front of the instructions we want to NOP.
	iAddr += Address:iCapOffset;
	g_iPatchAddress = iAddr;
	
	// Get how many bytes we want to NOP.
	g_iPatchRestoreBytes = GameConfGetOffset(hGameConf, "PatchBytes");
	if (g_iPatchRestoreBytes == -1)
	{
		CloseHandle(hGameConf);
		SetFailState("Can't find PatchBytes in gamedata.");
	}
	CloseHandle(hGameConf);
	
	//PrintToServer("CGameMovement::WalkMove VectorScale(wishvel, mv->m_flMaxSpeed/wishspeed, wishvel); ... at address %x", g_iPatchAddress);
	
	int iData;
	for (new i = 0; i < g_iPatchRestoreBytes; i++)
	{
		// Save the current instructions, so we can restore them on unload.
		iData = LoadFromAddress(iAddr, NumberType_Int8);
		g_iPatchRestore[i] = iData;
		
		//PrintToServer("%x: %x", iAddr, iData);
		
		// NOP
		StoreToAddress(iAddr, 0x90, NumberType_Int8);
		
		iAddr++;
	}
	
	iBaseVelocityOffset = FindSendPropInfo("CBasePlayer","m_vecBaseVelocity");
	
	#if defined INFLUX_TIMER
	/*
		Modes settings
	*/
	// Set all modes to have custom settings disabled
	for (int mode = 0; mode < MAX_MODES; mode++)
	{
		g_bCustomSettingsEnable[mode] = false;
	}
	
	// LEGIT HARD
	g_bCustomSettingsEnable[MODE_LEGITHARD] = true;
	g_bOnGroundDragEnable[MODE_LEGITHARD] = true;
	g_iOnGroundMaxFramesBeforeDrag[MODE_LEGITHARD] = 1;
	g_iOnGroundMaxFramesBeforeFrictionMax[MODE_LEGITHARD] = 32;
	g_flOnGroundFriction[MODE_LEGITHARD] = 0.05;
	g_bSpeedMaxEnable[MODE_LEGITHARD] = true;
	g_flSpeedMaxSpeed[MODE_LEGITHARD] = 276.0; // Max ground speed (prestrafe)
	
	RegConsoleCmd("sm_dragenabled", Cmd_DragEnabled);
	RegConsoleCmd("sm_drag", Cmd_Drag);
	RegConsoleCmd("sm_friction", Cmd_Friction);
	RegConsoleCmd("sm_groundfriction", Cmd_GroundFriction);
	RegConsoleCmd("sm_prespeed", Cmd_MaxSpeed);
	RegConsoleCmd("sm_settings", Cmd_Settings);
	#endif
}

public Action Cmd_Settings(int client, int args)
{
	PrintToChat(client, "Drag Enabled: %s", (g_bOnGroundDragEnable[MODE_LEGITHARD] ? "yes" : "no"));
	PrintToChat(client, "Max Ground Frames Before Drag: %i", g_iOnGroundMaxFramesBeforeDrag[MODE_LEGITHARD]);
	PrintToChat(client, "Max Ground Frames Before Friction Max: %i", g_iOnGroundMaxFramesBeforeFrictionMax[MODE_LEGITHARD]);
	PrintToChat(client, "On Ground Friction: %.4f", g_flOnGroundFriction[MODE_LEGITHARD]);
	PrintToChat(client, "Prespeed: %.4f", g_flSpeedMaxSpeed[MODE_LEGITHARD]);
	
	return Plugin_Handled;
}

public Action Cmd_DragEnabled(int client, int args)
{
	g_bOnGroundDragEnable[MODE_LEGITHARD] = (args == 0);
	
	return Plugin_Handled;
}

public Action Cmd_Drag(int client, int args)
{
	char szArg[128];
	GetCmdArg(1, szArg, 128);
	int arg = StringToInt(szArg);
	g_iOnGroundMaxFramesBeforeDrag[MODE_LEGITHARD] = arg;
	PrintToChat(client, "Changed to %i", arg);
	
	return Plugin_Handled;
}

public Action Cmd_Friction(int client, int args)
{
	char szArg[128];
	GetCmdArg(1, szArg, 128);
	int arg = StringToInt(szArg);
	g_iOnGroundMaxFramesBeforeFrictionMax[MODE_LEGITHARD] = arg;
	PrintToChat(client, "Changed to %i", arg);
	
	return Plugin_Handled;
}

public Action Cmd_GroundFriction(int client, int args)
{
	char szArg[128];
	GetCmdArg(1, szArg, 128);
	float arg = StringToFloat(szArg);
	g_flOnGroundFriction[MODE_LEGITHARD] = arg;
	PrintToChat(client, "Changed to %.1f", arg);
	
	return Plugin_Handled;
}

public Action Cmd_MaxSpeed(int client, int args)
{
	char szArg[128];
	GetCmdArg(1, szArg, 128);
	float arg = StringToFloat(szArg);
	g_flSpeedMaxSpeed[MODE_LEGITHARD] = arg;
	PrintToChat(client, "Changed to %.1f", arg);
	
	return Plugin_Handled;
}


public OnPluginEnd()
{
	// Restore the original instructions, if we patched them.
	if (g_iPatchAddress != Address_Null)
	{
		for (new i = 0; i < g_iPatchRestoreBytes; i++)
		{
			StoreToAddress(g_iPatchAddress + Address:i, g_iPatchRestore[i], NumberType_Int8);
		}
	}
}

#if defined INFLUX_TIMER
/*
	Handle InfTimer stuff
	
	Thank based eddy
*/

// Clear everything
public OnClientConnected(int client)
{
	g_bOnGround[client] = false;
	g_iOnGroundPlayerFrames[client] = 0;
	g_bOnGroundPlayerMaxSpeed[client] = false;
	g_bOnGroundPlayerHasKeyPress[client] = false;
	g_iOnGroundPlayerButtons[client] = 0;
}

// Store buttons on player input
public Action OnPlayerRunCmd(client, &buttons, &impulse, float vel[3], float angles[3], &weapon)
{
	g_iOnGroundPlayerButtons[client] = buttons;
}

// Reused variables for eddys drag tools
float nrGroundFramesAsFloat;
float dragFactorExp;
float dragFactor;
float speedDragFactor;
float playerVelocityVec[3];
float playerVelocity;

public ProcessClient(i)
{
	if (GetEntityFlags(i) & FL_ONGROUND) {
		g_iOnGroundPlayerFrames[i]++;
	} else {
		g_iOnGroundPlayerFrames[i] = 0;
	}
	
	int iMode = Influx_GetClientMode(i);
	
	if (!g_bCustomSettingsEnable[iMode])return;
	
	// Only actually start doing something when a player is on ground
	if (g_iOnGroundPlayerFrames[i] > g_iOnGroundMaxFramesBeforeDrag[iMode]) {
		GetEntPropVector(i, Prop_Data, "m_vecVelocity", playerVelocityVec);
		playerVelocity = playerVelocityVec[0] * playerVelocityVec[0] + playerVelocityVec[1] * playerVelocityVec[1];
		
		// Now only start doing something if the players velocity is higher than 10*10
		if (playerVelocity > 100.0) {
			// Only take square root if velocity is > 10 units
			playerVelocity = SquareRoot(playerVelocity);
			
			if (g_iOnGroundPlayerButtons[i] & IN_MOVELEFT || 
				g_iOnGroundPlayerButtons[i] & IN_BACK || 
				g_iOnGroundPlayerButtons[i] & IN_MOVERIGHT || 
				g_iOnGroundPlayerButtons[i] & IN_FORWARD) {
				g_bOnGroundPlayerHasKeyPress[i] = true;
			} else {
				g_bOnGroundPlayerHasKeyPress[i] = false;
			}
			
			if (playerVelocity > g_flSpeedMaxSpeed[iMode]) {
				g_bOnGroundPlayerMaxSpeed[i] = true;
			} else {
				g_bOnGroundPlayerMaxSpeed[i] = false;
			}
			
			dragFactor = 0.0;
			if (g_bOnGroundDragEnable[iMode] && !g_bOnGroundPlayerHasKeyPress[i])
			{
				// Drag amount is based on the number of frames that there is no movement
				// this way the drag is always of similar length based on the number of ticks passed
				nrGroundFramesAsFloat = float(g_iOnGroundPlayerFrames[i]);
				dragFactorExp = Exponential(nrGroundFramesAsFloat / g_iOnGroundMaxFramesBeforeFrictionMax[iMode]);
				
				// Clamp our exponential drag force
				if (dragFactorExp > 1.0)
					dragFactorExp = 1.0;
				
				// Apply inverse drag based on current velocity as a boost
				dragFactor = g_flOnGroundFriction[iMode] * dragFactorExp;
			}
			
			// If there is a max speed set, set the inverse drag to the difference to the current velocity
			if (g_bSpeedMaxEnable[iMode] && g_bOnGroundPlayerMaxSpeed[i])
			{
				//speedDragFactor = (1  - 3) /  1; =  2/1 = 2 nothing
				//speedDragFactor = (6  - 3) /  6; = -1/2 = 2 slow down
				//speedDragFactor = (12 - 3) / 12; = -3/4 = 2 slow down much
				speedDragFactor = (playerVelocity - g_flSpeedMaxSpeed[iMode]) / (playerVelocity * (1 + g_iBadFrameDragCoefficient[iMode]));
				
				if (speedDragFactor > dragFactor)
					dragFactor = speedDragFactor;
			}
			
			// Apply the inverse drag factor
			dragFactor *= -1.0;
			playerVelocityVec[0] = dragFactor * playerVelocityVec[0];
			playerVelocityVec[1] = dragFactor * playerVelocityVec[1];
			SetEntDataVector(i, iBaseVelocityOffset, playerVelocityVec, true);
		}
	}
}

// On game time apply either drag or maxpeed or both
public OnGameFrame()
{
	for (new i = 1; i <= MaxClients; i++) {
		if (IsClientConnected(i) && 
			IsClientInGame(i) && 
			!IsFakeClient(i) && 
			!IsClientObserver(i))
		{
			ProcessClient(i);
		}
	}
}
#endif
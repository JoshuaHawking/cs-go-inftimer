#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <influx/core>

#include <msharedutil/misc>

#pragma newdecls required

#define INFLUX_SERVERS_FILE		"influx_servers.cfg"

enum Server {
	ServerID, 
	String:Name[32], 
	String:IP[32]
}

ArrayList g_hServers;
Handle g_hServerTemplate[Server];

float g_flLastServersList[INF_MAXPLAYERS];

public Plugin myinfo = 
{
	name = "Server Listing", 
	author = "Fusion", 
	description = "!hop / !servers", 
	version = "1.0", 
	url = ""
};

public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegConsoleCmd("sm_servers", Cmd_Servers);
	RegConsoleCmd("sm_hop", Cmd_Servers);
	
	// Load ranks into cache
	if (!LoadServers())
	{
		SetFailState(INF_CON_PRE..."Couldn't load servers!");
	}
}

bool LoadServers()
{
	char szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "configs");
	
	if (!DirExistsEx(szPath))return false;
	
	Format(szPath, sizeof(szPath), "%s/%s", szPath, INFLUX_SERVERS_FILE);
	
	KeyValues kv = new KeyValues("Servers");
	kv.ImportFromFile(szPath);
	
	if (!kv.GotoFirstSubKey())
	{
		return false;
	}
	
	g_hServers = CreateArray(sizeof(g_hServerTemplate));
	
	do
	{
		Handle hTemplate[Server];
		
		hTemplate[ServerID] = kv.GetNum("ServerID", 0);
		kv.GetString("Name", hTemplate[Name], sizeof(hTemplate[Name]));
		kv.GetString("IP", hTemplate[IP], sizeof(hTemplate[IP]));
		
		PushArrayArray(g_hServers, hTemplate[ServerID]);
	} while (kv.GotoNextKey());
	
	delete kv;
	return true;
}

public Action Cmd_Servers(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (Inf_HandleCmdSpam(client, 1.0, g_flLastServersList[client], true))
	{
		return Plugin_Handled;
	}
	
	Menu_Servers(client);
	
	return Plugin_Handled;
}

public void Menu_Servers(int client)
{
	// Create menu
	Menu menu = new Menu(Hndlr_Servers);
	menu.SetTitle("House Of Climb servers");
	
	for (int server = 0; server < GetArraySize(g_hServers); server++)
	{
		Handle hServer[Server];
		GetArrayArray(g_hServers, server, hServer[ServerID]);
		
		char szInfo[16];
		FormatEx(szInfo, sizeof(szInfo), "%i", server);
		
		menu.AddItem(szInfo, hServer[Name]);
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int Hndlr_Servers(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	GetMenuItem(menu, index, szInfo, sizeof(szInfo));
	
	int server = StringToInt(szInfo);
	
	Handle hServer[Server];
	GetArrayArray(g_hServers, server, hServer[ServerID]);
	
	// Print connect info
	PrintToConsole(client, "");
	PrintToConsole(client, "");
	PrintToConsole(client, "-------------------- HoC Server Info --------------------------");
	PrintToConsole(client, "You are about to join %s! Type/copy in console:", hServer[Name]);
	PrintToConsole(client, "");
	PrintToConsole(client, "connect %s", hServer[IP]);
	PrintToConsole(client, "");
	PrintToConsole(client, "Have fun!");
	PrintToConsole(client, "----------------------------------------------------------------");
	PrintToConsole(client, "");
	PrintToConsole(client, "");
	
	Influx_PrintToChat(_, client, "%t", "CONNECTIONINFO_CONSOLE1");
	Influx_PrintToChat(_, client, "%t", "CONNECTIONINFO_CONSOLE2");
	
	return 0;
	
} 
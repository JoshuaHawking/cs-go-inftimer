#include <sourcemod>
#include <cstrike>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

ConVar g_nAllowThirdPerson;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Style - Third Person", 
	description = "", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	// CMDS
	RegConsoleCmd("sm_thirdperson", Cmd_Style_ThirdPerson, INF_NAME..." - Change your style to 3rd person.");
	RegConsoleCmd("sm_3rd", Cmd_Style_ThirdPerson, "");
	RegConsoleCmd("sm_3rdperson", Cmd_Style_ThirdPerson, "");
	
	g_nAllowThirdPerson = FindConVar("sv_allow_thirdperson");
	if (g_nAllowThirdPerson == INVALID_HANDLE)
		SetFailState("sv_allow_thirdperson not found!");
	
	SetConVarInt(g_nAllowThirdPerson, 1);
	
	// Hook events
	HookEvent("player_death", Player_Death, EventHookMode_Pre);
	HookEvent("player_spawn", Player_Spawn);
	HookEvent("player_team", Player_Team);
}

public ConVarChanged(Handle cvar, const char[] oldVal, const char[] newVal)
{
	if (cvar == g_nAllowThirdPerson)
	{
		if (StringToInt(newVal) != 1)
			SetConVarInt(g_nAllowThirdPerson, 1);
	}
}

public void OnAllPluginsLoaded()
{
	if (!Influx_AddStyle(STYLE_THIRDPERSON, "Third Person", "3rd", false))
	{
		SetFailState(INF_CON_PRE..."Couldn't add style!");
	}
}

public void OnPluginEnd()
{
	Influx_RemoveStyle(STYLE_THIRDPERSON);
}

public void Influx_OnRequestStyles()
{
	OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
    Influx_AddStyleInfo(STYLE_THIRDPERSON, "3rd Person consists of you in a 3rd Person camera (over the shoulder)");
}

public Action Influx_OnSearchType(const char[] szArg, Search_t &type, int &value)
{
	if (StrEqual(szArg, "thirdperson", false))
	{
		value = STYLE_THIRDPERSON;
		type = SEARCH_STYLE;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

public Action Cmd_Style_ThirdPerson(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	Influx_SetClientStyle(client, STYLE_THIRDPERSON);
	
	return Plugin_Handled;
}

public Action Influx_OnCheckClientStyle(int client, int style, float vel[3])
{
	if (style != STYLE_THIRDPERSON)return Plugin_Continue;
	
	return Plugin_Stop;
}

public void Influx_OnClientStyleChangePost(int client, int style)
{
	if (style != STYLE_THIRDPERSON)
	{
		ClientCommand(client, "firstperson");
		return;
	}
	
	// Change camera
	ClientCommand(client, "thirdperson");
	
	return;
}

public Action Player_Spawn(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (Influx_GetClientStyle(client) == STYLE_THIRDPERSON) {
		ClientCommand(client, "thirdperson");
	}
}

public Action Player_Death(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (Influx_GetClientStyle(client) == STYLE_THIRDPERSON) {
		ClientCommand(client, "firstperson");
	}
}

public Action Player_Team(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	int team = GetEventInt(event, "team");
	if (Influx_GetClientStyle(client) == STYLE_THIRDPERSON) {
		if(team == CS_TEAM_SPECTATOR)
		{
			ClientCommand(client, "firstperson");
		}
		else
		{
			ClientCommand(client, "thirdperson");
		}
	}
}
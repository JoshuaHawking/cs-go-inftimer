#include <sourcemod>

#include <influx/core>
#include <chat-processor>

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - blocks !knife / !ws", 
	description = "Tells people they can't use !knife / !ws", 
	version = INF_VERSION
};

public OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES); 
	
	RegConsoleCmd("sm_knife", Cmd_BlockedByValve);
	RegConsoleCmd("sm_weaponskins", Cmd_BlockedByValve);
	RegConsoleCmd("sm_ws", Cmd_BlockedByValve);
}

public Action Cmd_BlockedByValve(int client, int args)
{
	if (client)
	{
		Influx_PrintToChat(_, client, "%t", "BLOCKEDBYVALVE");
	}
	
	return Plugin_Handled;
} 
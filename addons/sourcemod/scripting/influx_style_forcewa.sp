#include <sourcemod>
#include <sdktools>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Style - Force WA",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    // CMDS
    RegConsoleCmd( "sm_waonly", Cmd_Style_ForceWA, "Change your style to Force WA." );
    RegConsoleCmd( "sm_forcewa", Cmd_Style_ForceWA, "" );
    RegConsoleCmd( "sm_wa", Cmd_Style_ForceWA, "" );
}

public void OnAllPluginsLoaded()
{
    if ( !Influx_AddStyle( STYLE_FORCEWA, "Force WA", "WA" ) )
    {
        SetFailState( INF_CON_PRE..."Couldn't add style!" );
    }
}

public void OnPluginEnd()
{
    Influx_RemoveStyle( STYLE_FORCEWA );
}

public void Influx_OnRequestStyles()
{
    OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
    Influx_AddStyleInfo(STYLE_FORCEWA, "Force WA means you can only use your W and A keys.");
}

public Action Influx_OnSearchType( const char[] szArg, Search_t &type, int &value )
{
    if (StrEqual( szArg, "waonly", false )
    ||  StrEqual( szArg, "forcewa", false ))
    {
        value = STYLE_FORCEWA;
        type = SEARCH_STYLE;
        
        return Plugin_Stop;
    }
    
    return Plugin_Continue;
}

public Action Cmd_Style_ForceWA( int client, int args )
{
    if ( !client ) return Plugin_Handled;
    
    
    Influx_SetClientStyle( client, STYLE_FORCEWA );
    
    return Plugin_Handled;
}

public Action Influx_OnCheckClientStyle( int client, int style, float vel[3] )
{
    if ( style != STYLE_FORCEWA ) return Plugin_Continue;
    
#define FWD     0
#define SIDE    1
    
    // If they are not holding W and A
    if ( !(vel[FWD] > 0.0) || !(vel[SIDE] < 0.0) )
    {
        vel[SIDE] = 0.0;
        vel[FWD] = 0.0;
    }
    
    // Holding D
    if ( vel[SIDE] > 0.0 )
    {
        vel[SIDE] = 0.0;
    }
    
    // Holding S
    if ( vel[FWD] < 0.0 )
    {
        vel[FWD] = 0.0;
    }
    
    return Plugin_Stop;
}
#pragma semicolon 1

#include <sourcemod>
#include <sdktools>

#define VOTE_NO "no"
#define VOTE_YES "yes"

public Plugin myinfo = 
{
	name = "VoteExtend", 
	author = "Fusion / Immortal", 
	description = "Allows VIPs / Admins to offer a vote for extending the map.", 
	version = "1.0", 
	url = "www.houseofclimb.com"
};

// Based on Immortal's code, cleaned up by Fusion
Handle g_hVoteMenu;
int g_iVoteMinutes;

ConVar g_ConVar_PercentageNeeded;

public void OnPluginStart()
{
	LoadTranslations("common.phrases");
	LoadTranslations("basevotes.phrases");
	
	RegAdminCmd("sm_voteextend", Cmd_VoteExtend, ADMFLAG_RESERVATION);
	
	g_ConVar_PercentageNeeded = CreateConVar("sm_voteextend_needed", "0.60", "Percentage of votes required for successful extend vote.", 0, true, 0.05, true, 1.0);
}

public Action Cmd_VoteExtend(int client, int args)
{
	// Check whether correct amount of arguments has been passed in.
	if (args < 1) {
		ReplyToCommand(client, "[SM] Usage: sm_voteextend <minutes>");
		return Plugin_Handled;
	}
	
	// Check whether vote is currently in progress
	if (IsVoteInProgress()) {
		ReplyToCommand(client, "[SM] %t", "Vote in Progress");
		return Plugin_Handled;
	}
	
	// Check whether we can display a menu yet.
	if (!TestVoteDelay(client)) {
		return Plugin_Handled;
	}
	
	int iMinutes;
	char szText[256];
	GetCmdArg(1, szText, sizeof(szText)); // Get input
	iMinutes = StringToInt(szText); // Convert to int
	
	// Check that the value is more than 0, or that it is a valid input
	if (iMinutes <= 0) {
		ReplyToCommand(client, "[SM] Invalid value - must be at least 1 minute or a valid integer.");
		return Plugin_Handled;
	}
	
	// Log extend vote
	LogAction(client, -1, "\"%L\" initiated a extend vote %i minutes.", client, iMinutes);
	ShowActivity2(client, "[SM] ", "Initiated a extend vote for %i minutes", iMinutes);
	
	// Create vote menu
	g_hVoteMenu = new Menu(Handle_VoteCallback);
	
	SetMenuTitle(g_hVoteMenu, "Extend map by %i minute(s)?", iMinutes);
	AddMenuItem(g_hVoteMenu, VOTE_YES, "Yes");
	AddMenuItem(g_hVoteMenu, VOTE_NO, "No");
	SetMenuExitButton(g_hVoteMenu, false);
	
	g_iVoteMinutes = iMinutes;
	
	// Display menu to all
	VoteMenuToAll(g_hVoteMenu, 20);
	
	return Plugin_Handled;
}

// Vote callback
public Handle_VoteCallback(Handle menu, MenuAction action, param1, param2)
{
	// Check whether to close menus
	if (action == MenuAction_End) {
		VoteMenuClose();
	}
	// Translation support
	else if (action == MenuAction_DisplayItem) {
		char szDisplay[64];
		GetMenuItem(menu, param2, "", 0, _, szDisplay, sizeof(szDisplay));
		
		if (strcmp(szDisplay, "No") == 0 || strcmp(szDisplay, "Yes") == 0) {
			char szBuffer[255];
			Format(szBuffer, sizeof(szBuffer), "%T", szDisplay, param1);
			return RedrawMenuItem(szBuffer);
		}
	}
	// Check whether the vote has been cancelled because no votes have been casted
	else if (action == MenuAction_VoteCancel && param1 == VoteCancel_NoVotes) {
		PrintToChatAll("[SM] %t", "No Votes Cast");
	}
	// End of the vote
	else if (action == MenuAction_VoteEnd) {
		char szItem[64];
		char szDisplay[64];
		float flPercentage, flLimit;
		int iVotes, iTotalVotes;
		
		// Get vote info
		GetMenuVoteInfo(param2, iVotes, iTotalVotes);
		GetMenuItem(menu, param1, szItem, sizeof(szItem), _, szDisplay, sizeof(szDisplay)); // Gets the winning item, 0 = yes, 1 = no
		
		flPercentage = GetVotePercent(iVotes, iTotalVotes);
		flLimit = GetConVarFloat(g_ConVar_PercentageNeeded);
		
		if (
			(strcmp(szItem, VOTE_YES) == 0 && param1 == 0 && FloatCompare(flPercentage, flLimit) < 0) // Check whether winning option was YES & that the percentage of people that voted for it is larger than the required percentage.
			 || (strcmp(szItem, VOTE_NO) == 0 && param1 == 1) // OR whether the winning option was no
			) {
			// Alert that the vote has failed.
			LogAction(-1, -1, "Vote failed for sm_voteextend.");
			PrintToChatAll("[SM] %t", "Vote Failed", RoundToNearest(100.0 * flLimit), RoundToNearest(100.0 * flPercentage), iTotalVotes);
		}
		else
		{
			// Alert that vote was successful
			PrintToChatAll("[SM] %t", "Vote Successful", RoundToNearest(100.0 * flPercentage), iTotalVotes);
			
			
			LogAction(-1, -1, "Time limit extend vote (%i minutes) was successful", g_iVoteMinutes);
			PrintToChatAll("[SM] Map time extended for %i minutes", g_iVoteMinutes);
			
			// Change time limit
			Handle hTimeLimit = FindConVar("mp_timelimit");
			
			if (hTimeLimit != INVALID_HANDLE) {
				int iNewTime = GetConVarInt(hTimeLimit) + g_iVoteMinutes;
				SetConVarInt(hTimeLimit, iNewTime, false, false);
			}
		}
	}
	
	return 0;
}

// Closes all the menu handles and sets global variable to INVALID_HANDLE.
VoteMenuClose()
{
	CloseHandle(g_hVoteMenu);
	g_hVoteMenu = INVALID_HANDLE;
	
	g_iVoteMinutes = 0;
}

float GetVotePercent(iVotes, iTotalVotes)
{
	return FloatDiv(float(iVotes), float(iTotalVotes));
}

// Checks whether we should display a menu. 
// "Returns the number of seconds you should "wait" before displaying a publicly invocable menu." - SourceMod wiki
bool TestVoteDelay(client)
{
	int delay = CheckVoteDelay();
	
	if (delay > 0) {
		if (delay > 60) {
			ReplyToCommand(client, "[SM] %t", "Vote Delay Minutes", delay % 60);
		}
		else
		{
			ReplyToCommand(client, "[SM] %t", "Vote Delay Seconds", delay);
		}
		return false;
	}
	return true;
} 
#include <sourcemod>
#include <sdkhooks>

#include <influx/core>
#include <influx/stocks_core>


#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

//#define DEBUG_THINK


float g_flAirAccelerate = 1000.0;

// CONVARS
ConVar g_ConVar_AirAccelerate;
ConVar g_ConVar_EnableBunnyhopping;

ConVar g_ConVar_Legit_AirAccelerate;

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Mode - Legit Hard",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    // CONVARS
    if ( (g_ConVar_AirAccelerate = FindConVar( "sv_airaccelerate" )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't find handle for sv_airaccelerate!" );
    }
    
    if ( (g_ConVar_EnableBunnyhopping = FindConVar( "sv_enablebunnyhopping" )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't find handle for sv_enablebunnyhopping!" );
    }
    
    
    g_ConVar_Legit_AirAccelerate = CreateConVar( "influx_legithard_airaccelerate", "1000", "", FCVAR_NOTIFY );
    g_ConVar_Legit_AirAccelerate.AddChangeHook( E_CvarChange_Legit_AA );
    
    AutoExecConfig( true, "mode_legit", "influx" );
    
    
    // CMDS
    RegConsoleCmd( "sm_scrollhard", Cmd_Mode_Scroll, INF_NAME..." - Change your mode to Legit." );
    RegConsoleCmd( "sm_scrllhard", Cmd_Mode_Scroll, "" );
    RegConsoleCmd( "sm_scrlhard", Cmd_Mode_Scroll, "" );
    RegConsoleCmd( "sm_legithard", Cmd_Mode_Scroll, "" );
    
}

public void OnAllPluginsLoaded()
{
    AddMode();
}

public void OnPluginEnd()
{
    Influx_RemoveMode( MODE_LEGITHARD );
}
public void Influx_OnRequestModes()
{
    AddMode();
}

public void Influx_RequestModeInfo()
{
    Influx_AddModeInfo(MODE_LEGITHARD, "Legit requires you to use your scroll wheel in order to jump and land hops. Some may find this mode harder than auto.");
}

stock void AddMode()
{
    if ( !Influx_AddMode( MODE_LEGITHARD, "Legit Hard", "LEGITHARD" ) )
    {
        SetFailState( INF_CON_PRE..."Couldn't add mode! (%i)", MODE_LEGITHARD );
    }
}

public Action Influx_OnClientModeChange( int client, int mode, int lastmode )
{
    if ( mode == MODE_LEGITHARD )
    {
        if ( !Inf_SDKHook( client, SDKHook_PreThinkPost, E_PreThinkPost_Client ) )
        {
            return Plugin_Handled;
        }
        
        Inf_SendConVarValueFloat( client, g_ConVar_AirAccelerate, g_ConVar_Legit_AirAccelerate.FloatValue );
        Inf_SendConVarValueBool( client, g_ConVar_EnableBunnyhopping, true );
    }
    else if ( lastmode == MODE_LEGITHARD )
    {
        UnhookThinks( client );
    }
    
    return Plugin_Continue;
}

stock void UnhookThinks( int client )
{
    SDKUnhook( client, SDKHook_PreThinkPost, E_PreThinkPost_Client );
}

public Action Influx_OnSearchType( const char[] szArg, Search_t &type, int &value )
{
    if (StrEqual( szArg, "legit", false )
    ||  StrEqual( szArg, "scroll", false ) )
    {
        value = MODE_LEGIT;
        type = SEARCH_MODE;
        
        return Plugin_Stop;
    }
    
    return Plugin_Continue;
}

public void E_CvarChange_Legit_AA( ConVar convar, const char[] oldval, const char[] newval )
{
    g_flAirAccelerate = convar.FloatValue;
}

public void E_PreThinkPost_Client( int client )
{
#if defined DEBUG_THINK
    PrintToServer( INF_DEBUG_PRE..."PreThinkPost - Scroll (aa: %.0f)", g_flAirAccelerate );
#endif
    
    g_ConVar_AirAccelerate.FloatValue = g_flAirAccelerate;
    g_ConVar_EnableBunnyhopping.BoolValue = true;
}

public Action Cmd_Mode_Scroll( int client, int args )
{
    if ( !client ) return Plugin_Handled;
    
    
    Influx_SetClientMode( client, MODE_LEGITHARD );
    
    return Plugin_Handled;
}
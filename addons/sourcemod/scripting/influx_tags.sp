#include <sourcemod>
#include <cstrike>

#include <influx/core>
#include <influx/tags>

#include <msharedutil/misc>
#include <chat-processor>

#undef REQUIRE_PLUGIN

#define TAGLENGTH 			24
#define COLOURLENGTH 		4

#define MAX_BLOCKEDWORDS 	512
#define BLOCKEDLENGTH		32

char g_szClientTag[INF_MAXPLAYERS][TAGLENGTH];
int g_iClientTagColour[INF_MAXPLAYERS];
int g_iClientChatColour[INF_MAXPLAYERS];
int g_iClientNameColour[INF_MAXPLAYERS];

char g_szClientExternalTag[INF_MAXPLAYERS][TAGLENGTH];

char g_szBlockedWords[MAX_BLOCKEDWORDS][BLOCKEDLENGTH];
int g_iBlockedWords;

char szColours[][][] = 
{
	{ "White", "\x01" }, 
	{ "Team Colours", "\x03" }, 
	{ "Red", "\x02" }, 
	{ "Light Red", "\x07" }, 
	{ "Lighter Red", "\x0F" }, 
	{ "Green", "\x04" }, 
	{ "Light Green", "\x05" }, 
	{ "Lime Green", "\x06" }, 
	{ "Blue", "\x0C" }, 
	{ "Sky Blue", "\x0B" }, 
	{ "Light Yellow", "\x09" }, 
	{ "Gold", "\x10" }, 
	{ "Pink", "\x0E" }, 
	{ "Grey", "\x08" }
}

float g_flLastSetTagTime[INF_MAXPLAYERS];
float g_flLastTagsTime[INF_MAXPLAYERS];

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Tags", 
	description = "!tags", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	RegPluginLibrary(INFLUX_LIB_TAGS);
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegAdminCmd("sm_tags", Cmd_Tags, ADMFLAG_RESERVATION);
	RegAdminCmd("sm_settag", Cmd_SetTag, ADMFLAG_RESERVATION);
	
	// NATIVES
	CreateNative("Influx_SetExternalChatTag", Native_SetExternalChatTag);
	CreateNative("Influx_GetExternalChatTag", Native_GetExternalChatTag);
	
	return APLRes_Success;
}

public void OnAllPluginsLoaded()
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	SQL_TQuery(db, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...INF_TABLE_TAGS..." (uid INTEGER PRIMARY KEY, tag VARCHAR(24), tagcolour INTEGER DEFAULT 0, namecolour INTEGER DEFAULT 0, chatcolour INTEGER DEFAULT 0)", _, DBPrio_High);
	
	if (!LoadBlockedWords())
	{
		SetFailState(INF_CON_PRE..."Failed to load blocked words!");
	}
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data)
{
}

public void OnClientConnected(int client)
{
	g_szClientTag[client] = "";
	g_szClientExternalTag[client] = "";
	g_iClientTagColour[client] = 0;
	g_iClientNameColour[client] = 0;
	g_iClientChatColour[client] = 0;
	g_flLastSetTagTime[client] = 0.0;
	g_flLastTagsTime[client] = 0.0;
}

public void Influx_OnClientIdRetrieved(int client)
{
	CreateTimer(5.0, LoadTagInfo, GetClientUserId(client), _);
}

public Action LoadTagInfo(Handle timer, int userId)
{
	int client;
	client = GetClientOfUserId(userId);
	
	if (!IsClientInGame(client))
	{
		return Plugin_Handled;
	}
	
	DB_GetClientTagInfo(client);
	
	return Plugin_Handled;
}

bool LoadBlockedWords()
{
	
	char szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "configs");
	
	if (!DirExists(szPath))return false;
	
	Format(szPath, sizeof(szPath), "%s/%s.cfg", szPath, INF_BLOCKED_FILE);
	
	Handle hFile = OpenFile(szPath, "rt");
	
	if (hFile == INVALID_HANDLE) {
		return false;
	}
	
	char szLine[BLOCKEDLENGTH];
	
	int iLine = 0;
	while (!IsEndOfFile(hFile) && ReadFileLine(hFile, szLine, sizeof(szLine)))
	{
		if (iLine >= MAX_BLOCKEDWORDS)break;
		
		int iLineLength;
		
		iLineLength = strlen(szLine);
		
		// Remove line ending
		if (szLine[iLineLength - 1] == '\n') {
			szLine[--iLineLength] = '\0';
		}
		
		if (szLine[0] == '\0')continue;
		
		strcopy(g_szBlockedWords[iLine], BLOCKEDLENGTH, szLine);
		
		iLine++;
	}
	
	g_iBlockedWords = iLine;
	return true;
}
/*
	Commands
*/
public Action Cmd_Tags(int client, int args)
{
	if (Inf_HandleCmdSpam(client, 5.0, g_flLastTagsTime[client], true))
	{
		return Plugin_Handled;
	}
	
	Menu_Tags(client);
	
	return Plugin_Handled;
}

public Action Cmd_SetTag(int client, int args)
{
	if (Inf_HandleCmdSpam(client, 2.5, g_flLastSetTagTime[client], true))
	{
		return Plugin_Handled;
	}
	
	// Check valid client.
	if (!client)
	{
		return Plugin_Handled;
	}
	
	// Build tag
	char arg[128];
	char szTag[TAGLENGTH];
	
	for (int i = 1; i <= args; i++)
	{
		GetCmdArg(i, arg, sizeof(arg));
		Format(szTag, sizeof(szTag), "%s %s", szTag, arg);
	}
	
	TrimString(szTag);
	
	// Check for blocked words unless they are admin
	if (!CheckCommandAccess(client, "sm_admin", ADMFLAG_GENERIC))
	{
		for (int i = 0; i < g_iBlockedWords; i++)
		{
			if (StrContains(szTag, g_szBlockedWords[i], false) != -1)
			{
				Influx_PrintToChat(_, client, "%t", "TAG_BLOCKED");
				return Plugin_Handled;
			}
		}
	}
	
	DB_SetClientTag(client, szTag);
	
	return Plugin_Handled;
}

/*
	Menus
*/

bool Menu_Tags(int client)
{
	char temp[128];
	
	// Build menu
	Menu menu = new Menu(Hndlr_MainMenu);
	FormatEx(temp, sizeof(temp), "Tags Menu\n \nUse !settag to set your tag:\n!settag VIP\n(%i Character Max)\n ", TAGLENGTH);
	menu.SetTitle(temp);
	menu.AddItem("cleartag", "Clear Tag");
	menu.AddItem("tagcolour", "Tag Colour");
	menu.AddItem("namecolour", "Name Colour");
	menu.AddItem("chatcolour", "Chat Colour");
	
	if (CheckCommandAccess(client, "sm_admin", ADMFLAG_GENERIC))
	{
		menu.AddItem("playersetup", "Check Player Setup");
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_MainMenu(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	if (!IsClientInGame(client))return 0;
	
	char szInfo[32];
	GetMenuItem(menu, index, szInfo, sizeof(szInfo));
	
	// Set the users tag to nothing
	if (StrEqual(szInfo, "cleartag"))
	{
		DB_SetClientTag(client, "");
		Menu_Tags(client);
		return 0;
	}
	else if (StrEqual(szInfo, "tagcolour"))
	{
		Menu_SelectColour(client, "tags")
		return 0;
	}
	else if (StrEqual(szInfo, "namecolour"))
	{
		Menu_SelectColour(client, "name")
		return 0;
	}
	else if (StrEqual(szInfo, "chatcolour"))
	{
		Menu_SelectColour(client, "chat")
		return 0;
	}
	else if (StrEqual(szInfo, "playersetup"))
	{
		Menu_SelectPlayerSetup(client);
		return 0;
	}
	
	return 0;
}

bool Menu_SelectColour(int client, char property[16])
{
	// Build menu
	Menu menu = new Menu(Hndlr_SelectColour);
	menu.SetTitle("Select a colour");
	
	char szInfo[32];
	for (int i = 0; i < sizeof(szColours); i++)
	{
		FormatEx(szInfo, sizeof(szInfo), "colours_%s_%i", property, i);
		menu.AddItem(szInfo, szColours[i][0]);
	}
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_SelectColour(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to the main menu.
		Menu_Tags(client);
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[3][8];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int colour = StringToInt(buffer[2]);
	
	if (StrEqual(buffer[1], "tags"))
	{
		DB_SetClientTagColour(client, colour);
	}
	else if (StrEqual(buffer[1], "name"))
	{
		DB_SetClientNameColour(client, colour);
	}
	else if (StrEqual(buffer[1], "chat"))
	{
		DB_SetClientChatColour(client, colour);
	}
	
	Menu_Tags(client);
	
	return 0;
}

bool Menu_SelectPlayerSetup(int client)
{
	Menu menu = new Menu(Hndlr_SelectPlayerSetup);
	menu.SetTitle("Select a player to view their setup");
	
	char szInfo[32];
	char szPlayer[MAXLENGTH_NAME];
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if (!IsClientInGame(i))continue;
		
		if (IsFakeClient(i))continue;
		
		FormatEx(szInfo, sizeof(szInfo), "player_%i", i);
		
		GetClientName(i, szPlayer, sizeof(szPlayer));
		menu.AddItem(szInfo, szPlayer);
	}
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_SelectPlayerSetup(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to the main menu.
		Menu_Tags(client);
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[2][8];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int viewedClient = StringToInt(buffer[1])
	
	if (!IsClientInGame(viewedClient))return 0;
	
	if (IsFakeClient(viewedClient))return 0;
	
	Menu_PlayerSetup(client, viewedClient);
	
	return 0;
}

bool Menu_PlayerSetup(int client, int viewedClient)
{
	Menu menu = new Menu(Hndlr_PlayerSetup);
	char szDisplay[256];
	char szTemp[128];
	
	Inf_GetClientSteam(viewedClient, szTemp, sizeof(szTemp));
	
	Format(szDisplay, sizeof(szDisplay), "%N\n \nSteam ID: %s\nUser ID: %i\n \nTag: %s\nTag Colour: %s\nChat Colour: %s\nName Colour: %s\n ", 
		viewedClient, 
		szTemp, 
		Influx_GetClientId(viewedClient), 
		(g_szClientTag[viewedClient][0] == '\0' ? "None" : g_szClientTag[viewedClient]), 
		szColours[g_iClientTagColour[viewedClient]][0], 
		szColours[g_iClientChatColour[viewedClient]][0], 
		szColours[g_iClientNameColour[viewedClient]][0]
		);
	
	menu.SetTitle(szDisplay);
	
	char szInfo[32];
	
	FormatEx(szInfo, sizeof(szInfo), "clear_%i", viewedClient);
	
	menu.AddItem(szInfo, "Remove Setup");
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

// Main menu handler
public int Hndlr_PlayerSetup(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They have selected to go back, so go to the main menu.
		Menu_SelectPlayerSetup(client);
		return 0;
	}
	
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[2][8];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int viewedClient = StringToInt(buffer[1])
	
	if (!IsClientInGame(viewedClient))return 0;
	
	if (IsFakeClient(viewedClient))return 0;
	
	DB_SetClientTag(viewedClient, "");
	DB_SetClientTagColour(viewedClient, 0);
	DB_SetClientNameColour(viewedClient, 0);
	DB_SetClientChatColour(viewedClient, 0);
	
	Menu_SelectPlayerSetup(client);
	
	return 0;
}

/*
	Database Stuff
*/
stock bool DB_GetEscaped(char[] out, int len, const char[] def = "")
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	if (!SQL_EscapeString(db, out, out, len))
	{
		strcopy(out, len, def);
		
		return false;
	}
	
	return true;
}

DB_GetClientTagInfo(int client)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	char query[512];
	FormatEx(query, sizeof(query), "SELECT tag, tagcolour, chatcolour, namecolour FROM "...INF_TABLE_TAGS..." WHERE uid = %i", Influx_GetClientId(client));
	
	SQL_TQuery(db, Thrd_GetClientTagInfo, query, client, DBPrio_Low);
	return true;
}

public void Thrd_GetClientTagInfo(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "get tag info for client");
		return;
	}
	
	while (SQL_FetchRow(res))
	{
		SQL_FetchString(res, 0, g_szClientTag[client], sizeof(g_szClientTag[]));
		g_iClientTagColour[client] = SQL_FetchInt(res, 1);
		g_iClientChatColour[client] = SQL_FetchInt(res, 2);
		g_iClientNameColour[client] = SQL_FetchInt(res, 3);
	}
	
}

bool DB_SetClientTag(int client, char szTag[TAGLENGTH])
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	RemoveChars(szTag, "`'\"");
	DB_GetEscaped(szTag, (sizeof(szTag) * 2) + 1)
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...INF_TABLE_TAGS..." (uid, tag) VALUES(%i, '%s') ON DUPLICATE KEY UPDATE uid=%i, tag='%s';", Influx_GetClientId(client), szTag, Influx_GetClientId(client), szTag);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteString(szTag);
	
	SQL_TQuery(db, Thrd_SetClientTag, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientTag(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "update tag for client");
		return;
	}
	
	char szTag[TAGLENGTH];
	int client;
	
	data.Reset();
	client = data.ReadCell();
	data.ReadString(szTag, sizeof(szTag));
	
	g_szClientTag[client] = szTag;
	if (StrEqual(szTag, ""))
	{
		Influx_PrintToChat(_, client, "%t", "SETTAG_BLANK_SUCCESS", szTag);
	}
	else
	{
		Influx_PrintToChat(_, client, "%t", "SETTAG_SUCCESS", szTag, szColours[g_iClientTagColour[client]][1]);
	}
}

// Colour is int (index of szColours)
bool DB_SetClientTagColour(int client, int colour)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...INF_TABLE_TAGS..." (uid, tagcolour) VALUES(%i, %i) ON DUPLICATE KEY UPDATE uid=%i, tagcolour=%i;", Influx_GetClientId(client), colour, Influx_GetClientId(client), colour);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteCell(colour);
	
	SQL_TQuery(db, Thrd_SetClientTagColour, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientTagColour(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "update tag colour for client");
		return;
	}
	
	int client;
	int colour;
	
	data.Reset();
	client = data.ReadCell();
	colour = data.ReadCell();
	
	g_iClientTagColour[client] = colour;
	Influx_PrintToChat(_, client, "%t", "SETCOLOUR_SUCCESS", "tag", szColours[colour][1], szColours[colour][0]);
}

// Colour is int (index of szColours)
bool DB_SetClientNameColour(int client, int colour)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...INF_TABLE_TAGS..." (uid, namecolour) VALUES(%i, %i) ON DUPLICATE KEY UPDATE uid=%i, namecolour=%i;", Influx_GetClientId(client), colour, Influx_GetClientId(client), colour);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteCell(colour);
	
	SQL_TQuery(db, Thrd_SetClientNameColour, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientNameColour(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "update name colour for client");
		return;
	}
	
	int client;
	int colour;
	
	data.Reset();
	client = data.ReadCell();
	colour = data.ReadCell();
	
	g_iClientNameColour[client] = colour;
	Influx_PrintToChat(_, client, "%t", "SETCOLOUR_SUCCESS", "name", szColours[colour][1], szColours[colour][0]);
}

// Colour is int (index of szColours)
bool DB_SetClientChatColour(int client, int colour)
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...INF_TABLE_TAGS..." (uid, chatcolour) VALUES(%i, %i) ON DUPLICATE KEY UPDATE uid=%i, chatcolour=%i;", Influx_GetClientId(client), colour, Influx_GetClientId(client), colour);
	
	DataPack data = new DataPack();
	data.WriteCell(client);
	data.WriteCell(colour);
	
	SQL_TQuery(db, Thrd_SetClientChatColour, query, data, DBPrio_Low);
	return true;
}

public void Thrd_SetClientChatColour(Handle db, Handle res, const char[] szError, DataPack data)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "update chat colour for client");
		return;
	}
	
	int client;
	int colour;
	
	data.Reset();
	client = data.ReadCell();
	colour = data.ReadCell();
	
	g_iClientChatColour[client] = colour;
	Influx_PrintToChat(_, client, "%t", "SETCOLOUR_SUCCESS", "chat", szColours[colour][1], szColours[colour][0]);
}

/*
	Add Tags
*/
public Action OnChatMessage(int & author, ArrayList recipients, eChatFlags & flag, char[] name, char[] message, bool & bProcessColors, bool & bRemoveColors)
{
	char extra[12];
	GetExtraTag(author, flag, extra, sizeof(extra));
	
	bProcessColors = false;
	
	Format(name, MAXLENGTH_NAME, " %s%s%s%s%s%s{WHITE}%s%s{WHITE}", 
		g_szClientExternalTag[author], 
		(g_szClientExternalTag[author][0] == '\0' ? "" : " "), 
		szColours[g_iClientTagColour[author]][1], 
		g_szClientTag[author], 
		(g_szClientTag[author][0] == '\0' ? "" : " "), 
		extra, 
		szColours[g_iClientNameColour[author]][1], 
		name);
	Influx_FormatChatColors(name, MAXLENGTH_NAME);
	
	Format(message, MAXLENGTH_MESSAGE, "%s%s", 
		szColours[g_iClientChatColour[author]][1], 
		message
		);
	return Plugin_Changed;
}

GetExtraTag(int client, eChatFlags flag, char[] extra, int extralen)
{
	if (flag == ChatFlag_Team)
	{
		if (GetClientTeam(client) == CS_TEAM_CT)
		{
			strcopy(extra, extralen, "{TEAM}CT ");
		}
		else if (GetClientTeam(client) == CS_TEAM_T)
		{
			strcopy(extra, extralen, "{TEAM}T ");
		}
	}
	if (GetClientTeam(client) == CS_TEAM_SPECTATOR)
	{
		strcopy(extra, extralen, "{TEAM}SPEC ");
	}
}

/*
	Natives
*/
public int Native_SetExternalChatTag(Handle hPlugin, int nParams)
{
	GetNativeString(2, g_szClientExternalTag[GetNativeCell(1)], sizeof(g_szClientExternalTag[]));
}
public int Native_GetExternalChatTag(Handle hPlugin, int nParams)
{
	SetNativeString(2, g_szClientExternalTag[GetNativeCell(1)], sizeof(g_szClientExternalTag[]));
} 
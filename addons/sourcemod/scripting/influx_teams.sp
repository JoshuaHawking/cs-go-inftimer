#include <sourcemod>
#include <cstrike>
#include <smlib>

#include <influx/core>

#undef REQUIRE_PLUGIN
#include <influx/pause>
#include <influx/hideme>

#define RESPAWN_DELAY				1.0

bool g_bLib_Pause;
bool g_bLib_HideMe;

// Cooldowns
float g_flLastSpecMessage[INF_MAXPLAYERS] = 0.0;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Teams", 
	description = "Handles team joins, respawning of clients & disconnection messages", 
	version = INF_VERSION
};

public OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS - copied from influx_core
	RegConsoleCmd("sm_r", Cmd_Spawn);
	RegConsoleCmd("sm_R", Cmd_Spawn);
	RegConsoleCmd("sm_re", Cmd_Spawn);
	RegConsoleCmd("sm_rs", Cmd_Spawn);
	RegConsoleCmd("sm_restart", Cmd_Spawn);
	RegConsoleCmd("sm_start", Cmd_Spawn);
	
	
	RegConsoleCmd("sm_main", Cmd_Spawn);
	RegConsoleCmd("sm_m", Cmd_Spawn);
	
	RegConsoleCmd("sm_bonus", Cmd_Spawn);
	RegConsoleCmd("sm_b", Cmd_Spawn);
	
	RegConsoleCmd("sm_bonus1", Cmd_Spawn);
	RegConsoleCmd("sm_b1", Cmd_Spawn);
	RegConsoleCmd("sm_bonus2", Cmd_Spawn);
	RegConsoleCmd("sm_b2", Cmd_Spawn);
	
	
	RegConsoleCmd("sm_spec", Cmd_Spec);
	RegConsoleCmd("sm_spectate", Cmd_Spec);
	RegConsoleCmd("sm_spectator", Cmd_Spec);
	
	// HOOKS
	HookEvent("player_death", E_PlayerDeath);
	HookEvent("player_team", E_PlayerTeam, EventHookMode_Pre);
	HookEvent("player_disconnect", E_PlayerDisconnect, EventHookMode_Pre);
	
	// LISTENERS
	AddCommandListener(Lstnr_JoinTeam, "jointeam");
	
	// LIBARIES
	g_bLib_Pause = LibraryExists(INFLUX_LIB_PAUSE);
	g_bLib_HideMe = LibraryExists(INFLUX_LIB_HIDEME);
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = true;
	if (StrEqual(lib, INFLUX_LIB_HIDEME))g_bLib_HideMe = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = false;
	if (StrEqual(lib, INFLUX_LIB_HIDEME))g_bLib_HideMe = false;
}

// Disconnect Message
public OnClientDisconnect(int client)
{
	char szPlayer[64];
	GetClientName(client, szPlayer, sizeof(szPlayer));
	Influx_PrintToChatAll(_, client, "%t", "PLAYERDISCONNECTED", szPlayer);
}

public Action E_PlayerDisconnect(Handle event, char[] name, bool dontBroadcast)
{
	SetEventBroadcast(event, true);
	return Plugin_Continue;
}

/*
	Team Joining
*/

// Listen for when a player joins the team.
public Action Lstnr_JoinTeam(int client, const char[] command, int argc)
{
	// Unlimited team joins
	if (!client)return Plugin_Continue;
	
	char g_szTeam[4];
	GetCmdArgString(g_szTeam, sizeof(g_szTeam));
	int team = StringToInt(g_szTeam);
	
	// Check whether it's a valid team (prevents things like jointeam rjnwe)
	if (team != CS_TEAM_SPECTATOR && team != CS_TEAM_CT && team != CS_TEAM_T)
	{
		return Plugin_Handled;
	}
	
	if (team == CS_TEAM_SPECTATOR)
	{
		// Pause client run if they join spectator
		if (g_bLib_Pause && IsPlayerAlive(client) && Influx_GetClientState(client) == STATE_RUNNING)
		{
			Influx_PauseClientRun(client);
		}
		
		// Change team
		ChangeClientTeam(client, CS_TEAM_SPECTATOR);
		SetClientObserverMode(client, OBS_MODE_IN_EYE);
	}
	else
	{
		// Change team
		ChangeClientTeam(client, team);
	}
	
	return Plugin_Handled;
}

// Called when a player has actually jointed the team.
public Action E_PlayerTeam(Event event, const char[] name, bool dontBroadcast)
{
	// Disable broadcasting
	if (!event.GetBool("silent"))
	{
		event.BroadcastDisabled = true;
	}
	
	int client = GetClientOfUserId(event.GetInt("userid"));
	int team = event.GetInt("team");
	
	
	if (team == CS_TEAM_CT || team == CS_TEAM_T)
	{
		// Respawn player.	
		CreateTimer(RESPAWN_DELAY, Timer_RespawnClient, event.GetInt("userid"));
	}
	
	// No need for message spam
	if (Inf_HandleCmdSpam(client, 10.0, g_flLastSpecMessage[client], false))
	{
		return Plugin_Continue;
	}
	
	if (team == CS_TEAM_SPECTATOR && (g_bLib_HideMe ? !Influx_IsClientHidden(client) : true))
	{
		char szPlayer[MAX_NAME_LENGTH];
		GetClientName(client, szPlayer, sizeof(szPlayer));
		Influx_PrintToChatAll(_, client, "%T", "JOINEDSPECTATORS", LANG_SERVER, szPlayer);
	}
	
	return Plugin_Continue;
}

/*
	Spawning
*/
// Respawn player
public Action E_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	CreateTimer(RESPAWN_DELAY, Timer_RespawnClient, event.GetInt("userid"));
	return Plugin_Continue;
}

public Action Timer_RespawnClient(Handle timer, int uid)
{
	int client = GetClientOfUserId(uid);
	
	if (!client)return;
	
	if (!IsClientInGame(client))return;
	
	if (GetClientTeam(client) == CS_TEAM_SPECTATOR)return;
	
	// Check whether player is alive.
	if (!IsPlayerAlive(client))
	{
		CS_RespawnPlayer(client);
	}
}

// Spawn player
public Action Cmd_Spawn(int client, int args)
{
	if (client)
	{
		if (GetClientTeam(client) == CS_TEAM_SPECTATOR)
		{
			// Change team
			ChangeClientTeam(client, CS_TEAM_CT);
			
			// Respawn player.	
			CS_RespawnPlayer(client);
		}
	}
	
	return Plugin_Handled;
}

/*
	Spectator
*/
public Action Cmd_Spec(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (!IsClientInGame(client))return Plugin_Handled;
	
	// Check they aren't currently spectator.
	if (GetClientTeam(client) == CS_TEAM_SPECTATOR)return Plugin_Handled;
	
	// Pause client run if they join spectator
	if (g_bLib_Pause && IsPlayerAlive(client) && Influx_GetClientState(client) == STATE_RUNNING)
	{
		Influx_PauseClientRun(client);
	}
	
	// Set them to spectator
	ChangeClientTeam(client, CS_TEAM_SPECTATOR);
	
	if (args)
	{
		// Attempt to find a name.
		char szArg[32];
		GetCmdArgString(szArg, sizeof(szArg));
		
		int targets[1];
		char szTemp[1];
		bool bUseless;
		
		if (ProcessTargetString(
				szArg, 
				0, 
				targets, 
				sizeof(targets), 
				COMMAND_FILTER_ALIVE, 
				szTemp, 
				sizeof(szTemp), 
				bUseless))
		{
			int target = targets[0];
			
			if (target != client
				 && IS_ENT_PLAYER(target)
				 && IsClientInGame(target)
				 && IsPlayerAlive(target))
			{
				if (GetClientObserverTarget(client) != target)
				{
					SetEntPropEnt(client, Prop_Send, "m_hObserverTarget", target);
					SetEntProp(client, Prop_Send, "m_iObserverMode", 4);
					
					char szPlayer[MAX_NAME_LENGTH];
					GetClientName(target, szPlayer, sizeof(szPlayer));
					
					Influx_PrintToChat(_, client, "%t", "NOWSPECTATING", szPlayer);
				}
				
				SetClientObserverMode(client, OBS_MODE_IN_EYE);
			}
		}
	}
	
	return Plugin_Handled;
} 
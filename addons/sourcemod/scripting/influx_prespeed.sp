#include <sourcemod>

#include <influx/core>
#include <influx/stocks_core>
#include <influx/truevel>

// LIBRARIES
bool g_bLib_Truevel;

// CONVARS
ConVar g_ConVar_MaxEnterSpeed;
ConVar g_ConVar_MaxExitSpeed;

RunState_t g_lastState[INF_MAXPLAYERS];

int g_nMaxEnterSpeed;
int g_nMaxExitSpeed;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Prespeed", 
	description = "Handles prespeed.", 
	version = INF_VERSION
};

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = true;
}
public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = false;
}

public void OnPluginStart()
{
	// CONVARS
	g_ConVar_MaxEnterSpeed = CreateConVar("influx_max_enterspeed", "210", "Speed that you will be clamped to when you enter the zone (assuming your faster than it)", FCVAR_NOTIFY, true, 0.0);
	g_ConVar_MaxExitSpeed = CreateConVar("influx_max_exitspeed", "300", "Speed that you will be clamped to when you exit the zone (assuming your faster than it)", FCVAR_NOTIFY, true, 0.0);

	AutoExecConfig(true, "prespeed", "influx");
	
	g_nMaxEnterSpeed = g_ConVar_MaxEnterSpeed.IntValue;
	g_nMaxExitSpeed = g_ConVar_MaxExitSpeed.IntValue;
	
	g_bLib_Truevel = LibraryExists(INFLUX_LIB_TRUEVEL);
	
	// EVENTS
	//HookEvent( "player_jump", E_PlayerJump );
}

public Action OnPlayerRunCmd(int client)
{
	if (!IsPlayerAlive(client))return Plugin_Continue;
	
	RunState_t currentState = Influx_GetClientState(client);
	//Entered the start zone
	if (currentState == STATE_START && g_lastState[client] == STATE_RUNNING)
	{
		ClampSpeed(client, g_nMaxEnterSpeed);
	}
	// Exited start zone
	else if (currentState == STATE_RUNNING && g_lastState[client] == STATE_START)
	{
		ClampSpeed(client, g_nMaxExitSpeed);
	}
	g_lastState[client] = currentState;
	return Plugin_Continue;
}
public void ClampSpeed(int client, int clampSpeed)
{
	float speed = GetSpeed(client);
	if (speed >= clampSpeed) {
		
		float fVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", fVelocity);
		
		if (fVelocity[0] == 0.0)
			fVelocity[0] = 1.0;
		if (fVelocity[1] == 0.0)
			fVelocity[1] = 1.0;
		if (fVelocity[2] == 0.0)
			fVelocity[2] = 1.0;
		
		float Multpl = speed / clampSpeed;
		fVelocity[0] /= Multpl;
		fVelocity[1] /= Multpl;
		
		TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, fVelocity);
	}
}
// Check if they want truevel.
stock float GetSpeed(int client)
{
	return (g_bLib_Truevel && Influx_IsClientUsingTruevel(client)) ? GetEntityTrueSpeed(client) : GetEntitySpeed(client);
} 
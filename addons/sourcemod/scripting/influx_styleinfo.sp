#include <sourcemod>

#include <influx/core> 
#include <influx/styleinfo>

#undef REQUIRE_PLUGIN

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Style Info", 
	description = "!styleinfo command, info about modes/styles", 
	version = INF_VERSION
};

#define MAX_INFO_SIZE		512

char g_szModeInfo[MAX_MODES][MAX_INFO_SIZE];
char g_szStyleInfo[MAX_STYLES][MAX_INFO_SIZE];

// FORWARDS
Handle g_hForward_RequestModeInfo;
Handle g_hForward_RequestStyleInfo;

public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegConsoleCmd("sm_modeinfo", Cmd_Info);
	RegConsoleCmd("sm_styleinfo", Cmd_Info);
	
	// NATIVES
	CreateNative("Influx_AddStyleInfo", Native_AddStyleInfo);
	CreateNative("Influx_RemoveStyleInfo", Native_RemoveStyleInfo);
	CreateNative("Influx_AddModeInfo", Native_AddModeInfo);
	CreateNative("Influx_RemoveModeInfo", Native_RemoveModeInfo);
	
	// FORWARDS
	g_hForward_RequestModeInfo = CreateGlobalForward("Influx_RequestModeInfo", ET_Ignore);
	g_hForward_RequestStyleInfo = CreateGlobalForward("Influx_RequestStyleInfo", ET_Ignore);
	
}

public void OnAllPluginsLoaded()
{
	Call_StartForward(g_hForward_RequestModeInfo);
	Call_Finish();
	
	Call_StartForward(g_hForward_RequestStyleInfo);
	Call_Finish();
}

public Action Cmd_Info(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	Menu menu = new Menu(Hndlr_Info);
	
	// Format Title
	char szTitle[96];
	char szMode[MAX_MODE_NAME];
	char szStyle[MAX_STYLE_NAME];
	
	szTitle = "Select a mode/style to display info!\n ";
	
	menu.SetTitle(szTitle);
	
	int mode, style;
	char szItem[32];
	
	// Load modes
	for (mode = 0; mode < MAX_MODES; mode++)
	{
		Influx_GetModeName(mode, szMode, sizeof(szMode));
		if (!StrEqual(szMode, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "mode_%i", mode);
			
			menu.AddItem(szItem, szMode);
		}
	}
	
	menu.AddItem("", "", ITEMDRAW_SPACER);
	
	// Load styles
	for (style = 0; style < MAX_STYLES; style++)
	{
		Influx_GetStyleName(style, szStyle, sizeof(szStyle));
		if (!StrEqual(szStyle, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "style_%i", style);
			
			menu.AddItem(szItem, szStyle);
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return Plugin_Handled;
}

public int Hndlr_Info(Menu menu, MenuAction action, int client, int index) {
	MENU_HANDLE(menu, action)
	
	char szInfo[16];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	char buffer[2][8];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	int id = StringToInt(buffer[1]);
	char szTemp[MAX_STYLE_NAME + MAX_MODE_NAME];
	
	if (StrEqual("mode", buffer[0]))
	{
		Influx_GetModeName(id, szTemp, sizeof(szTemp));
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {CHATCLR}------------- {LIMEGREEN}%s {CHATCLR}-------------", szTemp);
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {CHATCLR}%s", g_szModeInfo[id]);
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, "  ");
		
		Cmd_Info(client, 0);
	}
	else
	{
		Influx_GetStyleName(id, szTemp, sizeof(szTemp));
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {CHATCLR}------------- {LIMEGREEN}%s {CHATCLR}-------------", szTemp);
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {CHATCLR}%s", g_szStyleInfo[id]);
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, "  ");
		
		Cmd_Info(client, 0);
	}
	
	return 0;
}

public int Native_AddStyleInfo(Handle hPlugin, int nParms)
{
	GetNativeString(2, g_szStyleInfo[GetNativeCell(1)], MAX_INFO_SIZE);
}
public int Native_AddModeInfo(Handle hPlugin, int nParms)
{
	GetNativeString(2, g_szModeInfo[GetNativeCell(1)], MAX_INFO_SIZE);
}
public int Native_RemoveStyleInfo(Handle hPlugin, int nParms)
{
	strcopy(g_szStyleInfo[GetNativeCell(1)], MAX_INFO_SIZE, "");
}
public int Native_RemoveModeInfo(Handle hPlugin, int nParms)
{
	strcopy(g_szModeInfo[GetNativeCell(1)], MAX_INFO_SIZE, "");
} 
#include <sourcemod>
#include <sdktools>

#include <influx/core>

#define HIDEHUD_ALL 1<<2

bool g_bHideHud[INF_MAXPLAYERS];

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - HUD | Hide HUD", 
	description = "Allows you to hide the entire hud.", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_hidehud", Cmd_HideHud, "Toggle the hud display.");
}

public void OnClientConnected(int client)
{
	g_bHideHud[client] = false;
}

public Action Cmd_HideHud(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	g_bHideHud[client] = !g_bHideHud[client];

	if(g_bHideHud[client])
	{
		SetEntProp(client, Prop_Send, "m_iHideHUD", GetEntProp(client, Prop_Send, "m_iHideHUD") | HIDEHUD_ALL);
	}
	else
	{
		SetEntProp(client, Prop_Send, "m_iHideHUD", GetEntProp(client, Prop_Send, "m_iHideHUD") & ~HIDEHUD_ALL);
	}
	
	PrintToConsole(client, "Use sm_hidehud to enable your hud again!");
	
	return Plugin_Handled;
} 
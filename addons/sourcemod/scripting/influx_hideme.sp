#include <sourcemod>
#include <cstrike>
#include <smlib>

#include <influx/core>
#include <influx/hideme>

#undef REQUIRE_PLUGIN

// !HIDEME
bool g_bClientHidden[INF_MAXPLAYERS] = false;
int g_iAliveOffset;
int g_iConnectedOffset;
int g_iDeathsOffset;
int g_iHealthOffset;
int g_iPingOffset;
int g_iPlayerManager;
int g_iScoreOffset;
int g_iTeamOffset;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Hideme", 
	description = "!hideme command for admins", 
	version = INF_VERSION
};

public OnPluginStart()
{
	// LIBRARIES
	RegPluginLibrary(INFLUX_LIB_HIDEME);
	
	LoadTranslations("common.phrases");
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegAdminCmd("sm_hideme", Cmd_ToggleHideMe, ADMFLAG_GENERIC);
	
	// !HIDEME
	g_iConnectedOffset = FindSendPropInfo("CCSPlayerResource", "m_bConnected");
	g_iAliveOffset = FindSendPropInfo("CCSPlayerResource", "m_bAlive");
	g_iTeamOffset = FindSendPropInfo("CCSPlayerResource", "m_iTeam");
	g_iPingOffset = FindSendPropInfo("CCSPlayerResource", "m_iPing");
	g_iScoreOffset = FindSendPropInfo("CCSPlayerResource", "m_iScore");
	g_iDeathsOffset = FindSendPropInfo("CCSPlayerResource", "m_iDeaths");
	g_iHealthOffset = FindSendPropInfo("CCSPlayerResource", "m_iHealth");
	
	// NATIVES
	CreateNative("Influx_IsClientHidden", Native_IsClientHidden);
	
}

public void OnMapStart()
{
	g_iPlayerManager = FindEntityByClassname(-1, "cs_player_manager");
	if (g_iPlayerManager != -1) {
		SDKHook(g_iPlayerManager, SDKHook_ThinkPost, Hook_PMThink);
	}
}

public OnClientConnected(int client)
{
	g_bClientHidden[client] = false;
}

/*
	Hide Me
*/
public Action Cmd_ToggleHideMe(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	g_bClientHidden[client] = !g_bClientHidden[client];
	
	Influx_PrintToChat(_, client, "%t", "HIDDENENABLED", g_bClientHidden[client] ? "ON" : "OFF");
	
	return Plugin_Handled;
}

public Hook_PMThink(entity)
{
	for (new i = 1; i <= MaxClients; i++) {
		if (IsClientInGame(i) && g_bClientHidden[i]) {
			SetEntData(g_iPlayerManager, g_iAliveOffset + (i * 4), false, 4, true);
			SetEntData(g_iPlayerManager, g_iConnectedOffset + (i * 4), false, 4, true);
			SetEntData(g_iPlayerManager, g_iTeamOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iPingOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iScoreOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iDeathsOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iHealthOffset + (i * 4), 0, 4, true);
		}
	}
}

public OnGameFrame()
{
	for (new i = 1; i <= MaxClients; i++) {
		if (IsClientInGame(i) && g_bClientHidden[i]) {
			SetEntData(g_iPlayerManager, g_iAliveOffset + (i * 4), false, 4, true);
			SetEntData(g_iPlayerManager, g_iConnectedOffset + (i * 4), false, 4, true);
			SetEntData(g_iPlayerManager, g_iTeamOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iPingOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iScoreOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iDeathsOffset + (i * 4), 0, 4, true);
			SetEntData(g_iPlayerManager, g_iHealthOffset + (i * 4), 0, 4, true);
		}
	}
}

public Native_IsClientHidden(Handle hPlugin, int nParams)
{
	return (IsClientInGame(GetNativeCell(1)) && g_bClientHidden[GetNativeCell(1)]);
} 
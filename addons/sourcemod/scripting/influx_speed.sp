#include <sourcemod>

#include <influx/core>
#include <influx/speed>

#include <msharedutil/arrayvec>
#include <msharedutil/ents>

#undef REQUIRE_PLUGIN
#include <influx/pause>
#include <influx/truevel>


//#define DEBUG_SURF

// Speed
float g_flSpeed[INF_MAXPLAYERS];
float g_flMaxSpeed[INF_MAXPLAYERS];
float g_flFinalSpeed[INF_MAXPLAYERS];

// Distance Travelled
float g_flDistance[INF_MAXPLAYERS];
float g_flLastVector[INF_MAXPLAYERS][3];

// Mouse movement
float g_flLastAngle[INF_MAXPLAYERS][3];
int g_nMouseMovementFrames[INF_MAXPLAYERS];
int g_nMouseMovementCount[INF_MAXPLAYERS];

int g_nFrames[INF_MAXPLAYERS];

bool g_bLib_Pause;
bool g_bLib_Truevel;


public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Speed", 
	description = "Tracks max, avg and final speed. also tracks mouse movement & distance tracking.", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	RegPluginLibrary(INFLUX_LIB_SPEED);
	
	// NATIVES
	CreateNative("Influx_GetClientFinalSpeed", Native_GetClientFinalSpeed);
	CreateNative("Influx_GetClientAverageSpeed", Native_GetClientAverageSpeed);
	CreateNative("Influx_GetClientMaxSpeed", Native_GetClientMaxSpeed);
	CreateNative("Influx_GetClientDistanceTravelled", Native_GetClientDistanceTravelled);
	CreateNative("Influx_GetClientMouseMovement", Native_GetClientMouseMovement);
}

public void OnPluginStart()
{
	g_bLib_Pause = LibraryExists(INFLUX_LIB_PAUSE);
	g_bLib_Truevel = LibraryExists(INFLUX_LIB_TRUEVEL);
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = true;
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = false;
	if (StrEqual(lib, INFLUX_LIB_TRUEVEL))g_bLib_Truevel = false;
}

public void OnClientPutInServer(int client)
{
	if (!IsFakeClient(client))
		Inf_SDKHook(client, SDKHook_PostThinkPost, E_PostThinkPost_Client);
}

public void OnAllPluginsLoaded()
{
	Handle db = Influx_GetDB();
	
	if (db == null)
	{
		SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	}
	
	
	SQL_TQuery(db, Thrd_Empty, "ALTER TABLE "...INF_TABLE_TIMES..." ADD (avgspeed INTEGER DEFAULT 0.0, finalspeed FLOAT DEFAULT 0.0, maxspeed FLOAT DEFAULT 0.0, mousemovement FLOAT DEFAULT 0.0, distancetravelled FLOAT DEFAULT 0.0)", _, DBPrio_High);
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data) {  }


public void Influx_OnTimerStartPost(int client, int runid)
{
	// Reset speed
	g_flSpeed[client] = 0.0;
	g_flMaxSpeed[client] = 0.0;
	g_flFinalSpeed[client] = 0.0;
	
	// Reset distances/vectors
	g_flDistance[client] = 0.0;
	g_flLastVector[client][0] = 0.0;
	g_flLastVector[client][1] = 0.0;
	g_flLastVector[client][2] = 0.0;
	
	// Reset mouse movement
	g_nMouseMovementFrames[client] = 0;
	g_nMouseMovementCount[client] = 0;
	g_flLastAngle[client][0] = 0.0;
	g_flLastAngle[client][1] = 0.0;
	g_flLastAngle[client][2] = 0.0;
	
	// Reset frames
	g_nFrames[client] = 0;
	
}

public void Influx_OnTimerFinishPost(int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags)
{
	g_flFinalSpeed[client] = GetSpeed(client);
	
	if (flags & (RES_TIME_PB | RES_TIME_FIRSTOWNREC))
	{
		Handle db = Influx_GetDB();
		if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
		
		decl String:szQuery[256];
		FormatEx(szQuery, sizeof(szQuery), "UPDATE "...INF_TABLE_TIMES..." SET avgspeed=%.2f, finalspeed=%.2f, maxspeed=%.2f, distancetravelled=%.2f, mousemovement=%.2f WHERE uid=%i AND mapid=%i AND runid=%i AND mode=%i AND style=%i", 
			(g_flSpeed[client] / g_nFrames[client]), 
			g_flFinalSpeed[client], 
			g_flMaxSpeed[client], 
			g_flDistance[client], 
			((float(g_nMouseMovementFrames[client]) / float(g_nMouseMovementCount[client])) * 100),
			Influx_GetClientId(client), 
			Influx_GetCurrentMapId(), 
			runid, 
			mode, 
			style);
		
		SQL_TQuery(db, Thrd_Update, szQuery, GetClientUserId(client), DBPrio_High);
	}
}

public void Thrd_Update(Handle db, Handle res, const char[] szError, int client)
{
	if (res == null)
	{
		Inf_DB_LogError(db, "updating player's record with strafes", GetClientOfUserId(client), "Couldn't record your strafes!");
	}
}

public void E_PostThinkPost_Client(int client)
{
	if (!IsPlayerAlive(client))return;
	
	// Ignore completely if we're not running or we are paused.
	if (Influx_GetClientState(client) != STATE_RUNNING
		 || (g_bLib_Pause && Influx_IsClientPaused(client)))
	{
		return;
	}
	/*
		Calculate / Record speed
	*/
	float speed = GetSpeed(client);
	g_flSpeed[client] += speed;
	
	// Update max speed.
	if (speed > g_flMaxSpeed[client])
	{
		g_flMaxSpeed[client] = speed;
	}
	
	/*
		Calculate distance travelled
	*/
	
	float flPlayerVector[3];
	GetClientAbsOrigin(client, flPlayerVector);
	
	// If vector isn't 0.0,0.0,0.0 (just reset)
	if (!(g_flLastVector[client][0] == 0.0 && g_flLastVector[client][1] == 0.0 && g_flLastVector[client][2] == 0.0))
	{
		// Add distance
		g_flDistance[client] += GetVectorDistance(flPlayerVector, g_flLastVector[client]);
	}
	
	g_flLastVector[client] = flPlayerVector;
	
	/*
		Calculate mouse movement
	*/
	
	float flPlayerAngle[3];
	GetClientEyeAngles(client, flPlayerAngle);
	//PrintToChatAll("Current: %.2f  %.2f  %.2f   Previous:  %.2f  %.2f  %.2f ", flPlayerAngle[0], flPlayerAngle[1], flPlayerAngle[2], g_flLastAngle[client][0], g_flLastAngle[client][1], g_flLastAngle[client][2]);
	
	if (
		flPlayerAngle[0] != g_flLastAngle[client][0]
		 || flPlayerAngle[1] != g_flLastAngle[client][1]
		)
	{
		//PrintToChatAll("You moused your mouse. Moved frames: %i Total Frames: %i", g_nMouseMovementFrames[client], g_nMouseMovementCount[client]);
		g_nMouseMovementFrames[client]++;
	}
	
	g_flLastAngle[client] = flPlayerAngle;
	g_nMouseMovementCount[client]++;
	
	g_nFrames[client]++;
}

// Check if they want truevel.
stock float GetSpeed(int client)
{
	return (g_bLib_Truevel && Influx_IsClientUsingTruevel(client)) ? GetEntityTrueSpeed(client) : GetEntitySpeed(client);
}

public bool TraceFilter_AnythingButMe(int ent, int mask, int client)
{
	return (ent != client);
}
public int Native_GetClientFinalSpeed(Handle hPlugin, int nParams)
{
	// May have interesting side effects, who knows
	float speed = g_flFinalSpeed[GetNativeCell(1)];
	if (speed == 0.0)
	{
		return view_as<int>(GetSpeed(GetNativeCell(1)));
	}
	return view_as<int>(speed);
}
public int Native_GetClientAverageSpeed(Handle hPlugin, int nParams)
{
	return view_as<int>((g_flSpeed[GetNativeCell(1)] / g_nFrames[GetNativeCell(1)]));
}
public int Native_GetClientMaxSpeed(Handle hPlugin, int nParams)
{
	return view_as<int>(g_flMaxSpeed[GetNativeCell(1)]);
}
public int Native_GetClientDistanceTravelled(Handle hPlugin, int nParams)
{
	return view_as<int>(g_flDistance[GetNativeCell(1)]);
}
public int Native_GetClientMouseMovement(Handle hPlugin, int nParams)
{
	return view_as<int>((float(g_nMouseMovementFrames[GetNativeCell(1)]) / float(g_nMouseMovementCount[GetNativeCell(1)])) * 100);
}
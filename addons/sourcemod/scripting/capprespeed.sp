#pragma semicolon 1

#include <sourcemod>
#include <sdktools>

#pragma newdecls required

#define PRESPEED_MAX		276

int g_iGroundFrames[MAXPLAYERS + 1];

public Plugin myinfo = 
{
	name = "Cap Prespeed",
	author = "Fusion",
	description = "Caps prespeed to 276.",
	version = "1.0",
	url = "www.houseofclimb.com"
};

public void OnClientConnected(int client)
{
	g_iGroundFrames[client] = 0;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if (!IsValidClient(client, true))
		return Plugin_Continue;
	
	// Check whether they are in the air.
	if(!(GetEntityFlags(client) & FL_ONGROUND))
	{
		// Reset ground frames
		g_iGroundFrames[client] = 0;
		return Plugin_Continue;
	}
	
	float flSpeed = GetSpeed(client);
	
	if (flSpeed > PRESPEED_MAX)
	{
		// Give them 1 frame allowance.
		g_iGroundFrames[client]++;
		
		if(!(buttons & IN_JUMP) && g_iGroundFrames[client] > 3)
		{
			// Cap speed
			ClampSpeed(client, PRESPEED_MAX);
		}
	}
	// Check whether their speed is above PRESPEED_MAX.
	return Plugin_Continue;
}

stock bool IsValidClient(int client, bool bAlive = false)
{
	return (client >= 1 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client) && !IsClientSourceTV(client) && (!bAlive || IsPlayerAlive(client)));
}

public float GetSpeed(int ent)
{
	float vec[3];
	GetEntPropVector(ent, Prop_Data, "m_vecVelocity", vec);
	
	return SquareRoot(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
}

public void ClampSpeed(int client, int clampSpeed)
{
	float speed = GetSpeed(client);
	if (speed >= clampSpeed) {
		
		float fVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", fVelocity);
		
		if (fVelocity[0] == 0.0)
			fVelocity[0] = 1.0;
		if (fVelocity[1] == 0.0)
			fVelocity[1] = 1.0;
		if (fVelocity[2] == 0.0)
			fVelocity[2] = 1.0;
		
		float Multpl = speed / clampSpeed;
		fVelocity[0] /= Multpl;
		fVelocity[1] /= Multpl;
		
		TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, fVelocity);
	}
} 
#include <sourcemod>

#include <influx/core>
#include <influx/stocks_strf>
#include <influx/sync>

#include <msharedutil/arrayvec>
#include <msharedutil/ents>

#undef REQUIRE_PLUGIN
#include <influx/pause>


//#define DEBUG_SURF

float g_fStrafeAccuracy[INF_MAXPLAYERS];
float g_fGoodStrafeTicks[INF_MAXPLAYERS];
float g_fTotalStrafeTicks[INF_MAXPLAYERS];
float g_fLastVelocity[INF_MAXPLAYERS];
float g_fLastAngle[INF_MAXPLAYERS];

bool g_bLib_Pause;


public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Sync",
    description = "Sync Calculation",
    version = INF_VERSION
};

public APLRes AskPluginLoad2( Handle hPlugin, bool late, char[] szError, int error_len )
{
    RegPluginLibrary( INFLUX_LIB_SYNC );
    
    // NATIVES
    CreateNative( "Influx_GetClientStrafeSync", Native_GetClientStrafeSync );
    CreateNative( "Influx_GetClientJumpSync", Native_GetClientJumpSync );
}

public void OnPluginStart()
{
    g_bLib_Pause = LibraryExists( INFLUX_LIB_PAUSE );
}

public void OnLibraryAdded( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = true;
}

public void OnLibraryRemoved( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = false;
}

public void OnClientPutInServer( int client )
{
    ResetStrafeSyncForClient(client);
}

public void OnAllPluginsLoaded()
{
    Handle db = Influx_GetDB();
    
    if ( db == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't retrieve database handle!" );
    }
    
    
    SQL_TQuery( db, Thrd_Empty, "ALTER TABLE "...INF_TABLE_TIMES..." ADD (strf_sync FLOAT DEFAULT -1, jump_sync FLOAT DEFAULT -1)", _, DBPrio_High );
}

public void Thrd_Empty( Handle db, Handle res, const char[] szError, any data ) {}


public void Influx_OnTimerStartPost( int client, int runid )
{
    ResetStrafeSyncForClient(client);
}

public void Influx_OnTimerFinishPost( int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags )
{
    if ( flags & (RES_TIME_PB | RES_TIME_FIRSTOWNREC) )
    {
        Handle db = Influx_GetDB();
        if ( db == null ) SetFailState( INF_CON_PRE..."Couldn't retrieve database handle!" );
        
        
        decl String:szQuery[192];
        FormatEx( szQuery, sizeof( szQuery ), "UPDATE "...INF_TABLE_TIMES..." SET strf_sync=%f WHERE uid=%i AND mapid=%i AND runid=%i AND mode=%i AND style=%i",
            g_fStrafeAccuracy[client],
            Influx_GetClientId( client ),
            Influx_GetCurrentMapId(),
            runid,
            mode,
            style );
        
        SQL_TQuery( db, Thrd_Update, szQuery, GetClientUserId( client ), DBPrio_High );
    }
}

public void Thrd_Update( Handle db, Handle res, const char[] szError, int client )
{
    if ( res == null )
    {
        Inf_DB_LogError( db, "updating player's record with sync", GetClientOfUserId( client ), "Couldn't record your Sync!" );
    }
}

public Action OnPlayerRunCmd( int client, int &buttons, int &impulse, float vel[3] )
{
    if ( !IsPlayerAlive( client ) ) return;
    
    if ( IsFakeClient( client ) ) return;
    
    //if ( Influx_GetClientState( client ) != STATE_RUNNING ) return;
    
    if ( g_bLib_Pause && Influx_IsClientPaused( client ) ) return;
    
    UpdateStrafeSyncForClient(client);
}
// Resets the strafe stats
ResetStrafeSyncForClient(client)
{
	g_fStrafeAccuracy[client] = 0.0;
	g_fGoodStrafeTicks[client] = 0.0;
	g_fTotalStrafeTicks[client] = 0.0;
}

// Updates the strafe stats for client
UpdateStrafeSyncForClient(client)
{			
	float temp[3];
	float ang[3];
	
	bool onground = view_as<bool>(GetEntityFlags(client) & FL_ONGROUND);
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", temp);
	temp[2] = 0.0;
	float newvelo = GetVectorLength(temp);
	GetClientEyeAngles(client, ang);
	if(ang[1] < 0)
	{
		ang[1] += 360.0;
	}

	if(!onground)
	{
		if(ang[1] != g_fLastAngle[client])
		{
			if(g_fLastVelocity[client] < newvelo)
			{
				g_fGoodStrafeTicks[client] += 1.0;
			}
			g_fTotalStrafeTicks[client] += 1.0;
			g_fStrafeAccuracy[client] =  100.0 * (g_fGoodStrafeTicks[client] / g_fTotalStrafeTicks[client]);
		}
	}
	
	g_fLastAngle[client] = ang[1];
	g_fLastVelocity[client] = newvelo;
}
public int Native_GetClientStrafeSync( Handle hPlugin, int nParams )
{
    float strafeAcc = g_fStrafeAccuracy[GetNativeCell( 1 )];
    if(strafeAcc < 1) {
        strafeAcc = 0.0;
    }
    return view_as<int>(strafeAcc);
}
public int Native_GetClientJumpSync( Handle hPlugin, int nParams )
{
	return 5;
}
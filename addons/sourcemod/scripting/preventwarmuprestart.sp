#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>

#pragma newdecls required

public Plugin myinfo = 
{
	name = "Prevent Warmup Restart",
	author = "Fusion",
	description = "",
	version = "1.0",
	url = ""
};

/*
	So there is a feature where the round won't start until there is 2 people. The problem is, we don't really care about this in our servers.
	This just disables that round restart.
*/
public Action CS_OnTerminateRound(float &delay, CSRoundEndReason &reason)
{
    if (GameRules_GetProp("m_bWarmupPeriod") != 1 && reason == CSRoundEnd_GameStart)
        return Plugin_Handled;
        
    return Plugin_Continue;
}
#include <sourcemod>

#include <influx/core>

#undef REQUIRE_PLUGIN

#define INF_RULES_FILE		"influx_rules"
#define MAX_LINES			100
#define MAX_LINE_LENGTH		256

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Rules", 
	description = "Rules command", 
	version = INF_VERSION
};

char g_szRules[MAX_LINES][MAX_LINE_LENGTH];
int g_iLineCount;
float g_flLastRulePrintTime[INF_MAXPLAYERS];

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	LoadTranslations(INFLUX_PHRASES);
	
	RegConsoleCmd("sm_rules", Cmd_Rules);
	
	return APLRes_Success;
}

public OnPluginStart()
{
	if (!LoadRules())
	{
		SetFailState("Failed to load rules file!");
	}
}

bool LoadRules()
{
	
	char szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "configs");
	
	if (!DirExists(szPath))return false;
	
	Format(szPath, sizeof(szPath), "%s/%s.txt", szPath, INF_RULES_FILE);
	
	Handle hFile = OpenFile(szPath, "rt");
	
	if (hFile == INVALID_HANDLE) {
		return false;
	}
	
	char szLine[MAX_LINE_LENGTH];
	
	int iLine = 0;
	while (!IsEndOfFile(hFile) && ReadFileLine(hFile, szLine, sizeof(szLine)))
	{
		if (iLine >= MAX_LINES)break;
		
		int iLineLength;
		
		iLineLength = strlen(szLine);
		
		// Remove line ending
		if (szLine[iLineLength - 1] == '\n') {
			szLine[--iLineLength] = '\0';
		}
		
		if (szLine[0] == '\0')continue;
		
		strcopy(g_szRules[iLine], MAX_LINE_LENGTH, szLine);
		
		iLine++;
	}
	
	g_iLineCount = iLine;
	return true;
}

public Action Cmd_Rules(int client, int args)
{
	if (Inf_HandleCmdSpam(client, 10.0, g_flLastRulePrintTime[client], true))
	{
		return Plugin_Handled;
	}
	
	if (client)
	{
		// Print to chat
		Influx_PrintToChat(_, client, "The rules have been printed in the console.");
		Influx_PrintToChat(_, client, "Use the [`] key to view these rules.");
		
		// Print to console
		for (new i = 0; i < 20; i++) {
			PrintToConsole(client, "");
		}
		
		for (int i = 0; i < g_iLineCount; i++)
		{
			PrintToConsole(client, g_szRules[i]);
		}
		
		for (new i = 0; i < 10; i++) {
			PrintToConsole(client, "");
		}
		
	}
	
	return Plugin_Handled;
} 
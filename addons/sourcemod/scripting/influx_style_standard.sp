#include <sourcemod>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

/*
	Completely normal settings, nothing special.
*/

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Style - Standard",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    // CMDS
    RegConsoleCmd( "sm_standard", Cmd_Style_Standard, INF_NAME..." - Change your style to standard." );
}

public void OnAllPluginsLoaded()
{
    if ( !Influx_AddStyle( STYLE_STANDARD, "Standard", "STANDARD", false ) )
    {
        SetFailState( INF_CON_PRE..."Couldn't add style!" );
    }
}

public void OnPluginEnd()
{
    Influx_RemoveStyle( STYLE_STANDARD );
}

public void Influx_OnRequestStyles()
{
    OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
    Influx_AddStyleInfo(STYLE_STANDARD, "Standard is your plain old style. Nothing special around here.");
}

public Action Influx_OnSearchType( const char[] szArg, Search_t &type, int &value )
{
    if (StrEqual( szArg, "standard", false ))
    {
        value = STYLE_STANDARD;
        type = SEARCH_STYLE;
        
        return Plugin_Stop;
    }
    
    return Plugin_Continue;
}

public Action Cmd_Style_Standard( int client, int args )
{
    if ( !client ) return Plugin_Handled;
    
    
    Influx_SetClientStyle( client, STYLE_STANDARD );
    
    return Plugin_Handled;
}

public Action Influx_OnCheckClientStyle( int client, int style, float vel[3] )
{
    if ( style != STYLE_STANDARD ) return Plugin_Continue;
    
    return Plugin_Stop;
}
#include <sourcemod>
#include <cstrike>
#include <smlib>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/pause>
#include <influx/styleinfo>

ConVar g_nAllowThirdPerson;

int g_iClientState[INF_MAXPLAYERS];
int g_iClientJumps[INF_MAXPLAYERS];

#define GLITCHER_GRAVITY 200.0

#define EASY_GRAVITY 0.5
#define EASY_TIMESCALE 0.8

// LIBARIES
bool g_bLib_Pause;

enum VelocityOverride {
	
	VelocityOvr_None = 0, 
	VelocityOvr_Velocity, 
	VelocityOvr_OnlyWhenNegative, 
	VelocityOvr_InvertReuseVelocity
}

#define SHIFT_JUMPS	8

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Style - Shifter", 
	description = "", 
	version = INF_VERSION
};

enum
{
	STATE_INVALID = -1, 
	
	STATE_NORMAL, 
	STATE_LOWGRAV, 
	STATE_THIRDPERSON, 
	STATE_HIGHJUMP, 
	
	STATE_RESET
}

char g_szStateNames[5][32] = {
	"Normal",
	"Easy",
	"3rd",
	"Glitcher",
	"Normal" // Repeated for reset
}

public void OnPluginStart()
{
	// CMDS
	RegConsoleCmd("sm_shifter", Cmd_Style_Shifter, INF_NAME..." - Change your style to shifter.");
	RegConsoleCmd("sm_shift", Cmd_Style_Shifter, "");
	
	g_nAllowThirdPerson = FindConVar("sv_allow_thirdperson");
	if (g_nAllowThirdPerson == INVALID_HANDLE)
		SetFailState("sv_allow_thirdperson not found!");
	
	SetConVarInt(g_nAllowThirdPerson, 1);
	
	// Hook events
	HookEvent("player_death", Player_Death, EventHookMode_Pre);
	HookEvent("player_spawn", Player_Spawn);
	HookEvent("player_team", Player_Team);
	HookEvent("player_jump", E_PlayerJump);
	
	g_bLib_Pause = LibraryExists(INFLUX_LIB_PAUSE);
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = false;
}

public ConVarChanged(Handle cvar, const char[] oldVal, const char[] newVal)
{
	if (cvar == g_nAllowThirdPerson)
	{
		if (StringToInt(newVal) != 1)
			SetConVarInt(g_nAllowThirdPerson, 1);
	}
}

public void OnAllPluginsLoaded()
{
	if (!Influx_AddStyle(STYLE_SHIFTER, "Shifter", "Shift", false))
	{
		SetFailState(INF_CON_PRE..."Couldn't add style!");
	}
}

public void OnPluginEnd()
{
	Influx_RemoveStyle(STYLE_SHIFTER);
}

public void Influx_OnRequestStyles()
{
	OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
    Influx_AddStyleInfo(STYLE_SHIFTER, "Shifter consists of 4 different styles: Normal, Easy, 3rd Person and Glitcher. Every 8 jumps, you will shift between each style.");
}

public Action Influx_OnSearchType(const char[] szArg, Search_t &type, int &value)
{
	if (StrEqual(szArg, "shifter", false))
	{
		value = STYLE_SHIFTER;
		type = SEARCH_STYLE;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

public Action Cmd_Style_Shifter(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	Influx_SetClientStyle(client, STYLE_SHIFTER);
	
	return Plugin_Handled;
}

public Action Influx_OnCheckClientStyle(int client, int style, float vel[3])
{
	if (style != STYLE_THIRDPERSON)return Plugin_Continue;
	
	return Plugin_Stop;
}

public void Influx_OnClientStyleChangePost(int client, int style)
{
	if (style != STYLE_SHIFTER)
	{
		ClientCommand(client, "firstperson");
		return;
	}
	
	g_iClientState[client] = STATE_NORMAL;
	g_iClientJumps[client] = 0;
	return;
}

public void Influx_OnTimerResetPost(int client)
{
	if (Influx_GetClientStyle(client) == STYLE_SHIFTER)
	{
		ClientCommand(client, "firstperson");
		g_iClientState[client] = STATE_NORMAL;
		g_iClientJumps[client] = 0;
	}
	return;
}

public void Influx_OnTimerStartPost(int client, int runid)
{
	g_iClientState[client] = STATE_NORMAL;
	g_iClientJumps[client] = 0;
	if (Influx_GetClientStyle(client) == STYLE_SHIFTER)
	{
		SetEntityGravity(client, 1.0);
		SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", 1.0);
		ClientCommand(client, "firstperson");
		
		if (!(GetEntityFlags(client) & FL_ONGROUND))g_iClientJumps[client] = 1;
	
	}
}

public Action Player_Spawn(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	// Check what state they are in.
	if (Influx_GetClientStyle(client) == STYLE_SHIFTER) {
		// They are currently in third person.
		if (g_iClientState[client] == STATE_THIRDPERSON)
		{
			ClientCommand(client, "thirdperson");
		}
	}
}

public Action Player_Death(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (Influx_GetClientStyle(client) == STYLE_SHIFTER) {
		ClientCommand(client, "firstperson");
	}
}

public Action Player_Team(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	int team = GetEventInt(event, "team");
	if (Influx_GetClientStyle(client) == STYLE_SHIFTER) {
		if (team == CS_TEAM_SPECTATOR)
		{
			ClientCommand(client, "firstperson");
		}
		else
		{
			// They are currently in third person.
			if (g_iClientState[client] == STATE_THIRDPERSON)
			{
				ClientCommand(client, "thirdperson");
			}
		}
	}
}

// Counting every jump
public void E_PlayerJump(Event event, const char[] szEvent, bool bImUselessWhyDoIExist)
{
	int client;
	if (!(client = GetClientOfUserId(GetEventInt(event, "userid"))))return;
	
	if (!IsPlayerAlive(client))return;
	
	if (Influx_GetClientState(client) != STATE_RUNNING)
		return;
	
	if (g_bLib_Pause && Influx_IsClientPaused(client))
		return;
	
	if (Influx_GetClientStyle(client) != STYLE_SHIFTER)
		return;
	
	// Count jumps
	g_iClientJumps[client]++;
	
	if (g_iClientJumps[client] == SHIFT_JUMPS - 1)
	{
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " {PINK}▒  Shifting in 1 jump  ▒  %s  ▒", g_szStateNames[g_iClientState[client]+1]);
	}
	
	if (g_iClientJumps[client] == SHIFT_JUMPS)
	{
		g_iClientJumps[client] = 0;
		
		// Add state
		g_iClientState[client]++;
		
		if (g_iClientState[client] == STATE_RESET)
		{
			g_iClientState[client] = STATE_NORMAL;
		}
		
		if (g_iClientState[client] == STATE_LOWGRAV)
		{
			// Change gravity & timescale
			SetEntityGravity(client, EASY_GRAVITY);
			SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", EASY_TIMESCALE);
		}
		else
		{
			SetEntityGravity(client, 1.0);
			SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", 1.0);
		}
		
		if (g_iClientState[client] == STATE_THIRDPERSON)
		{
			ClientCommand(client, "thirdperson");
		}
		else
		{
			ClientCommand(client, "firstperson");
		}
	}
	
	if (g_iClientState[client] == STATE_HIGHJUMP)
	{
		CreateTimer(0.0, Timer_Push, client, TIMER_FLAG_NO_MAPCHANGE);
	}
	
	return;
}

/*
	Glitcher
*/

public Action Timer_Push(Handle timer, int client)
{
	Push_Client(client);
	return Plugin_Stop;
}

stock Push_Client(client)
{
	if (GLITCHER_GRAVITY > 0.0)
	{
		Client_Push(client, Float: { -90.0, 0.0, 0.0 }, GLITCHER_GRAVITY, VelocityOverride: { VelocityOvr_None, VelocityOvr_None, VelocityOvr_None } );
	}
}

Client_Push(client, float clientEyeAngle[3], float power, VelocityOverride:override[3] = VelocityOvr_None)
{
	float forwardVector[3];
	float newVel[3];
	
	GetAngleVectors(clientEyeAngle, forwardVector, NULL_VECTOR, NULL_VECTOR);
	NormalizeVector(forwardVector, forwardVector);
	ScaleVector(forwardVector, power);
	
	Entity_GetAbsVelocity(client, newVel);
	
	for (new i = 0; i < 3; i++) {
		switch (override[i]) {
			case VelocityOvr_Velocity: {
				newVel[i] = 0.0;
			}
			case VelocityOvr_OnlyWhenNegative: {
				if (newVel[i] < 0.0) {
					newVel[i] = 0.0;
				}
			}
			case VelocityOvr_InvertReuseVelocity: {
				if (newVel[i] < 0.0) {
					newVel[i] *= -1.0;
				}
			}
		}
		
		newVel[i] += forwardVector[i];
	}
	
	Entity_SetAbsVelocity(client, newVel);
} 
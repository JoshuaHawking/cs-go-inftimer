#include <sourcemod>


#include <influx/core>
#include <influx/stocks_core>

#include <cstrike>

#include <influx/tas>
#include <influx/pause>
#include <influx/recording>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>
#include <rocketjump>

// Data for a single replay frame
enum Frame
{
	Float:Position[3], 
	Float:Angles[3], 
	Float:Velocity[3], 
	Buttons, 
	Impulse, 
	String:Weapon_Primary[64], 
	String:Weapon_Secondary[64], 
	Seed, 
	Time, 
	StartTick, 
	Team, 
	EntityFlags, 
	GroundEntity, 
	EntityMoveType, 
	Float:Gravity, 
	
	/*
	   Member: m_bDucked (offset 80) (type integer) (bits 1) (Unsigned)
	   Member: m_bDucking (offset 81) (type integer) (bits 1) (Unsigned)
	   Member: m_flLastDuckTime (offset 84) (type float) (bits 0) (NoScale)
	   Member: m_bInDuckJump (offset 88) (type integer) (bits 1) (Unsigned)
	*/
	
	Ducked, 
	Ducking, 
	Float:LastDuckTime, 
	InDuckJump, 
	
	String:TargetName[64], 
	SlowMotionEnabled, 
	SlowMotionSpeed, 
	
	RocketsShot, 
	
	// Specfic Recording 
	Float:Sync, 
	Flags
}

Handle nFrameTemplate[Frame];

#define RECFLAG_CROUCH      ( 1 << 0 ) // We're crouching.
#define RECFLAG_ATTACK      ( 1 << 1 ) // IN_ATTACK
#define RECFLAG_ATTACK2     ( 1 << 2 ) // IN_ATTACK2
#define RECFLAG_WEP_SLOT1   ( 1 << 3 )
#define RECFLAG_WEP_SLOT2   ( 1 << 4 )
#define RECFLAG_WEP_SLOT3   ( 1 << 5 )
#define RECFLAG_FLASHLIGHT  ( 1 << 6 ) // impulse 100

// #define SLOWMOTION_ENABLED

enum
{
	SLOT_PRIMARY = 0, 
	SLOT_SECONDARY, 
	SLOT_MELEE, 
	
	SLOTS_SAVED
};

// Data for a player tas session
// This stores what they have enabled, any variables we need to store, etc.
enum TasMenuState
{
	bool:Paused, 
	bool:WasPausedLastFrame, 
	bool:Rewind, 
	bool:FastForward, 
	bool:AutoStrafe, 
	bool:AutoJump, 
	bool:AutoCrouch, 
	bool:SlowMotion, 
	Float:TimeScale, 
	bool:GoToCP, 
	CPFrame, 
	Float:CPTime
}

// How often the TAS menu reappears
#define TAS_MENU_REFRESH	4

// TAS Menu stuff
bool g_bRedrawTASMenu[INF_MAXPLAYERS];
Handle g_hTASMenuTimer[INF_MAXPLAYERS];
Handle g_hTASMenu[INF_MAXPLAYERS];

// Replay related
int g_iMovements;
int g_iRecording[INF_MAXPLAYERS] =  { -1, ... };
int g_iFrame[INF_MAXPLAYERS] =  { 0, ... };
//new Float:g_nFrameProc[MAXPLAYERS+1]={0.0, ...};
int g_iMaxFrame[INF_MAXPLAYERS] =  { 0, ... };
Handle g_hMovements[512] =  { INVALID_HANDLE, ... };
Handle g_hMenuState[INF_MAXPLAYERS][TasMenuState];

bool g_bStrafeSwap[INF_MAXPLAYERS];

bool g_bLastFrameCrouch[INF_MAXPLAYERS];
bool g_bLastFrameJump[INF_MAXPLAYERS];
bool g_bSlowMotionSaved[INF_MAXPLAYERS];

/*
The amount of frames to apply the rewind / replay. 
Speed = 1 / g_iEditSpeed
g_iEditSpeed = 1 will translate to 1 rewind every 1 frame, or 1.0x speed. 
g_iEditSpeed = 2 will translate to 1 rewind every 2 frames, or 0.5x speed.
g_iEditSpeed = 3 will translate to 1 rewind every 3 frames, or 0.33x speed.
g_iEditSpeed = 5 will translate to 1 rewind every 5 frames, or 0.2x speed.
g_iEditSpeed = 10 will translate to 1 rewind every 10 frames, or 0.1x speed.

Every rewind/replay frame:
g_iEditSpeedSkippedFrames++
if the g_iEditSpeedSkippedFrames is larger / equal to edit speed, reset to 0 and rewind/replay.
*/
int g_iEditSpeed[INF_MAXPLAYERS] = 1;
int g_iEditSpeedSkippedFrames[INF_MAXPLAYERS] = 0;

/*
The amount of frames to apply the slow motion increment of time (tick count)
Speed = 1 / g_iEditSpeed
g_iEditSpeed = 2 will translate to 1 rewind every 2 frames, or 0.5x speed.
g_iEditSpeed = 3 will translate to 1 rewind every 3 frames, or 0.33x speed.
g_iEditSpeed = 5 will translate to 1 rewind every 5 frames, or 0.2x speed.
g_iEditSpeed = 10 will translate to 1 rewind every 10 frames, or 0.1x speed.

*/
int g_iSlowMotionSpeed[INF_MAXPLAYERS] = 2;
int g_iSlowMotionSkippedFrames[INF_MAXPLAYERS] = 0;
int g_iLastFrameTime[INF_MAXPLAYERS] = 0;
int g_iAutoStrafeSkippedFrames[INF_MAXPLAYERS] = 0;


float g_flLastSpeed[INF_MAXPLAYERS];
float g_flLastGain[INF_MAXPLAYERS];

// LIBRARIES
bool g_bLib_Pause;
bool g_bLib_RocketJump;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Style - TAS", 
	description = "", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	RegPluginLibrary(INFLUX_LIB_STYLE_TAS);
	
	// CMDS
	RegConsoleCmd("sm_tas", Cmd_Style_TAS, INF_NAME..." - Change your style to TAS.");
	RegConsoleCmd("sm_tasmenu", Cmd_Menu, INF_NAME..." - Change your style to TAS.");
	
	CreateNative("Influx_IsClientTASPaused", Native_IsClientTASPaused);
	CreateNative("Influx_IsClientTASRewinding", Native_IsClientTASRewinding);
	CreateNative("Influx_IsClientTASSlowMotion", Native_IsClientTASSlowMotion);
	CreateNative("Influx_SetClientTASPaused", Native_SetClientTASPaused);
	CreateNative("Influx_ResetClientForTAS", Native_ResetClientForTAS);
	
	HookEvent("player_spawn", E_PlayerSpawn);
	
	// LIBRARIES
	g_bLib_Pause = LibraryExists(INFLUX_LIB_PAUSE);
	g_bLib_RocketJump = LibraryExists(LIB_ROCKETJUMP);
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = true;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_RocketJump = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_PAUSE))g_bLib_Pause = false;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_RocketJump = false;
}

public void OnAllPluginsLoaded()
{
	if (!Influx_AddStyle(STYLE_TAS, "Tool-Assisted", "TAS", false))
	{
		SetFailState(INF_CON_PRE..."Couldn't add style!");
	}
}

public void OnPluginEnd()
{
	Influx_RemoveStyle(STYLE_TAS);
}

public void Influx_OnRequestStyles()
{
	OnAllPluginsLoaded();
}

public void Influx_RequestStyleInfo()
{
	Influx_AddStyleInfo(STYLE_TAS, "TAS or Tool Assisted Speedrun allows you to use multiple tools to create a optimised run. Using things like rewind and frame advancement, you can go back in time and change tiny bits of your run. Furthermore, things like auto strafe allow you to gain lots of speed with minimal effort.");
}

public Action Influx_OnSearchType(const char[] szArg, Search_t &type, int &value)
{
	if (StrEqual(szArg, "tas", false))
	{
		value = STYLE_TAS;
		type = SEARCH_STYLE;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

/*
	TAS Menu Stuff
*/

public OnClientConnected(int client)
{
	g_bRedrawTASMenu[client] = false;
}

public OnClientDisconnect(int client)
{
	KillTASMenuTimer(client);
}

public void Influx_OnClientModeChangePost(int client, int mode)
{
	if (mode != MODE_AUTO)
	{
		g_hMenuState[client][AutoJump] = false;
		return;
	}
}

public Action Influx_OnClientStyleChange(int client, int style, int laststyle)
{
	if (style == STYLE_TAS)
	{
		if (!Inf_SDKHook(client, SDKHook_PostThink, E_PostThink_Client))
		{
			return Plugin_Handled;
		}
	}
	
	return Plugin_Continue;
	
}

public void Influx_OnClientStyleChangePost(int client, int style)
{
	if (style != STYLE_TAS)
	{
		g_bRedrawTASMenu[client] = false;
		return;
	}
	
	// Create timer and display menu.
	g_hTASMenuTimer[client] = CreateTimer(float(TAS_MENU_REFRESH), Timer_RefreshTASMenu, client, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
	ResetMenuState(client);
	Menu_TAS(client);
	
	return;
}

public Action Timer_RefreshTASMenu(Handle timer, int client)
{
	if (!IsValidEntity(client) || !IsClientInGame(client))
	{
		KillTASMenuTimer(client);
		
		return Plugin_Stop;
	}
	
	// Dealing with a client who is in the game and playing.
	if (!IsPlayerAlive(client))
	{
		return Plugin_Continue;
	}
	
	// No menu currently
	if (GetClientMenu(client) == MenuSource_None)
	{
		g_bRedrawTASMenu[client] = true;
	}
	
	// Check whether they need to redraw the menu.
	if (g_bRedrawTASMenu[client])
	{
		Menu_TAS(client);
		g_bRedrawTASMenu[client] = false;
	}
	
	if (Influx_GetClientStyle(client) != STYLE_TAS)
	{
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

stock ClearMenu(int client)
{
	if (!IsClientInGame(client))return;
	Menu menu = new Menu(Hndlr_Null);
	menu.AddItem("", "", ITEMDRAW_SPACER);
	menu.ExitButton = false;
	menu.Display(client, 1);
}
stock KillTASMenuTimer(int client)
{
	if (g_hTASMenu[client] != null)
	{
		//KillTimer(g_hSpecListMenu[client]);
		g_hTASMenu[client] = null;
		
		ClearMenu(client);
	}
}
public int Hndlr_Null(Handle menu, MenuAction action, int client, int index) {  }

/*
	Commands
*/

public Action Cmd_Style_TAS(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	Influx_SetClientStyle(client, STYLE_TAS);
	
	return Plugin_Handled;
}

public Action Cmd_Menu(int client, int args)
{
	Menu_TAS(client);
	return Plugin_Handled;
}

public Action Influx_OnClientPause(int client)
{
	if (Influx_GetClientStyle(client) == STYLE_TAS)
	{
		Influx_PrintToChat(_, client, "You cannot pause in TAS style!");
		return Plugin_Stop;
	}
	
	
	return Plugin_Continue;
}

public Action Influx_OnClientPracticeStart(int client)
{
	if (Influx_GetClientStyle(client) == STYLE_TAS)
	{
		Influx_PrintToChat(_, client, "You cannot practice in TAS style!");
		return Plugin_Stop;
	}
	
	
	return Plugin_Continue;
}

/*
	Menus
*/
Menu_TAS(int client, int index = 0)
{
	if (Influx_GetClientStyle(client) != STYLE_TAS)return;
	
	Menu menu = new Menu(Hndlr_TASMenu);
	char szTitle[128];
	Format(szTitle, sizeof(szTitle), "Tool Assisted Menu\n%i:%i", g_iRecording[client], g_iFrame[client]);
	SetMenuTitle(menu, szTitle);
	
	if (g_hMenuState[client][Paused])
		AddMenuItem(menu, "0", "Resume");
	else
		AddMenuItem(menu, "0", "Pause");
	
	if (g_hMenuState[client][Rewind])
		AddMenuItem(menu, "1", "Rewind [X]");
	else
		AddMenuItem(menu, "1", "Rewind");
	
	if (g_hMenuState[client][FastForward])
		AddMenuItem(menu, "2", "Fast Forward [X]");
	else
		AddMenuItem(menu, "2", "Fast Forward");
	
	
	#if defined SLOWMOTION_ENABLED
	if (g_hMenuState[client][SlowMotion])
		AddMenuItem(menu, "3", "Slow Motion [X]");
	else
		AddMenuItem(menu, "3", "Slow Motion");
	#endif
	
	AddMenuItem(menu, "4", "Save CP");
	AddMenuItem(menu, "5", "Goto CP");
	
	if (g_hMenuState[client][AutoStrafe])
		AddMenuItem(menu, "6", "Auto Strafe [X]");
	else
		AddMenuItem(menu, "6", "Auto Strafe");
	
	if (g_hMenuState[client][AutoJump])
		AddMenuItem(menu, "7", "Auto Jump [X]");
	else
		AddMenuItem(menu, "7", "Auto Jump", (Influx_GetClientMode(client) == MODE_AUTO ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED));
	
	
	/*
	if (g_hMenuState[client][AutoCrouch])
		AddMenuItem(menu, "8", "Auto Crouch [X]");
	else
		AddMenuItem(menu, "8", "Auto Crouch");
	*/
	
	decl String:temp[32];
	FormatEx(temp, sizeof(temp), "Edit Speed [%.1fx]", (1.0 / float(g_iEditSpeed[client])));
	AddMenuItem(menu, "9", temp);
	
	#if defined SLOWMOTION_ENABLED
	FormatEx(temp, sizeof(temp), "Slow Motion Speed [%.1fx]", (1.0 / float(g_iSlowMotionSpeed[client])));
	AddMenuItem(menu, "12", temp);
	#endif
	
	// Next frame
	AddMenuItem(menu, "10", "Next Frame");
	
	// Last frame
	AddMenuItem(menu, "11", "Last Frame");
	
	menu.ExitButton = false;
	
	if (index == 0)
	{
		menu.Display(client, MENU_TIME_FOREVER);
	}
	else
	{
		menu.DisplayAt(client, index, MENU_TIME_FOREVER);
	}
}

// Handler for TAS menu
public Hndlr_TASMenu(Handle menu, MenuAction action, int client, int index)
{
	if (client <= 0)return 0;
	// Interrupted
	if (action == MenuAction_Cancel && index == MenuCancel_Interrupted)
	{
		// Mark client to not redraw the menu.
		g_bRedrawTASMenu[client] = false;
		
		// If they are paused, stop their rewinding etc.
		if (g_hMenuState[client][Paused])
		{
			g_hMenuState[client][Paused] = true;
			g_hMenuState[client][Rewind] = false;
			g_hMenuState[client][FastForward] = false;
		}
		return 0;
	}
	
	MENU_HANDLE(menu, action)
	
	char szInfo[4];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int iAction;
	iAction = StringToInt(szInfo);
	
	// Pause / REsume
	if (iAction == 0)
	{
		g_hMenuState[client][Paused] = !g_hMenuState[client][Paused];
		g_hMenuState[client][Rewind] = false;
		g_hMenuState[client][FastForward] = false;
	}
	// Rewind
	else if (iAction == 1)
	{
		g_hMenuState[client][Rewind] = !g_hMenuState[client][Rewind];
		g_hMenuState[client][Paused] = true;
		g_hMenuState[client][FastForward] = false;
	}
	// Replay
	else if (iAction == 2)
	{
		g_hMenuState[client][Rewind] = false;
		g_hMenuState[client][Paused] = true;
		g_hMenuState[client][FastForward] = !g_hMenuState[client][FastForward];
	}
	// Slow Motion
	else if (iAction == 3)
	{
		if (g_hMenuState[client][SlowMotion]) {
			g_hMenuState[client][SlowMotion] = false;
			SetClientSpeed(client, 1.0);
		} else {
			g_hMenuState[client][SlowMotion] = true;
			g_bSlowMotionSaved[client] = false;
			SetClientSpeed(client, TAS_SLOWMOTION_SPEED);
		}
	}
	// Save CP
	else if (iAction == 4)
	{
		SaveCP(client);
	}
	// Goto CP
	else if (iAction == 5)
	{
		GoToLastCP(client);
	}
	// Auto-Strafe
	else if (iAction == 6)
	{
		g_hMenuState[client][AutoStrafe] = !g_hMenuState[client][AutoStrafe];
	}
	// Auto-Jump
	else if (iAction == 7)
	{
		g_hMenuState[client][AutoJump] = !g_hMenuState[client][AutoJump];
	}
	// Auto-Crouch
	else if (iAction == 8)
	{
		g_hMenuState[client][AutoCrouch] = !g_hMenuState[client][AutoCrouch];
	}
	// Edit Speed
	else if (iAction == 9)
	{
		switch (g_iEditSpeed[client])
		{
			case 1:g_iEditSpeed[client] = 2;
			case 2:g_iEditSpeed[client] = 4;
			case 4:g_iEditSpeed[client] = 10;
			case 10:g_iEditSpeed[client] = 1;
			default:g_iEditSpeed[client] = 1;
		}
	}
	// Next frame
	else if (iAction == 10)
	{
		g_hMenuState[client][Rewind] = false;
		g_hMenuState[client][FastForward] = false;
		g_hMenuState[client][Paused] = true;
		if (g_iFrame[client] + 1 < g_iMaxFrame[client])
		{
			++g_iFrame[client];
		}
	}
	// Last frame
	else if (iAction == 11)
	{
		g_hMenuState[client][Rewind] = false;
		g_hMenuState[client][FastForward] = false;
		g_hMenuState[client][Paused] = true;
		if (g_iFrame[client] > 1)
		{
			--g_iFrame[client];
		}
	}
	// Slow Motion Speed
	else if (iAction == 12)
	{
		switch (g_iSlowMotionSpeed[client])
		{
			case 2:g_iSlowMotionSpeed[client] = 4;
			case 4:g_iSlowMotionSpeed[client] = 10;
			case 10:g_iSlowMotionSpeed[client] = 2;
			default:g_iSlowMotionSpeed[client] = 2;
		}
		if (g_hMenuState[client][SlowMotion])
		{
			SetClientSpeed(client, (1.0 / g_iSlowMotionSpeed[client]));
		}
	}
	
	Menu_TAS(client, GetMenuSelectionPosition());
	return 0;
}

public SetPlayerPaused(client)
{
	g_hMenuState[client][Paused] = true;
	g_hMenuState[client][Rewind] = false;
	g_hMenuState[client][FastForward] = false;
	Menu_TAS(client);
}

/*
	Run command
*/
float slowMotionModify[11] =  { -1.0, -1.0, 0.0, -1.0, 0.0, -1.0, -1.0, -1.0, -1.0, -1.0, 0.0 };

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (!IsClientInGame(client) || !IsPlayerAlive(client))
		return Plugin_Continue;
	
	if (!IsFakeClient(client))
	{
		if (Influx_GetClientStyle(client) != STYLE_TAS)return Plugin_Continue;
		
		if (g_iRecording[client] != -1)
		{
			// Check whether they are running.
			if (Influx_GetClientState(client) != STATE_RUNNING)return Plugin_Continue;
			
			// Apply autostrafe, jump etc.
			if (g_hMenuState[client][AutoStrafe] && !g_hMenuState[client][Paused])
			{
				ApplyAutoStrafe(client, buttons, vel, angles);
			}
			
			if (g_hMenuState[client][AutoJump] && Influx_GetClientMode(client) == MODE_AUTO)
			{
				ApplyAutoJump(client, buttons);
			}
			
			if (g_hMenuState[client][AutoCrouch])
			{
				ApplyAutoCrouch(client, buttons);
			}
			
			if (g_bLib_Pause && Influx_IsClientPaused(client))return Plugin_Continue;
			
			if ((g_hMenuState[client][GoToCP] && g_hMenuState[client][CPFrame] < 1) || !(g_hMenuState[client][Rewind] || g_hMenuState[client][FastForward] || g_hMenuState[client][Paused] || g_hMenuState[client][Paused] || g_hMenuState[client][WasPausedLastFrame]))
			{
				RecordClientFrame(client, buttons, impulse, seed, weapon);
			}
			
			return Plugin_Changed;
		}
	}
	
	return Plugin_Continue;
}

ApplyAutoStrafe(int client, int &buttons, float vel[3], float angles[3])
{
	if (GetEntityFlags(client) & FL_ONGROUND)
		return;
	
	if (GetEntityMoveType(client) & MOVETYPE_LADDER)
		return;
	
	if (buttons & IN_MOVELEFT || buttons & IN_MOVERIGHT || buttons & IN_FORWARD || buttons & IN_BACK)
	{
		return;
	}
	
	if (g_hMenuState[client][SlowMotion] && false)
	{
		g_iAutoStrafeSkippedFrames[client]++;
		if (!g_bStrafeSwap[client] && g_iAutoStrafeSkippedFrames[client] >= g_iSlowMotionSpeed[client])
		{
			angles[1] += 1.0;
			vel[1] = 450.0;
			g_bStrafeSwap[client] = !g_bStrafeSwap[client];
			g_iAutoStrafeSkippedFrames[client] = 0;
		}
		// g_iSlowMotionSkippedFrames[client] >= g_iSlowMotionSpeed[client]
		else if (g_bStrafeSwap[client] && g_iAutoStrafeSkippedFrames[client] >= g_iSlowMotionSpeed[client])
		{
			angles[1] -= 1.0;
			vel[1] = -450.0;
			g_bStrafeSwap[client] = !g_bStrafeSwap[client];
			g_iAutoStrafeSkippedFrames[client] = 0;
		}
	}
	else
	{
		
		float fVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", fVelocity);
		
		float sidespeed = 450.0;
		float yVel = RadToDeg(ArcTangent2(fVelocity[1], fVelocity[0]));
		
		float difAngle = NormalizeAngle(angles[1] - yVel);
		
		vel[1] = sidespeed;
		
		if (difAngle > 0.0)
			vel[1] = -sidespeed;
		
		
		// This bit check whether the player has tried to turn their mouse faster than the strafer
		
		float OldGain = g_flLastGain[client];
		//float AngleGain = RadToDeg( ArcTangent( vel[1] / vel[0] ) );
		float AngleGain = difAngle;
		
		if (!((OldGain < 0.0 && AngleGain < 0.0) || (OldGain > 0.0 && AngleGain > 0.0))) //this check tells you when the mouse player movement is higher than the autostrafer one, and decide to put it or not
			angles[1] -= difAngle;
		
		g_flLastGain[client] = AngleGain;
		
	}
}

public float NormalizeAngle(float angle)
{
	float temp = angle;
	
	while (temp <= -180.0)
	{
		temp += 360.0;
	}
	
	while (temp > 180.0)
	{
		temp -= 360.0;
	}
	
	return temp;
}

ApplyAutoJump(int client, int &buttons)
{
	if (GetEntityFlags(client) & FL_ONGROUND && g_bLastFrameJump[client] == false)
	{
		buttons |= IN_JUMP;
		g_bLastFrameJump[client] = true;
	}
	else
	{
		buttons &= ~IN_JUMP;
		g_bLastFrameJump[client] = false;
	}
}

ApplyAutoCrouch(int client, int &buttons)
{
	if (g_bLastFrameCrouch[client] == false)
	{
		buttons &= ~IN_DUCK;
		g_bLastFrameCrouch[client] = true;
	}
	else
	{
		g_bLastFrameCrouch[client] = false;
	}
}

/*
	Reset Functions
*/

public Action E_PlayerSpawn(Handle event, const char[] szName, bool dontBroadcast)
{
	int iUserId = GetEventInt(event, "userid");
	int client = GetClientOfUserId(iUserId);
	ResetMenuState(client);
}

// Reset the player whenever they spawn
public ResetMenuState(int client)
{
	g_iFrame[client] = 0;
	g_iMaxFrame[client] = 0;
	g_hMenuState[client][Paused] = false;
	g_hMenuState[client][Rewind] = false;
	g_hMenuState[client][WasPausedLastFrame] = false;
	g_hMenuState[client][FastForward] = false;
	g_hMenuState[client][AutoStrafe] = false;
	g_hMenuState[client][AutoJump] = false;
	g_hMenuState[client][AutoCrouch] = false;
	g_hMenuState[client][TimeScale] = 1.0;
	g_hMenuState[client][CPFrame] = 0;
	g_hMenuState[client][CPTime] = 0.0;
	g_hMenuState[client][GoToCP] = false;
	g_hMenuState[client][SlowMotion] = false;
	g_iEditSpeed[client] = 1;
	g_iSlowMotionSpeed[client] = 2;
	g_iAutoStrafeSkippedFrames[client] = 0;
	SetClientSpeed(client, 1.0);
}

ResetForNewReplay(client)
{
	g_iFrame[client] = 0;
	g_iMaxFrame[client] = 0;
	g_hMenuState[client][Paused] = false;
	//g_hMenuState[client][SlowMotion] = false;
	g_hMenuState[client][WasPausedLastFrame] = false;
	g_hMenuState[client][Rewind] = false;
	g_hMenuState[client][FastForward] = false;
	g_hMenuState[client][CPFrame] = 0;
	g_hMenuState[client][TimeScale] = 1.0;
	g_hMenuState[client][CPTime] = 0.0;
	g_hMenuState[client][GoToCP] = false;
	g_iSlowMotionSkippedFrames[client] = 999;
	g_flLastSpeed[client] = GetSpeed(client);
	if (g_hMenuState[client][SlowMotion]) {
		SetClientSpeed(client, (1.0 / g_iSlowMotionSpeed[client]));
	} else {
		SetClientSpeed(client, 1.0);
	}
	Menu_TAS(client);
}

/*
	Frame Recording/GoTo/CP
*/

GoToClientFrame(client)
{
	Handle hFrame[Frame];
	float flPosition[3];
	float flAngles[3];
	float flVelocity[3];
	int iFrameNumber = g_iFrame[client] - 1;
	
	if (g_hMovements[g_iRecording[client]] == INVALID_HANDLE)
	{
		g_iRecording[client] = -1;
		LogMessage("Invalid handle %i:%i", g_iRecording[client], iFrameNumber);
		return;
	}
	
	if (iFrameNumber < 0 || iFrameNumber >= g_iMaxFrame[client]) {
		
		g_iRecording[client] = -1;
		LogMessage("Invalid frame %i:%i:%i:%i", g_iRecording[client], iFrameNumber, g_iFrame[client], g_iMaxFrame[client]);
		return;
	}
	
	GetArrayArray(g_hMovements[g_iRecording[client]], iFrameNumber, hFrame[0]);
	
	flPosition[0] = hFrame[Position][0];
	flPosition[1] = hFrame[Position][1];
	flPosition[2] = hFrame[Position][2];
	flAngles[0] = hFrame[Angles][0];
	flAngles[1] = hFrame[Angles][1];
	flAngles[2] = hFrame[Angles][2];
	if (g_hMenuState[client][FastForward]) {
		flVelocity[0] = hFrame[Velocity][0];
		flVelocity[1] = hFrame[Velocity][1];
		flVelocity[2] = hFrame[Velocity][2];
		SetEntProp(client, Prop_Data, "m_nButtons", hFrame[Buttons]);
	} else if (g_hMenuState[client][Rewind]) {
		flVelocity[0] = -hFrame[Velocity][0];
		flVelocity[1] = -hFrame[Velocity][1];
		flVelocity[2] = -hFrame[Velocity][2];
		SetEntProp(client, Prop_Data, "m_nButtons", hFrame[Buttons]);
	} else if (g_hMenuState[client][Paused]) {
		flVelocity[0] = 0.0;
		flVelocity[1] = 0.0;
		flVelocity[2] = 0.0;
		SetEntProp(client, Prop_Data, "m_nButtons", 0);
	} else if (g_hMenuState[client][WasPausedLastFrame]) {
		flVelocity[0] = hFrame[Velocity][0];
		flVelocity[1] = hFrame[Velocity][1];
		flVelocity[2] = hFrame[Velocity][2];
		SetEntProp(client, Prop_Data, "m_nButtons", hFrame[Buttons]);
	}
	//seed = hFrame[Seed];
	//impulse = hFrame[Impulse];
	//angles[0] = hFrame[Angles][0];
	//angles[1] = hFrame[Angles][1];
	//angles[2] = hFrame[Angles][2];
	
	TeleportEntity(client, flPosition, flAngles, flVelocity);
	
	decl String:szTargetName[64];
	GetEntPropString(client, Prop_Data, "m_iName", szTargetName, 64);
	
	if (!StrEqual(szTargetName, hFrame[TargetName]))
	{
		SetEntPropString(client, Prop_Data, "m_iName", hFrame[TargetName]);
	}
	
	SetEntityMoveType(client, view_as<MoveType>(hFrame[EntityMoveType]));
	SetEntPropEnt(client, Prop_Data, "m_hGroundEntity", hFrame[GroundEntity]);
	SetEntityFlags(client, hFrame[EntityFlags]);
	SetEntityGravity(client, hFrame[Gravity]);
	
	/*
	   Member: m_bDucked (offset 80) (type integer) (bits 1) (Unsigned)
	   Member: m_bDucking (offset 81) (type integer) (bits 1) (Unsigned)
	   Member: m_flLastDuckTime (offset 84) (type float) (bits 0) (NoScale)
	   Member: m_bInDuckJump (offset 88) (type integer) (bits 1) (Unsigned)
	*/
	
	SetEntProp(client, Prop_Data, "m_bDucked", hFrame[Ducked]);
	SetEntProp(client, Prop_Data, "m_bDucking", hFrame[Ducking]);
	SetEntPropFloat(client, Prop_Data, "m_flLastDuckTime", hFrame[LastDuckTime]);
	SetEntProp(client, Prop_Data, "m_bInDuckJump", hFrame[InDuckJump]);
	
	if (g_bLib_RocketJump)
	{
		RJ_SetRocketShotCount(client, hFrame[RocketsShot]);
	}
	
	// Calculate speed from vectors
	g_flLastSpeed[client] = GetSpeed(client);
	g_iLastFrameTime[client] = hFrame[Time];
	Influx_SetClientStartTick(client, GetGameTickCount() - hFrame[Time]);
}

RecordClientFrame(client, &buttons, &impulse, &seed, &weapon)
{
	if (g_hMenuState[client][SlowMotion]) {
		g_iSlowMotionSkippedFrames[client]++;
		if (g_iSlowMotionSkippedFrames[client] < g_iSlowMotionSpeed[client]) {
			Influx_SetClientStartTick(client, GetGameTickCount() - g_iLastFrameTime[client]);
			//PrintToConsole(client, "Start tick: %i", Influx_GetClientStartTick(client));
			return;
		}
		g_iSlowMotionSkippedFrames[client] = 0;
	}
	Handle hFrame[Frame];
	float flTemp[3];
	
	GetClientAbsOrigin(client, flTemp);
	hFrame[Position][0] = flTemp[0];
	hFrame[Position][1] = flTemp[1];
	hFrame[Position][2] = flTemp[2];
	GetClientEyeAngles(client, flTemp);
	hFrame[Angles][0] = flTemp[0];
	hFrame[Angles][1] = flTemp[1];
	hFrame[Angles][2] = flTemp[2];
	GetClientAbsVelocity(client, flTemp);
	hFrame[Velocity][0] = flTemp[0];
	hFrame[Velocity][1] = flTemp[1];
	hFrame[Velocity][2] = flTemp[2];
	hFrame[Buttons] = buttons;
	hFrame[Impulse] = impulse;
	hFrame[Seed] = seed;
	hFrame[Team] = GetClientTeam(client);
	hFrame[Time] = GetGameTickCount() - Influx_GetClientStartTick(client);
	
	hFrame[SlowMotionEnabled] = g_hMenuState[client][SlowMotion];
	hFrame[SlowMotionSpeed] = g_iSlowMotionSpeed[client];
	
	decl String:szTargetName[64];
	GetEntPropString(client, Prop_Data, "m_iName", szTargetName, 64);
	FormatEx(hFrame[TargetName], sizeof(hFrame[TargetName]), "%s", szTargetName);
	
	hFrame[EntityMoveType] = view_as<int>(GetEntityMoveType(client));
	hFrame[GroundEntity] = GetEntPropEnt(client, Prop_Data, "m_hGroundEntity");
	hFrame[EntityFlags] = GetEntityFlags(client);
	hFrame[Gravity] = GetEntityGravity(client);
	
	/*
	   Member: m_bDucked (offset 80) (type integer) (bits 1) (Unsigned)
	   Member: m_bDucking (offset 81) (type integer) (bits 1) (Unsigned)
	   Member: m_flLastDuckTime (offset 84) (type float) (bits 0) (NoScale)
	   Member: m_bInDuckJump (offset 88) (type integer) (bits 1) (Unsigned)
	*/
	
	hFrame[Ducked] = GetEntProp(client, Prop_Data, "m_bDucked");
	hFrame[Ducking] = GetEntProp(client, Prop_Data, "m_bDucking");
	hFrame[LastDuckTime] = GetEntPropFloat(client, Prop_Data, "m_flLastDuckTime");
	hFrame[InDuckJump] = GetEntProp(client, Prop_Data, "m_bInDuckJump");
	
	if (g_bLib_RocketJump)
	{
		hFrame[RocketsShot] = RJ_GetRocketShotCount(client);
	}
	
	hFrame[Sync] = -1.0;
	hFrame[Flags] = (GetEntityFlags(client) & FL_DUCKING) ? RECFLAG_CROUCH : 0;
	
	// Weapon
	switch (FindSlotByWeapon(client, weapon))
	{
		case SLOT_PRIMARY:
		{
			hFrame[Flags] |= RECFLAG_WEP_SLOT1; // Use recflag so we can just directly copy into recording array.
			
			GetEntityClassname(weapon, hFrame[Weapon_Secondary], sizeof(hFrame[Weapon_Primary]));
		}
		case SLOT_SECONDARY:
		{
			hFrame[Flags] |= RECFLAG_WEP_SLOT2;
			
			GetEntityClassname(weapon, hFrame[Weapon_Secondary], sizeof(hFrame[Weapon_Secondary]));
		}
		case SLOT_MELEE:hFrame[Flags] |= RECFLAG_WEP_SLOT3;
	}
	
	g_iLastFrameTime[client] = hFrame[Time];
	if (g_hMovements[g_iRecording[client]] == INVALID_HANDLE)
	{
		LogMessage("Created array %i", g_iRecording[client]);
		g_hMovements[g_iRecording[client]] = CreateArray(sizeof(nFrameTemplate));
	}
	
	if (g_iFrame[client] != g_iMaxFrame[client]) {
		LogMessage("Resizing array %i", g_iRecording[client]);
		ResizeArray(g_hMovements[g_iRecording[client]], g_iFrame[client]);
	}
	
	PushArrayArray(g_hMovements[g_iRecording[client]], hFrame[0]);
	
	++g_iFrame[client];
	g_iMaxFrame[client] = g_iFrame[client];
}

public SaveCP(client)
{
	if (g_iFrame[client] > 0) {
		g_hMenuState[client][CPFrame] = g_iFrame[client];
		g_hMenuState[client][GoToCP] = false;
	}
}

public GoToLastCP(client)
{
	if (g_hMenuState[client][CPFrame] != 0)
	{
		g_hMenuState[client][GoToCP] = true;
	}
}

public void E_PostThink_Client(int client)
{
	if (Influx_GetClientStyle(client) != STYLE_TAS)
	{
		UnhookThinks(client);
		return;
	}
	
	// Replay related 
	// GoToClientFrame must be done in post think. RecordClientFrame cannot be done here as buttons, impulse, angles etc. must be recorded in OnPlayerRunCmd.
	if (g_hMenuState[client][GoToCP]) {
		if (g_hMenuState[client][CPFrame] >= 1) {
			g_iFrame[client] = g_hMenuState[client][CPFrame];
			g_hMenuState[client][WasPausedLastFrame] = true;
			GoToClientFrame(client);
		}
		g_hMenuState[client][GoToCP] = false;
	} else if (g_hMenuState[client][Rewind]) {
		if (g_iFrame[client] < 2) {
			SetPlayerPaused(client);
		} else {
			g_iEditSpeedSkippedFrames[client]++;
			if (g_iEditSpeedSkippedFrames[client] >= g_iEditSpeed[client])
			{
				--g_iFrame[client];
				g_iEditSpeedSkippedFrames[client] = 0;
			}
		}
		GoToClientFrame(client);
	} else if (g_hMenuState[client][FastForward]) {
		if (g_iFrame[client] + 1 > g_iMaxFrame[client]) {
			SetPlayerPaused(client);
		} else {
			g_iEditSpeedSkippedFrames[client]++;
			if (g_iEditSpeedSkippedFrames[client] >= g_iEditSpeed[client])
			{
				++g_iFrame[client];
				g_iEditSpeedSkippedFrames[client] = 0;
			}
		}
		GoToClientFrame(client);
	} else if (g_hMenuState[client][Paused]) {
		if (g_iFrame[client] >= 1) {
			GoToClientFrame(client);
			g_hMenuState[client][WasPausedLastFrame] = true;
		}
	} else {
		if (g_hMenuState[client][WasPausedLastFrame]) {
			GoToClientFrame(client);
			g_hMenuState[client][WasPausedLastFrame] = false;
		}
	}
	
	// Calculate speed, if slowmotion, reduce by 100 - slowmotionspeedpercent
	// For example, strafing on 0.1x needs to be reduced by 90 percent
	// current / reduced
	
	/*
		float newspeed;
		float reduce;
		float diff;
		reduce = (1.0 / g_iSlowMotionSpeed[client]);
		diff = speed - g_flLastSpeed[client];
		diff = FloatMul(diff, reduce) - slowMotionModify[g_iSlowMotionSpeed[client]];
		newspeed = g_flLastSpeed[client] + diff;
	*/
	
	float speed;
	speed = GetSpeed(client);
	if (g_hMenuState[client][SlowMotion] && !g_hMenuState[client][Paused])
	{
		// Non-compacted up above
		// Clamp speed
		ClampSpeed(client, g_flLastSpeed[client] + FloatMul(speed - g_flLastSpeed[client], (1.0 / g_iSlowMotionSpeed[client])) - slowMotionModify[g_iSlowMotionSpeed[client]]);
	}
	
	g_flLastSpeed[client] = speed;
}

/*
	Timer Related
*/

StopRecording(client)
{
	if (g_iRecording[client] == -1)return;
	
	CloseHandle(g_hMovements[g_iRecording[client]]);
	g_hMovements[g_iRecording[client]] = INVALID_HANDLE;
	g_iMovements--;
	g_iRecording[client] = -1;
	g_iFrame[client] = 0;
}

public StartRecording(client)
{
	int idx = -1;
	for (new i = 0; i < sizeof(g_hMovements); ++i)
	{
		if (g_hMovements[i] == INVALID_HANDLE)
		{
			idx = i;
			break;
		}
	}
	
	if (idx != -1)
	{
		g_hMovements[idx] = CreateArray(sizeof(nFrameTemplate));
		g_iRecording[client] = idx;
		g_iFrame[client] = 0;
		ResetForNewReplay(client);
		g_iMovements++;
	}
	return idx;
}

public void Influx_OnTimerStartPost(int client, int run)
{
	if (Influx_GetClientStyle(client) != STYLE_TAS)return;
	
	StartRecording(client);
	Menu_TAS(client);
}

public void Influx_OnTimerResetPost(int client)
{
	if (Influx_GetClientStyle(client) != STYLE_TAS)return;
	
	ResetForNewReplay(client);
	StopRecording(client);
}

// Build recording array
public Action Influx_OnRequestNewRecording(int client, ArrayList & recording)
{
	if (Influx_GetClientStyle(client) != STYLE_TAS)return Plugin_Continue;
	
	ArrayList hRec;
	hRec = new ArrayList(REC_SIZE);
	Handle hFrame[Frame];
	static int data[REC_SIZE];
	
	int iSlowMotionFrames[11] = 0;
	
	for (int i = 0; i < g_iMaxFrame[client]; i++)
	{
		GetArrayArray(g_hMovements[g_iRecording[client]], i, hFrame[0]);
		
		// They have slow motion enabled, so we need to skip frames.
		if (hFrame[SlowMotionEnabled])
		{
			iSlowMotionFrames[hFrame[SlowMotionSpeed]]++;
			//PrintToChatAll("Slow motion frames: %i  Checking for: %i", iSlowMotionFrames[hFrame[SlowMotionSpeed]], hFrame[SlowMotionSpeed]);
			if (iSlowMotionFrames[hFrame[SlowMotionSpeed]] > hFrame[SlowMotionSpeed])
			{
				continue;
			}
			iSlowMotionFrames[hFrame[SlowMotionSpeed]] = 0;
		}
		
		CopyArray(hFrame[Position], data[REC_POS], 3);
		CopyArray(hFrame[Angles], data[REC_ANG], 2);
		data[REC_SYNC] = view_as<int>(hFrame[Sync]);
		data[REC_FLAGS] = hFrame[Flags];
		
		if (hFrame[Buttons] & IN_ATTACK)
		{
			data[REC_FLAGS] |= RECFLAG_ATTACK;
		}
		
		if (hFrame[Buttons] & IN_ATTACK2)
		{
			data[REC_FLAGS] |= RECFLAG_ATTACK2;
		}
		
		hRec.PushArray(data);
	}
	// PrintToChatAll("Final recording size: %i  Max frames: %i", hRec.Length, g_iMaxFrame[client]);
	
	recording = hRec;
	
	// PrintToChatAll("Final:%i  Recording size: %i", hRec.Length, recording.Length);
	
	return Plugin_Changed;
}

public void Influx_OnTimerFinishPost(int client)
{
	StopRecording(client);
}

/*
	Stocks
*/
stock float GetSpeed(int client)
{
	return GetEntitySpeed(client);
}

stock int FindSlotByWeapon(int client, int weapon)
{
	for (int i = 0; i < SLOTS_SAVED; i++)
	{
		if (weapon == GetPlayerWeaponSlot(client, i))return i;
	}
	
	return -1;
}

public SetClientSpeed(int client, float flSpeed)
{
	SetEntPropFloat(client, Prop_Send, "m_flLaggedMovementValue", flSpeed);
}

stock void UnhookThinks(int client)
{
	SDKUnhook(client, SDKHook_PostThinkPost, E_PostThink_Client);
}

public void ClampSpeed(int client, float clampSpeed)
{
	float speed = GetSpeed(client);
	if (speed >= clampSpeed) {
		
		//PrintToChatAll("clamping %.1f to %.1f", speed, clampSpeed);
		float fVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", fVelocity);
		
		if (fVelocity[0] == 0.0)
			fVelocity[0] = 1.0;
		if (fVelocity[1] == 0.0)
			fVelocity[1] = 1.0;
		if (fVelocity[2] == 0.0)
			fVelocity[2] = 1.0;
		
		float Multpl = speed / clampSpeed;
		fVelocity[0] /= Multpl;
		fVelocity[1] /= Multpl;
		
		TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, fVelocity);
	}
}

bool GetClientAbsVelocity(client, float velocity[3])
{
	static offset = -1;
	if (offset == -1 && (offset = FindDataMapInfo(client, "m_vecAbsVelocity")) == -1)
	{
		return false;
	}
	GetEntDataVector(client, offset, velocity);
	return true;
}

stock GetClientDistanceToGround(client)
{
	// Player is already standing on the ground?
	if (GetEntPropEnt(client, Prop_Send, "m_hGroundEntity") == 0)
		return 0.0;
	
	new Float:fOrigin[3], Float:fGround[3];
	GetClientAbsOrigin(client, fOrigin);
	
	fOrigin[2] += 10.0;
	
	TR_TraceRayFilter(fOrigin, Float: { 90.0, 0.0, 0.0 }, MASK_PLAYERSOLID, RayType_Infinite, TraceRayNoPlayers, client);
	if (TR_DidHit())
	{
		TR_GetEndPosition(fGround);
		fOrigin[2] -= 10.0;
		return GetVectorDistance(fOrigin, fGround);
	}
	return 0.0;
}

public bool:TraceRayNoPlayers(entity, mask, any:data)
{
	if (entity == data || (entity >= 1 && entity <= MaxClients))
	{
		return false;
	}
	return true;
}

// NATIVE
public Native_IsClientTASPaused(Handle hPlugin, int nParams)
{
	return (g_hMenuState[GetNativeCell(1)][Paused] || g_hMenuState[GetNativeCell(1)][Rewind]);
}

public Native_IsClientTASRewinding(Handle hPlugin, int nParams)
{
	return (g_hMenuState[GetNativeCell(1)][Rewind]);
}

public Native_IsClientTASSlowMotion(Handle hPlugin, int nParams)
{
	return (g_hMenuState[GetNativeCell(1)][SlowMotion]);
}

public Native_SetClientTASPaused(Handle hPlugin, int nParams)
{
	if (GetNativeCell(2) == false)
	{
		g_hMenuState[GetNativeCell(1)][Paused] = false;
		g_hMenuState[GetNativeCell(1)][Rewind] = false;
	}
	else
	{
		g_hMenuState[GetNativeCell(1)][Paused] = true;
	}
}

public Native_ResetClientForTAS(Handle hPlugin, int nParams)
{
	ResetForNewReplay(GetNativeCell(1));
} 
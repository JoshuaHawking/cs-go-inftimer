/**
 * vim: set ts=4 :
 * =============================================================================
 * Rock The Vote Extended
 * Creates a map vote when the required number of players have requested one.
 *
 * Rock The Vote Extended (C)2012-2013 Powerlord (Ross Bemrose)
 * SourceMod (C)2004-2007 AlliedModders LLC.  All rights reserved.
 * =============================================================================
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * As a special exception, AlliedModders LLC gives you permission to link the
 * code of this program (as well as its derivative works) to "Half-Life 2," the
 * "Source Engine," the "SourcePawn JIT," and any Game MODs that run on software
 * by the Valve Corporation.  You must obey the GNU General Public License in
 * all respects for all other code used.  Additionally, AlliedModders LLC grants
 * this exception to all derivative works.  AlliedModders LLC defines further
 * exceptions, found in LICENSE.txt (as of this writing, version JULY-31-2007),
 * or <http://www.sourcemod.net/license.php>.
 *
 * Version: $Id$
 */

#include <sourcemod>
#include <mapchooser>
#include "include/mapchooser_extended"
#include <nextmap>
#include <colors>

#pragma semicolon 1

public Plugin myinfo = 
{
	name = "Rock The Vote - HOC Edition", 
	author = "Powerlord and AlliedModders LLC, Josh", 
	description = "Provides RTV Map Voting", 
	version = "1.0.0", 
	url = "https://houseofclimb.com"
};

ConVar g_ConVar_WeightNeeded;
ConVar g_ConVar_MinPlayers;
ConVar g_ConVar_ChangeTime;
ConVar g_ConVar_InitialDelay;
ConVar g_ConVar_VoteDelay;
ConVar g_ConVar_PostVoteAction;

bool g_bCanRTV; 				// Maps have loaded correctly
bool g_bRTVAllowed; 			// Players can vote.

bool g_bInChange;				// Map currently in change
bool g_bVoted[MAXPLAYERS + 1]; 	// Stores the players who have voted

int g_iClientsConnected;		// Total amount of real players online

#define CHANGETIME_INSTANT		0
#define CHANGETIME_ROUNDEND 	1
#define CHANGETIME_MAPEND		2

#define POSTACTION_INSTANT		0
#define POSTACTION_DENY		 	1

public OnPluginStart()
{
	// TRANSLATIONS
	LoadTranslations("common.phrases");
	LoadTranslations("rockthevote.phrases");
	LoadTranslations("basevotes.phrases");
	
	// CONVARS
	g_ConVar_WeightNeeded = CreateConVar("sm_rtv_needed", "0.55", "Percentage of players required to RTV before the vote occurs", _, true, 0.0, true, 1.0);
	g_ConVar_MinPlayers = CreateConVar("sm_rtv_minplayers", "0", "Number of players required before RTV is enabled", _, true, 0.0, true, 64.0);
	g_ConVar_ChangeTime = CreateConVar("sm_rtv_changetime", "0", "When to change the map after a succesful RTV: 0 - Instant, 1 - RoundEnd, 2 - MapEnd", _, true, 0.0, true, 2.0);
	g_ConVar_InitialDelay = CreateConVar("sm_rtv_initialdelay", "30.0", "Time (in seconds) before RTV is enabled", _, true, 0.0);
	g_ConVar_VoteDelay = CreateConVar("sm_rtv_interval", "240.0", "Time (in seconds) before another RTV vote can be held.", _, true, 0.0);
	g_ConVar_PostVoteAction = CreateConVar("sm_rtv_postvoteaction", "0", "What to do with RTV's after a mapvote has completed. 0 - Allow, success = instant change, 1 - Deny", _, true, 0.0, true, 1.0);
	
	// EVENTS
	RegConsoleCmd("say", Cmd_Say);
	RegConsoleCmd("say_team", Cmd_Say);
	
	RegConsoleCmd("sm_rtv", Cmd_RTV);
	RegConsoleCmd("sm_abc", FakeClientConnect);
	
	RegAdminCmd("sm_forcertv", Cmd_ForceRTV, ADMFLAG_CHANGEMAP, "Force an RTV vote");
	RegAdminCmd("mce_forcertv", Cmd_ForceRTV, ADMFLAG_CHANGEMAP, "Force an RTV vote");
	
	AutoExecConfig(true, "rtv");
}

public OnMapStart()
{
	/* Handle late load */
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientConnected(i))
		{
			OnClientConnected(i);
		}
	}
}

public OnMapEnd()
{
	g_bCanRTV = false;
	g_bRTVAllowed = false;
}

public OnConfigsExecuted()
{
	g_bCanRTV = true; 			// Configs loaded, enable the ability to RTV
	g_bRTVAllowed = false;
	
	// Right after the inital delay of course :-)
	CreateTimer(GetConVarFloat(g_ConVar_InitialDelay), Timer_DelayRTV, _, TIMER_FLAG_NO_MAPCHANGE);
}

public OnClientConnected(int client)
{
	// Check for fake client
	if (IsFakeClient(client))
		return;

	g_bVoted[client] = false;
	g_iClientsConnected++;
}

public OnClientDisconnect(int client)
{
	// Check for fake client
	if (IsFakeClient(client))
		return;
		
	g_bVoted[client] = false;
	g_iClientsConnected--;
	
	CheckForRTV();
}

// Commands

public Action Cmd_ForceRTV(int client, int args)
{
	if (!g_bCanRTV)
		return Plugin_Handled;
	
	ShowActivity2(client, CHAT_PREFIX, " %t", "Initiated Vote Map");
	
	StartRTV();
	
	return Plugin_Handled;	
}

public Action Cmd_RTV(int client, int args)
{
	// Check whether they can RTV (before configs) and that they are a valid client
	if (!g_bCanRTV || !client)
		return Plugin_Handled;
		
	AttemptRTVVote(client);
	
	return Plugin_Handled;
}

public Action FakeClientConnect(int client, int args)
{
	g_iClientsConnected++;
}

// I'm sure there is a better way of doing this, but whatever
public Action Cmd_Say(int client, int args)
{
	// Check whether they can RTV (before configs) and that they are a valid client
	if (!g_bCanRTV || !client)
		return Plugin_Continue;
	
	// Check whether they have said anything
	decl String:szText[192];
	if (!GetCmdArgString(szText, sizeof(szText)))
	{
		return Plugin_Continue;
	}
	
	// Checks whether someone has included "", usually for console. say "rtv"
	int iStartId = 0;
	if (szText[strlen(szText) - 1] == '"')
	{
		szText[strlen(szText) - 1] = '\0';
		iStartId = 1;
	}
	
	ReplySource hOld = SetCmdReplySource(SM_REPLY_TO_CHAT);
	
	// Check whether they said rtv / rockthevote
	if (strcmp(szText[iStartId], "rtv", false) == 0 || strcmp(szText[iStartId], "rockthevote", false) == 0)
	{
		AttemptRTVVote(client);
	}
	
	SetCmdReplySource(hOld);
	
	return Plugin_Continue;
}

// Attempt to enable vote for a client
AttemptRTVVote(int client)
{
	if (!g_bRTVAllowed || (GetConVarInt(g_ConVar_PostVoteAction) == POSTACTION_DENY && HasEndOfMapVoteFinished()))
	{
		CReplyToCommand(client, "%s %t", CHAT_PREFIX, "RTV Not Allowed");
		return;
	}
	
	if (!CanMapChooserStartVote())
	{
		CReplyToCommand(client, "%s %t", CHAT_PREFIX, "RTV Started");
		return;
	}
	
	if (g_iClientsConnected < GetConVarInt(g_ConVar_MinPlayers))
	{
		CReplyToCommand(client, "%s %t", CHAT_PREFIX, "Minimal Players Not Met");
		return;
	}
	
	if (g_bVoted[client])
	{
		CReplyToCommand(client, "%s %t", CHAT_PREFIX, "Already Voted", GetTotalVotes(), GetVotesNeeded());
		return;
	}
	
	g_bVoted[client] = true;
	char szName[MAX_NAME_LENGTH];
	GetClientName(client, szName, sizeof(szName));
	
	CPrintToChatAll("%s %t", CHAT_PREFIX, "RTV Requested", szName, GetTotalVotes(), GetVotesNeeded());
	
	CheckForRTV();
}
// Returns the amount of players that have voted for RTV
int GetTotalVotes()
{
	int count;
	for (new i = 1; i <= MAXPLAYERS; i++)
	{
		if(g_bVoted[i])
		{
			count++;
		}
	}
	
	return count;
}

int GetVotesNeeded()
{
	switch(g_iClientsConnected)
	{
		case 1, 2, 3:return g_iClientsConnected;
	}
	
	return RoundToCeil(float(g_iClientsConnected) * GetConVarFloat(g_ConVar_WeightNeeded));
}

// Check whether the vote should begin
CheckForRTV()
{
	int iVotes = GetTotalVotes();
	int iVotesNeeded = GetVotesNeeded();
	
	if(iVotes >= iVotesNeeded && g_bRTVAllowed && g_bCanRTV)
	{
		//PrintToChatAll("Map vote end: %s", (HasEndOfMapVoteFinished() ? "Yes" : "No"));
		
		if(GetConVarInt(g_ConVar_PostVoteAction) == POSTACTION_DENY && HasEndOfMapVoteFinished())
		{
			return;
		}
		
		StartRTV();
	}
}

StartRTV()
{
	if (g_bInChange)
	{
		return;
	}
	
	// RTV has already been passed, change straight away rather than waiting for map/round end.
	if (EndOfMapVoteEnabled() && HasEndOfMapVoteFinished())
	{
		// Change straight away
		char szMap[PLATFORM_MAX_PATH];
		
		if (GetNextMap(szMap, sizeof(szMap)))
		{
			CPrintToChatAll("%s %t", CHAT_PREFIX, "Changing Maps", szMap);
			CreateTimer(5.0, Timer_ChangeMap, _, TIMER_FLAG_NO_MAPCHANGE);
			g_bInChange = true;
			
			ResetRTV();
			
			g_bRTVAllowed = false;
		}
		
		return;
	}
	
	// Otherwise, start map vote
	if (CanMapChooserStartVote())
	{
		MapChange hDelay = view_as<MapChange>(GetConVarInt(g_ConVar_ChangeTime));
		InitiateMapChooserVote(hDelay);
		
		ResetRTV();
		
		g_bRTVAllowed = false;
		CreateTimer(GetConVarFloat(g_ConVar_VoteDelay), Timer_DelayRTV, _, TIMER_FLAG_NO_MAPCHANGE);
	}
}

// Force change map, RTV after mapvote
public Action Timer_ChangeMap(Handle hTimer)
{
	g_bInChange = false;
	
	LogMessage("RTV changing map manually");
	
	char szMap[PLATFORM_MAX_PATH];
	if (GetNextMap(szMap, sizeof(szMap)))
	{
		ForceChangeLevel(szMap, "RTV after mapvote");
	}
	
	return Plugin_Stop;
}

// Allow RTV voting after delay
public Action Timer_DelayRTV(Handle hTimer)
{
	g_bRTVAllowed = true;
}

// Reset everyone
ResetRTV()
{
	for (new i = 1; i <= MAXPLAYERS; i++)
	{
		g_bVoted[i] = false;
	}
}

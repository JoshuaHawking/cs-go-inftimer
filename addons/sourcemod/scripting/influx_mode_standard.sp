#include <sourcemod>
#include <sdkhooks>

#include <influx/core>
#include <influx/stocks_core>

#undef REQUIRE_PLUGIN
#include <influx/styleinfo>

/*
	100 AA
*/

float g_flAirAccelerate;
ConVar g_ConVar_AirAccelerate;
ConVar g_ConVar_Standard_AirAccelerate;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Mode - Standard", 
	description = "", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	// CONVARS
	if ((g_ConVar_AirAccelerate = FindConVar("sv_airaccelerate")) == null)
	{
		SetFailState(INF_CON_PRE..."Couldn't find handle for sv_airaccelerate!");
	}
	
	g_ConVar_Standard_AirAccelerate = CreateConVar("influx_standard_airaccelerate", "100", "", FCVAR_NOTIFY);
	g_ConVar_Standard_AirAccelerate.AddChangeHook(E_CvarChange_Standard_AA);
	
	g_flAirAccelerate = g_ConVar_Standard_AirAccelerate.FloatValue;
	
	// CMDS
	RegConsoleCmd("sm_standardm", Cmd_Mode_Standard, INF_NAME..." - Change your mode to Standard.");
}

public void OnAllPluginsLoaded()
{
	AddMode();
}

public void OnPluginEnd()
{
	Influx_RemoveMode(MODE_STANDARD);
}
public void Influx_OnRequestModes()
{
	AddMode();
}

public void Influx_RequestModeInfo()
{
	Influx_AddModeInfo(MODE_STANDARD, "Just your basic old mode, nothing special here.");
}

stock void AddMode()
{
	if (!Influx_AddMode(MODE_STANDARD, "Standard", "STANDARDM"))
	{
		SetFailState(INF_CON_PRE..."Couldn't add mode! (%i)", MODE_STANDARD);
	}
}

public Action Influx_OnSearchType(const char[] szArg, Search_t &type, int &value)
{
	PrintToChatAll("Testing for: %s for 'standardm'", szArg);
	if (StrEqual(szArg, "standardm", false))
	{
		value = MODE_STANDARD;
		type = SEARCH_MODE;
		
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}

public Action Cmd_Mode_Standard(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	
	Influx_SetClientMode(client, MODE_STANDARD);
	
	return Plugin_Handled;
}

public void E_CvarChange_Standard_AA(ConVar convar, const char[] oldval, const char[] newval)
{
	g_flAirAccelerate = convar.FloatValue;
}

public Action Influx_OnClientModeChange(int client, int mode, int lastmode)
{
	if (mode == MODE_STANDARD)
	{
		if (!Inf_SDKHook(client, SDKHook_PreThinkPost, E_PreThinkPost_Client))
		{
			return Plugin_Handled;
		}
		
		Inf_SendConVarValueFloat(client, g_ConVar_AirAccelerate, g_ConVar_Standard_AirAccelerate.FloatValue);
	}
	
	return Plugin_Continue;
}
stock void UnhookThinks(int client)
{
	SDKUnhook(client, SDKHook_PreThinkPost, E_PreThinkPost_Client);
}

public void E_PreThinkPost_Client(int client)
{
	g_ConVar_AirAccelerate.FloatValue = g_flAirAccelerate;
} 
#include <sourcemod>

#include <influx/core>

#include <influx/jumps>
#include <influx/strafes>
#include <influx/sync>
#include <influx/hud>

#undef REQUIRE_PLUGIN
#include <influx/records>
#include <influx/parkour>
#include <rocketjump>

#include <influx/profiler>
#define DEBUG_PROFILING

ConVar g_ConVar_NumDecimals;

bool g_bLib_Hud;
bool g_bLib_Jumps;
bool g_bLib_Strafes;
bool g_bLib_Sync;
bool g_bLib_Records;
bool g_bLib_Rocketjump;

// Style libaries
bool g_bLib_Style_Parkour;

#if defined DEBUG_PROFILING
int iProfiling;
#endif

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Chat Records", 
	description = "Displays records in chat.", 
	version = INF_VERSION
};

public void OnPluginStart()
{
	g_ConVar_NumDecimals = CreateConVar("influx_recchat_numdecimals", "2", "Number of decimals to use when printing to chat.", FCVAR_NOTIFY, true, 0.0, true, 3.0);
	
	
	AutoExecConfig(true, "recchat", "influx");
	
	g_bLib_Hud = LibraryExists(INFLUX_LIB_HUD);
	g_bLib_Jumps = LibraryExists(INFLUX_LIB_JUMPS);
	g_bLib_Strafes = LibraryExists(INFLUX_LIB_STRAFES);
	g_bLib_Sync = LibraryExists(INFLUX_LIB_SYNC);
	g_bLib_Records = LibraryExists(INFLUX_LIB_RECORDS);
	g_bLib_Rocketjump = LibraryExists(LIB_ROCKETJUMP);
	
	g_bLib_Style_Parkour = LibraryExists(INFLUX_LIB_STYLE_PARKOUR);
	
	#if defined DEBUG_PROFILING
	iProfiling = Influx_RegisterProfiler("influx_recchat OnTimerFinishPost");
	#endif
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_HUD))g_bLib_Hud = true;
	if (StrEqual(lib, INFLUX_LIB_JUMPS))g_bLib_Jumps = true;
	if (StrEqual(lib, INFLUX_LIB_STRAFES))g_bLib_Strafes = true;
	if (StrEqual(lib, INFLUX_LIB_SYNC))g_bLib_Sync = true;
	if (StrEqual(lib, INFLUX_LIB_RECORDS))g_bLib_Records = true;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_Rocketjump = true;
	
	if (StrEqual(lib, INFLUX_LIB_STYLE_PARKOUR))g_bLib_Style_Parkour = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_HUD))g_bLib_Hud = false;
	if (StrEqual(lib, INFLUX_LIB_JUMPS))g_bLib_Jumps = false;
	if (StrEqual(lib, INFLUX_LIB_STRAFES))g_bLib_Strafes = false;
	if (StrEqual(lib, INFLUX_LIB_SYNC))g_bLib_Sync = false;
	if (StrEqual(lib, INFLUX_LIB_RECORDS))g_bLib_Records = false;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_Rocketjump = false;
	
	if (StrEqual(lib, INFLUX_LIB_STYLE_PARKOUR))g_bLib_Style_Parkour = false;
}

public void Influx_OnRequestResultFlags()
{
	Influx_AddResultFlag("Don't print record chat message", RES_CHAT_DONTPRINT);
}

stock bool ShouldPrint(float time, int flags)
{
	// We don't want to print for this run.
	if (flags & RES_CHAT_DONTPRINT)return false;
	
	
	// Let them see best records always!
	if (flags & (RES_TIME_ISBEST | RES_TIME_FIRSTREC))return true;
	
	return true;
}

stock bool CanPrintToClient(int client, int finisher, int flags)
{
	int hideflags = Influx_GetClientHideFlags(client);
	
	// Allow my own sounds.
	if (client == finisher)
	{
		return (hideflags & HIDEFLAG_CHAT_PERSONAL) ? false : true;
	}
	
	// Allow best sounds.
	if (flags & (RES_TIME_ISBEST | RES_TIME_FIRSTREC))
	{
		return (hideflags & HIDEFLAG_CHAT_BEST) ? false : true;
	}
	
	return (hideflags & HIDEFLAG_CHAT_NORMAL) ? false : true;
}

public void Influx_OnTimerFinishPost(int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags, int currentrank, int newrank)
{
	if (!ShouldPrint(time, flags))return;
	
	#if defined DEBUG_PROFILING
	Influx_StartProfiling(iProfiling);
	#endif
	
	int nClients = 0;
	int[] clients = new int[MaxClients];
	
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && !IsFakeClient(i))
		{
			if (g_bLib_Hud)
			{
				if (!CanPrintToClient(i, client, flags))continue;
			}
			
			clients[nClients++] = i;
		}
	}
	
	if (!nClients)return;
	
	bool isfirst = (flags & RES_TIME_FIRSTREC) ? true : false;
	bool isbest = (flags & RES_TIME_ISBEST) ? true : false;
	bool isfirstown = (flags & RES_TIME_FIRSTOWNREC) ? true : false;
	bool ispb = (flags & RES_TIME_PB) ? true : false;
	// Format our second formatting string.
	char szFormSec[10];
	Inf_DecimalFormat(g_ConVar_NumDecimals.IntValue, szFormSec, sizeof(szFormSec));
	
	
	char szName[MAX_NAME_LENGTH];
	char szForm[10];
	char szRun[MAX_RUN_NAME];
	char szMode[64];
	char szStyle[64];
	char szImprove[128];
	
	char szSRImprove[64];
	char szPBImprove[64];
	char szSymbol[1];
	char szTimeFormatted[10];
	float flTimeDifference;
	
	szImprove[0] = '\0';
	szSRImprove[0] = '\0';
	szPBImprove[0] = '\0';
	// Not first record, as we can't display a improvement on SR.
	if (!isfirst) {
		
		// No World Record
		if (time >= prev_best) {
			szSymbol = "+";
			flTimeDifference = time - prev_best;
		}
		else // New World Record
		{
			szSymbol = "-";
			flTimeDifference = prev_best - time;
		}
		
		// Format time
		Inf_FormatSeconds(flTimeDifference, szTimeFormatted, sizeof(szTimeFormatted), szFormSec);
		
		FormatEx(szSRImprove, sizeof(szSRImprove), "{%s}%c%s SR", 
			(isbest) ? "LIMEGREEN" : "LIGHTRED", 
			szSymbol, 
			szTimeFormatted
			)
	}
	
	// Not first own record, as we can't display a improvement on PB. Check ifnot best, as we don't need to display -SR and -PB.
	if (!isfirstown && !isbest && currentrank != 1) {
		// No Personal Best Record
		if (time >= prev_pb) {
			szSymbol = "+";
			flTimeDifference = time - prev_pb;
		}
		else // New Personal Best Record
		{
			szSymbol = "-";
			flTimeDifference = prev_pb - time;
		}
		
		// Format time
		Inf_FormatSeconds(flTimeDifference, szTimeFormatted, sizeof(szTimeFormatted), szFormSec);
		
		FormatEx(szPBImprove, sizeof(szPBImprove), "{%s}%c%s PB", 
			(ispb) ? "LIMEGREEN" : "LIGHTRED", 
			szSymbol, 
			szTimeFormatted
			)
	}
	
	if (szSRImprove[0] != '\0' || szPBImprove[0] != '\0') {
		FormatEx(szImprove, sizeof(szImprove), " {CHATCLR}(%s%s%s{CHATCLR})", 
			(szSRImprove[0] != '\0') ? szSRImprove : "", 
			(szSRImprove[0] != '\0' && szPBImprove[0] != '\0') ? "{CHATCLR}, " : "", 
			(szPBImprove[0] != '\0') ? szPBImprove : ""
			)
	}
	
	Inf_FormatSeconds(time, szForm, sizeof(szForm), szFormSec);
	
	// Get mode / style
	char szDetails[64];
	
	Influx_GetModeName(mode, szMode, sizeof(szMode));
	Influx_GetStyleName(style, szStyle, sizeof(szStyle));
	
	FormatEx(szDetails, sizeof(szDetails), "{GREY}[{PINK}%s - %s{GREY}]", szMode, szStyle);
	
	// Get run
	char szRunDetails[64]
	
	// Check if not main
	if (runid != 1)
	{
		Influx_GetRunName(runid, szRun, sizeof(szRun));
		FormatEx(szRunDetails, sizeof(szRunDetails), "{GREY}[{PINK}%s{GREY}]", szRun);
	}
	
	GetClientName(client, szName, sizeof(szName));
	Influx_RemoveChatColors(szName, sizeof(szName));
	
	// New server record
	if (isbest || isfirst) {
		Influx_PrintToChatEx(PRINTFLAGS_NOPREFIX, client, clients, nClients, " {LIMEGREEN}NEW SERVER RECORD");
		Influx_PrintToChatEx(PRINTFLAGS_NOPREFIX, client, clients, nClients, " {LIGHTGREEN}NEW SERVER RECORD");
		Influx_PrintToChatEx(PRINTFLAGS_NOPREFIX, client, clients, nClients, " {WHITE}NEW SERVER RECORD");
	}
	
	char szRecordColor[11] = "{LIMEGREEN}";
	
	char szRecordRank[14];
	if (g_bLib_Records)
	{
		int totalRank;
		if (isfirst) {
			totalRank = 1;
		} else {
			totalRank = Influx_GetRecordTotalRank(runid, mode, style);
			
			if (isfirstown)totalRank++;
		}
		Format(szRecordRank, sizeof(szRecordRank), " #%i/%i", newrank, totalRank);
	}
	Influx_PrintToChatEx(PRINTFLAGS_NOPREFIX, client, clients, nClients, " %s%s%s {MAINCLR1}%s{CHATCLR} in %s%s{CHATCLR}%s%s", 
		szRunDetails, 
		(szRunDetails[0] != '\0') ? " " : "", 
		szDetails, 
		szName, 
		szRecordColor, 
		szForm, 
		(szImprove[0] != '\0') ? szImprove : "", 
		szRecordRank
		);
	
	// Print stats about run.
	
	char szStat[256];
	
	if (g_bLib_Jumps)
		FormatEx(szStat, sizeof(szStat), "%s{CHATCLR}Jumps: {LIGHTGREEN}%i ", szStat, Influx_GetClientJumpCount(client));
	
	if (g_bLib_Strafes)
		FormatEx(szStat, sizeof(szStat), "%s{CHATCLR}Strafes: {LIGHTGREEN}%i ", szStat, Influx_GetClientStrafeCount(client));
	
	if (g_bLib_Sync)
		FormatEx(szStat, sizeof(szStat), "%s{CHATCLR}Sync: {LIGHTGREEN}%.1f ", szStat, Influx_GetClientStrafeSync(client));
	
	if (g_bLib_Rocketjump)
		FormatEx(szStat, sizeof(szStat), "%s{CHATCLR}Rockets Shot: {LIGHTGREEN}%i ", szStat, RJ_GetRocketShotCount(client));
	
	// Other stats
	if (g_bLib_Style_Parkour && style == STYLE_PARKOUR)
	{
		FormatEx(szStat, sizeof(szStat), "%s{CHATCLR}Boosts: {LIGHTGREEN}%i ", szStat, Influx_GetClientParkourBoosts(client));
	}
	
	if (szStat[0] != '0')
	{
		Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " %s", szStat);
	}
	
	Influx_PrintToChat(PRINTFLAGS_NOPREFIX, client, " ");
	
	#if defined DEBUG_PROFILING
	Influx_EndProfiling(iProfiling);
	#endif
}

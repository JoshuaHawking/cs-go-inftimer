#include <sourcemod>
#include <cstrike>
#include <smlib>
#include <clientprefs>

#include <influx/core>
#include <influx/speclist>

#include <msharedutil/misc>
#include <msharedutil/arrayvec>

#undef REQUIRE_PLUGIN
#include <influx/pause>
#include <influx/hideme>

#define SPECMODE_NONE 				0
#define SPECMODE_FIRSTPERSON 		4
#define SPECMODE_3RDPERSON 			5
#define SPECMODE_FREELOOK	 		6

#define SPECLIST_UPDATE             2

bool g_bLib_HideMe;

// !SPECLIST
bool g_bSpecList[INF_MAXPLAYERS] = false; // Stores who has !speclist enabled
bool g_bRedrawSpecList[INF_MAXPLAYERS] = false; // Stores whether the player's speclist can be redrawn
Handle g_hSpecListTimer[INF_MAXPLAYERS];
Handle g_hSpecListMenu[INF_MAXPLAYERS];

Handle g_hSpeclistEnabled;

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Speclist", 
	description = "Allows clients to view who they are spectating - !speclist", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	// LIBRARIES
	RegPluginLibrary(INFLUX_LIB_SPECLIST);
	
	// NATIVES
	CreateNative("Influx_HasSpeclistEnabled", Native_HasSpeclistEnabled);
	CreateNative("Influx_SetSpeclistEnabled", Native_SetSpeclistEnabled);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	LoadTranslations("common.phrases");
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegConsoleCmd("sm_speclist", Cmd_ToggleSpecList);
	
	g_bLib_HideMe = LibraryExists(INFLUX_LIB_HIDEME);
	
	g_hSpeclistEnabled = RegClientCookie("influx_speclist_enabled", "Whether speclist is enabled or not", CookieAccess_Private)
}

public void Influx_OnClientIdRetrieved(int client, int uid)
{
	if (IsFakeClient(client))return;
	
	if (!AreClientCookiesCached(client))return;
	
	// Check whether it's loaded
	char szEnabled[4];
	GetClientCookie(client, g_hSpeclistEnabled, szEnabled, sizeof(szEnabled));
	
	if (szEnabled[0] == '\0') // No value set
	{
		SetClientCookie(client, g_hSpeclistEnabled, "0");
		g_bSpecList[client] = true
	}
	else
	{
		g_bSpecList[client] = StrEqual(szEnabled, "1", false)
	}
	
	Update_Speclist(client)
}

public void OnMapEnd()
{
	for (int i = 1; i <= MaxClients; i++)
	{
		ClearMenu(i);
	}
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_HIDEME))g_bLib_HideMe = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_HIDEME))g_bLib_HideMe = false;
}

/*
	Speclist
*/
public Action Cmd_ToggleSpecList(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	g_bSpecList[client] = !g_bSpecList[client];
	
	Update_Speclist(client)
	
	Influx_PrintToChat(_, client, "%t", "SPECLISTENABLED", g_bSpecList[client] ? "ON" : "OFF");
	
	return Plugin_Handled;
}

// Create / delete speclist timer
public Update_Speclist(int client)
{
	if (g_bSpecList[client])
	{
		// Create timer
		g_hSpecListTimer[client] = CreateTimer(float(SPECLIST_UPDATE), Timer_UpdateSpeclist, client, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
		SetClientCookie(client, g_hSpeclistEnabled, "1");
	}
	else
	{
		g_bRedrawSpecList[client] = false;
		
		//if (g_hSpecListTimer[client] != INVALID_HANDLE)
		//KillTimer(g_hSpecListTimer[client]);
		
		// Send a empty menu to wipe it.
		ClearMenu(client);
		
		SetClientCookie(client, g_hSpeclistEnabled, "0");
	}
}

public OnClientPutInServer(int client)
{
	ClearMenu(client);
	g_bSpecList[client] = false;
	g_bRedrawSpecList[client] = false;
}

public OnClientDisconnect(int client)
{
	KillSpeclistTimer(client);
}

public Action Timer_UpdateSpeclist(Handle timer, int client)
{
	if (!IsValidEntity(client) || !IsClientInGame(client))
	{
		KillSpeclistTimer(client);
		
		return Plugin_Stop;
	}
	
	int iSpecModeUser = GetEntProp(client, Prop_Send, "m_iObserverMode");
	int iSpecMode, iTarget, iTargetUser, iSpecCount;
	
	decl String:szText[254];
	szText[0] = '\0';
	
	// Dealing with a client who is in the game and playing.
	if (IsPlayerAlive(client))
	{
		// Check for clients who are spectating.
		for (new i = 1; i <= MaxClients; i++)
		{
			if (!IsClientInGame(i) || !IsClientObserver(i))
				continue;
			
			iSpecMode = GetEntProp(i, Prop_Send, "m_iObserverMode");
			
			// The client isn't spectating any one person, so ignore them.
			if (iSpecMode != SPECMODE_FIRSTPERSON && iSpecMode != SPECMODE_3RDPERSON)
				continue;
			
			// Find out who the client is spectating.
			iTarget = GetEntPropEnt(i, Prop_Send, "m_hObserverTarget");
			
			// Are they spectating our player?
			if (iTarget == client && (g_bLib_HideMe ? !Influx_IsClientHidden(i) : true))
			{
				iSpecCount++;
				Format(szText, sizeof(szText), "%s%N\n", szText, i);
			}
		}
		
		Format(szText, sizeof(szText), "Spectating %N (%i):\n%s", client, iSpecCount, szText);
	}
	// A client is currently spectating.
	else if (iSpecModeUser == SPECMODE_FIRSTPERSON || iSpecModeUser == SPECMODE_3RDPERSON)
	{
		// Find out who the User is spectating.
		iTargetUser = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
		
		for (new i = 1; i <= MaxClients; i++)
		{
			if (!IsClientInGame(i) || !IsClientObserver(i))
				continue;
			
			iSpecMode = GetEntProp(i, Prop_Send, "m_iObserverMode");
			
			// The client isn't spectating any one person, so ignore them.
			if (iSpecMode != SPECMODE_FIRSTPERSON && iSpecMode != SPECMODE_3RDPERSON)
				continue;
			
			// Find out who the client is spectating.
			iTarget = GetEntPropEnt(i, Prop_Send, "m_hObserverTarget");
			
			// Are they spectating the same player as client is spectating?
			if (iTarget == iTargetUser && (g_bLib_HideMe ? !Influx_IsClientHidden(i) : true))
			{
				bool bAdd = false;
				if (g_bLib_HideMe)
				{
					bAdd = !Influx_IsClientHidden(i);
				}
				else
				{
					bAdd = true;
				}
				
				if (bAdd)
				{
					iSpecCount++;
					Format(szText, sizeof(szText), "%s%N\n", szText, i);
				}
			}
		}
		
		if (iTargetUser > 0)
		{
			Format(szText, sizeof(szText), "Spectating %N (%i):\n%s", iTargetUser, iSpecCount, szText);
		}
	}
	
	// No menu currently
	if (GetClientMenu(client) == MenuSource_None && iSpecCount > 0)
	{
		g_bRedrawSpecList[client] = true;
	}
	
	// Check whether they need to redraw the menu & there is more than 0 spectators.
	if (g_bRedrawSpecList[client] && iSpecCount > 0)
	{
		DisplaySpeclistMenu(client, szText);
	}
	else if (g_bRedrawSpecList[client])
	{
		ClearMenu(client);
	}
	
	if (!g_bSpecList[client])
	{
		return Plugin_Stop;
	}
	
	return Plugin_Continue;
}
stock DisplaySpeclistMenu(int client, char[] szText)
{
	Menu menu = new Menu(Hndlr_Interrupted);
	menu.SetTitle(szText);
	menu.AddItem("", "", ITEMDRAW_SPACER);
	menu.ExitButton = false;
	menu.Display(client, MENU_TIME_FOREVER);
	
	g_bRedrawSpecList[client] = true;
	g_hSpecListMenu[client] = menu;
}
stock ClearMenu(int client)
{
	if (!IsClientInGame(client))return;
	Menu menu = new Menu(Hndlr_Null);
	menu.AddItem("", "", ITEMDRAW_SPACER);
	menu.ExitButton = false;
	menu.Display(client, 1);
}
stock KillSpeclistTimer(int client)
{
	if (g_hSpecListMenu[client] != null)
	{
		//KillTimer(g_hSpecListMenu[client]);
		g_hSpecListMenu[client] = null;
		
		ClearMenu(client);
	}
}
public int Hndlr_Interrupted(Handle menu, MenuAction action, int client, int index) {
	if (client <= 0)return 0;
	// Interrupted
	if (action == MenuAction_Cancel && index == MenuCancel_Interrupted)
	{
		// Mark client to not redraw the menu.
		g_bRedrawSpecList[client] = false;
	}
	
	return 0;
}
public int Hndlr_Null(Handle menu, MenuAction action, int client, int index) {  } 

public int Native_HasSpeclistEnabled(Handle hPlugin, int nParams)
{
	return g_bSpecList[GetNativeCell(1)];
} 

public int Native_SetSpeclistEnabled(Handle hPlugin, int nParams)
{
	g_bSpecList[GetNativeCell(1)] = GetNativeCell(2);
} 

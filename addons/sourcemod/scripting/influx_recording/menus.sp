public Action Cmd_Replay(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (Inf_HandleCmdSpam(client, 1.0, g_flLastReplayMenu[client], true))
	{
		return Plugin_Handled;
	}
	
	if (!IsValidReplayBot())return Plugin_Handled;
	
	Show_ReplayRunMenu(client);
	
	return Plugin_Handled;
}
bool Show_ReplayRunMenu(int client)
{
	char szRun[MAX_RUN_NAME];
	char szItem[64];
	int irun, runid;
	
	int len = g_hRunRec.Length;
	if (len < 2) {
		// No run to currently select, just load into the mode selection; assuming main run.
		Show_ReplayModeMenu(client, Influx_GetRunsArray().Get(0, RUN_ID), true);
		return true;
	}
	
	Menu menu = new Menu(Hndlr_ReplayRunSelection);
	menu.SetTitle("Replay Menu\n ");
	
	for (irun = 0; irun < len; irun++)
	{
		runid = g_hRunRec.Get(irun, RUNREC_RUN_ID);
		Influx_GetRunName(runid, szRun, sizeof(szRun));
		FormatEx(szItem, sizeof(szItem), "%i", runid);
		
		menu.AddItem(szItem, szRun);
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}
bool Show_ReplayModeMenu(int client, int runid, bool forced = false)
{
	char szRun[MAX_RUN_NAME];
	char szMode[MAX_MODE_NAME];
	char szItem[64];
	int mode;
	
	Influx_GetRunName(runid, szRun, sizeof(szRun));
	
	int len = Influx_GetModesArray().Length;
	if (len < 2) {
		// No mode to currently select, just load into the style selection; assuming auto run.
		Show_ReplayStyleMenu(client, runid, Influx_GetModesArray().Get(0, MODE_ID), true);
		return true;
	}
	
	Menu menu = new Menu(Hndlr_ReplayModeSelect);
	menu.SetTitle("Replay Menu\nRun: %s\n ", szRun);
	menu.ExitBackButton = !(forced);
	
	for (mode = 0; mode < MAX_MODES; mode++)
	{
		Influx_GetModeName(mode, szMode, sizeof(szMode));
		if (!StrEqual(szMode, "N/A")) {
			FormatEx(szItem, sizeof(szItem), "%i_%i", runid, mode);
			
			menu.AddItem(szItem, szMode);
		}
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
}
bool Show_ReplayStyleMenu(int client, int runid, int mode, bool forced = false)
{
	char szRun[MAX_RUN_NAME];
	char szStyle[MAX_STYLE_NAME];
	char szItem[64];
	int style;
	
	Influx_GetRunName(runid, szRun, sizeof(szRun));
	
	Menu menu = new Menu(Hndlr_Replay);
	menu.SetTitle("Replay Menu\nRun: %s\n ", szRun);
	menu.ExitBackButton = forced;
	
	int validReplay = 0;
	
	for (style = 0; style < MAX_STYLES; style++)
	{
		Influx_GetStyleName(style, szStyle, sizeof(szStyle));
		if (!StrEqual(szStyle, "N/A")) {
			int bestuserid;
			Influx_GetRunBestTime(runid, mode, style, bestuserid);
			Handle nCache[RecordCache];
			nCache = GetReplayCache(bestuserid, runid, mode, style);
			if (nCache[Recording] != null) {
				FormatEx(szStyle, sizeof(szStyle), "%s by %s", szStyle, nCache[PlayerName]);
				// Check whether replay is already being played.
				if (CheckBotsRecording(nCache[Recording])) {
					FormatEx(szStyle, sizeof(szStyle), "%s%s", szStyle, " (Active)");
					menu.AddItem("", szStyle, ITEMDRAW_DISABLED);
				}
				else
				{
					FormatEx(szItem, sizeof(szItem), "%i_%i_%i_%i", bestuserid, runid, mode, style);
					menu.AddItem(szItem, szStyle);
				}
				validReplay++;
			}
		}
	}
	
	if (validReplay == 0) {
		Influx_PrintToChat(_, client, "%t", "NOSERVERRECORDS");
		delete menu;
		return false;
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
	return true;
} 
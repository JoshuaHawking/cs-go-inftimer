public Action Cmd_MyReplay(int client, int args)
{
	if (!client)return Plugin_Handled;
	
	if (!IsValidReplayBot())return Plugin_Handled;
	
	if (CanReplayOwn(client))
	{
		int botIndex;
		ReplayOwn(client, botIndex);
		
		ObserveTarget(client, g_iReplayBots[botIndex]);
	}
	
	return Plugin_Handled;
} 
public int Hndlr_ReplayRunSelection(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (!IsValidReplayBot())return 0;
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int runid = StringToInt(szInfo);
	
	Show_ReplayModeMenu(client, runid);
	return 0;
}
public int Hndlr_ReplayModeSelect(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// Send them back.
		Show_ReplayRunMenu(client);
		return 0;
	}
	
	if (!IsValidReplayBot())return 0;
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	
	char buffer[2][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	
	int runid = StringToInt(buffer[0]);
	int mode = StringToInt(buffer[1]);
	
	Show_ReplayStyleMenu(client, runid, mode);
	return 0;
}
public int Hndlr_Replay(Menu menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// Send them back. 
		Show_ReplayRunMenu(client);
		return 0;
	}
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	if (!IsValidReplayBot())return 0;
	
	if (szInfo[0] == 'z')
	{
		if (CanReplayOwn(client))
		{
			int botIndex;
			ReplayOwn(client, botIndex);
			
			ObserveTarget(client, g_iReplayBots[botIndex]);
		}
		
		return 0;
	}
	
	char buffer[4][6];
	if (ExplodeString(szInfo, "_", buffer, sizeof(buffer), sizeof(buffer[])) != sizeof(buffer))
	{
		return 0;
	}
	
	
	int userid = StringToInt(buffer[0]);
	int runid = StringToInt(buffer[1]);
	int mode = StringToInt(buffer[2]);
	int style = StringToInt(buffer[3]);
	
	if (!VALID_MODE(mode))return 0;
	
	if (!VALID_STYLE(style))return 0;
	
	int queue;
	if ((queue = RequestRecording(client, userid, runid, mode, style)) == -1) // Not valid
	{
		Influx_PrintToChat(_, client, "%t", "ALREADYQUEUED");
	}
	else
	{
		if (queue == 0)return 0;
		Influx_PrintToChat(_, client, "%t", "REPLAYQUEUED", queue);
	}
	return 0;
}
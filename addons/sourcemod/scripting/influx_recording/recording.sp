/*
	Recording
*/
stock void StartRecording(int client, bool bInsertFrame = false)
{
	ArrayList rec = g_hRec[client];
	
	if (rec != null)
	{
		DeleteRecording(rec);
	}
	
	
	g_hRec[client] = new ArrayList(REC_SIZE);
	
	g_bIsRec[client] = true;
	g_nCurRec[client] = 0;
	
	g_fCurButtons[client] = 0;
	g_iCurWep[client] = 0;
	
	g_szWep_Prim[client][0] = '\0';
	g_szWep_Sec[client][0] = '\0';
	
	if (bInsertFrame)
	{
		#if defined DEBUG_INSERTFRAME
		PrintToServer(INF_DEBUG_PRE..."Inserting starting frame! (%i) Frame: %i", client, GetGameTickCount());
		#endif
		InsertFrame(client);
		
		
		// Make sure we get the starting weapon right!
		if (g_hRec[client].Length)
		{
			int wep = GetEntPropEnt(client, Prop_Data, "m_hActiveWeapon");
			int flags = g_hRec[client].Get(0, REC_FLAGS);
			
			switch (FindSlotByWeapon(client, wep))
			{
				case SLOT_PRIMARY : g_hRec[client].Set(0, flags | RECFLAG_WEP_SLOT1, REC_FLAGS);
				case SLOT_SECONDARY : g_hRec[client].Set(0, flags | RECFLAG_WEP_SLOT2, REC_FLAGS);
				case SLOT_MELEE : g_hRec[client].Set(0, flags | RECFLAG_WEP_SLOT3, REC_FLAGS);
			}
		}
	}
}

stock InsertFrame(int client)
{
	if (Influx_GetClientStyle(client) == STYLE_TAS)return;
	
	#if defined DEBUG_INSERTFRAME
	PrintToServer(INF_DEBUG_PRE..."(%i) | Frame: %i", client, GetGameTickCount());
	#endif
	
	static int data[REC_SIZE];
	static float temp[3];
	
	GetClientAbsOrigin(client, temp);
	CopyArray(temp, data[REC_POS], 3);
	
	GetClientEyeAngles(client, temp);
	CopyArray(temp, data[REC_ANG], 2);
	
	data[REC_SYNC] = view_as<int>(Influx_GetClientStrafeSync(client));
	
	data[REC_FLAGS] = (GetEntityFlags(client) & FL_DUCKING) ? RECFLAG_CROUCH : 0;
	
	
	if (g_ConVar_WeaponAttack.BoolValue && g_fCurButtons[client] & IN_ATTACK)
	{
		data[REC_FLAGS] |= RECFLAG_ATTACK;
	}
	
	if (g_ConVar_WeaponAttack2.BoolValue && g_fCurButtons[client] & IN_ATTACK2)
	{
		data[REC_FLAGS] |= RECFLAG_ATTACK2;
	}
	
	
	if (g_ConVar_WeaponSwitch.BoolValue && g_iCurWep[client])
	{
		switch (FindSlotByWeapon(client, g_iCurWep[client]))
		{
			case SLOT_PRIMARY : 
			{
				data[REC_FLAGS] |= RECFLAG_WEP_SLOT1;
				
				if (g_szWep_Prim[client][0] == '\0') // We haven't added a gun here yet.
				{
					GetEntityClassname(g_iCurWep[client], g_szWep_Prim[client], sizeof(g_szWep_Prim[]));
				}
			}
			case SLOT_SECONDARY : 
			{
				data[REC_FLAGS] |= RECFLAG_WEP_SLOT2;
				
				if (g_szWep_Sec[client][0] == '\0') // We haven't added a gun here yet.
				{
					GetEntityClassname(g_iCurWep[client], g_szWep_Sec[client], sizeof(g_szWep_Sec[]));
				}
			}
			case SLOT_MELEE : data[REC_FLAGS] |= RECFLAG_WEP_SLOT3;
		}
	}
	
	if (g_hRec[client].PushArray(data) > g_nMaxRecLength)
	{
		Influx_PrintToChat(_, client, "%t", "STOPPEDRECORDING", g_ConVar_MaxLength.IntValue);
		StopRecording(client);
	}
}

stock int FindSlotByWeapon(int client, int weapon)
{
	for (int i = 0; i < SLOTS_SAVED; i++)
	{
		if (weapon == GetPlayerWeaponSlot(client, i))return i;
	}
	
	return -1;
}

public void E_PostThinkPost_Client(int client)
{
	if (!IsPlayerAlive(client))return;
	
	if (!g_bIsRec[client])return;
	
	if (g_bLib_Practice && Influx_IsClientPractising(client))return;
	
	if (g_bLib_Pause && Influx_IsClientPaused(client))return;
	
	InsertFrame(client);
}

stock void FinishRecording(int client, bool bInsertFrame = false)
{
	g_bIsRec[client] = false;
	
	if (bInsertFrame)
	{
		#if defined DEBUG_INSERTFRAME
		PrintToServer(INF_DEBUG_PRE..."Inserting finishing frame! (%i) Frame: %i", client, GetGameTickCount());
		#endif
		InsertFrame(client);
	}
	
	//CopyToBot( g_hRec[client] );
}

// Used when they go over the time.
stock void StopRecording(int client)
{
	g_nCurRec[client] = 0;
	g_bIsRec[client] = false;
}
/*
	Queue System
*/

// Returns the queue position or -1
stock int RequestRecording(int requester, int userid, int runid, int mode, int style)
{
	// Check whether a player already has a request
	
	for (int i = 0; g_hReplayQueue.Length; i++)
	{
		decl data[QUEUE_SIZE];
		g_hReplayQueue.GetArray(i, data);
		if (data[QUEUE_REQUESTER] == requester)
		{
			return -1;
		}
	}
	
	// Add entry to queue arraylist:
	decl data[QUEUE_SIZE];
	
	data[QUEUE_REQUESTER] = requester;
	data[QUEUE_USERID] = userid;
	data[QUEUE_RUNID] = runid;
	data[QUEUE_MODE] = mode;
	data[QUEUE_STYLE] = style;
	
	
	g_hReplayQueue.PushArray(data);
	
	UpdateQueue();
	return g_hReplayQueue.Length;
}

stock UpdateQueue()
{
	// Iterate through the queue, check whether they are not active
	for (int i = 0; g_hReplayQueue.Length; i++)
	{
		int botIndex = GetInactiveBotIndex();
		
		// No valid bots
		if (botIndex == -1)return;
		
		// Play record
		decl data[QUEUE_SIZE];
		g_hReplayQueue.GetArray(i, data);
		
		int userid = data[QUEUE_USERID];
		int runid = data[QUEUE_RUNID];
		int mode = data[QUEUE_MODE];
		int style = data[QUEUE_STYLE];
		
		Handle recCache[RecordCache];
		recCache = GetReplayCache(userid, runid, mode, style);
		if (recCache[Recording] != null)
		{
			StartPlayback(
				botIndex, 
				recCache[Recording], 
				runid, 
				mode, 
				style, 
				recCache[Time], 
				recCache[PlayerName], 
				data[QUEUE_REQUESTER]
				);
		}
		else
		{
			Influx_PrintToChat(_, data[QUEUE_REQUESTER], "%t", "INVALIDREPLAY");
		}
		
		// Remove from queue
		g_hReplayQueue.Erase(0);
	}
}

/*
	Playback
*/
public Action T_PlaybackToStart(Handle hTimer, int client)
{
	if ((client = GetClientOfUserId(client)) != -1)
	{
		if (g_nCurRec[client] == PLAYBACK_END)
		{
			int botIndex = GetBotIndex(client);
			ResetReplay(botIndex);
		}
	}
}

public Action T_PlaybackStart(Handle hTimer, int client)
{
	if ((client = GetClientOfUserId(client)) != -1)
	{
		if (g_nCurRec[client] == PLAYBACK_START)
		{
			int botIndex = GetBotIndex(client);
			g_iReplayRunStartTick[botIndex] = GetGameTickCount();
			g_flReplayRunSync[botIndex] = 0.0;
			g_nCurRec[client] = 0;
		}
	}
}

stock void StartPlayback(int botIndex, ArrayList rec, int runid, int mode, int style, float time, const char[] szName, int requester = 0, bool bForce = false)
{
	if (!IsValidReplayBot(botIndex))
	{
		#if defined DEBUG_REPLAY
		PrintToServer(INF_DEBUG_PRE..."Tried to start a playback with invalid replay bot!");
		#endif
		return;
	}
	
	int botClient = g_iReplayBots[botIndex];
	
	// Make sure they are not dead! D:
	bool wasdead = false;
	if (GetClientTeam(botClient) <= CS_TEAM_SPECTATOR)
	{
		ChangeClientTeam(botClient, CS_TEAM_CT);
		
		wasdead = true;
	}
	
	if (!IsPlayerAlive(botClient))
	{
		CS_RespawnPlayer(botClient);
		
		wasdead = true;
	}
	
	
	if (!wasdead && requester && g_hReplay[botIndex] == rec)
	{
		Influx_PrintToChat(0, requester, "%t", "ALREADYREPLAYING");
		return;
	}
	
	#if defined DEBUG_REPLAY
	PrintToServer(INF_DEBUG_PRE..."Starting playback requested by %i! (%i, %i, %i)", requester, runid, mode, style);
	#endif
	
	
	g_iReplayRunId[botIndex] = runid;
	g_iReplayMode[botIndex] = mode;
	g_iReplayStyle[botIndex] = style;
	
	g_flReplayTime[botIndex] = time;
	
	strcopy(g_szReplayName[botIndex], sizeof(g_szReplayName[]), szName);
	
	g_bReplayedOnce[botIndex] = false;
	g_bForcedReplay[botIndex] = bForce;
	
	g_iReplayRequester[botIndex] = requester;
	
	g_hReplay[botIndex] = rec;
	
	g_nCurRec[botClient] = PLAYBACK_START;
	
	CreateTimer(g_ConVar_StartTime.FloatValue, T_PlaybackStart, GetClientUserId(botClient), TIMER_FLAG_NO_MAPCHANGE);
	
	// Set bot name
	char szSecFormat[16];
	char szTime[16];
	Influx_GetSecondsFormat_PersonalBest(szSecFormat, sizeof(szSecFormat));
	Inf_FormatSeconds(time, szTime, sizeof(szTime), szSecFormat);
	
	char szBotName[64];
	FormatEx(szBotName, sizeof(szBotName), "(%s) %s", szTime, szName);
	SetClientName(botClient, szBotName);
	
	// Chat message
	char szMode[MAX_MODE_NAME];
	char szStyle[MAX_STYLE_NAME];
	Influx_GetModeName(mode, szMode, sizeof(szMode));
	Influx_GetStyleName(style, szStyle, sizeof(szStyle));
	
	// Check whether it is bonus
	char szRun[MAX_RUN_NAME];
	Influx_GetRunName(runid, szRun, sizeof(szRun));
	
	Influx_PrintToChatAll(_, requester, "%T", "NOWREPLAYING", LANG_SERVER, szMode, szStyle, szName, szTime, (runid > 1 ? szRun : ""));
	
	// Set tag.
	char szTag[32];
	
	StringToUpper(szRun);
	
	
	// Ignore if possible.
	if (Influx_ShouldModeDisplay(mode))
	{
		Influx_GetModeShortName(mode, szMode, sizeof(szMode));
	}
	else
	{
		szMode[0] = '\0';
	}
	
	if (Influx_ShouldStyleDisplay(style))
	{
		Influx_GetStyleShortName(style, szStyle, sizeof(szStyle));
	}
	else
	{
		szStyle[0] = '\0';
	}
	
	
	FormatEx(szTag, sizeof(szTag), "%s%s%s%s%s", 
		szRun, 
		(szStyle[0] != '\0') ? " " : "", 
		szStyle, 
		(szMode[0] != '\0') ? " " : "", 
		szMode);
	
	CS_SetClientClanTag(botClient, szTag);
	
	if (IS_ENT_PLAYER(requester))
	{
		//Influx_PrintToChat(_, requester, "Replay is now being played!");
		
		
		// Observe bot
		if (Influx_GetClientState(requester) != STATE_RUNNING)
		{
			if (GetClientTeam(requester) != CS_TEAM_SPECTATOR)
			{
				ChangeClientTeam(requester, CS_TEAM_SPECTATOR);
			}
			
			if (GetClientObserverTarget(requester) != botClient)
			{
				SetEntPropEnt(requester, Prop_Send, "m_hObserverTarget", botClient);
				SetEntProp(requester, Prop_Send, "m_iObserverMode", 4);
			}
			
			SetClientObserverMode(requester, OBS_MODE_IN_EYE);
		}
	}
}
stock void FinishPlayback(int botIndex)
{
	if (g_iReplayRequester[botIndex] > 0 || g_bForcedReplay[botIndex])
	{
		g_hReplay[botIndex] = null;
		for (int i = 1; i <= MaxClients; i++)
		{
			if (IsClientInGame(i) && IsObservingTarget(i, g_iReplayBots[botIndex]))
			{
				Influx_PrintToChat(_, i, "%t", "CANREQUEST");
			}
		}
	}
	
	// Reset bot name.
	SetBotName(botIndex);
	
	g_bReplayedOnce[botIndex] = true;
	g_bForcedReplay[botIndex] = false;
	
	g_iReplayRequester[botIndex] = -1;
	g_iReplayRunStartTick[botIndex] = -1;
	
	// Check for queue.
	UpdateQueue();
}

stock void ResetReplay(int botIndex)
{
	// Try to teleport the bot to start.
	if (IsValidReplayBot(botIndex) && g_hReplay[botIndex] != null && g_hReplay[botIndex].Length > 0)
	{
		float pos[3];
		float angles[3];
		
		int data[REC_SIZE];
		
		g_hReplay[botIndex].GetArray(0, data);
		
		CopyArray(data[REC_POS], pos, 3);
		CopyArray(data[REC_ANG], angles, 2);
		
		TeleportEntity(g_iReplayBots[botIndex], pos, angles, ORIGIN_VECTOR);
	}
	
	
	g_hReplay[botIndex] = null;
	
	g_iReplayRunId[botIndex] = -1;
	g_iReplayMode[botIndex] = -1;
	g_iReplayStyle[botIndex] = -1;
	g_flReplayTime[botIndex] = INVALID_RUN_TIME;
}

stock bool CanReplayOwn(int client)
{
	return (g_hRec[client] != null);
}

// Bot index returns the index that the replay is being played on.
stock void ReplayOwn(int client, int &botIndex)
{
	decl String:szName[MAX_NAME_LENGTH];
	GetClientName(client, szName, sizeof(szName));
	
	StartPlayback(
		0, 
		g_hRec[client], 
		g_iFinishedRunId[client], 
		g_iFinishedMode[client], 
		g_iFinishedStyle[client], 
		g_flFinishedTime[client], 
		szName, 
		client);
}

stock void DeleteRecording(ArrayList &rec)
{
	for (int i = 0; i < REPLAYBOT_COUNT; i++)
	{
		if (rec == g_hReplay[i])
		{
			g_hReplay[i] = null;
		}
	}
	
	delete rec;
} 
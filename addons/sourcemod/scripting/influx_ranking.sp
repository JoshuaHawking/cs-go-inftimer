#include <sourcemod>
#include <smlib>

#include <cstrike>
#include <influx/core>

#include <msharedutil/arrayvec>
#include <msharedutil/ents>
#include <msharedutil/misc>
#include <chat-processor>

#include <influx/ranking>
#include <influx/records>

#undef REQUIRE_PLUGIN
#include <influx/tags>
#include <influx/playerinfo>

// #define DEBUG_RANKS

/*
	RANKING FACTORS
*/

/*
	Base Points
	Base points given to a record.
*/
#define BASE_POINTS			75

/*
	TOP 10
	The top 10 records are given a bonus amount of points. If there is less than TOP10_LIMIT records, the points are reduced using this equation:
	TOP10_POINTS * ((RECORDCOUNT / TOP10_LIMIT) * TOP10_REDUCTION) (Limited to TOP10_POINTS)
	70 * ((30 / 100) * 1.1) = 23.1 points
*/
#define TOP10_POINTS		70			// The amount of points given to those in the top 10.
#define TOP10_REDUCTION		1.1 		// The multiplier to the reduction if they are under the top 10 limit.
#define TOP10_LIMIT			100			// The amount of records needed for there to be no reduction to the amount of points.

/*
	TIERS
	There are 5 tiers, these tiers are groups of times, based on their percentage.
	Tier 1 will cover the top 15% of times, then tier 2 will cover the top 40% etc. etc.
*/
#define TIER1_PERCENTAGE	15
#define TIER1_MULTIPLIER	1.35

#define TIER2_PERCENTAGE	30
#define TIER2_MULTIPLIER	1.25

#define TIER3_PERCENTAGE	60
#define TIER3_MULTIPLIER	1.15

#define TIER4_PERCENTAGE	75
#define TIER4_MULTIPLIER	1.1

#define TIER5_PERCENTAGE	100
#define TIER5_MULTIPLIER	1

/*
	RECORD PERCENTAGE
	The percentage of the record is what percentage does the record stand in. For example, the #1 record will be 1%, and the 99th record will be 99% if there is 100 records in total.
	PERCENTAGE_POINTS is the amount of points that is multiplied by the 1 - the percentage in decimal. Equation:
	PERCENTAGE_POINTS * (1 - (RECORDRANK / RECORDCOUNT))
	200 * (1 - (5 / 100)) = 200 * 0.95 = 190 points
*/
#define PERCENTAGE_POINTS 200

bool g_bUpdateNeeded[MAX_RUNS + 1][MAX_MODES][MAX_STYLES]; // Used to indiciate whether a new record was ever set on a certain run, style, mode. used for optimisation on database.

// Actual point values
int g_iRankPoints[INF_MAXPLAYERS]; // The amount of points that a player has.
int g_iRankLCPoints[INF_MAXPLAYERS]; // The amount of points that player connected with. (Last Connected Points)
int g_iPlayerRank[INF_MAXPLAYERS]; // The player rank based on their amount of points.

// Point Leaderboard
int g_iLeaderboardPointRank[INF_MAXPLAYERS]; // The rank of the player compared to everyone else.
int g_iLeaderboardPointRanked = 0; // Amount of players who are currently ranked.

bool g_bLib_Tags;
bool g_bLib_PlayerInfo;

/*
	Chat Tags
*/
enum Rank {
	RankNumber, 
	String:Name[32], 
	String:Tag[32], 
	String:Colour[32], 
	Points
}

ArrayList g_hRanks;
Handle g_hRankTemplate[Rank];

// Top 100
float g_flLastTop100Time[INF_MAXPLAYERS];

#include "influx_ranking/db.sp"
#include "influx_ranking/cmd.sp"

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Ranking", 
	description = "Ranking", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	RegPluginLibrary(INFLUX_LIB_RANKING);
	LoadTranslations(INFLUX_PHRASES);
	
	// COMMANDS
	RegConsoleCmd("sm_ranks", Cmd_RankInfo);
	RegConsoleCmd("sm_rankinfo", Cmd_RankInfo);
	RegConsoleCmd("sm_rank", Cmd_RankInfo);
	RegConsoleCmd("sm_nextrank", Cmd_RankInfo);
	RegConsoleCmd("sm_playerrank", Cmd_PlayerRank);
	RegConsoleCmd("sm_prank", Cmd_PlayerRank);
	RegConsoleCmd("sm_top100", Cmd_Top100);
	
	// NATIVES
	CreateNative("Influx_GetClientSkillRank", Native_GetClientSkillRank);
	
	return APLRes_Success;
}

public void OnAllPluginsLoaded()
{
	Handle db = Influx_GetDB();
	if (db == null)SetFailState(INF_CON_PRE..."Couldn't retrieve database handle!");
	
	SQL_TQuery(db, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...INF_TABLE_RANKS..." (uid INTEGER PRIMARY KEY, jumps INTEGER DEFAULT 0, prestige INTEGER DEFAULT 0, points INTEGER DEFAULT 0 NOT NULL, pointrank INTEGER DEFAULT -1, lastconnectedpoints INTEGER DEFAULT 0)", _, DBPrio_High);
	
	SQL_TQuery(db, Thrd_Empty, "ALTER TABLE "...INF_TABLE_TIMES..." ADD COLUMN points INTEGER DEFAULT 0, ADD COLUMN percentage FLOAT DEFAULT 0.0", _, DBPrio_High);
	
	// Load ranked player count.
	DB_GetRankedPlayers();
}
public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);
	
	g_bLib_Tags = LibraryExists(INFLUX_LIB_TAGS);
	g_bLib_PlayerInfo = LibraryExists(INFLUX_LIB_PLAYERINFO);
	
	int run, mode, style;
	
	// Set update needed to false
	for (run = 1; run <= MAX_RUNS; run++) {
		for (mode = 0; mode < MAX_MODES; mode++) {
			for (style = 0; style < MAX_STYLES; style++) {
				g_bUpdateNeeded[run][mode][style] = false;
			}
		}
	}
	
	// Load ranks into cache
	if (!LoadRanks())
	{
		SetFailState(INF_CON_PRE..."Couldn't load ranks!");
	}
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_TAGS))g_bLib_Tags = true;
	if (StrEqual(lib, INFLUX_LIB_PLAYERINFO))g_bLib_PlayerInfo = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_TAGS))g_bLib_Tags = false;
	if (StrEqual(lib, INFLUX_LIB_PLAYERINFO))g_bLib_PlayerInfo = false;
}

public OnMapEnd()
{
	DB_UpdateRanks();
}

public OnClientConnected(int client)
{
	// Reset clients
	g_iPlayerRank[client] = 0;
	g_iRankPoints[client] = 0;
	g_iRankLCPoints[client] = 0;
	g_iLeaderboardPointRank[client] = -1;
	g_flLastTop100Time[client] = 0.0;
}

/*
	Timer Functions
*/

public void Influx_OnClientIdRetrieved(int client, int uid, bool newId)
{
	if (IsFakeClient(client))return;
	// Create timer to get their points	
	
	// Load ranked player count.
	int userId;
	userId = GetClientUserId(client);
	
	DB_GetRankedPlayers();
	CreateTimer(1.0, Timer_UpdatePoints, userId, _);
	CreateTimer(2.5, Timer_PrintJoinMessage, userId);
	CreateTimer(5.0, Timer_UpdateRank, userId, _);
	
}

public Action Timer_PrintJoinMessage(Handle timer, int userId) {
	// Check whether client is still in game.
	int client = GetClientOfUserId(userId);
	
	if (!IsClientInGame(client))return;
	
	char szClient[MAX_NAME_LENGTH];
	GetClientName(client, szClient, sizeof(szClient));
	Influx_RemoveChatColors(szClient, sizeof(szClient));
	
	if (g_iLeaderboardPointRank[client] <= 0)
	{
		Influx_PrintToChatAll(_, client, "%T", "PLAYERCONNECTED_UNRANKED", LANG_SERVER, szClient);
	}
	else
	{
		Influx_PrintToChatAll(_, client, "%T", "PLAYERCONNECTED", LANG_SERVER, szClient, g_iLeaderboardPointRank[client], g_iLeaderboardPointRanked);
	}
}

public Action Timer_UpdatePoints(Handle timer, int userId)
{
	// Check whether client is still in game.
	int client = GetClientOfUserId(userId);
	
	if (!IsClientInGame(client))return;
	
	DB_GetPoints(client);
	
}

public Action Timer_UpdateRank(Handle timer, int userId)
{
	// Check whether client is still in game.
	int client = GetClientOfUserId(userId);
	
	if (!IsClientInGame(client))return;
	
	// Set clan tag
	if(g_iLeaderboardPointRank[client] <= 0) 
	{
		CS_SetClientClanTag(client, "Unranked |");
	}
	else
	{
		char szRank[16];
		FormatEx(szRank, sizeof(szRank), "%i |", g_iLeaderboardPointRank[client]);
		CS_SetClientClanTag(client, szRank);
	}
	
	for (int rank = 0; rank < GetArraySize(g_hRanks); rank++)
	{
		Handle hRank[Rank];
		GetArrayArray(g_hRanks, rank, hRank[RankNumber]);
		
		#if defined DEBUG_RANKS
		LogAction(-1, -1, "Points needed for rank %i: %i", hRank[RankNumber], hRank[Points]);
		#endif
		
		if (hRank[Points] <= g_iRankPoints[client]) {  // More points
			#if defined DEBUG_RANKS
			LogAction(-1, -1, "Rank for client selected: %i", rank);
			#endif
			
			g_iPlayerRank[client] = hRank[RankNumber];
			//CS_SetClientClanTag(client, hRank[ScoreboardTag]);
			
			if (g_bLib_Tags)
			{
				char szTag[64];
				FormatEx(szTag, sizeof(szTag), "{CHATCLR}[%s%s{CHATCLR}]", hRank[Colour], hRank[Tag]);
				Influx_FormatChatColors(szTag, sizeof(szTag));
				Influx_SetExternalChatTag(client, szTag);
			}
			
			break;
		}
	}
}

public void Influx_OnTimerFinishPost(int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags, int currentrank, int newrank)
{
	if (newrank <= 0)
		return;
	
	if ((flags & RES_TIME_PB) || (flags & RES_TIME_FIRSTOWNREC) || (flags & RES_TIME_FIRSTREC)) {
		// New record, update this at the end.
		g_bUpdateNeeded[runid][mode][style] = true;
	}
}

/*
	Chat Messages & Commands
*/
public Action OnChatMessage(int & author, ArrayList recipients, eChatFlags & flag, char[] name, char[] message, bool & bProcessColors, bool & bRemoveColors)
{
	// Check whether influx_tags is loaded, if so, let it deal with the tag.
	if (g_bLib_Tags)
		return Plugin_Continue;
	
	char extra[12];
	GetExtraTag(author, flag, extra, sizeof(extra));
	
	bProcessColors = false;
	
	// Add tag to chat message
	if (g_iPlayerRank[author] > 0) {
		Handle hRank[Rank];
		GetArrayArray(g_hRanks, g_hRanks.Length - g_iPlayerRank[author], hRank[RankNumber]);
		
		Format(name, MAXLENGTH_NAME, " {CHATCLR}[%s%s{CHATCLR}] %s{WHITE}%s", hRank[Colour], hRank[Tag], extra, name);
		Influx_FormatChatColors(name, MAXLENGTH_NAME);
		return Plugin_Changed;
	}
	else
	{
		// Set colour to white.
		Format(name, MAXLENGTH_NAME, " %s{WHITE}%s", extra, name);
		Influx_FormatChatColors(name, MAXLENGTH_NAME);
		return Plugin_Changed;
	}
}

GetExtraTag(int client, eChatFlags flag, char[] extra, int extralen)
{
	if (flag == ChatFlag_Team)
	{
		if (GetClientTeam(client) == CS_TEAM_CT)
		{
			strcopy(extra, extralen, "{TEAM}CT ");
		}
		else if (GetClientTeam(client) == CS_TEAM_T)
		{
			strcopy(extra, extralen, "{TEAM}T ");
		}
	}
	if (GetClientTeam(client) == CS_TEAM_SPECTATOR)
	{
		strcopy(extra, extralen, "{TEAM}SPEC ");
	}
}
/*
	Load Ranks
*/
bool LoadRanks()
{
	
	char szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "configs");
	
	if (!DirExistsEx(szPath))return false;
	
	Format(szPath, sizeof(szPath), "%s/%s", szPath, INF_RANKS_FILE);
	
	KeyValues kv = new KeyValues("Ranks");
	kv.ImportFromFile(szPath);
	
	if (!kv.GotoFirstSubKey())
	{
		return false;
	}
	
	g_hRanks = CreateArray(sizeof(g_hRankTemplate));
	
	do
	{
		Handle hTemplate[Rank];
		
		hTemplate[RankNumber] = kv.GetNum("RankNumber", 0);
		kv.GetString("Name", hTemplate[Name], sizeof(hTemplate[Name]));
		kv.GetString("Tag", hTemplate[Tag], sizeof(hTemplate[Tag]));
		kv.GetString("Colour", hTemplate[Colour], sizeof(hTemplate[Colour]));
		hTemplate[Points] = kv.GetNum("Points", 0);
		
		#if defined DEBUG_RANKS
		LogAction(-1, -1, "Added rank: %i. Points: %i", hTemplate[RankNumber], hTemplate[Points]);
		#endif
		
		PushArrayArray(g_hRanks, hTemplate[RankNumber]);
	} while (kv.GotoNextKey());
	
	#if defined DEBUG_RANKS
	LogAction(-1, -1, "Length of rank array: %i", GetArraySize(g_hRanks));
	#endif
	
	delete kv;
	return true;
}

public int Native_GetClientSkillRank(Handle hPlugin, int nParams)
{
	return g_iLeaderboardPointRank[GetNativeCell(1)];
} 
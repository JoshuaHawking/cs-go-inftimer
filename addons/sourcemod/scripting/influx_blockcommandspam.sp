#include <sourcemod>

#include <chat-processor>
#include <colorvariables>

public Plugin myinfo = 
{
	author = "Fusion", 
	url = "www.houseofclimb.com", 
	name = "Block Command Spam", 
	description = "Stop !command showing up in chat.", 
	version = "1.0.0"
};

public Action OnChatMessage(int &author, ArrayList recipients, eChatFlags &flag, char[] name, char[] message, bool &bProcessColors, bool &bRemoveColors)
{
	char szMessage[MAXLENGTH_MESSAGE];
	strcopy(szMessage, sizeof(szMessage), message);
	
	// Remove all the color unicodes
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x01", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x03", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x02", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x07", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x0F", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x04", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x05", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x06", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x0C", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x0B", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x09", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x10", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x0E", "");
	ReplaceString(szMessage, MAXLENGTH_MESSAGE, "\x08", "");
	
	//CProcessVariables(message, MAXLENGTH_MESSAGE, true);
	
	// Get first character
	if (szMessage[0] == '/' || szMessage[0] == '!')
	{
		return Plugin_Handled;
	}
	else if (StrEqual("rockthevote", szMessage) || StrEqual("rtv", szMessage) || StrEqual("nextmap", szMessage))
	{
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
} 
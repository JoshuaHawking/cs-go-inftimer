#include <sourcemod>
#include <cstrike>

#include <influx/core>
#include <influx/records>
#include <influx/hud>

#include <msharedutil/arrayvec>
#include <msharedutil/ents>
#include <msharedutil/misc>

#undef REQUIRE_PLUGIN
#include <influx/jumps>
#include <influx/strafes>
#include <influx/sync>
#include <influx/speed>
#include <influx/recording>
#include <influx/parkour>
#include <influx/ranking>
#include <influx/playerinfo>
#include <rocketjump>

#include <influx/profiler>
#define DEBUG_PROFILING

/**
 * New World Record Cache
 */

Handle g_hRecordCache[MAX_RUNS + 1][MAX_MODES][MAX_STYLES];
Handle nCacheTemplate[RecordCache];
bool g_bUpdateNeeded[MAX_RUNS + 1][MAX_MODES][MAX_STYLES]; // Used to indiciate whether a new record was ever set on a certain run, style, mode. used for optimisation on database.
bool g_bCacheLoaded[MAX_RUNS + 1];

float g_flLastRecPrintTime[INF_MAXPLAYERS];

ConVar g_ConVar_Admin_RemoveFlags;

// MENU STORAGE
int g_nLastSelectedRun[INF_MAXPLAYERS];
int g_nLastSelectedMode[INF_MAXPLAYERS];
int g_nLastSelectedStyle[INF_MAXPLAYERS];

// LIBARIES
bool g_bLib_Jumps;
bool g_bLib_Strafes;
bool g_bLib_Sync;
bool g_bLib_Speed;
bool g_bLib_Recording;
bool g_bLib_Ranking;
bool g_bLib_PlayerInfo;
bool g_bLib_Rocketjump;

// STYLE LIBARIES
bool g_bLib_Style_Parkour;

#include "influx_records/db.sp"
#include "influx_records/db_cb.sp"
#include "influx_records/menus.sp"
#include "influx_records/menus_hndlrs.sp"

#if defined DEBUG_PROFILING
int iProfiling;
#endif

public Plugin myinfo = 
{
	author = INF_AUTHOR, 
	url = INF_URL, 
	name = INF_NAME..." - Records", 
	description = "Map ranks, WR menus", 
	version = INF_VERSION
};

public APLRes AskPluginLoad2(Handle hPlugin, bool late, char[] szError, int error_len)
{
	RegPluginLibrary(INFLUX_LIB_RECORDS);
	LoadTranslations(INFLUX_PHRASES);
	
	// NATIVES
	CreateNative("Influx_GetRecordRank", Native_GetRecordRank);
	CreateNative("Influx_GetRecordTotalRank", Native_GetRecordTotalRank);
	CreateNative("Influx_GetNewPossibleRank", Native_GetNewPossibleRank);
	CreateNative("Influx_RefreshCache", Native_RefreshCache);
	CreateNative("Influx_RefreshRunCache", Native_RefreshRunCache);
	
	CreateNative("Influx_GetRecordPosition", Native_GetRecordPosition);
	CreateNative("Influx_DisplayRecord", Native_DisplayRecord);
	CreateNative("Influx_DisplayRecordFromCache", Native_DisplayRecordFromCache);
	
	
	RegConsoleCmd("sm_wr", Cmd_MapRecords);
	RegConsoleCmd("sm_worldrecord", Cmd_MapRecords);
	RegConsoleCmd("sm_top", Cmd_MapRecords);
	RegConsoleCmd("sm_sr", Cmd_MapRecords);
	RegConsoleCmd("sm_records", Cmd_MapRecords);
	
	// Check whether late
	if(late)
	{
		// Check for late load.
		if(Influx_GetCurrentMapId() != 0)
		{
			CheckCache();
		}
	}
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	LoadTranslations(INFLUX_PHRASES);

	g_ConVar_Admin_RemoveFlags = CreateConVar("influx_core_removeflags", "z", "Required flags to remove records and runs.");
	
	// LIBRARIES
	g_bLib_Jumps = LibraryExists(INFLUX_LIB_JUMPS);
	g_bLib_Strafes = LibraryExists(INFLUX_LIB_STRAFES);
	g_bLib_Sync = LibraryExists(INFLUX_LIB_SYNC);
	g_bLib_Recording = LibraryExists(INFLUX_LIB_RECORDING);
	g_bLib_Speed = LibraryExists(INFLUX_LIB_SPEED);
	g_bLib_Ranking = LibraryExists(INFLUX_LIB_RANKING);
	g_bLib_Ranking = LibraryExists(LIB_ROCKETJUMP);
	
	g_bLib_Style_Parkour = LibraryExists(INFLUX_LIB_STYLE_PARKOUR);
	g_bLib_PlayerInfo = LibraryExists(INFLUX_LIB_PLAYERINFO);
	
	#if defined DEBUG_PROFILING
	iProfiling = Influx_RegisterProfiler("influx_records OnTimerFinishPost");
	#endif
}

public void OnLibraryAdded(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_JUMPS))g_bLib_Jumps = true;
	if (StrEqual(lib, INFLUX_LIB_STRAFES))g_bLib_Strafes = true;
	if (StrEqual(lib, INFLUX_LIB_SYNC))g_bLib_Sync = true;
	if (StrEqual(lib, INFLUX_LIB_RECORDING))g_bLib_Recording = true;
	if (StrEqual(lib, INFLUX_LIB_SPEED))g_bLib_Speed = true;
	if (StrEqual(lib, INFLUX_LIB_RANKING))g_bLib_Ranking = true;
	if (StrEqual(lib, INFLUX_LIB_PLAYERINFO))g_bLib_PlayerInfo = true;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_Rocketjump = true;
	
	if (StrEqual(lib, INFLUX_LIB_STYLE_PARKOUR))g_bLib_Style_Parkour = true;
}

public void OnLibraryRemoved(const char[] lib)
{
	if (StrEqual(lib, INFLUX_LIB_JUMPS))g_bLib_Jumps = false;
	if (StrEqual(lib, INFLUX_LIB_STRAFES))g_bLib_Strafes = false;
	if (StrEqual(lib, INFLUX_LIB_SYNC))g_bLib_Sync = false;
	if (StrEqual(lib, INFLUX_LIB_RECORDING))g_bLib_Recording = false;
	if (StrEqual(lib, INFLUX_LIB_SPEED))g_bLib_Speed = false;
	if (StrEqual(lib, INFLUX_LIB_RANKING))g_bLib_Ranking = false;
	if (StrEqual(lib, INFLUX_LIB_PLAYERINFO))g_bLib_PlayerInfo = false;
	if (StrEqual(lib, LIB_ROCKETJUMP))g_bLib_Rocketjump = false;
	
	if (StrEqual(lib, INFLUX_LIB_STYLE_PARKOUR))g_bLib_Style_Parkour = false;
}

public void Influx_OnMapIdRetrieved()
{
	CheckCache();
}

CheckCache()
{
	// Reset cache
	ResetCache();
	DB_RefreshCache();
}

public void OnPluginEnd()
{
	DB_UpdateRanks();
}

ResetCache() {
	// Init world record cache
	int run, mode, style;
	for (run = 1; run <= MAX_RUNS; run++)
	{
		for (mode = 0; mode < MAX_MODES; mode++)
		{
			for (style = 0; style < MAX_STYLES; style++)
			{
				if (g_hRecordCache[run][mode][style] != INVALID_HANDLE)
				{
					ClearArray(g_hRecordCache[run][mode][style]);
				}
				else
				{
					// Set new array as no handle currently exists.
					g_hRecordCache[run][mode][style] = CreateArray(sizeof(nCacheTemplate));
				}
				
				g_bCacheLoaded[run] = false;
				g_bUpdateNeeded[run][mode][style] = false;
			}
		}
	}
}

ResetRunCache(int run) {
	// Init world record cache
	int style, mode;
	for (mode = 0; mode < MAX_MODES; mode++)
	{
		for (style = 0; style < MAX_STYLES; style++)
		{
			if (g_hRecordCache[run][mode][style] != INVALID_HANDLE)
			{
				ClearArray(g_hRecordCache[run][mode][style]);
			}
			else
			{
				// Set new array as no handle currently exists.
				g_hRecordCache[run][mode][style] = CreateArray(sizeof(nCacheTemplate));
			}
			
			g_bCacheLoaded[run] = false;
		}
	}
}

public void Influx_OnTimerFinishPost(int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags, int currentrank, int newrank)
{
	if (newrank <= 0)
		return;
	
	if ((flags & RES_TIME_PB) || (flags & RES_TIME_FIRSTOWNREC) || (flags & RES_TIME_FIRSTREC)) {
		
		#if defined DEBUG_PROFILING
		Influx_StartProfiling(iProfiling);
		#endif
		
		// Build new cache
		Handle nNewCache[RecordCache];
		
		// Record Information
		nNewCache[RecordId] = -1;
		nNewCache[MapId] = Influx_GetCurrentMapId();
		nNewCache[RunId] = runid;
		nNewCache[Mode] = mode;
		nNewCache[Style] = style;
		nNewCache[Time] = time;
		FormatTime(nNewCache[DateString], sizeof(nNewCache[DateString]), "%Y-%m-%d", GetTime());
		
		// Check if it's the first record, otherwise retrieve the finish count from the old record.
		if (flags & RES_TIME_FIRSTOWNREC)
		{
			nNewCache[FinishCount] = 1;
		}
		else
		{
			for (new i = 0; i < GetArraySize(g_hRecordCache[runid][mode][style]); i++)
			{
				Handle nCache[RecordCache];
				GetArrayArray(g_hRecordCache[runid][mode][style], i, nCache[0]);
				
				if (nCache[UserId] == Influx_GetClientId(client))
				{
					nNewCache[FinishCount] = nCache[FinishCount] + 1;
				}
			}
		}
		
		// User details
		nNewCache[UserId] = Influx_GetClientId(client);
		GetClientName(client, nNewCache[UserName], sizeof(nNewCache[UserName]));
		
		// Strafes / Jumps / Sync
		if (g_bLib_Strafes) {
			nNewCache[StrafeCount] = Influx_GetClientStrafeCount(client);
		}
		if (g_bLib_Jumps) {
			nNewCache[JumpCount] = Influx_GetClientJumpCount(client);
		}
		if (g_bLib_Sync) {
			nNewCache[StrafeSync] = Influx_GetClientStrafeSync(client);
			//nNewCache[JumpSync] = Influx_GetClientJumpSync(client);
		}
		
		// Speed / Distance Tracking
		if (g_bLib_Speed) {
			nNewCache[AvgSpeed] = Influx_GetClientAverageSpeed(client);
			nNewCache[FinalSpeed] = Influx_GetClientFinalSpeed(client);
			nNewCache[MaxSpeed] = Influx_GetClientMaxSpeed(client);
			
			// Distance
			nNewCache[DistanceTravelled] = Influx_GetClientDistanceTravelled(client);
			nNewCache[MouseMovement] = Influx_GetClientMouseMovement(client);
		}
		
		// Set Custom Values
		nNewCache[Custom1] = -1;
		nNewCache[Custom2] = -1;
		nNewCache[Custom3] = -1;
		
		// Rocketjump Shots
		if(g_bLib_Rocketjump)
		{
			nNewCache[Custom3] = RJ_GetRocketShotCount(client);
		}
		
		// Parkour Boosts
		if (style == STYLE_PARKOUR && g_bLib_Style_Parkour)
		{
			nNewCache[Custom1] = Influx_GetClientParkourBoosts(client);
		}
		
		// Currently has a record - remove it.
		if (currentrank > 0)
		{
			RemoveFromArray(g_hRecordCache[runid][mode][style], currentrank - 1);
		}
		
		// New worldrecord with existing records
		if (newrank == 1 && GetArraySize(g_hRecordCache[runid][mode][style]) > 0)
		{
			ShiftArrayUp(g_hRecordCache[runid][mode][style], newrank - 1);
			SetArrayArray(g_hRecordCache[runid][mode][style], newrank - 1, nNewCache[0]);
		}
		// Not last rank, so shift and set
		else if (newrank <= GetArraySize(g_hRecordCache[runid][mode][style]))
		{
			ShiftArrayUp(g_hRecordCache[runid][mode][style], newrank - 1);
			SetArrayArray(g_hRecordCache[runid][mode][style], newrank - 1, nNewCache[0]);
		}
		
		// First record on this track or last place
		else
		{
			PushArrayArray(g_hRecordCache[runid][mode][style], nNewCache[0]);
		}
		
		// New record, update this at the end.
		g_bUpdateNeeded[runid][mode][style] = true;
		
		#if defined DEBUG_PROFILING
		Influx_EndProfiling(iProfiling);
		#endif
	}
	else
	{
		// Increment finish count.
		Handle nOldCache[RecordCache];
		GetArrayArray(g_hRecordCache[runid][mode][style], currentrank - 1, nOldCache[0]);
		nOldCache[FinishCount]++;
		SetArrayArray(g_hRecordCache[runid][mode][style], currentrank - 1, nOldCache[0])
	}
}

// Other shit
// Check whether user can remove records
stock bool CanUserRemoveRecords(int client)
{
	if (client == 0)return true;
	
	
	char szFlags[32];
	g_ConVar_Admin_RemoveFlags.GetString(szFlags, sizeof(szFlags));
	
	int wantedflags = ReadFlagString(szFlags);
	
	return ((GetUserFlagBits(client) & wantedflags) == wantedflags);
}

// Add new line to menu
stock AddMenuLine(char[] sz, int len, char[] newline, any...)
{
	// Format the new line
	char formatted[256];
	VFormat(formatted, sizeof(formatted), newline, 4);
	
	// Format the new string
	Format(sz, len, "%s\n%s", sz, formatted);
}
// Add spacer
stock AddMenuSpacer(char[] sz, int len)
{
	Format(sz, len, "%s\n \n", sz);
}

// Get position of record for a certain uid
stock int GetRecordPosition(int uid, int run, int mode, style)
{
	if (Influx_GetRunsArray().Length == 0 || Influx_GetModesArray().Length == 0 || Influx_GetStylesArray().Length == 0 || run == -1 || mode == -1 || style == -1) {
		return -1;
	}
	
	for (new i = 0; i < GetArraySize(g_hRecordCache[run][mode][style]); i++)
	{
		new nCache[RecordCache];
		GetArrayArray(g_hRecordCache[run][mode][style], i, nCache[0]);
		
		if (nCache[UserId] == uid)
			return i;
	}
	
	return -1;
}

// NATIVES
public Native_GetRecordRank(Handle hPlugin, int nParams)
{
	int client = GetNativeCell(1);
	int run = GetNativeCell(2);
	int mode = GetNativeCell(3);
	int style = GetNativeCell(4);
	
	int userId = Influx_GetClientId(client);
	
	if (Influx_GetRunsArray().Length == 0 || Influx_GetModesArray().Length == 0 || Influx_GetStylesArray().Length == 0 || run == -1 || mode == -1 || style == -1) {
		return 0;
	}
	
	for (new i = 0; i < GetArraySize(g_hRecordCache[run][mode][style]); i++)
	{
		new nCache[RecordCache];
		GetArrayArray(g_hRecordCache[run][mode][style], i, nCache[0]);
		
		if (nCache[UserId] == userId)
			return i + 1;
	}
	
	return 0;
}
public Native_GetRecordTotalRank(Handle hPlugin, int nParams)
{
	return GetArraySize(g_hRecordCache[GetNativeCell(1)][GetNativeCell(2)][GetNativeCell(3)]);
}
public int Native_GetNewPossibleRank(Handle hPlugin, int nParams)
{
	int run = GetNativeCell(1);
	int mode = GetNativeCell(2);
	int style = GetNativeCell(3);
	float time = GetNativeCell(4);
	
	if (time == 0.0)
		return 0;
	
	if (GetArraySize(g_hRecordCache[run][mode][style]) <= 0)
		return 1;
	
	for (int i = 0; i < GetArraySize(g_hRecordCache[run][mode][style]); i++)
	{
		Handle nCache[RecordCache];
		GetArrayArray(g_hRecordCache[run][mode][style], i, nCache[0]);
		
		if (nCache[Time] > time)
			return i + 1;
	}
	
	return GetArraySize(g_hRecordCache[run][mode][style]) + 1;
}
public Native_RefreshCache(Handle hPlugin, int nParams)
{
	DB_RefreshCache();
}
public Native_RefreshRunCache(Handle hPlugin, int nParams)
{
	ResetRunCache(GetNativeCell(1));
	DB_RefreshRunCache(GetNativeCell(1));
}
public Native_GetRecordPosition(Handle hPlugin, int nParams)
{
	// UserID
	// Run
	// Mode
	// Style
	return GetRecordPosition(GetNativeCell(1), GetNativeCell(2), GetNativeCell(3), GetNativeCell(4));
}
public Native_DisplayRecord(Handle hPlugin, int nParams)
{
	int run = GetNativeCell(1);
	int mode = GetNativeCell(2);
	int style = GetNativeCell(3);
	Menu menu = view_as<Menu>(GetNativeCellRef(4));
	int recordIndex = GetNativeCell(5);
	int client = GetNativeCell(6);
	bool disableOptions = GetNativeCell(7);
	
	// Get record array.
	Handle nCache[RecordCache];
	GetArrayArray(g_hRecordCache[run][mode][style], recordIndex, nCache[0]);
	
	DisplayRecordMenu(nCache, menu, recordIndex, client, disableOptions);
}
public Native_DisplayRecordFromCache(Handle hPlugin, int nParams)
{
	Menu menu = view_as<Menu>(GetNativeCellRef(2));
	int recordIndex = GetNativeCell(3);
	int client = GetNativeCell(4);
	bool disableOptions = GetNativeCell(5);
	
	// Get record array.
	Handle nCache[RecordCache];
	GetNativeArray(1, nCache[0], sizeof(nCacheTemplate));
	
	DisplayRecordMenu(nCache, menu, recordIndex, client, disableOptions);
}
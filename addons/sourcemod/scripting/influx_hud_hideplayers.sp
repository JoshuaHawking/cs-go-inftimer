#include <sourcemod>

#include <influx/core>
#include <influx/hud>

#undef REQUIRE_PLUGIN


public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - HUD | Hide Players",
    description = "Hide players or bots.",
    version = INF_VERSION
};

public void OnClientPutInServer( int client )
{
    // Has to be hooked to everybody.
    SDKHook( client, SDKHook_SetTransmit, E_SetTransmit_Client );
}

public void OnClientDisconnect( int client )
{
    SDKUnhook( client, SDKHook_SetTransmit, E_SetTransmit_Client );
}

public Action E_SetTransmit_Client( int ent, int client )
{
    if ( !IS_ENT_PLAYER( ent ) || client == ent ) return Plugin_Continue;
    
    // If we're spectating somebody, show them no matter what.
    if ( !IsPlayerAlive( client ) && GetClientObserverTarget( client ) == ent )
    {
        return Plugin_Continue;
    }
    
    
    if ( IsFakeClient( ent ) )
    {
        return ( Influx_GetClientHideFlags( client ) & HIDEFLAG_HIDE_BOTS ) ? Plugin_Handled : Plugin_Continue;
    }
    else
    {
        return ( Influx_GetClientHideFlags( client ) & HIDEFLAG_HIDE_PLAYERS ) ? Plugin_Handled : Plugin_Continue;
    }
}
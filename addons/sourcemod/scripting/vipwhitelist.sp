#pragma semicolon 1

#include <sourcemod>
#include <sdktools>

#pragma newdecls required

ConVar g_szWhitelistMessage;
ConVar g_bWhitelistEnabled;

public Plugin myinfo = 
{
	name = "VIP Whitelist", 
	author = "Fusion", 
	description = "Whitelist based on ADMFLAGS.", 
	version = "1.0", 
	url = ""
};

public void OnPluginStart()
{
	g_szWhitelistMessage = CreateConVar("vip_whitelist_message", "You need to be VIP to join this server", "Sets the message people will see when they are kicked.");
	g_bWhitelistEnabled = CreateConVar("vip_whitelist_enabled", "0", "Whether the whitelist is enabled. 1 = Yes, 0 = No", _, true, 0.0, true, 1.0);
}

public void OnClientPostAdminCheck(int client)
{
	// Check whitelist is enabled
	if (!GetConVarBool(g_bWhitelistEnabled))
	{
		return;
	}
	
	// Check whether it's a real client.
	if(IsFakeClient(client))
	{
		return;
	}
	
	if(!CheckCommandAccess(client, "vipwhitelist", ADMFLAG_RESERVATION))
	{
		char szMessage[256];
		GetConVarString(g_szWhitelistMessage, szMessage, sizeof(szMessage));
		
		KickClient(client, szMessage);
	}
}
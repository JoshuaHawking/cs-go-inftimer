#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>

#include <msharedutil/misc>

#include <pets>

public Plugin myinfo = 
{
	name = "Pets", 
	author = "Fusion", 
	description = "Pets, pets, pets!", 
	version = "1.0.0", 
	url = "HouseOfClimb.com"
};

/*
	Pets Storing
*/
enum Pet
{
	PetID = 0, 
	String:Name[64], 
	String:Description[64], 
	
	String:Model[128], 
	String:AnimIdle[64], 
	String:AnimWalk[64], 
	String:AnimJump[64], 
	Height, 
	HeightCustom, 
	Float:ModelScale, 
	
	AdmFlag, 
	ColoredEnabled
}

#define STATE_IDLE  1
#define STATE_WALKING 2
#define STATE_JUMPING 3

enum PetColor
{
	PETCOLOR_NORMAL = 0, 
	PETCOLOR_RED, 
	PETCOLOR_GREEN, 
	PETCOLOR_BLUE, 
	PETCOLOR_PINK, 
	PETCOLOR_ORANGE, 
	PETCOLOR_CYAN, 
	PETCOLOR_LIME, 
	PETCOLOR_BLACK, 
	PETCOLOR_TEAM, 
	PETCOLOR_LTEAM, 
	PETCOLOR_DTEAM, 
	PETCOLOR_MAGIC
}

// Chat / Log Prefixes
#define ERROR_PREFIX			"[Pets] "
#define CHAT_PREFIX				" \x08[\x0BPets\x08] "

#define MAX_PETS	64
#define MAX_DOWNLOADS 8192

#define PET_TARGETNAME		"csgo_pet"
#define PET_NONAME			"Unnamed Pet"
#define PET_CHANGEDELAY		3.5

#define TRANSLATION_FILE		"pets.phrases"
#define MYSQL_CONFIG_NAME		"pets"
#define MYSQL_TABLE_NAME		"pets"
#define MAX_STEAMAUTH_LENGTH	21

#define MENU_HANDLE(%0,%1)       if ( %1 == MenuAction_End ) { delete %0; return 0; } else if ( %1 == MenuAction_Cancel ) { return 0; }
#define MENU_HANDLE_BACK(%0,%1,%2)       if ( %1 == MenuAction_End ) { delete %0; return 0; } else if ( %1 == MenuAction_Cancel && %2 != MenuCancel_ExitBack ) { return 0; }

/*
	Globals
*/
Handle g_hPetInfo[MAX_PETS][Pet];
int g_iPetsCount;

int g_iPet[MAXPLAYERS + 1];
int g_iPetType[MAXPLAYERS + 1];
int g_iPetColor[MAXPLAYERS + 1];
int g_iPetState[MAXPLAYERS + 1];
char g_szPetName[MAXPLAYERS + 1][32];
float g_flPetCooldown[MAXPLAYERS + 1];
bool g_bEnableTalking[MAXPLAYERS + 1];
bool g_bHidePets[MAXPLAYERS + 1];

Handle g_hDB;

/*
	Plugin Loading
*/
public void OnPluginStart()
{
	RegPluginLibrary(LIB_PETS);
	
	// COMMANDS
	RegConsoleCmd("sm_pet", Cmd_PetsMenu);
	RegConsoleCmd("sm_pets", Cmd_PetsMenu);
	RegAdminCmd("sm_petname", Cmd_PetsName, ADMFLAG_RESERVATION);
	
	// EVENTS
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("player_death", Event_Death);
	HookEvent("player_team", Event_Team, EventHookMode_Pre);
	
	// NATIVES
	CreateNative("Pets_SetPetsHidden", Native_SetPetsHidden);
	CreateNative("Pets_HasPetsHidden", Native_HasPetsHidden);
	CreateNative("Pets_SetPetsGreeting", Native_SetPetsGreeting);
	CreateNative("Pets_HasPetsGreeting", Native_HasPetsGreeting);
	
	// LOAD PETS FROM CONFIG
	LoadConfig();
	
	// LOAD DATABASE
	DB_LoadDatabase();
}

LoadConfig()
{
	Handle hKV = CreateKeyValues("Pets");
	
	char szFile[128];
	BuildPath(Path_SM, szFile, sizeof(szFile), "configs/pets.cfg");
	
	// Load pets from keyfile
	FileToKeyValues(hKV, szFile);
	
	char szValue[128];
	
	// Loop through pets
	for (new i = 1; i <= MAX_PETS; i++)
	{
		Format(szValue, sizeof(szValue), "%i", i);
		
		// Jump to key, if it hasn't been found, error as no pets are set.
		if (!KvJumpToKey(hKV, szValue))
		{
			if (i == 1) // No pets found
			{
				SetFailState(ERROR_PREFIX..."Failed to load sourcemod/configs/pets.cfg, please check the config!");
			}
			g_iPetsCount = i - 1;
			break;
		}
		
		// Successfully found config, load pets entry
		KvGetString(hKV, "name", g_hPetInfo[i][Name], 64);
		KvGetString(hKV, "desc", g_hPetInfo[i][Description], 64);
		
		// Models
		KvGetString(hKV, "model", g_hPetInfo[i][Model], 128);
		KvGetString(hKV, "anim_idle", g_hPetInfo[i][AnimIdle], 64);
		KvGetString(hKV, "anim_walk", g_hPetInfo[i][AnimWalk], 64);
		KvGetString(hKV, "anim_jump", g_hPetInfo[i][AnimJump], 64);
		
		g_hPetInfo[i][Height] = KvGetNum(hKV, "height_type");
		g_hPetInfo[i][HeightCustom] = KvGetNum(hKV, "height_custom");
		g_hPetInfo[i][ModelScale] = KvGetFloat(hKV, "modelscale", 1.0);
		
		// Admin flags / colors
		char szFlag[4];
		KvGetString(hKV, "adminflag", szFlag, sizeof(szFlag), "0");
		g_hPetInfo[i][AdmFlag] = DetermineAdminFlag(szFlag);
		g_hPetInfo[i][ColoredEnabled] = KvGetNum(hKV, "can_be_colored", 1);
		
		KvGoBack(hKV);
	}
	
	CloseHandle(hKV);
}

// Cache models and add files to downloads table
public OnMapStart()
{
	// Kill all pets
	
	Handle hKV = CreateKeyValues("Pets");
	
	char szFile[128];
	BuildPath(Path_SM, szFile, sizeof(szFile), "configs/pets.cfg");
	
	// Load pets from keyfile
	FileToKeyValues(hKV, szFile);
	KvRewind(hKV);
	
	if (!KvJumpToKey(hKV, "Downloads"))
	{
		LogAction(-1, -1, "Failed to go to download keys");
	}
	
	char szBuffer[128];
	char szDownload[128];
	
	// Add files to download table
	for (new i = 1; i <= MAX_DOWNLOADS; i++)
	{
		Format(szBuffer, sizeof(szBuffer), "%i", i);
		
		// Get download string
		KvGetString(hKV, szBuffer, szDownload, sizeof(szDownload), "final");
		
		if (StrEqual(szDownload, "final"))
			break;
		
		AddFileToDownloadsTable(szDownload);
	}
	
	KvRewind(hKV);
	
	// Now precache models
	for (new i = 1; i <= MAX_PETS; i++)
	{
		Format(szBuffer, sizeof(szBuffer), "%i", i);
		
		if (!KvJumpToKey(hKV, szBuffer))
		{
			break;
		}
		
		// Models
		KvGetString(hKV, "model", szDownload, 128);
		PrecacheModel(szDownload, true);
		
		KvGoBack(hKV);
	}
	
	CloseHandle(hKV);
}

/*
	Player (Dis)Connect
*/
public OnClientPutInServer(client)
{
	g_iPet[client] = 0;
	g_iPetColor[client] = 0;
	g_iPetType[client] = 0;
	g_szPetName[client] = PET_NONAME;
	g_flPetCooldown[client] = 0.0;
	g_bEnableTalking[client] = true;
	
	DB_LoadClient(client);
}

public OnClientDisconnect(client)
{
	KillPet(client);
	DB_SaveClient(client);
	
}

public Action Event_Spawn(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (g_iPetType[client] > 0 && GetClientTeam(client) != CS_TEAM_SPECTATOR)
	{
		if (IsValidPet(g_iPet[client]))
			KillPet(client);
			
		SpawnPet(client);
	}
}

public Action Event_Death(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	if (IsValidPet(g_iPet[client]))
		KillPet(client);
}

// Called when a player has actually jointed the team.
public Action Event_Team(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int team = event.GetInt("team");
	
	if (team == CS_TEAM_SPECTATOR)
	{
		if (IsValidPet(g_iPet[client]))
			KillPet(client);
	}
	
	return Plugin_Continue;
}

/*
	Commands
*/
public Action Cmd_PetsMenu(int client, int args)
{
	// Display menu
	Menu menu = new Menu(Hndlr_PetsMenu);
	
	// Check whether they have a pet name set and a pet currently selected
	char szMessage[128];
	szMessage = "";
	
	if (g_iPetType[client] != 0 && StrEqual(g_szPetName[client], PET_NONAME))
	{
		// Add hint to the top of the menu
		szMessage = "\nHint: Your pet is currently unnnamed. \nUse !petname command in chat to give your pet a name!\n ";
	}
	
	SetMenuTitle(menu, "Pets Menu\n %s", szMessage);
	
	// Add options if they have permission
	if (CheckCommandAccess(client, "sm_petname", ADMFLAG_RESERVATION))
	{
		menu.AddItem("selectpet", "Select your pet");
		menu.AddItem("selectpetcolor", "Select your pet color\n ", (g_iPetType[client] == 0 ? ITEMDRAW_DISABLED : ITEMDRAW_CONTROL));
	}
	// Format options
	Format(szMessage, sizeof(szMessage), "Hide Pets: %s", (g_bHidePets[client] ? "ON" : "OFF"));
	menu.AddItem("hidepets", szMessage);
	
	if (CheckCommandAccess(client, "sm_petname", ADMFLAG_RESERVATION))
	{	
		Format(szMessage, sizeof(szMessage), "Pet Greeting: %s", (g_bEnableTalking[client] ? "ON" : "OFF"));
		menu.AddItem("petgreeting", szMessage);
	}
	
	menu.Display(client, MENU_TIME_FOREVER);
	
}

public Action Cmd_PetsName(int client, int args)
{
	// Check valid client.
	if (!client)
	{
		return Plugin_Handled;
	}
	
	// Check whether they have a pet equipped first
	if (g_iPetType[client] == 0)
	{
		PrintToChat(client, "%sYou must have a pet to name! Select one!", CHAT_PREFIX);
		return Plugin_Handled;
	}
	// Build tag
	char arg[128];
	char szName[32];
	
	for (int i = 1; i <= args; i++)
	{
		GetCmdArg(i, arg, sizeof(arg));
		Format(szName, sizeof(szName), "%s %s", szName, arg);
	}
	
	TrimString(szName);
	
	if (strlen(szName) < 2)
	{
		PrintToChat(client, "%sYou can't use that name!", CHAT_PREFIX);
	}
	else
	{
		Format(g_szPetName[client], sizeof(g_szPetName[]), "%s", szName)
		PrintToChat(client, "%sSet your pet's name to \x06%s", CHAT_PREFIX, szName);
	}
	
	return Plugin_Handled;
}

/*
	Menus
*/

public Menu_SelectPetType(int client)
{
	Menu menu = new Menu(Hndlr_SelectPetType);
	
	// Get their selected pet
	char szMessage[128];
	szMessage = "";
	
	if (g_iPetType[client] != 0)
	{
		// Add hint to the top of the menu
		Format(szMessage, sizeof(szMessage), "\nCurrent pet: %s", g_hPetInfo[g_iPetType[client]][Name]);
	}
	else
	{
		// Add hint to the top of the menu
		Format(szMessage, sizeof(szMessage), "\nCurrent pet: None");
	}
	
	SetMenuTitle(menu, "Pets Menu - Select your pet%s", szMessage);
	
	int iBits;
	char szInfo[4];
	char szText[48];
	
	menu.AddItem("-1", "No Pet");
	
	for (new i = 1; i <= g_iPetsCount; i++)
	{
		if (strlen(g_hPetInfo[i][Name]) > 1)
		{
			IntToString(i, szInfo, sizeof(szInfo));
			iBits = GetUserFlagBits(client);
			if (g_hPetInfo[i][AdmFlag] == 0 || iBits & g_hPetInfo[i][AdmFlag] || iBits & ADMFLAG_ROOT)
			{
				Format(szText, sizeof(szText), "%s", g_hPetInfo[i][Name])
				menu.AddItem(szInfo, szText);
			}
			else
			{
				Format(szText, sizeof(szText), "%s (No Access)", g_hPetInfo[i][Name])
				menu.AddItem(szInfo, szText, ITEMDRAW_DISABLED);
			}
		}
	}
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public Menu_SelectPetColor(int client)
{
	Menu menu = new Menu(Hndlr_SelectPetColor);
	
	// Get their selected pet
	char szMessage[128];
	szMessage = "";
	char szColor[64];
	
	// Add hint to the top of the menu
	DetermineColorName(g_iPetColor[client], szColor, sizeof(szColor));
	Format(szMessage, sizeof(szMessage), "\nCurrent pet color: %s", szColor);
	
	SetMenuTitle(menu, "Pets Menu - Select your pet color%s", szMessage);
	
	menu.AddItem("0", "No Color");
	menu.AddItem("1", "Red");
	menu.AddItem("2", "Green");
	menu.AddItem("3", "Blue");
	menu.AddItem("4", "Pink");
	menu.AddItem("5", "Orange");
	menu.AddItem("6", "Cyan");
	menu.AddItem("7", "Lime");
	menu.AddItem("8", "Black");
	menu.AddItem("9", "Team Based");
	menu.AddItem("10", "L-Team");
	menu.AddItem("11", "D-Team");
	menu.AddItem("12", "Magic");
	
	menu.ExitBackButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}
public int Hndlr_PetsMenu(Handle menu, MenuAction action, int client, int index)
{
	MENU_HANDLE(menu, action)
	
	char szInfo[32];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	if (StrEqual(szInfo, "selectpet"))
	{
		Menu_SelectPetType(client);
		return 0;
	}
	else if (StrEqual(szInfo, "selectpetcolor"))
	{
		Menu_SelectPetColor(client);
		return 0;
	}
	else if(StrEqual(szInfo, "hidepets"))
	{
		g_bHidePets[client] = !g_bHidePets[client];
		Cmd_PetsMenu(client, 0);
	}
	else if(StrEqual(szInfo, "petgreeting"))
	{
		g_bEnableTalking[client] = !g_bEnableTalking[client];
		Cmd_PetsMenu(client, 0);
	}
	return 0;
}

public int Hndlr_SelectPetType(Handle menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They selected to go back, so go back to the first menu.
		Cmd_PetsMenu(client, 0);
		return 0;
	}
	
	char szInfo[4];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	int iPet;
	iPet = StringToInt(szInfo);
	
	// Check cooldown
	if (!HandleCmdSpam(client, PET_CHANGEDELAY, g_flPetCooldown[client], true))
	{
		if (iPet == -1)
		{
			g_iPetType[client] = 0;
			KillPet(client);
		}
		else
		{
			g_iPetType[client] = iPet;
			
			if (IsValidPet(client))
				KillPet(client);
				
			if (IsPlayerAlive(client))
			{
				SpawnPet(client);
			}
		}
	}
	Menu_SelectPetType(client);
	
	return 0;
}

public int Hndlr_SelectPetColor(Handle menu, MenuAction action, int client, int index)
{
	MENU_HANDLE_BACK(menu, action, index)
	
	if (action == MenuAction_Cancel && index == MenuCancel_ExitBack) {
		// They selected to go back, so go back to the first menu.
		Cmd_PetsMenu(client, 0);
		return 0;
	}
	
	char szInfo[4];
	if (!GetMenuItem(menu, index, szInfo, sizeof(szInfo)))return 0;
	
	g_iPetColor[client] = StringToInt(szInfo);
	
	UpdatePetColor(client);
	
	Menu_SelectPetColor(client);
	return 0;
}
/*
	Pet Spawning
*/

SpawnPet(int client)
{
	// Check valid pet type
	if (g_iPetType[client] == 0)
		return;
	
	if (IsValidPet(g_iPet[client]))
		KillPet(client);
		
	// Firstly, prepare the spawning of the prop
	float flPos[3];
	GetClientAbsOrigin(client, flPos);
	OffsetPosition(flPos);
	
	// Spawn pet
	if (!(g_iPet[client] = CreateEntityByName("prop_dynamic_override")))
		return;
	
	PrecacheModel(g_hPetInfo[g_iPetType[client]][Model]);
	SetEntityModel(g_iPet[client], g_hPetInfo[g_iPetType[client]][Model]);
	DispatchKeyValue(g_iPet[client], "targetname", PET_TARGETNAME)
	DispatchSpawn(g_iPet[client]);
	TeleportEntity(g_iPet[client], flPos, NULL_VECTOR, NULL_VECTOR);
	
	// Set model scale	
	if (g_hPetInfo[g_iPetType[client]][ModelScale] > 0.0)
	{
		SetEntPropFloat(g_iPet[client], Prop_Send, "m_flModelScale", g_hPetInfo[g_iPetType[client]][ModelScale])
	}
	
	// Update pet color
	UpdatePetColor(client);
	
	// Set pet name
	if (strlen(g_szPetName[client]) < 2)
	{
		Format(g_szPetName[client], sizeof(g_szPetName[]), PET_NONAME);
	}
	
	// Say hello
	if (g_bEnableTalking[client] == true)
	{
		int iRandom = GetRandomInt(1, 6);
		char szMessage[128];
		switch (iRandom)
		{
			case 1:Format(szMessage, sizeof(szMessage), "Hi, %N!", client);
			case 2:Format(szMessage, sizeof(szMessage), "How are you doing %N?", client);
			case 3:Format(szMessage, sizeof(szMessage), "What's up %N?", client);
			case 4:Format(szMessage, sizeof(szMessage), "Hey there %N!", client);
			case 5:Format(szMessage, sizeof(szMessage), "Hello, %N!", client);
			case 6:Format(szMessage, sizeof(szMessage), "Looking good %N!", client);
		}
		PrintToChat(client, " \x06%s\x01: %s", g_szPetName[client], szMessage);
	}
	
	// Hook SDK hooks
	
	SDKHook(client, SDKHook_PreThink, E_PetThink);
	SDKHook(g_iPet[client], SDKHook_SetTransmit, E_SetTransmit_Pet);
	
}

// SDK Hooks
public E_PetThink(int client)
{
	// invalid pet, unhook and return
	if (!IsValidPet(g_iPet[client]))
	{
		SDKUnhook(client, SDKHook_PreThink, E_PetThink);
		return;
	}
	
	// Get locations, angles, distances
	float flPos[3], flAng[3], flClientPos[3];
	GetEntPropVector(g_iPet[client], Prop_Data, "m_vecOrigin", flPos);
	GetEntPropVector(g_iPet[client], Prop_Data, "m_angRotation", flAng);
	GetClientAbsOrigin(client, flClientPos);
	
	float flDist = GetVectorDistance(flClientPos, flPos);
	float flDistX = flClientPos[0] - flPos[0];
	float flDistY = flClientPos[1] - flPos[1];
	float flSpeed = (flDist - 64.0) / 54;
	Math_Clamp(flSpeed, -4.0, 4.0);
	if (FloatAbs(flSpeed) < 0.3)
		flSpeed *= 0.1;
	
	// Teleport to owner if too far
	if (flDist > 1024.0)
	{
		float flTemp[3];
		GetClientAbsOrigin(client, flTemp);
		OffsetPosition(flTemp);
		TeleportEntity(g_iPet[client], flTemp, NULL_VECTOR, NULL_VECTOR);
		GetEntPropVector(g_iPet[client], Prop_Data, "m_vecOrigin", flPos);
	}
	
	// Set new location data
	if (flPos[0] < flClientPos[0])flPos[0] += flSpeed;
	if (flPos[0] > flClientPos[0])flPos[0] -= flSpeed;
	if (flPos[1] < flClientPos[1])flPos[1] += flSpeed;
	if (flPos[1] > flClientPos[1])flPos[1] -= flSpeed;
	
	// Height
	switch (g_hPetInfo[g_iPetType[client]][Height])
	{
		case 1:flPos[2] = flClientPos[2]
		case 2:flPos[2] = flClientPos[2] + g_hPetInfo[g_iPetType[client]][HeightCustom];
	}
	
	// Pet states
	if (!(GetEntityFlags(client) & FL_ONGROUND)) // In air
		SetPetState(client, STATE_JUMPING);
	else if (FloatAbs(flSpeed) > 0.2) // Moving
		SetPetState(client, STATE_WALKING);
	else // Stationary / Little Movement
		SetPetState(client, STATE_IDLE);
	
	// Look at owner
	flAng[1] = (ArcTangent2(flDistY, flDistX) * 180) / 3.14;
	
	// Finalize new location
	/*if(TR_GetPointContents(pos) == 1)
		return;*/
	
	TeleportEntity(g_iPet[client], flPos, flAng, NULL_VECTOR);
}

public Action E_SetTransmit_Pet(int ent, int client)
{
	// Check whether the entity is a pet
	if (!IsValidPet(ent))
		return Plugin_Continue;
	
	// Check whether it's the client's own pet
	if (ent == g_iPet[client])
		return Plugin_Continue;
	
	if (g_bHidePets[client])
		return Plugin_Handled;
	
	return Plugin_Continue;
}

UpdatePetColor(int client)
{
	if (g_hPetInfo[g_iPetType[client]][ColoredEnabled] == 1)
	{
		if (IsValidPet(g_iPet[client]))
		{
			SetEntityRenderColor(g_iPet[client], 255, 255, 255)
			switch (g_iPetColor[client])
			{
				case PETCOLOR_RED:SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
				case PETCOLOR_GREEN:SetEntityRenderColor(g_iPet[client], 0, 255, 0, 255);
				case PETCOLOR_BLUE:SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
				case PETCOLOR_PINK:SetEntityRenderColor(g_iPet[client], 255, 0, 255, 255);
				case PETCOLOR_ORANGE:SetEntityRenderColor(g_iPet[client], 255, 128, 0, 255);
				case PETCOLOR_CYAN:SetEntityRenderColor(g_iPet[client], 128, 255, 255, 255);
				case PETCOLOR_LIME:SetEntityRenderColor(g_iPet[client], 128, 255, 0, 255);
				case PETCOLOR_BLACK:SetEntityRenderColor(g_iPet[client], 0, 0, 0, 255);
				case PETCOLOR_TEAM:
				{
					if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 255, 0, 0, 255);
					else SetEntityRenderColor(g_iPet[client], 0, 0, 255, 255);
				}
				case PETCOLOR_LTEAM:
				{
					if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 255, 128, 128, 255);
					else SetEntityRenderColor(g_iPet[client], 128, 128, 255, 255);
				}
				case PETCOLOR_DTEAM:
				{
					if (GetClientTeam(client) == 2)SetEntityRenderColor(g_iPet[client], 128, 0, 0, 255);
					else SetEntityRenderColor(g_iPet[client], 0, 0, 128, 255);
				}
				case PETCOLOR_MAGIC:SetEntityRenderColor(g_iPet[client], GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, GetRandomInt(0, 2) * 127 + 1, 255);
			}
		}
	}
}

SetPetState(int client, int iState)
{
	// Get position
	float flPos[3];
	GetEntPropVector(g_iPet[client], Prop_Data, "m_vecOrigin", flPos);
	
	// If we are currently in that state, no need to change anything
	if (g_iPetState[client] == iState)
		return;
	
	switch (iState)
	{
		case STATE_IDLE:SetPetAnim(client, g_hPetInfo[g_iPetType[client]][AnimIdle]);
		case STATE_WALKING:SetPetAnim(client, g_hPetInfo[g_iPetType[client]][AnimWalk]);
		case STATE_JUMPING:SetPetAnim(client, g_hPetInfo[g_iPetType[client]][AnimJump]);
	}
	
	g_iPetState[client] = iState;
}

SetPetAnim(client, const char[] szAnim)
{
	// Set animation for pet
	SetVariantString(szAnim);
	AcceptEntityInput(g_iPet[client], "SetAnimation");
}

KillPet(client)
{
	// Check for valid pet
	if (g_iPet[client] == 0)
		return;
	
	// Unhook SDK hooks
	SDKUnhook(client, SDKHook_PreThink, E_PetThink);
	SDKUnhook(g_iPet[client], SDKHook_SetTransmit, E_SetTransmit_Pet);
	
	// Kill and set pet to zero
	AcceptEntityInput(g_iPet[client], "Kill");
	g_iPet[client] = 0;
}

bool IsValidPet(int entity)
{
	if (entity > 0 && IsValidEntity(entity))
	{
		char szName[16];
		GetEntPropString(entity, Prop_Data, "m_iName", szName, sizeof(szName));
		return StrEqual(szName, PET_TARGETNAME);
	}
	return false;
}

/*
	Databases
*/
DB_LoadDatabase()
{
	char szError[128];
	
	// Connect to database
	if (SQL_CheckConfig(MYSQL_CONFIG_NAME))
	{
		g_hDB = SQL_Connect(MYSQL_CONFIG_NAME, true, szError, sizeof(szError));
	}
	
	if (g_hDB == null)
	{
		SetFailState("Couldn't retrieve database handle!");
	}
	
	// Create table
	SQL_TQuery(g_hDB, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...MYSQL_TABLE_NAME..." (`auth` VARCHAR(21) NOT NULL, `pettype` INT NULL, `petname` VARCHAR(32) NULL, `petcolor` INT NULL, `pettalks` INT NULL, `petshidden` INT NULL, PRIMARY KEY (`auth`));", _, DBPrio_High);
	
}

stock bool DB_GetEscaped(char[] out, int len, const char[] def = "")
{
	if (!SQL_EscapeString(g_hDB, out, out, len))
	{
		strcopy(out, len, def);
		
		return false;
	}
	
	return true;
}

DB_LoadClient(int client)
{
	if (g_hDB == null)
	{
		return;
	}
	
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return;
	}
	
	char query[512];
	FormatEx(query, sizeof(query), "SELECT `pettype`, `petname`, `petcolor`, `pettalks`, `petshidden` FROM "...MYSQL_TABLE_NAME..." WHERE auth = '%s'", szAuth);
	
	SQL_TQuery(g_hDB, Thrd_GetClientSettings, query, GetClientUserId(client), DBPrio_Low);
	return;
}

public void Thrd_GetClientSettings(Handle db, Handle res, const char[] szError, int userId)
{
	if (res == null)
	{
		LogError("Failed database query: getting pet info for client");
		return;
	}
	
	int client;
	
	// User has disconnected
	if (!(client = GetClientOfUserId(userId)))
	{
		return;
	}
	
	while (SQL_FetchRow(res))
	{
		g_iPetType[client] = SQL_FetchInt(res, 0);
		SQL_FetchString(res, 1, g_szPetName[client], sizeof(g_szPetName[]));
		g_iPetColor[client] = SQL_FetchInt(res, 2);
		g_bEnableTalking[client] = (SQL_FetchInt(res, 3) == 1 ? true : false);
		g_bHidePets[client] = (SQL_FetchInt(res, 4) == 1 ? true : false);
		
		// Spawn pet
		if (IsValidPet(g_iPet[client]))
			KillPet(client);
		SpawnPet(client);
		
	}
	
}

DB_SaveClient(int client)
{
	if (g_hDB == null)
	{
		return;
	}
	
	// Get auth
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return;
	}
	
	// Escape pet name
	
	char szPetName[64];
	szPetName = g_szPetName[client];
	
	RemoveChars(szPetName, "`'\"");
	DB_GetEscaped(szPetName, (sizeof(szPetName) * 2) + 1)
	
	// Build query
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO "...MYSQL_TABLE_NAME..." (auth, pettype, petname, petcolor, pettalks, petshidden) VALUES('%s', %i, '%s', %i, %i, %i) ON DUPLICATE KEY UPDATE auth='%s', pettype=%i, petname='%s',petcolor=%i,pettalks=%i,petshidden=%i;", szAuth, g_iPetType[client], szPetName, g_iPetColor[client], view_as<int>(g_bEnableTalking[client]), view_as<int>(g_bHidePets[client]), szAuth, g_iPetType[client], szPetName, g_iPetColor[client], view_as<int>(g_bEnableTalking[client]), view_as<int>(g_bHidePets[client]));
	
	SQL_TQuery(g_hDB, Thrd_Empty, query, _, DBPrio_Normal);
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data)
{
}

/*
	Stocks
*/

bool HandleCmdSpam(int client, float delay = 1.0, float &lasttime, bool bPrint = false)
{
	float dif = (lasttime + delay) - GetEngineTime();
	
	if (dif > 0.0)
	{
		if (bPrint)
			PrintToChat(client, "%sYou must wait %.1f seconds before changing pet again!", CHAT_PREFIX, dif);
		
		return true;
	}
	
	lasttime = GetEngineTime();
	
	return false;
}

stock any Math_Clamp(any value, any min, any max) // Thanks to SMLIB for this stock!
{
	value = Math_Min(value, min);
	value = Math_Max(value, max);
	
	return value;
}

stock any Math_Min(any value, any min) // Thanks to SMLIB for this stock!
{
	if (value < min) {
		value = min;
	}
	
	return value;
}

stock any Math_Max(any value, any max) // Thanks to SMLIB for this stock!
{
	if (value > max) {
		value = max;
	}
	
	return value;
}

stock OffsetPosition(float flPos[3])
{
	flPos[0] += GetRandomFloat(-128.0, 128.0);
	flPos[1] += GetRandomFloat(-128.0, 128.0);
}

stock void DetermineColorName(int iColor, char[] szColor, int iLength)
{
	switch (iColor)
	{
		case 0:Format(szColor, iLength, "No Color")
		case 1:Format(szColor, iLength, "Red")
		case 2:Format(szColor, iLength, "Green")
		case 3:Format(szColor, iLength, "Blue")
		case 4:Format(szColor, iLength, "Pink")
		case 5:Format(szColor, iLength, "Orange")
		case 6:Format(szColor, iLength, "Cyan")
		case 7:Format(szColor, iLength, "Lime")
		case 8:Format(szColor, iLength, "Black")
		case 9:Format(szColor, iLength, "Team Based")
		case 10:Format(szColor, iLength, "L-Team")
		case 11:Format(szColor, iLength, "D-Team")
		case 12:Format(szColor, iLength, "Magic")
	}
}

stock int DetermineAdminFlag(char[] szFlag)
{
	if (StrEqual(szFlag, "0"))
	{
		return 0;
	}
	else if (StrEqual(szFlag, "a"))
	{
		return ADMFLAG_RESERVATION;
	}
	else if (StrEqual(szFlag, "b"))
	{
		return ADMFLAG_GENERIC;
	}
	else if (StrEqual(szFlag, "c"))
	{
		return ADMFLAG_KICK;
	}
	else if (StrEqual(szFlag, "d"))
	{
		return ADMFLAG_BAN;
	}
	else if (StrEqual(szFlag, "e"))
	{
		return ADMFLAG_UNBAN;
	}
	else if (StrEqual(szFlag, "f"))
	{
		return ADMFLAG_SLAY;
	}
	else if (StrEqual(szFlag, "g"))
	{
		return ADMFLAG_CHANGEMAP;
	}
	else if (StrEqual(szFlag, "h"))
	{
		return ADMFLAG_CONVARS;
	}
	else if (StrEqual(szFlag, "i"))
	{
		return ADMFLAG_CONFIG;
	}
	else if (StrEqual(szFlag, "j"))
	{
		return ADMFLAG_CHAT;
	}
	else if (StrEqual(szFlag, "k"))
	{
		return ADMFLAG_VOTE;
	}
	else if (StrEqual(szFlag, "l"))
	{
		return ADMFLAG_PASSWORD;
	}
	else if (StrEqual(szFlag, "m"))
	{
		return ADMFLAG_RCON;
	}
	else if (StrEqual(szFlag, "n"))
	{
		return ADMFLAG_CHEATS;
	}
	else if (StrEqual(szFlag, "o"))
	{
		return ADMFLAG_CUSTOM1;
	}
	else if (StrEqual(szFlag, "p"))
	{
		return ADMFLAG_CUSTOM2;
	}
	else if (StrEqual(szFlag, "q"))
	{
		return ADMFLAG_CUSTOM3;
	}
	else if (StrEqual(szFlag, "r"))
	{
		return ADMFLAG_CUSTOM4;
	}
	else if (StrEqual(szFlag, "s"))
	{
		return ADMFLAG_CUSTOM5;
	}
	else if (StrEqual(szFlag, "t"))
	{
		return ADMFLAG_CUSTOM6;
	}
	else if (StrEqual(szFlag, "z"))
	{
		return ADMFLAG_ROOT;
	}
	
	return 0;
}

/*
	Natives
*/
public Native_SetPetsHidden(Handle hPlugin, int nParams)
{
	g_bHidePets[GetNativeCell(1)] = GetNativeCell(2);
}
public Native_HasPetsHidden(Handle hPlugin, int nParams)
{
	return g_bHidePets[GetNativeCell(1)];
} 
public Native_SetPetsGreeting(Handle hPlugin, int nParams)
{
	g_bEnableTalking[GetNativeCell(1)] = GetNativeCell(2);
}
public Native_HasPetsGreeting(Handle hPlugin, int nParams)
{
	return g_bEnableTalking[GetNativeCell(1)];
} 
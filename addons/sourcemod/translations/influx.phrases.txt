"Phrases"
{
    //
    //  General / Core
    //
    
	"MUSTBERUNNING"
	{
		"en"			"You must be running!"
	}
	
    "MUSTBEALIVE"
    {
        "en"            "You must be alive to use this command!"
    }
    
    "VALIDMODES"
    {
        "#format"       "{1:s}"
        "en"            "Valid mode(s): {1}"
    }
    
    "SELECTEDRUN"
    {
        "#format"       "{1:s}"
        "en"            "Your run is now {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "SELECTEDMODE"
    {
        "#format"       "{1:s}"
        "en"            "Your mode is now {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "SELECTEDSTYLE"
    {
        "#format"       "{1:s}"
        "en"            "Your style is now {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "MODENOTALLOWED"
    {
        "#format"       "{1:s},{2:s}"
        "en"            "Sorry, mode {MAINCLR1}{1}{CHATCLR} is not allowed in {MAINCLR1}{2}{CHATCLR}!"
    }
    
    "HELPCOMMAND"
    {
        "en"            "Type {MAINCLR1}!help{CHATCLR} to see a list of commands."
    }
    
    "NOSERVERRECORDS"
    {
        "en"            "There are no server records currently set."
    }
    
    "COMMANDCOOLDOWN"
    {
        "#format"       "{1:.1f}"
        "en"            "Please wait {MAINCLR1}{1}{CHATCLR} seconds before using this command!"
    }
    
    "SERVERVERSION"
    {
        "#format"       "{1:s},{2:s}"
        "en"            "Server is running {MAINCLR1}{1}{CHATCLR} version {MAINCLR1}{2}{CHATCLR}!"
    }
    
    "RUNCREATED"
    {
        "#format"       "{1:s}"
        "en"            "{MAINCLR1}{1}{CHATCLR} has been created!"
    }
    
    "TIMERDISABLED"
    {
        "en"            "Your timer has been disabled!"
    }
    
    // Errors
    
    "RUNDOESNTEXIST"
    {
        "en"            "That run does not exist!"
    }
    
    "ENDDOESNTEXIST"
    {
        "#format"       "{1:s}"
        "en"            "Couldn't find end to {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "STEAMIDFAIL"
    {
        "en"            "Couldn't retrieve your Steam Id. Please reconnect and make sure you are connected to Steam!!"
    }
    
    "SELECTMODEFAILED"
    {
        "en"            "Something went wrong when changing your mode!"
    }
    
    "SELECTSTYLEFAILED"
    {
        "en"            "Something went wrong when changing your style!"
    }
    
    //
    // Styles
    //
    
    "INVALIDWEPSPEED"
    {
        "#format"       "{1:.0f}"
        "en"            "Invalid weapon speed! Can be {MAINCLR1}{1}{CHATCLR} at most!"
    }
    
    //
    //  Noclip
    //
  
    "NOCLIPENABLED"
    {
        "#format"       "{1:s}"
        "en"            "Noclip: {MAINCLR1}{1}"
    }
    
    //
    //  Stop Music
    //
  
    "MAPMUSICENABLED"
    {
        "#format"       "{1:s}"
        "en"            "Map Music: {MAINCLR1}{1}"
    }
    
    //
    //  Stage Chat
    //
    
    // Stage 3: {RED}-00:30.00 SR
    "STAGETIME"
    {
        "#format"       "{1:i},{2:s},{3:c},{4:s}"
        "en"            "Stage {1}: {{2}}{3}{4} {CHATCLR}SR"
    }
    
    "STAGENOTIME"
    {
        "#format"       "{1:i}"
        "en"            "Stage {1}: No server record."
    }
    
    //
    // SSJ
    //
    
    "SSJENABLED"
    {
        "#format"       "{1:s}"
        "en"            "SSJ: {MAINCLR1}{1}"
    }
    
    "SSJINFO"
    {
        "#format"       "{1:s},{2:s},{3:s},{4:s}"
        "en"            "{MAINCLR1}{3} {CHATCLR}| {CHATCLR}Speed: {1} {CHATCLR}| {CHATCLR}Gain: {2} {4}"
    }
    
    //
    //  Player Ranks
    //  
    
    "PLAYERRANK"
    {
        "#format"	    "{1:i},{2:i}"
        "en"            "{CHATCLR}Your player rank is [{LIMEGREEN}{1}/{2}{CHATCLR}]"
    }
    
    "PLAYERRANK_UNRANKED"
    {
        "#format"	    ""
        "en"            "{CHATCLR}You are currently not ranked! Gain some more points and you will be ranked soon!"
    }
    
    "PLAYERCONNECTED"
    {
        "#format"	    "{1:s},{2:i},{3:i}"
        "en"            "{MAINCLR1}{1}{CHATCLR} has connected [{LIMEGREEN}{2}/{3}{CHATCLR}]"
    }
    
    "PLAYERCONNECTED_UNRANKED"
    {
        "#format"	    "{1:s}"
        "en"            "{MAINCLR1}{1}{CHATCLR} has connected [{LIMEGREEN}Unranked{CHATCLR}]"
    }
    
    //
    //  Servers
    //
    
    "CONNECTIONINFO_CONSOLE1"
    {
        "en"            "The connection details can be found in the console."
    }
    
    "CONNECTIONINFO_CONSOLE2"
    {
        "en"            "Use the [`] key to open the console!"
    }
    
    // 
    //  Weapon Skins / Knife Blocked
    //
    
    "BLOCKEDBYVALVE"
    {
        "en"            "{MAINCLR1}!knife / !ws{CHATCLR} has been restricted by Valve - we cannot fake or display items that you do not own. Sorry!"
    }
    
    //
    //  Pause
    //

	"NOTPAUSED"
	{
		"en"			"You aren't even paused!"
	}
    "CANTPAUSE_PRAC"
	{
		"en"			"You cannot pause a practising run!"
	}
	
    "PAUSEFINISH"
    {
        "en"            "You cannot finish the run while paused!"
    }
    
    "PAUSECOOLDOWN"
    {
        "#format"       "{1:.1f}"
        "en"            "You cannot pause so soon! Please wait {MAINCLR1}{1}{CHATCLR} seconds!"
    }
    
    "PAUSEDISABLED"
    {
        "en"            "Pauses aren't allowed!"
    }
    
    "PAUSED"
    {
        "en"            "Your run is now paused. Type {MAINCLR1}!continue{CHATCLR} to resume."
    }
    
    "RESUMED"
    {
        "en"            "Your run is no longer paused."
    }
    
    "PAUSELIMIT"
    {
        "#format"       "{1:i}"
        "en"            "You cannot pause more than {1} time(s) every run!"
    }
    
    //
    //  Practice
    //
    
    "MUSTBEPRACTISING"
	{
		"en"			"You must be practising!"
	}
 
    "PRACFINISH"
    {
		"en"			"You cannot finish the run while practising!"
    }
    
    "PRACENABLED"
    {
        "en"            "Practice mode: {MAINCLR1}ON"
    }
    
    "PRACDISABLED"
    {
        "en"            "Practice mode: {MAINCLR1}OFF"
    }

	"MUSTBERUNNING_CP"
	{
		"en"			"You must be running to use checkpoints!"
	}
    
    //
    //  Viewmodel Drawing
    //

    "VIEWMODELENABLED"
    {
        "#format"       "{1:s}"
        "en"            "Your viewmodel is now {MAINCLR1}{1}{CHATCLR}!"
    }
    
    //
    //  Records
    //  WR Menus, !playerinfo
    
    "RECORDDELETED"
    {
        "#format"       "{1:s},{2:s},{3:s}"
        "en"            "Your {1} {2} ({3}) run has been deleted!"
    }
    
    //  Player Info
    
    "PARSEPLAYERFAILED"
    {
        "en"            "Unable to parse player name!"
    }
    
    "PLAYERSEARCHFAILED"
    {
        "en"            "No players found with that name."
    }
    
    "PLAYERSEARCHMAX"
    {
        "#format"       "{1:i}"
        "en"            "Too many results - limited to {1} results."
    }
    
    //
    //  Ranking
    //
    
    "POINTAMOUNT"
    {
        "#format"       "{1:i},{2:s}"
        "en"            "You have {MAINCLR1}{1}{CHATCLR} points{2}"
    }
    
    "POINTAMOUNTGAINED"
    {
        "#format"       "{1:i},{2:s},{3:s},{4:i},{5:s}"
        "en"            "You have {MAINCLR1}{1}{CHATCLR} points. ({{2}}{3}{4}{5}{CHATCLR})"
    }
    
    // 
    //  True velocity
    //
    
    "TRUEVELENABLED"
    {
        "#format"       "{1:s}"
        "en"            "Truevel: {MAINCLR1}{1}{CHATCLR}!"
    }
    
    //
    //  Defaults
    //
    
    "SETDEFAULTMODE"
    {
        "#format"       "{1:s}"
        "en"            "Set your default mode to {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "SETDEFAULTSTYLE"
    {
        "#format"       "{1:s}"
        "en"            "Set your default style to {MAINCLR1}{1}{CHATCLR}!"
    }
    
    //
    //  Speclist
    //  

    "SPECLISTENABLED"
    {
        "#format"       "{1:s}"
        "en"            "Speclist: {MAINCLR1}{1}"
    }
    
    //
    //  Hide Me
    //
    
    "HIDDENENABLED"
    {
        "#format"       "{1:s}"
        "en"            "Hidden: {MAINCLR1}{1}"
    }
    
    
    //
    //  HUD
    //
    
    "SHOWKEYSENABLED"
    {
        "#format"       "{1:s}"
        "en"            "Showkeys: {MAINCLR1}{1}"
    }
    
    //
    //  Teams
    //
    
    "NOWSPECTATING"
    {
        "#format"       "{1:s}"
        "en"            "You are now spectating {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "JOINEDSPECTATORS"
    {
        "#format"       "{1:s}"
        "en"            "{MAINCLR1}{1}{CHATCLR} started spectating."
    }
    
    "PLAYERDISCONNECTED"
    {
        "#format"       "{1:s}"
        "en"            "{MAINCLR1}{1}{CHATCLR} disconnected."
    }
    
    //
    //  Replays
    //
    
    "INVALIDREPLAY"
    {
        "en"            "Sorry, this replay isn't valid - unable to play your request."
    }
    
    "ALREADYREPLAYING"
    {
        "en"            "That run is already being replayed!"
    }
    
    "REPLAYSAVED"
    {
        "en"            "Your run has been successfully saved!"
    }
    
    "REPLAYSAVEFAILED"
    {
        "en"            "We were unable to save your recording, sorry!"
    }
    
    "REPLAYSAVETIME"
    {
        "#format"       "{1:.1f}"
        "en"            "No recording was saved; you need to be {1} seconds faster!"
    }
    
    "ALREADYQUEUED"
    {
        "en"            "You have already queued a replay! Wait until that replay has finished."
    }
    
    "REPLAYQUEUED"
    {
        "#format"       "{1:i}"
        "en"            "Your request has been added the queue. You are in position {1}."
    }
    
    "STOPPEDRECORDING"
    {
        "#format"       "{1:03i}"
        "en"            "Stopped recording. Recordings cannot exceed {1} minutes!"
    }
    
    "NOWREPLAYING"
    {
        "#format"       "{1:s},{2:s},{3:s},{4:s},{5:s}"
        "en"            "Now replaying {5} {1} - {2} for {3} ({4})"
    }
    
    "CANREQUEST"
    {
        "en"            "You can now request a replay by typing {MAINCLR1}!replay{CHATCLR} in chat."
    }
    
    // 
    //  Tags
    //
        
    "TAGS_USAGE"
    {
        "#format"       ""
        "en"            "Usage: sm_tags <tag>"
    }
    
    "TAG_BLOCKED"
    {
        "#format"       ""
        "en"            "You cannot use this tag!"
    }
    "SETTAG_SUCCESS"
    {
        "#format"       "{1:s},{2:s}"
        "en"            "Set your tag to: {2}{1}"
    }
    
    "SETTAG_BLANK_SUCCESS"
    {
        "#format"       "{1:s}"
        "en"            "Cleared your tag!"
    }
    
    "SETCOLOUR_SUCCESS"
    {
        "#format"       "{1:s},{2:s},{3:s}"
        "en"            "Set your {1} colour to: {2}{3}"
    }
    
    // 
    //  Admin Commands
    //
    
    // Zones
    "ADMIN_SPAWNEDZONES"
    {
        "#format"       "{1:i}"
        "en"            "Spawned {MAINCLR1}{1}{CHATCLR} zones!"
    }
    
    "ADMIN_ZONEBUILDSTARTED"
    {
        "#format"       "{1:s}"
        "en"            "Started building {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "ADMIN_ZONECREATED"
    {
        "#format"       "{1:s}"
        "en"            "Created zone {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "ADMIN_ZONENOTFOUND"
    {
        "en"            "Couldn't find a zone!"
    }
    
    "ADMIN_ZONEDELETED"
    {
        "#format"       "{1:s}"
        "en"            "Deleted {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "ADMIN_ZONEDELETEFAILED"
    {
        "#format"       "{1:s}"
        "en"            "Couldn't delete {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "ADMIN_SAVEDZONES"
    {
        "#format"       "{1:i}"
        "en"            "Wrote {MAINCLR1}{1}{CHATCLR} zones to file!"
    }
    
    "ADMIN_ZONESIZE"
    {
        "en"            "Bad zone size! Please make the zone bigger!"
    }
    
    "ADMIN_NOZONESETTINGS"
    {
        "en"            "Sorry, no settings can be set for this zone!"
    }
    
    // Runs & Stages
    
    "ADMIN_RUNRENAME"
    {
        "#format"       "{1:s},{2:s}"
        "en"            "Run {MAINCLR1}{1}{CHATCLR} has been renamed to {MAINCLR1}{2}{CHATCLR}!"
    }
    
    "ADMIN_RUNDELETE"
    {
        "#format"       "{1:s}"
        "en"            "Run {MAINCLR1}%s{CHATCLR} has been deleted!"
    }
    "ADMIN_UPDATEDPOS"
    {
        "#format"       "{1:s}"
        "en"            "Updated run's {MAINCLR1}{1}{CHATCLR} teleport position and angle!"
    }
    
    "ADMIN_UPDATEDSTAGEPOS"
    {
        "#format"       "{1:i}"
        "en"            "Updated stage {1}'s teleport position and angle!"
    }
    
    "ADMIN_INVALIDPOS"
    {
        "en"            "That position isn't a valid teleport destination!
    }
    
    "ADMIN_INVALIDRUNID"
    {
        "#format"       "{1:i}"
        "en"            "Run with an ID of {MAINCLR1}{1}{CHATCLR} does not exist!"
    }
    
    "ADMIN_SETENTITYPOS"
    {
        "#format"       "{1:i}"
        "en"            "Copying position and angles from entity {MAINCLR1}{1}{CHATCLR}!"
    }
    
    "ADMIN_NOENTITY"
    {
        "#format"       "{1:.0f}"
        "en"            "Couldn't find a teleport destination within {MAINCLR1}{1}{CHATCLR} units!"
    }
}